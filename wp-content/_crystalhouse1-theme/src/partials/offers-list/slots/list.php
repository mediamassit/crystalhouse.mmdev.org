<?php

	$current_page = $config->getFilter('page');
	$max_pages = $offers->max_num_pages;
	$found_posts = $offers->found_posts;

?>					<section class="widget offers-list">
						<?php while ($offers->have_posts()) : $offers->the_post(); ?>
							<?php
								if(has_term(pll_get_term(84), 'offer_category', $post)) {
									get_template_part('src/partials/offers-list/investment');
								}
								else {
									switch($post->post_type) {
										case 'offer_apartment':
										default:
											get_template_part('src/partials/offers-list/apartment');
											break;
										case 'offer_house':
											get_template_part('src/partials/offers-list/house');
											break;
										case 'offer_lot':
											get_template_part('src/partials/offers-list/lot');
											break;
										case 'offer_business':
											get_template_part('src/partials/offers-list/business');
											break;
									}
								}
							?>
						<?php endwhile; ?>
					</section>
					<!-- .widget.offers-list -->

					<?php if($max_pages > 1) : ?>
					<div class="pagination">
						<ul>
							<?php
								$params = $_GET;
								unset($params['page']);

								$pagination = cht_get_pagination(array(
									'total' => $max_pages,
									'current' => $current_page,
									'after_page_number' => '',
									'before_page_number' => '',
									'end_size' => 5,
									'mid_size' => 5,
									'add_args' => $params
								));
							?>
							<?php if($current_page > 1) : ?>
								<li><a href="<?php echo chtof_mod_url(array('page' => 1)); ?>">&laquo;</a></li>
							<?php else : ?>
								<li class="disabled"><a href="javascript:void(0);">&laquo;</a></li>
							<?php endif; ?>

							<?php if($current_page-1 > 0) : ?>
								<li><a href="<?php echo chtof_mod_url(array('page' => $current_page-1)); ?>">&lsaquo;</a></li>
							<?php else : ?>
								<li class="disabled"><a href="javascript:void(0);">&lsaquo;</a></li>
							<?php endif; ?>

							<?php foreach($pagination as $page) : ?>
							<li<?php if($page['current']) : ?> class="active"<?php endif; ?>><a href="<?php if(is_null($page['link'])) : ?>javascript:void(0);<?php else : ?><?php echo $page['link']; ?><?php endif; ?>"><?php echo $page['text']; ?></a></li>
							<?php endforeach; ?>

							<?php if($current_page+1 <= $max_pages) : ?>
								<li><a href="<?php echo chtof_mod_url(array('page' => $current_page+1)); ?>">&rsaquo;</a></li>
							<?php else : ?>
								<li class="disabled"><a href="javascript:void(0);">&rsaquo;</a></li>
							<?php endif; ?>

							<?php if($current_page < $max_pages) : ?>
								<li><a href="<?php echo chtof_mod_url(array('page' => $max_pages)); ?>">&raquo;</a></li>
							<?php else : ?>
								<li class="disabled"><a href="javascript:void(0);">&raquo;</a></li>
							<?php endif; ?>
						</ul>
					</div>
					<?php endif; ?>
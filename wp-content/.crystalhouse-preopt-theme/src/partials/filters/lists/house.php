					<div class="widget offers-list-filter">
						<h2><?php _e('Znajdź nieruchomość', 'chtheme'); ?></h2>
						<form method="get" action="<?php echo chtof_remf_url(); ?>" name="offers_filter" data-form-values='{"type": "house", "country": "Polska"}'>
							<?php include locate_template('src/partials/filters/slots/q.php'); ?>
							<?php include locate_template('src/partials/filters/slots/keyword.php'); ?>
							<?php include locate_template('src/partials/filters/slots/offer.php'); ?>
							<?php include locate_template('src/partials/filters/slots/currency.php'); ?>

							<div data-group="offer_type_house">
								<?php chtof_get_slot('src/partials/filters/slots/rent_type', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/market_type'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/lot_type', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/area'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/area_lot', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/price'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/price_m2', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/city'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/district'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/province', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/rooms'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/bathrooms'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/built_year'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/center_distance'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/attributes'); ?>

								<div class="form-group text-center">
									<input type="hidden" name="type" value="house">
									<?php get_template_part('src/partials/filters/slots/submit'); ?>
								</div>
								<!-- .form-group -->
							</div>
							<!-- [data-group] -->
						</form>
					</div>
					<!-- .widget.offers-list-filter -->
(function($) {

	$.fn.offersCarousel = function(options) {
		return this.each(function() {
			var $self = $(this),
				$row = $(this).find('.entries .row');

			// Stop if there is less then 3 offers
			if($row.find('> [class^=col-]').length <= 3) {
				return true;
			}

			// Set fixed width for columns and slides layer
			$row.find('> [class^=col-]').each(function() {
				$(this).css('width', $(this).outerWidth()).css('float', 'left');
			});
			$row.css('width', '2000em');

			// Setup carousel
			$(this).jcarousel({
				list: '.row',
				wrap: 'circular',
				animation: {
					duration: 400,
					easing: 'swing',
				}
			});

			// Setup controller
			var $controller = $(this).find('.toolbar .carousel-controller').show();

			$controller.find('.prev').click(function() {
				$self.jcarousel('scroll', '-=1');
				return false;
			});
			$controller.find('.next').click(function() {
				$self.jcarousel('scroll', '+=1');
				return false;
			});

			$(window).resize(function() {
				var $row = $self.find('.entries .row');

				// Reset row and columns fixed width
				$row.find('> [class^=col-]').each(function() {
					$(this).css('width', '').css('float', '');;
				});
				$row.css('width', '');

				// Set fixed width for columns and slides layer
				$row.find('> [class^=col-]').each(function() {
					$(this).css('width', $(this).outerWidth()).css('float', 'left');
				});
				$row.css('width', '2000em');

				// Refresh carousel
				$self.jcarousel('reload');
			});
		});
	};

})(jQuery);
msgid ""
msgstr ""
"Project-Id-Version: crystalhouse.pl v1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2014-07-24 19:29:18+0200\n"
"Last-Translator: admin <slightyboy@gmail.com>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: CSL v1.x\n"
"X-Poedit-Language: Polish\n"
"X-Poedit-Country: POLAND\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-Bookmarks: \n"
"X-Poedit-SearchPath-0: .\n"
"X-Textdomain-Support: yes"

#: 404.php:11
#@ chtheme
msgid "Nie odnaleziono"
msgstr ""

#: 404.php:18
#@ chtheme
msgid "Wskazana podstrona nie istnieje."
msgstr ""

#: src/partials/filters/navigation_tabs.php:8
#@ chtheme
msgid "Sprzedaż"
msgstr ""

#: src/partials/filters/navigation_tabs.php:12
#@ chtheme
msgid "Wynajem"
msgstr ""

#: src/partials/filters/navigation_tabs.php:15
#@ chtheme
msgid "Zgłaszam"
msgstr ""

#: src/partials/filters/navigation_tabs.php:16
#@ chtheme
msgid "Poszukuję"
msgstr ""

#: single-offer_apartment.php:49
#: single-offer_apartment.php:128
#: src/partials/filters/page_title.php:8
#@ chtheme
msgid "Oferty"
msgstr ""

#: src/partials/filters/page_title.php:12
#@ chtheme
msgid "w kategorii:"
msgstr ""

#: src/partials/filters/page_title.php:16
#@ chtheme
msgid "na sprzedaż"
msgstr ""

#: src/partials/filters/page_title.php:21
#@ chtheme
msgid "na wynajem"
msgstr ""

#: src/partials/filters/page_title.php:24
#, php-format
#@ chtheme
msgid "1 oferta"
msgid_plural "%s ofert"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: src/partials/filters/list.php:8
#@ chtheme
msgid "Brak ofert."
msgstr ""

#: page-templates/search.php:33
#: src/partials/filters/lists/apartment.php:2
#: src/partials/filters/lists/business.php:2
#: src/partials/filters/lists/buy.php:2
#: src/partials/filters/lists/house.php:2
#: src/partials/filters/lists/investments.php:2
#: src/partials/filters/lists/lot.php:2
#: src/partials/filters/lists/rent_long.php:2
#: src/partials/filters/lists/rent_short.php:2
#: taxonomy-offer_section.php:22
#@ chtheme
msgid "Znajdź nieruchomość"
msgstr ""

#: src/partials/filters/slots/q.php:2
#@ chtheme
msgid "słowo kluczowe"
msgstr ""

#: page-templates/homepage-business.php:44
#: page-templates/homepage-rent-long.php:39
#: page-templates/homepage-rent-short.php:39
#: page-templates/homepage.php:43
#: src/partials/filters/slots/currency.php:3
#@ chtheme
msgid "Waluta"
msgstr ""

#: page-templates/homepage-rent-long.php:51
#: page-templates/homepage-rent-short.php:51
#: page-templates/homepage.php:67
#: src/partials/filters/slots/type.php:2
#@ chtheme
msgid "Typ nieruchomości"
msgstr ""

#: page-templates/homepage-business.php:61
#: page-templates/homepage-business.php:76
#: page-templates/homepage-rent-long.php:60
#: page-templates/homepage-rent-long.php:84
#: page-templates/homepage-rent-short.php:60
#: page-templates/homepage-rent-short.php:84
#: page-templates/homepage.php:76
#: page-templates/homepage.php:101
#: page-templates/homepage.php:153
#: page-templates/homepage.php:177
#: page-templates/homepage.php:225
#: page-templates/homepage.php:292
#: page-templates/homepage.php:304
#: src/form_filters.php:4
#: src/form_filters.php:22
#: src/form_filters.php:1034
#: src/partials/filters/slots/business_type.php:8
#: src/partials/filters/slots/center_distance.php:5
#: src/partials/filters/slots/developer.php:5
#: src/partials/filters/slots/district.php:5
#: src/partials/filters/slots/investment.php:5
#: src/partials/filters/slots/lot_type.php:8
#: src/partials/filters/slots/premise_type.php:8
#: src/partials/filters/slots/province.php:5
#: src/partials/filters/slots/type.php:11
#@ chtheme
msgid "Wszystkie"
msgstr ""

#: page-templates/homepage.php:85
#: src/partials/filters/slots/rent_type.php:6
#@ chtheme
msgid "Rodzaj wynajmu"
msgstr ""

#: page-templates/homepage.php:188
#: page-templates/homepage.php:263
#: src/form_filters.php:39
#: src/form_filters.php:60
#: src/form_filters.php:81
#: src/form_filters.php:104
#: src/form_filters.php:124
#: src/form_filters.php:144
#: src/form_filters.php:167
#: src/form_filters.php:185
#: src/form_filters.php:203
#: src/form_filters.php:227
#: src/form_filters.php:251
#: src/form_filters.php:275
#: src/form_filters.php:305
#: src/form_filters.php:331
#: src/form_filters.php:357
#: src/form_filters.php:389
#: src/form_filters.php:411
#: src/form_filters.php:432
#: src/form_filters.php:461
#: src/form_filters.php:487
#: src/form_filters.php:513
#: src/form_filters.php:549
#: src/form_filters.php:569
#: src/form_filters.php:588
#: src/form_filters.php:618
#: src/form_filters.php:647
#: src/form_filters.php:677
#: src/form_filters.php:736
#: src/form_filters.php:760
#: src/form_filters.php:793
#: src/form_filters.php:797
#: src/form_filters.php:801
#: src/form_filters.php:807
#: src/form_filters.php:811
#: src/form_filters.php:815
#: src/form_filters.php:821
#: src/form_filters.php:825
#: src/form_filters.php:829
#: src/form_filters.php:837
#: src/form_filters.php:841
#: src/form_filters.php:845
#: src/form_filters.php:851
#: src/form_filters.php:855
#: src/form_filters.php:859
#: src/form_filters.php:865
#: src/form_filters.php:869
#: src/form_filters.php:873
#: src/form_filters.php:881
#: src/form_filters.php:885
#: src/form_filters.php:889
#: src/form_filters.php:895
#: src/form_filters.php:899
#: src/form_filters.php:909
#: src/form_filters.php:913
#: src/form_filters.php:917
#: src/form_filters.php:925
#: src/form_filters.php:929
#: src/form_filters.php:933
#: src/form_filters.php:943
#: src/form_filters.php:947
#: src/form_filters.php:951
#: src/form_filters.php:961
#: src/form_filters.php:967
#: src/form_filters.php:975
#: src/form_filters.php:981
#: src/form_filters.php:989
#: src/form_filters.php:998
#: src/form_filters.php:1002
#: src/form_filters.php:1006
#: src/form_filters.php:1012
#: src/form_filters.php:1016
#: src/form_filters.php:1020
#: src/form_filters.php:1027
#: src/partials/filters/slots/built_year.php:5
#: src/partials/filters/slots/price.php:7
#@ chtheme
msgid "Od"
msgstr ""

#: page-templates/homepage.php:193
#: page-templates/homepage.php:275
#: src/form_filters.php:49
#: src/form_filters.php:70
#: src/form_filters.php:91
#: src/form_filters.php:113
#: src/form_filters.php:133
#: src/form_filters.php:153
#: src/form_filters.php:175
#: src/form_filters.php:193
#: src/form_filters.php:211
#: src/form_filters.php:238
#: src/form_filters.php:262
#: src/form_filters.php:286
#: src/form_filters.php:317
#: src/form_filters.php:343
#: src/form_filters.php:369
#: src/form_filters.php:399
#: src/form_filters.php:421
#: src/form_filters.php:443
#: src/form_filters.php:473
#: src/form_filters.php:499
#: src/form_filters.php:525
#: src/form_filters.php:558
#: src/form_filters.php:578
#: src/form_filters.php:597
#: src/form_filters.php:628
#: src/form_filters.php:658
#: src/form_filters.php:689
#: src/form_filters.php:744
#: src/form_filters.php:770
#: src/form_filters.php:794
#: src/form_filters.php:798
#: src/form_filters.php:802
#: src/form_filters.php:808
#: src/form_filters.php:812
#: src/form_filters.php:816
#: src/form_filters.php:822
#: src/form_filters.php:826
#: src/form_filters.php:830
#: src/form_filters.php:838
#: src/form_filters.php:842
#: src/form_filters.php:846
#: src/form_filters.php:852
#: src/form_filters.php:856
#: src/form_filters.php:860
#: src/form_filters.php:866
#: src/form_filters.php:870
#: src/form_filters.php:874
#: src/form_filters.php:882
#: src/form_filters.php:886
#: src/form_filters.php:890
#: src/form_filters.php:896
#: src/form_filters.php:900
#: src/form_filters.php:904
#: src/form_filters.php:910
#: src/form_filters.php:914
#: src/form_filters.php:918
#: src/form_filters.php:926
#: src/form_filters.php:930
#: src/form_filters.php:934
#: src/form_filters.php:944
#: src/form_filters.php:948
#: src/form_filters.php:952
#: src/form_filters.php:962
#: src/form_filters.php:968
#: src/form_filters.php:976
#: src/form_filters.php:982
#: src/form_filters.php:990
#: src/form_filters.php:999
#: src/form_filters.php:1003
#: src/form_filters.php:1007
#: src/form_filters.php:1013
#: src/form_filters.php:1017
#: src/form_filters.php:1021
#: src/form_filters.php:1028
#: src/partials/filters/slots/built_year.php:9
#: src/partials/filters/slots/price.php:18
#@ chtheme
msgid "Do"
msgstr ""

#: header.php:197
#: page-templates/homepage-business.php:139
#: page-templates/homepage-rent-long.php:136
#: page-templates/homepage-rent-short.php:136
#: page-templates/homepage.php:321
#: src/partials/filters/slots/submit.php:6
#@ chtheme
msgid "Szukaj"
msgstr ""

#: src/partials/filters/slots/submit.php:7
#@ chtheme
msgid "Usuń filtry"
msgstr ""

#: footer-slim.php:13
#: footer.php:40
#@ chtheme
msgid "by Roogmedia"
msgstr ""

#: footer.php:23
#: page-templates/page-contact.php:45
#@ chtheme
msgid "Telefon"
msgstr ""

#: footer.php:28
#: page-templates/page-contact.php:46
#@ chtheme
msgid "Email"
msgstr ""

#: functions.php:83
#: src/partials/popular-entries.php:33
#@ chtheme
msgid "Najczęściej wyszukiwane"
msgstr ""

#: functions.php:106
#@ chtheme
msgid "Menu główne"
msgstr ""

#: header.php:152
#@ chtheme
msgid "Call me"
msgstr ""

#: header.php:153
#@ chtheme
msgid "Wyślij wiadomość"
msgstr ""

#: header.php:196
#: page-templates/homepage-business.php:33
#: page-templates/homepage-rent-long.php:32
#: page-templates/homepage-rent-short.php:32
#: page-templates/homepage.php:32
#@ chtheme
msgid "wpisz nr oferty, miasto, ulicę"
msgstr ""

#: header.php:203
#: header.php:271
#@ chtheme
msgid "Schowek"
msgstr ""

#: header.php:206
#@ chtheme
msgid "Twoje oferty"
msgstr ""

#: header.php:224
#: page-templates/page-clipboard.php:26
#@ chtheme
msgid "Wyczyść schowek"
msgstr ""

#: header.php:225
#: page-templates/page-clipboard.php:27
#@ chtheme
msgid "Zapisz schowek"
msgstr ""

#: header.php:226
#@ chtheme
msgid "Chcę obejrzeć"
msgstr ""

#: page-templates/homepage-business.php:40
#: page-templates/homepage.php:39
#@ chtheme
msgid "Chcę kupić"
msgstr ""

#: page-templates/homepage-business.php:41
#: page-templates/homepage.php:40
#@ chtheme
msgid "Chcę wynająć"
msgstr ""

#: header.php:23
#: page-templates/homepage.php:315
#: src/partials/modals/modal-clipboard-clean.php:7
#@ chtheme
msgid "Zamknij"
msgstr ""

#: page-templates/homepage.php:327
#@ chtheme
msgid "Wyszukiwarka zaawansowana"
msgstr ""

#: page-templates/page-clipboard.php:32
#: single-offer_apartment.php:57
#: src/partials/page-static-sidebar.php:5
#@ chtheme
msgid "Udostępnij"
msgstr ""

#: page-templates/page-clipboard.php:60
#@ chtheme
msgid "Nr. oferty"
msgstr ""

#: page-templates/page-clipboard.php:64
#@ chtheme
msgid "Ulica"
msgstr ""

#: page-templates/page-clipboard.php:68
#@ chtheme
msgid "Powierzchnia"
msgstr ""

#: page-templates/page-clipboard.php:72
#@ chtheme
msgid "Cena:"
msgstr ""

#: page-templates/page-clipboard.php:73
#: src/partials/clipboard-offer.php:9
#: src/partials/offer-meta/price.php:5
#: src/partials/offers-list/slots/map-infobox.php:9
#@ chtheme
msgid "/ msc"
msgstr ""

#: page-templates/page-clipboard.php:74
#: src/partials/clipboard-offer.php:12
#: src/partials/offer-meta/price.php:7
#: src/partials/offers-list/slots/map-infobox.php:12
#@ chtheme
msgid "/ dzień"
msgstr ""

#: page-templates/page-clipboard.php:88
#@ chtheme
msgid "Schowek jest pusty."
msgstr ""

#: single-offer_apartment.php:28
#: src/partials/page-meta.php:2
#@ chtheme
msgid "Opublikowane"
msgstr ""

#: single-offer_apartment.php:29
#: src/partials/page-meta.php:2
#@ chtheme
msgid "ostatnia aktualizacja"
msgstr ""

#: single-offer_apartment.php:30
#: src/partials/offers-list/apartment.php:49
#: src/partials/offers-list/business.php:50
#: src/partials/offers-list/house.php:49
#: src/partials/offers-list/investment.php:47
#: src/partials/offers-list/lot.php:49
#@ chtheme
msgid "Edytuj ofertę"
msgstr ""

#: functions.php:849
#: single-offer_apartment.php:37
#@ chtheme
msgid "Numer oferty"
msgstr ""

#: single-offer_apartment.php:46
#: single-offer_apartment.php:106
#@ chtheme
msgid "Opis"
msgstr ""

#: single-offer_apartment.php:47
#: single-offer_apartment.php:117
#: src/options.php:159
#@ chtheme
msgid "Mapa"
msgstr ""

#: single-offer_apartment.php:48
#: single-offer_apartment.php:67
#@ chtheme
msgid "Zdjęcia"
msgstr ""

#: single-offer_apartment.php:50
#@ chtheme
msgid "Wirtualna wycieczka"
msgstr ""

#: single-offer_apartment.php:51
#@ chtheme
msgid "Film"
msgstr ""

#: single-offer_apartment.php:55
#: src/partials/offers-list/slots/side.php:4
#@ chtheme
msgid "Dodaj do schowka"
msgstr ""

#: single-offer_apartment.php:110
#@ chtheme
msgid "Brak opisu."
msgstr ""

#: header.php:22
#@ chtheme
msgid "Nie udało się załadować.<br>Spróbuj ponownie."
msgstr ""

#: header.php:24
#@ chtheme
msgid "Następny"
msgstr ""

#: header.php:25
#@ chtheme
msgid "Poprzedni"
msgstr ""

#: src/fields.php:280
#@ chtheme
msgid "label"
msgstr ""

#: src/meta-boxes/page-clipboard.php:9
#: src/meta-boxes/page-contact.php:9
#@ chtheme
msgid "Formularz"
msgstr ""

#: src/meta-boxes/page-contact.php:22
#@ chtheme
msgid "Mapa Google"
msgstr ""

#: src/meta-boxes/page-contact.php:28
#@ chtheme
msgid "Adres mapy"
msgstr ""

#: src/meta-boxes/page-contact.php:33
#@ chtheme
msgid "Koordynaty GPS (lat,lng)"
msgstr ""

#: src/meta-boxes/page-contact.php:36
#@ chtheme
msgid "Własność pola \"Adres mapy\" zostanie zignorowana."
msgstr ""

#: src/meta-boxes/page-contact.php:39
#@ chtheme
msgid "Przybliżenie mapy (zoom)"
msgstr ""

#: src/meta-boxes/page-contact.php:45
#@ chtheme
msgid "Tytuł markera"
msgstr ""

#: src/meta-boxes/page-homepage.php:9
#@ chtheme
msgid "Slider"
msgstr ""

#: src/options.php:56
#@ chtheme
msgid "Nagłówek"
msgstr ""

#: src/options.php:75
#@ chtheme
msgid "Kontakt"
msgstr ""

#: src/options.php:82
#@ chtheme
msgid "Adres"
msgstr ""

#: src/options.php:88
#@ chtheme
msgid "Numer telefonu"
msgstr ""

#: src/options.php:89
#@ chtheme
msgid "Kolejne numery w nowej linii."
msgstr ""

#: src/options.php:94
#@ chtheme
msgid "Adres email"
msgstr ""

#: src/options.php:100
#@ chtheme
msgid "Skype"
msgstr ""

#: src/options.php:105
#@ chtheme
msgid "Serwisy społecznościowe"
msgstr ""

#: src/options.php:111
#@ chtheme
msgid "Pinterest"
msgstr ""

#: src/options.php:117
#@ chtheme
msgid "Facebook"
msgstr ""

#: src/options.php:123
#@ chtheme
msgid "Google+"
msgstr ""

#: src/options.php:129
#@ chtheme
msgid "Twitter"
msgstr ""

#: src/options.php:135
#@ chtheme
msgid "Youtube"
msgstr ""

#: src/options.php:301
#@ chtheme
msgid "Sesja PHP"
msgstr ""

#: src/options.php:325
#@ chtheme
msgid "Kursy walut"
msgstr ""

#: src/fields.php:382
#: src/options.php:325
#: src/partials/offer-meta/price.php:15
#: src/shortcodes.php:236
#: src/shortcodes.php:237
#@ chtheme
msgid "b/d"
msgstr ""

#: src/options.php:336
#@ chtheme
msgid "Systemowe"
msgstr ""

#: src/partials/breadcrumbs.php:7
#@ chtheme
msgid "jesteś tutaj:"
msgstr ""

#: src/partials/modals/modal-clipboard-saved.php:7
#: src/partials/modals/modal-clipboard.php:7
#@ chtheme
msgid "Obejrzyj"
msgstr ""

#: src/partials/modals/modal-clipboard-saved.php:12
#@ chtheme
msgid "Zapisano schowek"
msgstr ""

#: src/partials/modals/modal-clipboard.php:8
#@ chtheme
msgid "Dodaj kolejne"
msgstr ""

#: src/partials/modals/modal-clipboard.php:13
#@ chtheme
msgid "Dodałeś nieruchomość do"
msgstr ""

#: src/partials/modals/modal-clipboard.php:13
#@ chtheme
msgid "schowka"
msgstr ""

#: src/partials/offer-contact-broker.php:16
#@ chtheme
msgid "Jesteś zainteresowany wynajmem lub kupnem nieruchomości w tej lokalizacji?"
msgstr ""

#: src/partials/offer-contact-broker.php:19
#@ chtheme
msgid "Skontaktuj się z agentem"
msgstr ""

#: src/partials/offer-contact-broker.php:23
#@ chtheme
msgid "agent nieruchomości"
msgstr ""

#: src/partials/offer-contact-broker.php:26
#: src/partials/offer-contact-broker.php:28
#@ chtheme
msgid "T:"
msgstr ""

#: src/partials/offer-contact-broker.php:35
#@ chtheme
msgid "E:"
msgstr ""

#: src/partials/offer-meta/business.php:43
#@ chtheme
msgid "Tak"
msgstr ""

#: src/partials/offers-list/slots/price.php:4
#: src/shortcodes.php:107
#: src/shortcodes.php:342
#@ chtheme
msgid "Doba"
msgstr ""

#: src/partials/offers-list/slots/price.php:6
#: src/shortcodes.php:110
#: src/shortcodes.php:345
#@ chtheme
msgid "Miesiąc"
msgstr ""

#: src/partials/offer-meta/price.php:16
#: src/partials/offers-list/slots/price.php:11
#: src/partials/offers-list/slots/side.php:3
#@ chtheme
msgid "Pytanie o cenę"
msgstr ""

#: src/partials/offer-meta/price.php:16
#: src/partials/offers-list/slots/price.php:11
#: src/partials/offers-list/slots/side.php:3
#@ chtheme
msgid "Zapytaj o cenę"
msgstr ""

#: src/partials/offers-list/slots/side.php:5
#@ chtheme
msgid "Zobacz więcej"
msgstr ""

#: src/partials/offers-list/slots/switches.php:3
#@ chtheme
msgid "Pokaż mapę nieruchomości"
msgstr ""

#: src/partials/offers-list/slots/switches.php:3
#@ chtheme
msgid "Zamknij mapę nieruchomości"
msgstr ""

#: src/partials/offers-list/slots/switches.php:16
#@ chtheme
msgid "Najnowsze"
msgstr ""

#: src/partials/offers-list/slots/switches.php:17
#@ chtheme
msgid "Najstarsze"
msgstr ""

#: src/partials/offers-list/slots/switches.php:18
#@ chtheme
msgid "Cena najwyższa"
msgstr ""

#: src/partials/offers-list/slots/switches.php:19
#@ chtheme
msgid "Cena najniższa"
msgstr ""

#: src/partials/page-contact-form.php:6
#@ chtheme
msgid "Zapraszamy do wypełnienia formularza kontaktowego"
msgstr ""

#: src/shortcodes.php:207
#@ chtheme
msgid "Zobacz wszystkie"
msgstr ""

#: src/shortcodes.php:209
#@ chtheme
msgid "Poprzednie"
msgstr ""

#: src/shortcodes.php:210
#@ chtheme
msgid "Następne"
msgstr ""

#: functions.php:118
#@ chtheme
msgid "Apartamenty"
msgstr ""

#: functions.php:119
#: src/options.php:179
#: src/options.php:197
#: src/options.php:219
#@ chtheme
msgid "Apartament"
msgstr ""

#: functions.php:120
#@ chtheme
msgid "Oferty komercyjne"
msgstr ""

#: functions.php:121
#@ chtheme
msgid "Komercyjna"
msgstr ""

#: functions.php:122
#@ chtheme
msgid "Domy"
msgstr ""

#: functions.php:123
#: src/options.php:180
#: src/options.php:198
#: src/options.php:225
#@ chtheme
msgid "Dom"
msgstr ""

#: functions.php:124
#@ chtheme
msgid "Działki"
msgstr ""

#: functions.php:125
#: src/options.php:181
#: src/options.php:199
#: src/options.php:231
#@ chtheme
msgid "Działka"
msgstr ""

#: functions.php:126
#@ chtheme
msgid "Kategorie ofert"
msgstr ""

#: functions.php:127
#@ chtheme
msgid "Kategoria oferty"
msgstr ""

#: functions.php:128
#@ chtheme
msgid "Tagi ofert"
msgstr ""

#: functions.php:129
#@ chtheme
msgid "Tag oferty"
msgstr ""

#: header.php:136
#@ chtheme
msgid "cookie_notifier"
msgstr ""

#: src/partials/filters/slots/offer.php:2
#@ chtheme
msgid "Chcę"
msgstr ""

#: src/partials/filters/slots/offer.php:5
#@ chtheme
msgid "kupić"
msgstr ""

#: src/partials/filters/slots/offer.php:10
#@ chtheme
msgid "wynająć"
msgstr ""

#: src/partials/popular-entries.php:57
#@ chtheme
msgid "Zwiń"
msgstr ""

#: src/partials/popular-entries.php:57
#@ chtheme
msgid "Rozwiń"
msgstr ""

#: functions.php:94
#@ chtheme
msgid "Panel boczny strony"
msgstr ""

#: functions.php:749
#@ chtheme
msgid "Wszystkie kategorie"
msgstr ""

#: functions.php:791
#@ chtheme
msgid "Wszystkie sekcje"
msgstr ""

#: functions.php:824
#@ default
msgid "ID oferty"
msgstr ""

#: functions.php:833
#: functions.php:852
#@ chtheme
msgid "Wordpress ID"
msgstr ""

#: functions.php:833
#: functions.php:855
#@ chtheme
msgid "Asari ID"
msgstr ""

#: functions.php:862
#@ chtheme
msgid "Identyfikatory oferty"
msgstr ""

#: page-templates/homepage-business.php:117
#: page-templates/homepage.php:110
#@ chtheme
msgid "Zakres cen"
msgstr ""

#: page-templates/homepage-rent-long.php:113
#: page-templates/homepage-rent-short.php:113
#@ chtheme
msgid "za miesiąc"
msgstr ""

#: page-templates/homepage.php:57
#, php-format
#@ chtheme
msgid "Rynek %s"
msgstr ""

#: page-templates/homepage.php:226
#: page-templates/homepage.php:227
#: page-templates/homepage.php:228
#: page-templates/homepage.php:229
#: src/partials/filters/slots/center_distance.php:6
#: src/partials/filters/slots/center_distance.php:7
#: src/partials/filters/slots/center_distance.php:8
#: src/partials/filters/slots/center_distance.php:9
#@ chtheme
msgid "do"
msgstr ""

#: page-templates/homepage.php:230
#: src/partials/filters/slots/center_distance.php:10
#@ chtheme
msgid "powyżej"
msgstr ""

#: page-templates/homepage.php:289
#: src/partials/filters/slots/developer.php:2
#@ chtheme
msgid "Developer"
msgstr ""

#: src/fields.php:307
#@ chtheme
msgid "choices"
msgstr ""

#: src/options.php:50
#@ chtheme
msgid "Treść"
msgstr ""

#: src/options.php:152
#@ chtheme
msgid "Subdomeny"
msgstr ""

#: src/options.php:170
#@ chtheme
msgid "Mapy Google"
msgstr ""

#: src/options.php:176
#@ chtheme
msgid "Widok oferty"
msgstr ""

#: src/options.php:177
#@ chtheme
msgid "Odznaczenie skutkuje usunięciem mapy z widoku oferty danego typu."
msgstr ""

#: src/options.php:182
#: src/options.php:200
#: src/options.php:237
#@ chtheme
msgid "Oferta komercyjna"
msgstr ""

#: src/options.php:194
#@ chtheme
msgid "Widok listy ofert"
msgstr ""

#: src/options.php:195
#@ chtheme
msgid "Odznaczenie skutkuje usunięciem oferty danego typu z mapy nieruchomości w listach ofert."
msgstr ""

#: src/options.php:212
#@ chtheme
msgid "Wzorce adresów dla markerów mapy Google"
msgstr ""

#: src/options.php:213
#, php-format
#@ chtheme
msgid "<strong>Dostępne zmienne:</strong> %country <em>kraj</em> &nbsp;&middot;&nbsp; %province <em>województwo</em> &nbsp;&middot;&nbsp; %city <em>miasto</em> &nbsp;&middot;&nbsp; %street <em>ulica</em> &nbsp;&middot;&nbsp; %district <em>dzielnica</em>"
msgstr ""

#: src/options.php:356
#@ chtheme
msgid "Witryna"
msgstr ""

#: src/options.php:357
#@ chtheme
msgid "Ustawienia witryny"
msgstr ""

#: src/partials/filters/slots/measure.php:3
#@ chtheme
msgid "Miara"
msgstr ""

#: src/partials/modals/modal-clipboard-clean.php:12
#@ chtheme
msgid "Wyczyszczono schowek"
msgstr ""

#: src/partials/offer-contact-broker.php:32
#@ chtheme
msgid "M:"
msgstr ""

#: page-templates/homepage-business.php:97
#: page-templates/homepage.php:130
#: src/partials/filters/slots/area.php:6
#@ chtheme
msgid "Powierzchnia domu"
msgstr ""


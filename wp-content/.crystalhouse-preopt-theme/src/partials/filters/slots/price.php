<?php

	$form_filters = cht_get_form_filters(true);

?>								<div class="form-group" data-form-fieldset="price">
									<label><?php echo chtof_label('offer_price_primary'); ?></label>
									<div class="row">
										<div class="col-xs-6">
											<?php
												echo chofl_select('price_from', $form_filters['price']['default']['buy']['pln']['from']);
											?>
										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<?php
												echo chofl_select('price_to', $form_filters['price']['default']['buy']['pln']['to']);
											?>
										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->
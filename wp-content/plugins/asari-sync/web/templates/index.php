<?php

	$is_processing = $status->isProcessing();
	$running_process = $status->getRunningProcess();
	$processes = $status->getProcesses();

?><div class="asc-wrap">
	<?php if($is_processing) : ?>
	<div class="asc-process active">
		<p><strong data-processname><?php echo $running_process['label']; ?></strong>: <small><span data-lastaction><?php echo $running_process['last_message']; ?></span>...</small></p>
		<div data-progressbar class="uk-progress uk-progress-striped uk-active"><div class="uk-progress-bar" style="width: <?php echo $running_process['progress']; ?>%;"><?php echo $running_process['progress']; ?>%</div></div>
	</div>
	<!-- asc-process -->
	<?php endif; ?>

	<?php require Asari::dir('templates').'/process_messages.php'; ?>

	<div class="asc-status asc-params">
		<h3>Status AsariSync</h3>
		<ul>
			<li><strong>Ostatnia aktualizacja kolejki:</strong> <span data-syncdata-queueupdatedate><?php if($status->has('last_queue_update')) : ?><?php echo date_i18n('Y/m/d H:i:s', $status->get('last_queue_update')); ?><?php else : ?>b/d<?php endif; ?></span></li>
			<li><strong>Rozmiar kolejki wpisów:</strong> <span data-syncdata-queuesize><?php if($status->has('queue_size')) : ?><?php echo $status->get('queue_size'); ?><?php else : ?>0<?php endif; ?></span> wpisów</li>
			<li><strong>Maks. rozmiar przetw. kolejki:</strong> <span><?php echo @$options['sync_max_queue_size'] ? $options['sync_max_queue_size'] : 50; ?></span></li>
		</ul>
		<?php if(!$is_processing) : ?>
			<a href="javascript:void(0);" onclick="return start_process_popup('<?php echo plugins_url(); ?>/asari-sync/web/run-process.php?name=Asari_Process_Synchronize');" target="_blank" class="button" data-sync-start><strong>Importuj kolejkę wpisów</strong></a>
			<a href="javascript:void(0);" onclick="return start_process_popup('<?php echo plugins_url(); ?>/asari-sync/web/run-process.php?name=Asari_Process_UpdateQueue');" target="_blank" class="button" data-sync-start>Aktualizuj kolejkę</a>
		<?php else : ?>
			<a href="#" class="button" disabled="disabled" data-sync-start><strong>Importuj kolejkę wpisów</strong></a>
			<a href="#" class="button" disabled="disabled" data-sync-updatequeue>Aktualizuj kolejkę</a>
		<?php endif; ?>
			<span class="advanced" style="display: none;"><a href="<?php echo Asari::url(array('action' => 'hard-reset')); ?>" class="button">Resetuj status pluginu</a></span>
			<p class="advanced-button" style="margin-top: 10px;"><a href="javascript:void(0);" onclick="jQuery('span.advanced').show(); jQuery('.advanced-button').hide(); return false;">opcje zaawansowane</a></p>
	</div>
	<!-- asc-status -->

	<?php
		if($is_processing) {
			switch($running_process['name']) {
				default:
					break;
				case 'update-queue':
					require Asari::dir('templates').'/process_update-queue.php';
					break;
				case 'synchronize':
					require Asari::dir('templates').'/process_synchronize.php';
					break;
			}
		}
	?>

	<?php
		$added = trim((string) @file_get_contents(Asari::dir('tmp').'/added.txt'));

		if($added && sizeof($added = explode("\r\n", $added)) > 0) :
	?>
	<div>
		<h3>Ostatnio zaimportowane (<?php echo sizeof($added); ?>)</h3>
		<ul style="max-height: 190px; overflow: auto;">
		<?php foreach($added as $id) : $post = get_post($id); ?>
			<li><?php echo $post->post_type; ?>: <a href="<?php echo get_permalink($post->ID); ?>" target="_blank"><?php echo $post->post_title; ?></a></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<!-- .asc-log -->
	<?php endif; ?>

	<script>
	function start_process_popup(url) {
		var popup = window.open(url, 'name', 'height=200,width=400');
		
		if(window.focus) {
			popup.focus()
		}

		setTimeout(function() { location.reload(); }, 1200);
		
		return false;
	}
	</script>
</div>
<!-- .asc-wrap -->
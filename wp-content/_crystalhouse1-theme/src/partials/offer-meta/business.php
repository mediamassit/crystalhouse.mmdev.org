						<section class="widget offer-meta">
							<?php if(chtof_has_any(array('offer_location_city', 'offer_location_district', 'offer_location_street'))) : ?>
							<ul>
								<?php if(chtof_has('offer_location_city')) : ?>
								<li><strong><?php echo chtof_label('offer_location_city'); ?></strong>
									<span><?php the_field('offer_location_city'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_location_district')) : ?>
								<li><strong><?php echo chtof_label('offer_location_district'); ?></strong>
									<span><?php the_field('offer_location_district'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_location_street')) : ?>
								<li><strong><?php echo chtof_label('offer_location_street'); ?></strong>
									<span><?php the_field('offer_location_street'); ?></span></li>
								<?php endif; ?>
							</ul>
							<?php endif; ?>

							<?php if(chtof_has_any(array('offer_business_type', 'offer_business_premise-type', 'offer_business_premise-has-tenant'))) : ?>
							<ul>
								<?php
									if(chtof_has('offer_business_type')) :
										$field = get_field_object('offer_business_type');
										$business_type = $field['choices'][get_field('offer_business_type')];
								?>
								<li><strong><?php echo chtof_label('offer_business_type'); ?></strong>
									<span><?php pll_e($business_type); ?></span></li>
								<?php endif; ?>

								<?php
									if(chtof_has('offer_business_premise-type')) :
										$field = get_field_object('offer_business_premise-type');
										$business_premise_type = $field['choices'][get_field('offer_business_premise-type')];
								?>
								<li><strong><?php echo chtof_label('offer_business_premise-type'); ?></strong>
									<span><?php pll_e($business_premise_type); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_business_premise-has-tenant')) : ?>
								<li><strong><?php echo chtof_label('offer_business_premise-has-tenant'); ?></strong>
									<span><?php _e('Tak', 'chtheme'); ?></li>
								<?php endif; ?>
							</ul>
							<?php endif; ?>

							<ul>
								<?php if(chtof_has('offer_property_area-primary')) : ?>
								<li><strong><?php echo chtof_label('offer_property_area-primary'); ?></strong>
									<span><?php the_field('offer_property_area-primary'); ?> m<sup>2</sup> / <?php echo cht_sqm2feet(get_field('offer_property_area-primary')); ?> sqft</span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_property_floors-floor')) : ?>
								<li><strong><?php echo chtof_label('offer_property_floors-floor'); ?></strong>
									<span><?php the_field('offer_property_floors-floor'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_property_floors-all')) : ?>
								<li><strong><?php echo chtof_label('offer_property_floors-all'); ?></strong>
									<span><?php the_field('offer_property_floors-all'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_property_built-year')) : ?>
								<li><strong><?php echo chtof_label('offer_property_built-year'); ?></strong>
									<span><?php the_field('offer_property_built-year'); ?></span></li>
								<?php endif; ?>
							</ul>

							<?php
								// Additional fields
								if(chtof_has('offer_additional-fields')) :
									$additional = explode("\r\n", trim(get_field('offer_additional-fields')));
							?>
							<ul>
								<?php
									foreach($additional as $line) :
										$field = explode(':', $line);

										if(sizeof($field) === 2) :
								?>
								<li><strong><?php echo trim(@$field[0]); ?></strong>
									<span><?php echo trim(@$field[1]); ?></span></li>
								<?php
										endif;
									endforeach;
								?>
							</ul>
							<?php endif; ?>

							<?php if(chtof_has('offer_location_center-distance')) : ?>
							<ul>
								<li><strong><?php echo chtof_label('offer_location_center-distance'); ?></strong>
									<span><?php the_field('offer_location_center-distance'); ?> km</span></li>
							</ul>
							<?php endif; ?>

							<?php get_template_part('src/partials/offers-list/slots/attributes'); ?>
						</section>
						<!-- .offer-meta -->
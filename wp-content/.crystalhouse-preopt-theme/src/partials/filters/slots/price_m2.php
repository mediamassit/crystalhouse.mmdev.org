<?php

	$form_filters = cht_get_form_filters(true);

?>								<div class="form-group" data-form-fieldset="price_m2"<?php if($disabled) : ?> style="display: none;"<?php endif; ?>>
									<label><?php echo chtof_label('offer_price_m2'); ?></label>
									<div class="row">
										<div class="col-xs-6">
											<?php
												echo chofl_select('price_m2_from', $form_filters['price_m2']['default']['buy']['pln']['from'], null, $disabled ? array('disabled' => 'disabled') : array());
											?>
										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<?php
												echo chofl_select('price_m2_to', $form_filters['price_m2']['default']['buy']['pln']['to'], null, $disabled ? array('disabled' => 'disabled') : array());
											?>
										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->
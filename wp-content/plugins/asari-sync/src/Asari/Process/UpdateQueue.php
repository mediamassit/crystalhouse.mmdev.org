<?php

class Asari_Process_UpdateQueue extends Asari_Process {
	protected $_name = 'update-queue';

	public function __construct() {
		parent::__construct();

		$this->getStatus()->setLabel('Aktualizacja kolejki');
	}

	public function run() {
		$this->getLog()->log('Rozpoczynanie aktualizacji kolejki');

		// Get listings chunk by chunk
		try {
			$options = get_option('asarisync_options');
			$api = new AsariApi($options['api_userid'], $options['api_token']);
			$queue = new Asari_Queue;

			if(!isset($options['api_userid']) || strlen(trim($options['api_userid'])) <= 0 || !isset($options['api_token']) || strlen(trim($options['api_token'])) <= 0) {
				return $this->error('Nie udało się pobrać nowej kolejki, AsariAPI jest nieskonfigurowane');
			}

			$step = 150;
			$offset = 0;
			$listings = array();
			$cycle = 0;

			$this->getLog()->log('Przygotowanie do pobierania danych z API');

			do {
				$response = $api->get("http://api.asariweb.pl/apiSite/listingList", array('offset' => $offset, 'limit' => $step, 'status' => 'Active'));
				$total_count = $response->body->totalCount;

				$offset += $step;
				$listings = array_merge($listings, $response->body->data);

				++$cycle;

				$this->getLog()->log(sprintf('Pobieranie danych z API (cykl #%s)', $cycle));
				$this->getStatus()
					->setItemsCount($total_count)
					->setDoneItemsCount(count($listings))
					->save();
			}
			while($offset <= $total_count);

			unset($step, $offset, $cycle, $response, $total_count);
		}
		catch(AsariApiException $e) {
			return $this->error(sprintf('Nie udało się pobrać nowej kolejki, wystąpił wyjątek AsariAPI: %s', $e->getMessage()));
		}
		catch(Exception $e) {
			return $this->error(sprintf('Nie udało się pobrać nowej kolejki, wystąpił wyjątek: %s', $e->getMessage()));
		}

		// Calculate differences to prepare the queue
		$this->getLog()->log('Obliczanie różnic');

		try{
			// Unset records that are already in database
			$wp = new AsariWp;
			$wp_ids = $wp->getPostsAsariIDs();
			$ids = $api->extractListingsIdentifiers($listings);
			$ids_diff = array();
			$listing_ids = array();

			foreach($ids as $id) {
				if(!in_array($id->listing, $wp_ids->listing, true) && !in_array($id->listing, $listing_ids, true)) {
					$ids_diff[] = $id;
					$listing_ids[] = $id->listing;
				}
			}

			unset($wp_ids, $ids);
		}
		catch(Exception $e) {
			return $this->error(sprintf('Nie udało się pobrać nowej kolejki, wystąpił wyjątek: %s', $e->getMessage()));
		}

		// Save listings to file
		$this->getLog()->log('Zapisywanie kolejki do pliku');
		
		try {
			$queue->put($ids_diff)->save();
		}
		catch(Exception $e) {
			return $this->error(sprintf('Nie udało się pobrać nowej kolejki, wystąpił wyjątek: %s', $e->getMessage()));
		}

		// End process
		$this->getLog()->log('Poprawnie zaktualizowano kolejkę');
		$this->getLog()->remove();
		return $this->getStatus()->finish(array('success' => true));
	}
}
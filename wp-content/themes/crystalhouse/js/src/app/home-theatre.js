(function($) {

	$.fn.homeTheatre = function(options) {
		return this.each(function() {
			var $self = $(this);

			/*
			 * Based on plugin: Resize Image to Parent Container
			 * Author: Christian Varga (http://christianvarga.com)
			 * Plugin Source: https://github.com/levymetal/jquery-resize-image-to-parent/
			 */
			function positionSlide($img) {
				var $viewport = $img.closest('.viewport'),
					$figure = $img.closest('figure'),
					$parent = $viewport;

				$figure.css('width', $viewport.width());

				// reset image (in case we're calling this a second time, for example on resize)
				$img.css({'width': '', 'height': '', 'margin-left': '', 'margin-top': ''});

				// dimensions of the parent
				var parentWidth = $parent.width();
				var parentHeight = $parent.height();

				// dimensions of the image
				var imageWidth = $img.width();
				var imageHeight = $img.height();

				if((imageWidth <= 0 || imageHeight <= 0) && $img.prop('position-on-load') !== true) {
					$img.load(function() {
						positionSlide($(this));
					}).prop('position-on-load', true);
				}

				// step 1 - calculate the percentage difference between image width and container width
				var diff = imageWidth / parentWidth;

				// step 2 - if height divided by difference is smaller than container height, resize by height. otherwise resize by width
				if ((imageHeight / diff) < parentHeight) {
					$img.css({'width': 'auto', 'height': parentHeight});

					// set image variables to new dimensions
					imageWidth = imageWidth / (imageHeight / parentHeight);
					imageHeight = parentHeight;
				}
				else {
					$img.css({'height': 'auto', 'width': parentWidth});

					// set image variables to new dimensions
					imageWidth = parentWidth;
					imageHeight = imageHeight / diff;
				}

				// step 3 - center image in container
				var leftOffset = (imageWidth - parentWidth) / -2;
				var topOffset = (imageHeight - parentHeight) / -2;

				$img.css({'margin-left': leftOffset, 'margin-top': topOffset});
			}

			function stretchViewport() {
				var $filters = $('.offers-home-filters'),
					$top = $('#top');

				if($filters.length <= 0) {
					return;
				}

				if($(document).width() > 992-18) {
					$self.addClass('fixed-height').css('height', $(window).height() - $filters.outerHeight()-$top.outerHeight());
				}
				else {
					$self.removeClass('fixed-height').css('height', 'auto');
				}
			}

			$(window).resize(function() {
				$self.find('.slides figure img').each(function() {
					stretchViewport();
					positionSlide($(this));
				});
			});

			// Home theatre
			$(this).find('.slides').css('width', '2000em');

			$(this)
				// Setup carousel
				.jcarousel({
					list: '.slides',
					wrap: 'circular',
					animation: {
						duration: 1000,
						easing: 'easeInOutCirc',
					}
				})
				// Setup carousel autoscroll
				.jcarouselAutoscroll({
					interval: 4000,
					target: '+=1',
					autostart: true
				})
				// Position image when slide comes in
				.on('jcarousel:targetin', 'figure', function() {
					$(this).find('img').each(function() {
						positionSlide($(this));
					});
				})
				// Setup label fade in
				.on('jcarousel:fullyvisiblein', 'figure', function() {
					var $f = $(this).find('figcaption').hide();

					setTimeout(function() {
						$f.show().removeClass('fadeOutDown').addClass('fadeInUp animated');
					}, 600);
				})
				// Setup label fade out
				.on('jcarousel:targetout', 'figure', function() {
					$(this).find('figcaption').removeClass('fadeInUp').addClass('fadeOutDown animated');
				});

			// Position first slide on load
			/*$(this).find('.slides figure img:first').load(function() {
				alert('s');
			});*/

			// Setup carousel controls
			$(this).find('.prev').show().click(function() {
				$self.jcarousel('scroll', '-=1');
				return false;
			});
			$(this).find('.next').show().click(function() {
				$self.jcarousel('scroll', '+=1');
				return false;
			});

			// Setup carousel dots navigation
			$(this).find('.dots ul')
				.on('jcarouselpagination:active', 'li', function() {
					$(this).addClass('active');
				})
				.on('jcarouselpagination:inactive', 'li', function() {
					$(this).removeClass('active');
				})
				.jcarouselPagination({
					carousel: $self,
					item: function(page, carouselItems) {
						return '<li><a href="javascript:void(0);"></a></li>';
					}
				});

			// Stop autosliding on hover
			$(this).hover(
				function() {
					$(this).jcarouselAutoscroll('stop');
				},
				function() {
					$(this).jcarouselAutoscroll('start');
				}
			);

			// Reload carousel on window resize
			$(window).resize(function() {
				$self.jcarousel('reload');
			});
		});
	};

})(jQuery);
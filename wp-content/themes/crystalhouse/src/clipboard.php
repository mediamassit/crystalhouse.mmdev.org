<?php

define('CHP_CLIPBOARD_COOKIE', 'chclipboard');

/**
 * Saves clipboard data.
 * @param array $ids
 * @return bool
 */
function chp_clipboard_save_data($ids) {
	return setcookie(CHP_CLIPBOARD_COOKIE, implode('.', $ids), 0, '/', '.'.cht_get_domain());
}

/**
 * Adds offer to clipboard.
 * @param int $id
 * @return bool
 */
function chp_clipboard_add_offer($id) {
	$offers = chp_clipboard_get();

	if(!in_array($id, $offers)) {
		$offers[] = $id;
	}

	return chp_clipboard_save_data($offers);
}

/**
 * Returns clipboard content.
 * @return array
 */
function chp_clipboard_get() {
	return isset($_COOKIE[CHP_CLIPBOARD_COOKIE]) ? explode('.', $_COOKIE[CHP_CLIPBOARD_COOKIE]) : array();
}

/**
 * Checks whether given offer is in clipboard.
 * @param int $id
 * @return bool
 */
function chp_offer_in_clipboard($id) {
	$offers = chp_clipboard_get();

	return in_array((string) $id, $offers);
}

/**
 * Returns the list of offers from clipboard.
 * @return WP_Query
 */
function chp_clipboard_get_offers() {
	$offers = chp_clipboard_get();

	if(sizeof($offers) <= 0) {
		return array();
	}

	return new WP_Query(array(
		'numberposts' => -1,
		'post_type' => array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business'),
		'post__in' => $offers
	));
}

/**
 * Removes offer from clipboard.
 * @param int $id
 * @return bool
 */
function chp_clipboard_remove_offer($id) {
	$offers = chp_clipboard_get();

	if(sizeof($offers) > 0) {
		if(in_array($id, $offers)) {
			unset($offers[array_search($id, $offers)]);
		}
	}

	return chp_clipboard_save_data($offers);
}

/**
 * Clears clipboard.
 * @return bool
 */
function chp_clipboard_clear() {
	if(isset($_COOKIE[CHP_CLIPBOARD_COOKIE])) {
		unset($_COOKIE[CHP_CLIPBOARD_COOKIE]);
		return setcookie(CHP_CLIPBOARD_COOKIE, null, -1, '/', '.'.cht_get_domain());
	}
}

/**
 * Saves clipboard.
 * @return bool
 */
function chp_clipboard_save() {
	return setcookie(CHP_CLIPBOARD_COOKIE, $_COOKIE[CHP_CLIPBOARD_COOKIE], time()+(60*60*24*30), '/', '.'.cht_get_domain()); // 30 days
}

/**
 * Clipboard temporary AJAX end for XHR requests.
 */
function chp_clipboard_temp_ajax_end() {
	if(!isset($_GET['ajax'])) {
		return;
	}

	header('Content-type: text/javascript');

	// Add offer
	if(isset($_GET['put']) && isset($_GET['id'])) {
		die(json_encode(array(
			'success' => chp_clipboard_add_offer(@$_GET['id'])
		)));
	}

	// Remove offer
	if(isset($_GET['remove']) && isset($_GET['id'])) {
		die(json_encode(array(
			'success' => chp_clipboard_remove_offer(@$_GET['id'])
		)));
	}

	// Clear clipboard
	if(isset($_GET['clear'])) {
		die(json_encode(array(
			'success' => chp_clipboard_clear()
		)));
	}

	// Save clipboard
	if(isset($_GET['save'])) {
		die(json_encode(array(
			'success' => chp_clipboard_save()
		)));
	}

	// Get cookie
	if(isset($_GET['get-cookie'])) {
		die(json_encode(array(
			'success' => true,
			'data' => chp_clipboard_get()
		)));
	}

	// Returns offers list
	if(isset($_GET['get'])) {
		$data = null;
		$offers = chp_clipboard_get_offers();

		if(is_object($offers) && $offers->have_posts()) {
			ob_start();

			while($offers->have_posts()) {
				$offers->the_post();
				get_template_part('src/partials/clipboard-offer');
			}

			$data = ob_get_clean();
		}

		die(json_encode(array(
			'success' => true,
			'data' => $data
		)));
	}
}
<?php

class Asari_Page_Options {
	
	/**
	 * Holds the values to be used in the fields callbacks
	 */
	private $options;

	/**
	 * Settings group name
	 */
	private $option_group = 'asarisync_settings';

	/**
	 * The name of an option to sanitize and save
	 */
	private $option_name = 'asarisync_options';

	/**
	 * Settings page ID
	 */
	private $page_id = 'asarisync';

	/**
	 * Start up
	 */
	public function __construct() {
		add_action('admin_menu', array($this, 'add_plugin_page'));
		add_action('admin_init', array($this, 'page_init'));
	}

	/**
	 * Add options page
	 */
	public function add_plugin_page() {
		// This page will be under "Settings"
		add_options_page(
			'AsariSync Options',
			'AsariSync',
			'manage_options',
			$this->page_id,
			array($this, 'create_admin_page')
		);
	}

	/**
	 * Options page callback
	 */
	public function create_admin_page() {
		// Set class property
		$this->options = get_option($this->option_name);

		?>
		<div class="wrap">
			<?php screen_icon(); ?>
			<h2>AsariSync Options</h2>
			<?php if(@$_GET['settings-updated'] == 'true') : ?><div class="updated fade"><p>Settings updated successfully.</p></div><?php endif; ?>
			<form method="post" action="options.php">
			<?php
				settings_fields($this->option_group);
				do_settings_sections($this->page_id);
				submit_button();
			?>
			</form>
		</div>
		<?php
	}

	/**
	 * Register and add settings
	 */
	public function page_init() {
		register_setting(
			$this->option_group, // Option group
			$this->option_name, // Option name
			array($this, 'sanitize') // Sanitize
		);

		add_settings_section(
			'asarisync_api_settings', // ID
			'API Settings', // Title
			array(), // Callback
			$this->page_id // Page
		);

			add_settings_field(
				'api_userid', // ID
				'User ID', // Title
				array($this, 'print_text_field'), // Callback
				$this->page_id, // Page
				'asarisync_api_settings', // Section
				array('type' => 'text', 'name' => 'api_userid')
			);

			add_settings_field(
				'api_token', // ID
				'Token', // Title
				array($this, 'print_text_field'), // Callback
				$this->page_id, // Page
				'asarisync_api_settings', // Section
				array('type' => 'text', 'name' => 'api_token')
			);

			add_settings_field(
				'sync_max_queue_size', // ID
				'Max queue size', // Title
				array($this, 'print_text_field'), // Callback
				$this->page_id, // Page
				'asarisync_api_settings', // Section
				array('type' => 'text', 'name' => 'sync_max_queue_size')
			);
	}

	/**
	 * Sanitize each setting field as needed
	 *
	 * @param array $input Contains all settings fields as array keys
	 */
	public function sanitize($input) {
		$new_input = array();

		if(isset($input['api_userid'])) {
			$new_input['api_userid'] = absint($input['api_userid']);
		}

		if(isset($input['api_token'])) {
			$new_input['api_token'] = sanitize_text_field($input['api_token']);
		}

		if(isset($input['sync_max_queue_size'])) {
			$new_input['sync_max_queue_size'] = absint($input['sync_max_queue_size']);
		}

		return $new_input;
	}

	public function print_text_field(array $args) {
		$name = $args['name'];
		$type = $args['type'];
		$id = $this->option_name.'-'.$name;
		$value = isset($this->options[$name]) ? esc_attr( $this->options[$name]) : null;

		if('email' == $type AND '' == $value) {
			$value = $this->admin_mail;
		}

		$name = $this->option_name.'['.$name.']';
		
		printf(
			'<input type="%s" name="%s" value="%s" id="%s" />',
			$type, $name, $value, $id
		);
	}
}
<?php

	$found_posts = $offers->found_posts;

?>			<div class="page-title">
				<h2><?php
					if(sizeof($config->getPostType()) > 1) {
						if(is_tax('offer_section')) {
							$term = $wp_query->queried_object;
							_e($term->name, 'chtheme');
						}
						else _e('Oferty', 'chtheme');
					}
					else _e(chtof_type_label(reset($config->getPostType())), 'chtheme');

				?> <?php if(sizeof($config->getCategory()) > 0) : ?><?php _e('w kategorii:', 'chtheme'); ?> <strong><?php
					if($config->isCategory('buy')) {
						_e('na sprzedaż', 'chtheme');
					}
					elseif($config->isCategory('rent')) {
						_e('na wynajem', 'chtheme');
					}
				?></strong><?php endif; ?> <strong>(<?php printf(_n('1 oferta', '%s ofert', $found_posts, 'chtheme'), $found_posts); ?>)</strong></h2>
			</div>
			<!-- .page-title -->
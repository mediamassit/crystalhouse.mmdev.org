<?php
	$flags = cht_get_offer_flags();

	if(sizeof($flags) > 0) {
		$caption_flags = array_flip($flags);
		unset($caption_flags['no-fee']);
	}	
?>
										<figure>
											<span>
												<a href="<?php echo get_permalink($post->ID); ?>">
													<?php if($flags && array_search('no-fee', $flags) !== false) : ?>
													<em class="label"><img src="<?php bloginfo('template_url'); ?>/images/icons/no-fee-<?php echo pll_current_language(); ?>.png" alt=""></em>
													<?php endif; ?>
				
													<?php
														$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(370, 215));
													?>
													<?php if($thumb) : ?>
														<img src="<?php echo $thumb[0]; ?>" width="<?php echo $thumb[1]; ?>" alt="">
													<?php else : ?>
														<img src="<?php bloginfo('template_url'); ?>/tmp/thumb_370x215.jpg" width="370" alt="">
													<?php endif; ?>
												</a>
											</span>
											<?php if(sizeof(@$caption_flags)) : ?><figcaption><span><?php echo implode(' | ', array_map('pll__', $caption_flags)); ?></span></figcaption><?php endif; ?>
										</figure>
<?php

get_header();

$config = Cht_Offers_List_Config::init($_GET);
$categories = cht_get_offer_categories();

// Setup post type
switch(get_queried_object()->term_id) {
	default:
	case $categories->buy:
		$config->setPostType(array('offer_apartment', 'offer_house', 'offer_lot'))->setCategory($categories->buy);
		break;
	case $categories->rent:
		$config->setPostType(array('offer_apartment', 'offer_house'));
		$config->setCategory(isset($_GET['rent_type']) ? $_GET['rent_type'] : array($categories->long_rent, $categories->short_rent));
		break;
	case $categories->long_rent:
		$config->setPostType(array('offer_apartment', 'offer_house'))->setCategory($categories->long_rent);
		break;
	case $categories->short_rent:
		$config->setPostType(array('offer_apartment', 'offer_house'))->setCategory($categories->short_rent);
		break;
	case $categories->investments:
		$config->setPostType(array('offer_apartment', 'offer_house'))->setCategory($categories->investments);
		break;
}

// Setup post type
if(in_array(@$_GET['type'], array('apartment', 'house', 'lot'))) {
	$config->setPostType(array('offer_'.$_GET['type']));
}

$list = new Cht_Offers_List($config);
$offers = $list->fetchAll();
$offers_map = $list->fetchAllForMap();

?>

	<div id="content">
		<div class="container">
			<?php get_template_part('src/partials/breadcrumbs'); ?>
			<?php chtof_get_slot('src/partials/filters/navigation_tabs', array('no_rent' => (!$config->isCategory('rent') || $config->isCategory('investments')), 'no_buy' => (!$config->isCategory('buy') || $config->isCategory('investments')), 'categories' => $categories, 'config' => $config)); ?>
			<?php include locate_template('src/partials/filters/page_title.php'); ?>

			<div class="row">
				<?php include locate_template('src/partials/filters/list.php'); ?>

				<div class="col-md-3">
					<?php
						if($config->isCategory('investments')) :
							include locate_template('src/partials/filters/lists/investments.php');
						else :
							if($config->isCategory('rent')) :
								switch(get_queried_object()->term_id) :
									default:
										include locate_template('src/partials/filters/lists/rent.php');
										break;
									case $categories->long_rent:
										include locate_template('src/partials/filters/lists/rent_long.php');
										break;
									case $categories->long_short:
										include locate_template('src/partials/filters/lists/rent_short.php');
										break;
								endswitch;
							else :
								include locate_template('src/partials/filters/lists/buy.php');
							endif;
						endif;
					?>
				</div>
				<!-- .col-md-3 -->
			</div>
			<!-- .row -->
		</div>
		<!-- .container -->
	</div>
	<!-- #content -->

<?php get_footer(); ?>
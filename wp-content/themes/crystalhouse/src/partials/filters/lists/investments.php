					<div class="widget offers-list-filter">
						<h2><?php _e('Znajdź nieruchomość', 'chtheme'); ?></h2>
						<form method="get" action="<?php echo chtof_remf_url(); ?>" name="offers_filter" data-form-values='{"offer": "buy", "country": "Polska", "is_investment": 1}'>
							<?php include locate_template('src/partials/filters/slots/q.php'); ?>
							<?php include locate_template('src/partials/filters/slots/keyword.php'); ?>
							<?php include locate_template('src/partials/filters/slots/currency.php'); ?>

							<div data-group="offer_type_apartment-house">
								<?php chtof_get_slot('src/partials/filters/slots/type', array('types' => array('offer_apartment', 'offer_house'))); ?>
								<?php chtof_get_slot('src/partials/filters/slots/developer'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/investment'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/area'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/price'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/city'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/district'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/attributes', array('no_long_attributes' => true)); ?>


								<div class="form-group text-center">
									<?php get_template_part('src/partials/filters/slots/submit'); ?>
								</div>
								<!-- .form-group -->
							</div>
							<!-- [data-group] -->
						</form>
					</div>
					<!-- .widget.offers-list-filter -->
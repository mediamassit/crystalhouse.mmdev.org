<?php

class Asari_Queue extends Asari_JsonFile {
	protected $_filenamePrefix = 'queue';
	protected $_name = 'basic';

	public function remove($realId) {
		$listings = $this->get();

		foreach($listings as $k => $listing) {
			if($listing['real'] === $realId) {
				unset($listings[$k]);
				break;
			}
		}

		$listings = array_values($listings);

		return $this->put($listings)->save();
	}

}
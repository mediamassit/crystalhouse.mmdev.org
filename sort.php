<?php

	setlocale(LC_COLLATE, 'pl_PL.UTF-8');

	$array = array( 'a', 'b', 'c', 'd', 'e', 'f', 'z', 'ź', 'ć', 'ę','ą', 'o', 'ó', 's' );
	asort($array, SORT_LOCALE_STRING);

	header('Content-Type: text/html; charset=utf-8');
	echo '<pre>' . print_r( $array, true ) . '</pre>';
<?php

/**
 * Return the contents of given template file.
 *
 * @param string $filePath The path to template file.
 * @return string
 */
function get($filePath)
{
	// If template file does not exists
	if(!file_exists($filePath)) {
		return false;
	}

	$contents = @file_get_contents($filePath);
	$dirname = strlen(dirname($filePath)) > 0 ? dirname($filePath) : '.';

	// If template file could not be loaded
	if(!$contents) {
		return false;
	}

	// Include defined template partials
	while(preg_match('#' . preg_quote('<!-- @include{') . '([^\{\}]+)' . preg_quote('} -->') . '#i', $contents, $match, PREG_OFFSET_CAPTURE) > 0) {
		$inclusion = get($dirname . '/' . $match[1][0]);
		$contents = substr_replace($contents, $inclusion, $match[0][1], strlen($match[0][0]));
	}

	return ltrim($contents);
}

// Fetch the path to template file
$file = isset($_GET['file']) ?
	'./' . $_GET['file'] : 
	'./index.html';

$content = get($file);

if(!$content) {
	header('HTTP/1.0 404 Not Found');
	die('<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found on this server.</p>
</body></html>');
}

echo $content;

	<div id="footer" class="slim">
		<div class="container">
			<div class="row hidden-xs">
				<h3 class="slogan"><?php echo get_opt('opt-theme-headline'); ?></h3>
			</div>
			<!-- .row -->
		</div>
		<!-- .container -->

		<div class="copyrights">
			<div class="container">
				<p>&copy; <?php echo date('Y'); ?> <a href="<?php echo pll_home_url(); ?>"><?php bloginfo('title'); ?></a> | <a href="http://roogmedia.pl/" style="cursor: default; opacity: 1;"><?php _e('by Roogmedia', 'chtheme'); ?></a></p>
			</div>
			<!-- .container -->
		</div>
		<!-- .copyrights -->
	</div>
	<!-- #footer -->

	<?php wp_footer(); ?>
	<?php get_template_part('src/footer-scripts'); ?>

</body>
</html>
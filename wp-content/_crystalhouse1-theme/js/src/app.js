Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
		c = isNaN(c = Math.abs(c)) ? 2 : c, 
		d = d == undefined ? "." : d, 
		t = t == undefined ? " " : t, 
		s = n < 0 ? "-" : "", 
		i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
		j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };

 (function($) {
	$.namespace = function(namespaceName, closures) {
		if($.fn[namespaceName] === undefined) {
			$.fn[namespaceName] = function executor(context) {
				if(this instanceof executor) {
					this.__context__ = context;
				}
				else {
					return new executor(this);
				}
			};
		}

		$.each(closures, function(closureName, closure) {
			$.fn[namespaceName].prototype[closureName] = function() {
				closure.apply(this.__context__, arguments);
			};
		});
	};
})(jQuery);

(function() {$(document).ready(function() {
	// Input placeholders
	/*$('textarea[placeholder],input[placeholder]').each(function() {
		if(!$(this).is('textarea') && !$(this).is('[type=text]') && !$(this).is('[type=email]')) {
			return;
		}

		$(this).attr('data-placeholder', $(this).attr('placeholder')).attr('placeholder', null);
	});*/

	// Home filters search query
	$('.offers-home-filters').each(function() {
		var $sq = $(this).find('.search-query');

		function position() {
			$sq.css({
				width: $sq.closest('.offers-home-filters').find('.side').outerWidth()-15,
				marginTop: '-'+$sq.outerHeight()+'px'
			});
		}

		function bind() {
			$sq.addClass('floating');
			position();

			$(window).bind('resize.position-search-query', function() {
				position();
			});
		}

		function unbind() {
			$sq.removeClass('floating').css({
				width: 'auto',
				marginTop: 'auto'
			});

			$(window).unbind('resize.position-search-query');
		}

		function init() {
			$(window).width() > 992 ? bind() : unbind();
		}

		$(window).resize(function() {
			init();
		});

		init();
	});

	// Sticky #top
	$('#top').sticky({ className: 'sticky' });
	$(window).resize(function() {
		var $wrapper = $('#top').parent();

		$wrapper.css('height', $('#top').height());
		//$('#top').sticky('update');
	});
	/*$(window).resize(function() {
		var $wrapper = $('#top').parent();

		$('#top').sticky('update');
		$wrapper.css('height', $wrapper.hasClass('sticky') ? $('#top').height() : 'auto');

		setTimeout(function() {
			$wrapper.css('height', $wrapper.hasClass('sticky') ? $('#top').height() : 'auto');
		}, 320);
	});
	$(window).scroll(function() {
		if($(this).scrollTop() <= 0) {
			$('#top').parent().css('height', 'auto');
		}
		else {
			$('#top').parent().css('height', $('#top').height());
		}
	});*/

	// Setup widgets & components
	$('#home-theatre').homeTheatre();
	$('.offers-carousel').offersCarousel();
	$('.widget.carousel-gallery').offerGalleryCarousel();
	$('.popular-entries').popularEntries();
	$(window).offersWishlist();

	// Tooltips
	$('[data-tooltip]').each(function() {
		$(this).tooltip({
			placement: $(this).attr('data-placement') ? $(this).attr('data-placement') : $(window).width() > 768 ? 'right' : 'left',
			container: 'body',
			delay: 300
		});
	});

	// Target links
	function scrollto($elm) {
		var $elm = $($elm);

		if($elm.length <= 0) return true;

		var mt = $('#top').attr('data-sticky-height') ? parseInt($('#top').attr('data-sticky-height')) : $('#top').height();

		if($(window).width() <= 992) mt = $('#top').height();

		$(document).scrollTo($elm, 600, { offset: mt*-1 });
	}

	$('[data-section]').each(function() {
		if($(this).attr('data-section') == '#') return;

		$(this).bind('click.target', function(e) {
			scrollto($(this).attr('data-section'));
			return false;
		});
	});

	// Tabs
	$('[data-target-tab]').each(function() {
		$(this)
			.unbind('click.target')
			.bind('click.tab', function(e) {
				var $tab = $($(this).attr('data-target-tab'));

				if($tab.length <= 0) return false;

				$tab.parent().find('.tab.active').removeClass('active');
				$tab.addClass('active');

				scrollto($(this).attr('href'));

				return false;
			});
	});

	// Cookie notifier
	$('#cookie-notifier').each(function() {
		var $self = $(this);

		if($.cookie('ch-cn') != 1) {
			$self.show();
		}

		$(this).find('[data-close-cookie-notifier]').click(function() {
			$.cookie('ch-cn', 1, { expires: 30, path: '/' });

			$self.fadeOut(400);
			return false;
		});
	});

	$('.offers-list-filter,.offers-home-filters').find('input[type=checkbox],input[type=radio]').each(function() {
		var $label = $(this).closest('label');

		if($label.length <= 0) {
			return;
		}

		if($(this).is('[type=checkbox]')) {
			$(this).hide().change(function() {
				if($(this).is(':checked')) {
					$label.removeClass('not-active').addClass('active');
				}
				else {
					$label.removeClass('active').addClass('not-active');
				}
			});
		}
		else if($(this).is('[type=radio]')) {
			$(this).hide().change(function() {
				$(this).closest('form').find('input[name="'+$(this).attr('name')+'"]:radio:checked').closest('label').removeClass('not-active').addClass('active');
				$(this).closest('form').find('input[name="'+$(this).attr('name')+'"]:radio:not(:checked)').closest('label').removeClass('active').addClass('not-active');
			});
		}
	}).trigger('change');

	$(window).trigger('resize');
}); })(jQuery);
<?php
/**
 * Plugin Name: AsariSync
 * Description: Imports new Asari listings to website.
 * Version: 1.0
 * Author: 365grupa
 */
define('ASARISYNC_DEVMODE', true);

function asarisync_autoload($class) {
	$path = __DIR__.'/src/'.str_replace('_', '/', $class).'.php';

	if(file_exists($path)) {
		include $path;
	}
}
spl_autoload_register('asarisync_autoload');

require __DIR__.'/src/Unirest.php';

$asari = new Asari;
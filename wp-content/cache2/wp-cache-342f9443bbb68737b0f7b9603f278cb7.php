<?php die(); ?><!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Szukaj - Crystal HouseCrystalHouse</title>
	<link rel="stylesheet" media="screen,print" href="http://crystalhouse.pl/wp-content/themes/crystalhouse/css/style.css">
	<link rel="shortcut icon" href="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/favicon.ico"/>
	
<!-- This site is optimized with the Yoast SEO plugin v2.3.4 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="description" content="Szukaj"/>
<link rel="canonical" href="http://crystalhouse.pl/szukaj/" />
<meta property="og:locale" content="pl_PL" />
<meta property="og:locale:alternate" content="en_US" />
<meta property="og:locale:alternate" content="de_DE" />
<meta property="og:locale:alternate" content="fr_FR" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Szukaj - Crystal House" />
<meta property="og:description" content="Szukaj" />
<meta property="og:url" content="http://crystalhouse.pl/szukaj/" />
<meta property="og:site_name" content="CrystalHouse" />
<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="Szukaj"/>
<meta name="twitter:title" content="Szukaj - Crystal House"/>
<meta name="twitter:domain" content="CrystalHouse"/>
<!-- / Yoast SEO plugin. -->


            <script type="text/javascript">//<![CDATA[
            // Google Analytics for WordPress by Yoast v4.3.5 | http://yoast.com/wordpress/google-analytics/
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-17189051-42']);
				            _gaq.push(['_trackPageview']);
            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
            //]]></script>
					<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/crystalhouse.pl\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.5.2"}};
			!function(a,b,c){function d(a){var c,d,e,f=b.createElement("canvas"),g=f.getContext&&f.getContext("2d"),h=String.fromCharCode;if(!g||!g.fillText)return!1;switch(g.textBaseline="top",g.font="600 32px Arial",a){case"flag":return g.fillText(h(55356,56806,55356,56826),0,0),f.toDataURL().length>3e3;case"diversity":return g.fillText(h(55356,57221),0,0),c=g.getImageData(16,16,1,1).data,d=c[0]+","+c[1]+","+c[2]+","+c[3],g.fillText(h(55356,57221,55356,57343),0,0),c=g.getImageData(16,16,1,1).data,e=c[0]+","+c[1]+","+c[2]+","+c[3],d!==e;case"simple":return g.fillText(h(55357,56835),0,0),0!==g.getImageData(16,16,1,1).data[0];case"unicode8":return g.fillText(h(55356,57135),0,0),0!==g.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='contact-form-7-css'  href='http://crystalhouse.pl/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=3.8.1' type='text/css' media='all' />
<script type='text/javascript' src='http://crystalhouse.pl/wp-includes/js/jquery/jquery.js?ver=1.12.3'></script>
<script type='text/javascript' src='http://crystalhouse.pl/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.0'></script>
<link rel='https://api.w.org/' href='http://crystalhouse.pl/wp-json/' />
<link rel="alternate" type="application/json+oembed" href="http://crystalhouse.pl/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fcrystalhouse.pl%2Fszukaj%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://crystalhouse.pl/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fcrystalhouse.pl%2Fszukaj%2F&#038;format=xml" />
<link rel="alternate" href="http://crystalhouse.pl/szukaj/" hreflang="pl" />
<link rel="alternate" href="http://crystalhouse.pl/search-offers/" hreflang="en" />
<link rel="alternate" href="http://crystalhouse.pl/suchen/" hreflang="de" />
<link rel="alternate" href="http://crystalhouse.pl/rechercher/" hreflang="fr" />
<script type="text/javascript">
(function(url){
if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
var wfscr = document.createElement('script');
wfscr.type = 'text/javascript';
wfscr.async = true;
wfscr.src = url + '&r=' + Math.random();
(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
})('//crystalhouse.pl/wp-admin/admin-ajax.php?action=wordfence_logHuman&hid=E5B81EBB0A5DEB7AB4BC8C33215A0AE9'); 
</script>	<script>
	var THEME_URL = "http://crystalhouse.pl/wp-content/themes/crystalhouse",
		SITE_DOMAIN = 'crystalhouse.pl',
		WISHLIST_API_GET = 'http://crystalhouse.pl/twoj-schowek/?ajax&get',
		WISHLIST_API_PUT = 'http://crystalhouse.pl/twoj-schowek/?ajax&put',
		WISHLIST_API_REMOVE = 'http://crystalhouse.pl/twoj-schowek/?ajax&remove',
		WISHLIST_API_CLEAR = 'http://crystalhouse.pl/twoj-schowek/?ajax&clear',
		WISHLIST_API_SAVE = 'http://crystalhouse.pl/twoj-schowek/?ajax&save',
		addthis_config = {
			"ui_language": "pl"
		},
		FANCYBOX_LANG = {
			error: '<p class="fancybox-error">Nie udało się załadować.<br>Spróbuj ponownie.</p>',
			closeBtn: '<a title="Zamknij" class="fancybox-item fancybox-close" href="javascript:;"></a>',
			next: '<a title="Następny" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
			prev: '<a title="Poprzedni" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
		};
	</script>
	<style>
	.widget.offers-home-filters form .submit .closer {
		display: none;
		text-align: right;
	}

		.widget.offers-home-filters form .submit .closer a {
			margin-top: 14px;
			color: #fff;
			font-weight: 700;
			font-size: 1em;
		}

	.widget.offers-home-filters form.extended .submit .closer {
		display: block;
	}

	.widget.offers-home-filters form.extended .submit .expander {
		display: none;
	}

	.widget.contact-broker figure img {
		max-width: 180px;
	}

	.widget.offers-featured .offer.page figure figcaption {
		width: auto;
		font-weight: 400;
		background: #5fbcce;
		bottom: 10px;
		left: auto;
		right: 0;
	}

	.widget.carousel-gallery .viewport {
		/*max-width: 800px;*/
		max-height: 450px;
		margin: 0 auto;
	}

	.widget.carousel-gallery .slides figure img {
		width: auto;
	}

	/*.widget.contact-broker figure {
		width: 80%;
	}

		.widget.contact-broker figure img {
			width: 100%;
		}
	
	@media (max-width: 992px) {
		.widget.contact-broker figure {
			width: auto;
		}

			.widget.contact-broker figure img {
				width: auto;
			}
	}*/

	#top .main-navigation ul li {
		margin-right: 2.15%;
	}

	@media (max-width: 1280px) {
		#top .main-navigation ul li {
			margin-right: 1.95%;
		}
	}

	@media (max-width: 1220px) {
		#top .main-navigation ul li {
			margin-right: 0.9%;
		}
	}

	@media (max-width: 1100px) {
		#top .main-navigation ul li {
			margin-right: 0;
		}
	}

	@media (max-width: 768px) {
		section.page.team .team-member figure {
			width: auto;
			display: block;
		}

		section.page.team .team-member figure img {
			width: auto;
		}
	}

	@media (max-width: 480px) {
		section.page.team .team-member figure {
			float: none;
		}
	}
	</style>
</head>

<body data-scrollto-append="100">

	<div id="cookie-notifier">
		<p>Serwis używa cookies. Wyrażasz zgodę na używanie cookie, zgodnie z aktualnymi ustawieniami przeglądarki. Zapoznaj się z <a href="http://crystalhouse.pl">polityką cookies</a>. <a href="javascript:void(0);" class="close-x" data-close-cookie-notifier></a></p>
	</div>

	<div id="top" data-sticky-height="100" class="hidden-print">
		<div class="header">
			<div class="container">
				<div class="col-md-2 logo">
					<h1><a href="http://crystalhouse.pl/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/logo.png" alt="CrystalHouse"></a></h1>
				</div>
				<!-- .logo -->

				<div class="col-md-10 right">
					<nav class="tray">
						<ul class="contact">
							<li><span class="hidden-md">T:</span> <a href="tel:+48 22 856 78 60">+48 22 856 78 60</a></li>							<li><span class="hidden-md">E:</span> <a href="mailto:biuro@crystalhouse.pl">biuro@crystalhouse.pl</a></li>
							<li><a href="skype:d.przewocki"><em class="icon-skype"></em> <span class="hidden-md">Call me</span></a></li>
							<li><a href="#" data-toggle="modal" data-target="#modal-contact"><em class="icon-message"></em> <span class="hidden-md">Wyślij wiadomość</span></a></li>
							<li></li>
						</ul>
						<!-- .contact -->

						<ul class="switch">
							<li class="currency">
								<a href="javascript:void(0);" data-toggle="dropdown">PLN</a>
								<ul class="options" role="menu">
																		<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&_set_session_currency=USD">USD</a></li>
																		<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&_set_session_currency=EUR">EUR</a></li>
																	</ul>
							</li>
							<li class="language">
																<a href="javascript:void(0);" data-toggle="dropdown"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/flags/pl.png" alt=""> pl</a>
								<ul class="options" role="menu">
																		<li><a href="http://crystalhouse.pl/search-offers/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/flags/en.png" alt=""> en</a></li>
																		<li><a href="http://crystalhouse.pl/suchen/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/flags/de.png" alt=""> de</a></li>
																		<li><a href="http://crystalhouse.pl/rechercher/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/flags/fr.png" alt=""> fr</a></li>
																	</ul>
							</li>
						</ul>
						<!-- .switch -->
					</nav>
					<!-- .tray -->

					<div class="middle">
						<h2><strong>Luxury</strong> Realty Estate Agency</h2>

						<div class="side">
							<form method="get" action="http://crystalhouse.pl/szukaj/" class="search-form">
								<input type="text" name="q" placeholder="wpisz nr oferty, miasto, ulicę">
								<input type="submit" value="Szukaj">
							</form>
							<!-- .search-form -->

														<div class="clipboard">
								<a href="http://crystalhouse.pl/twoj-schowek/" data-toggle="wishlist"><span>Schowek</span> <em></em></a>

								<div class="widget wishlist-dropdown">
									<h3>Twoje oferty</h3>

									<section class="offers">
																		</section>

									<div class="toolbar">
										<ul>
											<li><a href="javascript:void(0);" data-clearclipboard="1"><em class="icon-close"></em> Wyczyść schowek</a></li>
											<li><a href="javascript:void(0);" data-saveclipboard="1"><em class="icon-download"></em> Zapisz schowek</a></li>
											<li class="more"><a href="http://crystalhouse.pl/twoj-schowek/">Chcę obejrzeć</a></li>
										</ul>
									</div>
								</div>
								<!-- .widget.wishlist-popup -->
							</div>
							<!-- .clipboard -->
						</div>
						<!-- .side -->
					</div>
					<!-- .middle -->
				</div>
				<!-- .right -->

				<div class="col-md-8 mobile-right">
					<div class="row">
						<ul class="switch">
							<li class="currency">
								<a href="javascript:void(0);" data-toggle="dropdown">PLN</a>
								<ul class="options" role="menu">
																		<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&_set_session_currency=USD">USD</a></li>
																		<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&_set_session_currency=EUR">EUR</a></li>
																	</ul>
							</li>
							<li class="language">
																<a href="javascript:void(0);" data-toggle="dropdown"><img src="http://crystalhouse.pl/wp-content/plugins/polylang/flags/pl.png" alt=""> pl</a>
								<ul class="options" role="menu">
																		<li><a href="http://crystalhouse.pl/search-offers/"><img src="http://crystalhouse.pl/wp-content/plugins/polylang/flags/us.png" alt=""> en</a></li>
																		<li><a href="http://crystalhouse.pl/suchen/"><img src="http://crystalhouse.pl/wp-content/plugins/polylang/flags/de.png" alt=""> de</a></li>
																		<li><a href="http://crystalhouse.pl/rechercher/"><img src="http://crystalhouse.pl/wp-content/plugins/polylang/flags/fr.png" alt=""> fr</a></li>
																	</ul>
							</li>
							<li class="language">
								<a href="http://crystalhouse.pl/twoj-schowek/" style="background-image: none; width: auto;">Schowek</a>
							</li>
						</ul>
						<!-- .switch -->
					</div>
					<!-- .row -->

					<div class="row">
						<div class="menu-main_menu_pl-container"><form method="get" action="#"><select onchange="if(this.value) window.location.href=this.value"><option value="http://crystalhouse.pl/">Strona główna</option>
<option value="http://crystalhouse.pl/offer-category/kup/">Kup</option>
<option value="http://crystalhouse.pl/sprzedaj/">Sprzedaj</option>
<option value="http://crystalhouse.pl/offer-category/wynajem/">Wynajmij<option value="http://crystalhouse.pl/offer-category/wynajem/">&nbsp;&nbsp;&nbsp;Lista nieruchomości</option>
<option value="http://crystalhouse.pl/strona-glowna/wynajem-dlugoterminowy/">&nbsp;&nbsp;&nbsp;Wynajem długoterminowy</option>
</option>
<option value="http://crystalhouse.pl/strona-glowna/komercja/">Komercja<option value="http://crystalhouse.pl/strona-glowna/komercja/">&nbsp;&nbsp;&nbsp;Lokale komercyjne</option>
<option value="/business">&nbsp;&nbsp;&nbsp;Lista nieruchomości</option>
</option>
<option value="http://crystalhouse.pl/szukaj/?keyword=wakacje">Inne nieruchomości<option value="http://crystalhouse.pl/szukaj/?keyword=wakacje">&nbsp;&nbsp;&nbsp;Nieruchomości wakacyjne</option>
<option value="http://crystalhouse.pl/szukaj/?keyword=woda">&nbsp;&nbsp;&nbsp;Nieruchomości nad wodą</option>
<option value="http://crystalhouse.pl/szukaj/?keyword=gory">&nbsp;&nbsp;&nbsp;Nieruchomości w górach</option>
<option value="http://crystalhouse.pl/szukaj/?keyword=historia">&nbsp;&nbsp;&nbsp;Nieruchomości historyczne</option>
<option value="http://crystalhouse.pl/szukaj/?keyword=hotel,pensjonat">&nbsp;&nbsp;&nbsp;Pensjonaty i hotele</option>
<option value="http://crystalhouse.pl/lot/">&nbsp;&nbsp;&nbsp;Działki</option>
</option>
<option value="http://crystalhouse.pl/uslugi/finansowanie/">Usługi<option value="http://crystalhouse.pl/uslugi/finansowanie/">&nbsp;&nbsp;&nbsp;Finansowanie</option>
<option value="http://crystalhouse.pl/uslugi/wykonczenie-wnetrz-fit-out/">&nbsp;&nbsp;&nbsp;Wykończenie wnętrz / Fit-out</option>
<option value="http://crystalhouse.pl/uslugi/zarzadzanie-nieruchomosciami/">&nbsp;&nbsp;&nbsp;Zarządzanie nieruchomościami</option>
<option value="http://crystalhouse.pl/uslugi/doradztwo-inwestycyjne/">&nbsp;&nbsp;&nbsp;Doradztwo inwestycyjne</option>
</option>
<option value="http://crystalhouse.pl/nasz-zespol/">O nas<option value="http://crystalhouse.pl/historia-firmy/">&nbsp;&nbsp;&nbsp;Historia firmy</option>
<option value="http://crystalhouse.pl/strategia-firmy/">&nbsp;&nbsp;&nbsp;Strategia firmy</option>
<option value="http://crystalhouse.pl/nasz-zespol/">&nbsp;&nbsp;&nbsp;Nasz zespół</option>
<option value="http://crystalhouse.pl/uslugi/ubezpieczenia/">&nbsp;&nbsp;&nbsp;Ubezpieczenia</option>
<option value="http://crystalhouse.pl/oferty-pracy/">&nbsp;&nbsp;&nbsp;Oferty pracy</option>
</option>
<option value="http://crystalhouse.pl/kontakt/">Kontakt</option>
</select></form></div>					</div>
					<!-- .row -->
				</div>
				<!-- .mobile-right -->
			</div>
			<!-- .container -->
		</div>
		<!-- .header -->

		<nav class="main-navigation static">
			<div class="container">
				<div class="menu-main_menu_pl-container"><ul id="menu-main_menu_pl-1" class="menu"><li><a href="http://crystalhouse.pl/" >Strona główna</a></li>
<li><a href="http://crystalhouse.pl/offer-category/kup/" >Kup</a></li>
<li><a href="http://crystalhouse.pl/sprzedaj/" >Sprzedaj</a></li>
<li><a href="http://crystalhouse.pl/offer-category/wynajem/"  data-toggle="dropdown" data-hover="dropdown">Wynajmij</a>
<ul class="dropdown-menu" role="menu">
	<li><a href="http://crystalhouse.pl/offer-category/wynajem/" >Lista nieruchomości</a></li>
	<li><a href="http://crystalhouse.pl/strona-glowna/wynajem-dlugoterminowy/" >Wynajem długoterminowy</a></li>
</ul>
</li>
<li><a href="http://crystalhouse.pl/strona-glowna/komercja/"  data-toggle="dropdown" data-hover="dropdown">Komercja</a>
<ul class="dropdown-menu" role="menu">
	<li><a href="http://crystalhouse.pl/strona-glowna/komercja/" >Lokale komercyjne</a></li>
	<li><a href="/business">Lista nieruchomości</a></li>
</ul>
</li>
<li><a href="http://crystalhouse.pl/szukaj/?keyword=wakacje"  data-toggle="dropdown" data-hover="dropdown">Inne nieruchomości</a>
<ul class="dropdown-menu" role="menu">
	<li><a href="http://crystalhouse.pl/szukaj/?keyword=wakacje" >Nieruchomości wakacyjne</a></li>
	<li><a href="http://crystalhouse.pl/szukaj/?keyword=woda" >Nieruchomości nad wodą</a></li>
	<li><a href="http://crystalhouse.pl/szukaj/?keyword=gory" >Nieruchomości w górach</a></li>
	<li><a href="http://crystalhouse.pl/szukaj/?keyword=historia" >Nieruchomości historyczne</a></li>
	<li><a href="http://crystalhouse.pl/szukaj/?keyword=hotel,pensjonat" >Pensjonaty i hotele</a></li>
	<li><a href="http://crystalhouse.pl/lot/" >Działki</a></li>
</ul>
</li>
<li><a href="http://crystalhouse.pl/uslugi/finansowanie/"  data-toggle="dropdown" data-hover="dropdown">Usługi</a>
<ul class="dropdown-menu" role="menu">
	<li><a href="http://crystalhouse.pl/uslugi/finansowanie/" >Finansowanie</a></li>
	<li><a href="http://crystalhouse.pl/uslugi/wykonczenie-wnetrz-fit-out/" >Wykończenie wnętrz / Fit-out</a></li>
	<li><a href="http://crystalhouse.pl/uslugi/zarzadzanie-nieruchomosciami/" >Zarządzanie nieruchomościami</a></li>
	<li><a href="http://crystalhouse.pl/uslugi/doradztwo-inwestycyjne/" >Doradztwo inwestycyjne</a></li>
</ul>
</li>
<li><a href="http://crystalhouse.pl/nasz-zespol/"  data-toggle="dropdown" data-hover="dropdown">O nas</a>
<ul class="dropdown-menu" role="menu">
	<li><a href="http://crystalhouse.pl/historia-firmy/" >Historia firmy</a></li>
	<li><a href="http://crystalhouse.pl/strategia-firmy/" >Strategia firmy</a></li>
	<li><a href="http://crystalhouse.pl/nasz-zespol/" >Nasz zespół</a></li>
	<li><a href="http://crystalhouse.pl/uslugi/ubezpieczenia/" >Ubezpieczenia</a></li>
	<li><a href="http://crystalhouse.pl/oferty-pracy/" >Oferty pracy</a></li>
</ul>
</li>
<li><a href="http://crystalhouse.pl/kontakt/" >Kontakt</a></li>
</ul></div>			</div>
			<!-- .container -->
		</nav>
		<!-- .main-navigation -->
	</div>
	<!-- #top -->

		<div class="modal clipboard vertical-middle fade" id="modal-clipboard" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="javascript:void(0);" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<div class="nav">
						<p><a href="http://crystalhouse.pl/twoj-schowek/" class="btn">Obejrzyj</a></p>
						<p>lub <strong><a href="javascript:void(0);" data-dismiss="modal" aria-hidden="true">Dodaj kolejne</a></strong></p>
					</div>
					<!-- .nav -->

					<div class="message">
						<p>Dodałeś nieruchomość do <a href="http://crystalhouse.pl/twoj-schowek/">schowka</a></p>
					</div>
					<!-- .message -->
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->		<div class="modal clipboard vertical-middle fade" id="modal-clipboard-saved" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="javascript:void(0);" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<div class="nav">
						<p style="margin-top: 25px;"><a href="http://crystalhouse.pl/twoj-schowek/" class="btn">Obejrzyj</a></p>
					</div>
					<!-- .nav -->

					<div class="message">
						<p>Zapisano schowek</p>
					</div>
					<!-- .message -->
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->		<div class="modal clipboard vertical-middle fade" id="modal-clipboard-clean" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="javascript:void(0);" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<div class="nav">
						<p style="margin-top: 25px;"><a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true" style="background-image: none; padding-right: 15px;">Zamknij</a></p>
					</div>
					<!-- .nav -->

					<div class="message">
						<p>Wyczyszczono schowek</p>
					</div>
					<!-- .message -->
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->		<div class="modal classic vertical-middle fade" id="modal-contact" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="#" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<h3>Wyślij wiadomość</h3>
<div class="wpcf7" id="wpcf7-f60-o1">
<div class="screen-reader-response"></div>
<form action="/szukaj/?is_recommended=1#wpcf7-f60-o1" method="post" class="wpcf7-form form-horizontal" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="60" />
<input type="hidden" name="_wpcf7_version" value="3.8.1" />
<input type="hidden" name="_wpcf7_locale" value="pl_PL" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f60-o1" />
<input type="hidden" name="_wpnonce" value="c34566d764" />
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcf-name">Imię i nazwisko*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap name"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcf-name" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcf-email">Email*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" id="pcf-email" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcf-phone">Telefon*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap phone"><input type="text" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcf-phone" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcf-message">Wiadomość*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap message"><textarea name="message" cols="5" rows="5" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form-control" id="pcf-message" aria-required="true" aria-invalid="false"></textarea></span></div>
</div>
<div class="form-group">
<div class="col-sm-6 text-right"><br><input type="hidden" name="_wpcf7_captcha_challenge_captcha-260" value="1242240450" /><img class="wpcf7-form-control wpcf7-captchac wpcf7-captcha-captcha-260" width="84" height="28" alt="captcha" src="http://crystalhouse.pl/wp-content/uploads/wpcf7_captcha/1242240450.png" /></div>
<div class="col-sm-6"><label>przepisz tekst:</label><span class="wpcf7-form-control-wrap captcha-260"><input type="text" name="captcha-260" value="" size="40" class="wpcf7-form-control wpcf7-captchar form-control" aria-invalid="false" /></span></div>
</div>
<div class="form-group text-right">
<div class="col-sm-12"><input type="submit" value="Wyślij" class="wpcf7-form-control wpcf7-submit btn" /></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->		<div class="modal classic vertical-middle fade" id="modal-contact-offer" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="#" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<h3>Zadaj pytanie o nieruchomość</h3>
<div class="wpcf7" id="wpcf7-f732-o2">
<div class="screen-reader-response"></div>
<form action="/szukaj/?is_recommended=1#wpcf7-f732-o2" method="post" class="wpcf7-form form-horizontal" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="732" />
<input type="hidden" name="_wpcf7_version" value="3.8.1" />
<input type="hidden" name="_wpcf7_locale" value="pl_PL" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f732-o2" />
<input type="hidden" name="_wpnonce" value="b66e3f140f" />
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfo-offer-number">Nr oferty:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap offer_number"><input type="text" name="offer_number" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcfo-offer-number" readonly="readonly" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfo-name">Imię i nazwisko*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap name"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcfo-name" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfo-email">Email*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" id="pcfo-email" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfo-phone">Telefon*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap phone"><input type="text" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcfo-phone" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfo-message">Wiadomość*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap message"><textarea name="message" cols="5" rows="5" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form-control" id="pcfo-message" aria-required="true" aria-invalid="false"></textarea></span></div>
</div>
<div class="form-group">
<div class="col-sm-6 text-right"><br><input type="hidden" name="_wpcf7_captcha_challenge_captcha-260" value="1914025660" /><img class="wpcf7-form-control wpcf7-captchac wpcf7-captcha-captcha-260" width="84" height="28" alt="captcha" src="http://crystalhouse.pl/wp-content/uploads/wpcf7_captcha/1914025660.png" /></div>
<div class="col-sm-6"><label>przepisz tekst:</label><span class="wpcf7-form-control-wrap captcha-260"><input type="text" name="captcha-260" value="" size="40" class="wpcf7-form-control wpcf7-captchar form-control" aria-invalid="false" /></span></div>
</div>
<div class="form-group text-right">
<div class="col-sm-12"><input type="submit" value="Wyślij" class="wpcf7-form-control wpcf7-submit btn" /></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->		<div class="modal classic vertical-middle fade" id="modal-contact-subject" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="#" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<h3>Wyślij wiadomość</h3>
<div class="wpcf7" id="wpcf7-f738-o3">
<div class="screen-reader-response"></div>
<form action="/szukaj/?is_recommended=1#wpcf7-f738-o3" method="post" class="wpcf7-form form-horizontal" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="738" />
<input type="hidden" name="_wpcf7_version" value="3.8.1" />
<input type="hidden" name="_wpcf7_locale" value="pl_PL" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f738-o3" />
<input type="hidden" name="_wpnonce" value="f032dd70bb" />
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfos-name">Imię i nazwisko*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap name"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcfos-name" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfos-email">Email*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" id="pcfos-email" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfos-subject">Temat*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap subject"><input type="text" name="subject" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcfos-subject" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfos-phone">Telefon*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap phone"><input type="text" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcfos-phone" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfos-message">Wiadomość*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap message"><textarea name="message" cols="5" rows="5" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form-control" id="pcfo-message" aria-required="true" aria-invalid="false"></textarea></span></div>
</div>
<div class="form-group">
<div class="col-sm-6 text-right"><br><input type="hidden" name="_wpcf7_captcha_challenge_captcha-260" value="224054035" /><img class="wpcf7-form-control wpcf7-captchac wpcf7-captcha-captcha-260" width="84" height="28" alt="captcha" src="http://crystalhouse.pl/wp-content/uploads/wpcf7_captcha/224054035.png" /></div>
<div class="col-sm-6"><label>przepisz tekst:</label><span class="wpcf7-form-control-wrap captcha-260"><input type="text" name="captcha-260" value="" size="40" class="wpcf7-form-control wpcf7-captchar form-control" aria-invalid="false" /></span></div>
</div>
<div class="form-group text-right">
<div class="col-sm-12"><input type="submit" value="Wyślij" class="wpcf7-form-control wpcf7-submit btn" /></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->
	<div id="content">
		<div class="container">
						<div class="breadcrumbs hidden-print">
				<ul>
					<li>jesteś tutaj:</li>
					<li><a href="http://crystalhouse.pl/">CrystalHouse</a></li>
											
												<li class="active"><a href="http://crystalhouse.pl/szukaj/">Szukaj</a></li>
									</ul>
			</div>
			<!-- .breadcrumbs -->						<div class="widget navigation-tabs">
				<ul>
											<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&offer=buy">Sprzedaż</a></li>
					
											<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&offer=rent">Wynajem</a></li>
					
					<li><a href="#" data-toggle="modal" data-target="#modal-contact-subject" data-form-subject="Zgłaszam: ">Zgłaszam</a></li>
					<li><a href="#" data-toggle="modal" data-target="#modal-contact-subject" data-form-subject="Poszukuję: ">Poszukuję</a></li>
				</ul>
			</div>
			<!-- .widget.tab-buttons -->						<div class="page-title">
				<h2>Oferty  <strong>(96 ofert)</strong></h2>
			</div>
			<!-- .page-title -->
			<div class="row">
								<div class="col-md-9">
														<div class="row widget offers-list-switches">
						<div class="col-md-5 left">
							<a href="#" class="button-googlemap" data-close-label="Zamknij mapę nieruchomości" data-open-gmap="#content .google-map">Pokaż mapę nieruchomości</a>						</div>
						<!-- .col-md-5 -->

						<div class="col-md-7 right">
							<ul class="inline-switch">
															<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&show_currency=PLN" class="active">PLN</a></li>
															<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&show_currency=USD">USD</a></li>
															<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&show_currency=EUR">EUR</a></li>
														</ul>

							<select name="order"><option value="date_desc">Najnowsze</option><option value="date_asc">Najstarsze</option><option value="price_desc">Cena najwyższa</option><option value="price_asc">Cena najniższa</option></select>						</div>
						<!-- .col-md-7 -->
					</div>
					<!-- .widget.offers-list-switches -->															<section class="widget google-map offers-list-map closed" style="display: none;">
						<div class="map"></div>
					</section>
					<!-- .widget.google-map -->

					<script>
						(function($) {
							$(window).load(function() {
								$('.widget.offers-list-map').offersListMap({
									map: {
										options: {
											zoom: 10,
											center: new google.maps.LatLng(52.2329379,21.0011941), // Warszawa
											disableDefaultUI: false,
											scrollwheel: false
										}
									},
									markers: [
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/agencja-nieruchomosci-warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/06/46912352-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 57m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 700 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/agencja-nieruchomosci-warszawa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2247629, 21.0313199],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/komfortowy-50m2-apartament-ul-gornoslaska-16-srodmiescie-przy-sejmie-rp/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/81-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Górnośląska 16</p><p><strong>Powierzchnia:</strong> 50.3m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>850 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/komfortowy-50m2-apartament-ul-gornoslaska-16-srodmiescie-przy-sejmie-rp/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2355285, 21.0637005],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-saska-kepa/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/03/44394437-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Londyńska</p><p><strong>Powierzchnia:</strong> 40m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>699 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-saska-kepa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2058972, 20.9960626],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-wynajem-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/06/46561274-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Biały Kamień</p><p><strong>Powierzchnia:</strong> 93m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>8 000 PLN <small>/ msc</small></strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-wynajem-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2010249, 21.0273894],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/06/2-3-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ludowa</p><p><strong>Powierzchnia:</strong> 120m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2308079, 21.0593249],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-sprzedaz-saska-kepa/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/04/45986411-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Meksykańska</p><p><strong>Powierzchnia:</strong> 82.5m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 200 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-sprzedaz-saska-kepa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [54.6834175, 18.714682],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-jurata-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/10/slides.01_pokazgk-is-89-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 47m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 230 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-jurata-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1851099, 21.0266881],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-do-wynajecia-warszawa-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/06/46619725-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Czerniowiecka</p><p><strong>Powierzchnia:</strong> 380m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>45 000 PLN <small>/ msc</small></strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-do-wynajecia-warszawa-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45379776-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 58m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2308079, 21.0593249],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-saska-kepa-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/04/35909040-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Meksykańska</p><p><strong>Powierzchnia:</strong> 83m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>5 000 PLN <small>/ msc</small></strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-saska-kepa-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2577856, 20.9945166],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-srodmiescie/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/49-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Zygmunta Słomińskiego</p><p><strong>Powierzchnia:</strong> 142m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 690 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-srodmiescie/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1694204, 21.0794764],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/46209667-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Urodzajna</p><p><strong>Powierzchnia:</strong> 180m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 990 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2298015, 21.038103],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartamenty-solec-residence/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45823842w1-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Solec</p><p><strong>Powierzchnia:</strong> 66.48m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>947 340 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartamenty-solec-residence/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2010249, 21.0273894],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-sprzedaz-warszawa-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45820782-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ludowa</p><p><strong>Powierzchnia:</strong> 121m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-sprzedaz-warszawa-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1889807, 21.006445],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-sprzedaz-warszawa-mokotow-woronicza/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45513932-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Woronicza</p><p><strong>Powierzchnia:</strong> 60.31m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>670 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-sprzedaz-warszawa-mokotow-woronicza/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.193104, 20.9871044],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-marina-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/02/43253956-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Sztormowa</p><p><strong>Powierzchnia:</strong> 160m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 849 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-marina-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2197158, 20.9809803],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-na-sprzedaz-warszawa-ochota/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/03/43927693-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Kaliska</p><p><strong>Powierzchnia:</strong> 207m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 990 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-na-sprzedaz-warszawa-ochota/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartamenty-cosmopolitan-twarda-4-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/09/14-09_JAHN-cosmo_28-min-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p> <a href="http://crystalhouse.pl/apartment/apartamenty-cosmopolitan-twarda-4-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1857926, 21.0439166],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-mokotow-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/120-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Pory</p><p><strong>Powierzchnia:</strong> 220m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-mokotow-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2360987, 21.0215288],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-kamienica-ordynacka/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/11/41130144-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Okólnik</p><p><strong>Powierzchnia:</strong> 93m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 600 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-kamienica-ordynacka/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.263893, 20.9870859],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-warszawa-zoliborz-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/IMG_7181w-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Alojzego Felińskiego</p><p><strong>Powierzchnia:</strong> 161m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 238 380 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-warszawa-zoliborz-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2240994, 21.0757657],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-saska-kepa/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/20395459-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Libijska</p><p><strong>Powierzchnia:</strong> 151.6m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>8 500 PLN <small>/ msc</small></strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-saska-kepa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [51.7592485, 19.4559833],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-z-najemca-lodz-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/01/42729752-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 255m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 350 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-z-najemca-lodz-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1760247, 20.9954915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-na-sprzedaz-warszawa-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/02/214-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Cybernetyki</p><p><strong>Powierzchnia:</strong> 141m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 200 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-na-sprzedaz-warszawa-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartamenty-cosmopolitan-twarda-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/09/235uj2-min-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 136m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartamenty-cosmopolitan-twarda-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1380948, 21.0069918],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-na-sprzedaz-ursynow/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/01/33646965-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Gawota</p><p><strong>Powierzchnia:</strong> 146m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 890 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-na-sprzedaz-ursynow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2165128, 21.0414737],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-srodmiescie-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/12/41932658-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Szwoleżerów</p><p><strong>Powierzchnia:</strong> 112m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 280 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-srodmiescie-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.228361, 21.0124691],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/luksusowe-biuro-na-wynajem-warszawa-centrum/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/10/91-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Marszałkowska</p><p><strong>Powierzchnia:</strong> 520m<sup>2</sup></p> <a href="http://crystalhouse.pl/business/luksusowe-biuro-na-wynajem-warszawa-centrum/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1688552, 21.1070882],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/10/40529900-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Syta</p><p><strong>Powierzchnia:</strong> 249m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 750 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1764506, 20.9935391],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/10/40168257-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Cybernetyki</p><p><strong>Powierzchnia:</strong> 53.3m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>450 385 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2301039, 21.1089661],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-saska-kepa-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/09/39947689-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Jana Nowaka-Jeziorańskiego</p><p><strong>Powierzchnia:</strong> 80.41m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 100 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-saska-kepa-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2009348, 21.0389299],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-rezydencja-maltanska/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/39801024-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Jazgarzewska</p><p><strong>Powierzchnia:</strong> 143.72m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 050 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-rezydencja-maltanska/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartamenty-cosmopolitan-twarda-4-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/09/14-09_JAHN-cosmo_28-min-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p> <a href="http://crystalhouse.pl/apartment/apartamenty-cosmopolitan-twarda-4-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2010249, 21.0273894],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-ludowa-6/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/07/36w-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ludowa</p><p><strong>Powierzchnia:</strong> 289m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>7 800 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-ludowa-6/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-warszawa-mokotow-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/01/1-1w-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 324m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 200 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-warszawa-mokotow-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2228513, 21.007506],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-koszykowa/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 65m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>893 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-koszykowa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 57m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>750 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2834463, 20.9519544],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-bielany/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/09/29159679-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Grębałowska</p><p><strong>Powierzchnia:</strong> 190m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 050 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-bielany/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [42.6951525, 27.7104213],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartamenty-bulgaria-okazja-cenowa/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/03/34893559-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 69m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>159 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartamenty-bulgaria-okazja-cenowa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2261486, 21.013761],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-hoza/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/12/hoza50-32-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Hoża</p><p><strong>Powierzchnia:</strong> 136m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 506 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-hoza/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-w-kamienicy-ul-noakowskiego/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 56m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>729 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-w-kamienicy-ul-noakowskiego/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.191338, 21.0205633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-rezydencja-mokotow-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36697597-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Adama Naruszewicza</p><p><strong>Powierzchnia:</strong> 250m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 750 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-rezydencja-mokotow-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2450664, 20.9773098],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-wola-okopowa/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/12/MG_0019-HDR3_Default-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Okopowa</p><p><strong>Powierzchnia:</strong> 78m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>899 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-wola-okopowa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.167442, 21.0120413],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-na-sprzedaz-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18800844-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> al. Wyścigowa</p><p><strong>Powierzchnia:</strong> 208m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 490 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-na-sprzedaz-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2010249, 21.0273894],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/nowoczesny-apartament-warszawa-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/10/29958648-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ludowa</p><p><strong>Powierzchnia:</strong> 125m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 150 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/nowoczesny-apartament-warszawa-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2306557, 20.975674],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/mieszkanie-do-wynajecia-warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/27146975-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Giełdowa</p><p><strong>Powierzchnia:</strong> 54m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 700 PLN <small>/ msc</small></strong></strong></p> <a href="http://crystalhouse.pl/apartment/mieszkanie-do-wynajecia-warszawa/" class="more">&raquo;</a></div>'
											}
										},
																			]
								});
							});
						})(jQuery);
					</script>
															<section class="widget offers-list">
																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/apartment/agencja-nieruchomosci-warszawa/">
																										<em class="label"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/no-fee-pl.png" alt=""></em>
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2016/06/46912352-370x215.jpg" width="370" alt="">
																									</a>
											</span>
											<figcaption><span>Nowa oferta | Polecana</span></figcaption>										</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/apartment/agencja-nieruchomosci-warszawa/">Cosmopolitan apartamenty sprzedaż</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																							<li><strong>Ulica:</strong> Twarda</li>
																							<li><strong>Powierzchnia:</strong> 57 m<sup>2</sup></li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Dzielnica:</strong> Śródmieście</li>
																							<li><strong>Ilość pokoi:</strong> 2</li>
																							<li><strong>Rok budowy:</strong> 2015</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>0% PROWIZJI OD KUPUJĄCEGO Pomagamy i oferujemy wiedzę na każdym etapie planowanej inwestycji ! Cosmopolitan, ul. Twarda 4, Śródmieście – jedynie tak...</p>
																															<p class="flags">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/parking.png" alt="Parking" title="Parking" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/reception.png" alt="Portier" title="Portier" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/aircondition.png" alt="Klimatyzacja" title="Klimatyzacja" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/internet.png" alt="Internet" title="Internet" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/elevator.png" alt="Winda" title="Winda" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/security.png" alt="Ochrona" title="Ochrona" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/foreign-schools.png" alt="Szkoły zagraniczne w pobliżu" title="Szkoły zagraniczne w pobliżu" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/shopcentre.png" alt="Centra handlowe w pobliżu" title="Centra handlowe w pobliżu" data-tooltip="1" data-placement="bottom">
																					</p>
																				</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 2368/3389/OMS 
											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				1 700 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="2368/3389/OMS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="29393" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/apartment/agencja-nieruchomosci-warszawa/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/apartment/komfortowy-50m2-apartament-ul-gornoslaska-16-srodmiescie-przy-sejmie-rp/">
																										<em class="label"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/no-fee-pl.png" alt=""></em>
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2016/05/81-370x215.jpg" width="370" alt="">
																									</a>
											</span>
											<figcaption><span>Nowa oferta | Polecana</span></figcaption>										</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/apartment/komfortowy-50m2-apartament-ul-gornoslaska-16-srodmiescie-przy-sejmie-rp/">Komfortowy 50m2 Apartament ul. Górnośląska 16 Śródmieście przy Sejmie RP</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																							<li><strong>Ulica:</strong> Górnośląska 16</li>
																							<li><strong>Powierzchnia:</strong> 50.3 m<sup>2</sup></li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Dzielnica:</strong> Śródmieście</li>
																							<li><strong>Ilość pokoi:</strong> 2</li>
																							<li><strong>Ilość sypialni:</strong> 1</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>Mieszkanie sprzedaż Warszawa Śródmieście CRYSTAL HOUSE S.A. LUKSUSOWE NIERUCHOMOŚCI ŚRÓDMIEŚCIE – GÓRNOŚLĄSKA 16 Prezentujemy 50 m2 apartament 2 pokojowy na sprzedaż w...</p>
																															<p class="flags">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/parking.png" alt="Parking" title="Parking" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/foreign-schools.png" alt="Szkoły zagraniczne w pobliżu" title="Szkoły zagraniczne w pobliżu" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/shopcentre.png" alt="Centra handlowe w pobliżu" title="Centra handlowe w pobliżu" data-tooltip="1" data-placement="bottom">
																					</p>
																				</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 2307/3389/OMS 
											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				850 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="2307/3389/OMS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="28354" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/apartment/komfortowy-50m2-apartament-ul-gornoslaska-16-srodmiescie-przy-sejmie-rp/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/house/luksusowa-rezydencja-warszawa-radosc/">
																										<em class="label"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/no-fee-pl.png" alt=""></em>
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2015/07/9V2A0531-370x215.jpg" width="370" alt="">
																									</a>
											</span>
											<figcaption><span>Nowa oferta | Polecana</span></figcaption>										</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/house/luksusowa-rezydencja-warszawa-radosc/">Luksusowa rezydencja Warszawa Radość</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																							<li><strong>Powierzchnia:</strong> 462 m<sup>2</sup></li>
																							<li><strong>Powierzchnia działki:</strong> 2785 m<sup>2</sup></li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Dzielnica:</strong> Wawer</li>
																							<li><strong>Ilość pokoi:</strong> 9</li>
																							<li><strong>Rok budowy:</strong> 2014</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>Luksusowa rezydencja Warszawa Radość OBNIŻKA CENY !!!! JEDYNY TAKI DOM W OKOLICY !!! Agencja nieruchomości Crystal House prezentuje luksusową rezydencję na sprzedaż...</p>
																															<p class="flags">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/parking.png" alt="Parking" title="Parking" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/balcony.png" alt="Balkon" title="Balkon" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/utility-room.png" alt="Schowek" title="Schowek" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/security.png" alt="Ochrona" title="Ochrona" data-tooltip="1" data-placement="bottom">
																					</p>
																				</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 1012/3389/ODS											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				3 850 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="1012/3389/ODS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="21297" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/house/luksusowa-rezydencja-warszawa-radosc/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-saska-kepa/">
																										<em class="label"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/no-fee-pl.png" alt=""></em>
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2016/03/44394437-370x215.jpg" width="370" alt="">
																									</a>
											</span>
											<figcaption><span>Nowa oferta | Polecana</span></figcaption>										</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-saska-kepa/">Apartament na sprzedaż Saska Kępa</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																							<li><strong>Ulica:</strong> Londyńska</li>
																							<li><strong>Powierzchnia:</strong> 40 m<sup>2</sup></li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Dzielnica:</strong> Praga-Południe</li>
																							<li><strong>Ilość pokoi:</strong> 2</li>
																							<li><strong>Ilość sypialni:</strong> 1</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>0% prowizji od kupującego ! Apartament na sprzedaż Saska Kępa ul. Londyńska Crystal House prezentuje luksusowy apartament na sprzedaż w inwestycji Londyńska...</p>
																															<p class="flags">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/parking.png" alt="Parking" title="Parking" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/balcony.png" alt="Balkon" title="Balkon" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/elevator.png" alt="Winda" title="Winda" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/foreign-schools.png" alt="Szkoły zagraniczne w pobliżu" title="Szkoły zagraniczne w pobliżu" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/shopcentre.png" alt="Centra handlowe w pobliżu" title="Centra handlowe w pobliżu" data-tooltip="1" data-placement="bottom">
																					</p>
																				</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 2303/3389/OMS 
											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				699 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="2303/3389/OMS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="27551" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-saska-kepa/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/house/zespol-palacowo-parkowy-w-kwitajnach/">
																										<em class="label"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/no-fee-pl.png" alt=""></em>
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2016/05/86-370x215.jpg" width="370" alt="">
																									</a>
											</span>
											<figcaption><span>Nowa oferta | Polecana</span></figcaption>										</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/house/zespol-palacowo-parkowy-w-kwitajnach/">Zespół Pałacowo &#8211; Parkowy w Kwitajnach</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Kwitajny</li>
																							<li><strong>Powierzchnia:</strong> 1670 m<sup>2</sup></li>
																							<li><strong>Powierzchnia działki:</strong> 175000 m<sup>2</sup></li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>Pałac na sprzedaż na Mazurach CRYSTAL HOUSE S.A. LUKSUSOWE NIERUCHOMOŚCI WARMIA &#8211; MAZURY Prezentujemy na sprzedaż UNIKATOWY zespół PAŁACOWO-PARKOWY w KWITAJNACH PRZEDMIOT...</p>
																															</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				3 300 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="28567" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/house/zespol-palacowo-parkowy-w-kwitajnach/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/house/domy-na-sprzedaz-warszawa-anin/">
																										<em class="label"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/no-fee-pl.png" alt=""></em>
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2016/05/122-370x215.jpg" width="370" alt="">
																									</a>
											</span>
											<figcaption><span>Nowa oferta | Polecana</span></figcaption>										</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/house/domy-na-sprzedaz-warszawa-anin/">Przestronny dom w Aninie wysoka jakość materiałów</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																							<li><strong>Powierzchnia:</strong> 560 m<sup>2</sup></li>
																							<li><strong>Powierzchnia działki:</strong> 1079 m<sup>2</sup></li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Dzielnica:</strong> Wawer</li>
																							<li><strong>Ilość pokoi:</strong> 8</li>
																							<li><strong>Ilość sypialni:</strong> 4</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>CRYSTAL HOUSE S.A. LUKSUSOWE NIERUCHOMOŚCI STARY ANIN Prezentujemy na sprzedaż rodzinny dom o wysokim standardzie wykończenia w wyjątkowej lokalizacji Lokalizacja: ========== Nieruchomość...</p>
																															<p class="flags">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/parking.png" alt="Parking" title="Parking" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/balcony.png" alt="Balkon" title="Balkon" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/utility-room.png" alt="Schowek" title="Schowek" data-tooltip="1" data-placement="bottom">
																					</p>
																				</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 1087/3389/ODS											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				3 500 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="1087/3389/ODS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="28387" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/house/domy-na-sprzedaz-warszawa-anin/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/apartment/apartament-na-wynajem-mokotow/">
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2016/06/46561274-370x215.jpg" width="370" alt="">
																									</a>
											</span>
											<figcaption><span>Nowa oferta | Polecana</span></figcaption>										</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/apartment/apartament-na-wynajem-mokotow/">Apartament na wynajem Mokotów Biały Kamień</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																							<li><strong>Ulica:</strong> Biały Kamień</li>
																							<li><strong>Powierzchnia:</strong> 93 m<sup>2</sup></li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Dzielnica:</strong> Mokotów</li>
																							<li><strong>Ilość pokoi:</strong> 3</li>
																							<li><strong>Rok budowy:</strong> 2005</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>Apartament na wynajem Mokotów Agencja nieruchomości Crystal House prezentuje niepowtarzalny apartament na wynajem o powierzchni 93 m2 z dwoma przynależnymi ogródkami zlokalizowany...</p>
																															<p class="flags">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/parking.png" alt="Parking" title="Parking" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/balcony.png" alt="Balkon" title="Balkon" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/elevator.png" alt="Winda" title="Winda" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/foreign-schools.png" alt="Szkoły zagraniczne w pobliżu" title="Szkoły zagraniczne w pobliżu" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/shopcentre.png" alt="Centra handlowe w pobliżu" title="Centra handlowe w pobliżu" data-tooltip="1" data-placement="bottom">
																					</p>
																				</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 2368/3389/OMW 
											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				8 000 <small>PLN</small><small> / MIESIĄC</small>
																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="2368/3389/OMW" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="29146" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/apartment/apartament-na-wynajem-mokotow/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/house/willa-na-sprzedaz-podkowa-lesna/">
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2014/03/23618224-370x215.jpg" width="370" alt="">
																									</a>
											</span>
											<figcaption><span>Polecana</span></figcaption>										</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/house/willa-na-sprzedaz-podkowa-lesna/">Podkowa Leśna Klimat II RP</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Podkowa Leśna</li>
																							<li><strong>Powierzchnia:</strong> 512 m<sup>2</sup></li>
																							<li><strong>Powierzchnia działki:</strong> 2667 m<sup>2</sup></li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Ilość pokoi:</strong> 9</li>
																							<li><strong>Ilość sypialni:</strong> 6</li>
																							<li><strong>Rok budowy:</strong> 1931</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>Willa na sprzedaż Podkowa Leśna CRYSTAL HOUSE S.A. LUKSUSOWE NIERUCHOMOŚCI PODKOWA LEŚNA Prezentujemy na sprzedaż UNIKATOWĄ WILLĘ z Lat 30 XXw. DOM...</p>
																															<p class="flags">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/parking.png" alt="Parking" title="Parking" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/balcony.png" alt="Balkon" title="Balkon" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/utility-room.png" alt="Schowek" title="Schowek" data-tooltip="1" data-placement="bottom">
																					</p>
																				</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 932/3389/ODS											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				4 000 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="932/3389/ODS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="16979" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/house/willa-na-sprzedaz-podkowa-lesna/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-mokotow/">
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2016/06/2-3-370x215.jpg" width="370" alt="">
																									</a>
											</span>
											<figcaption><span>Nowa oferta | Polecana</span></figcaption>										</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-mokotow/">Apartament na sprzedaż Mokotów</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																							<li><strong>Ulica:</strong> Ludowa</li>
																							<li><strong>Powierzchnia:</strong> 120 m<sup>2</sup></li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Dzielnica:</strong> Mokotów</li>
																							<li><strong>Ilość pokoi:</strong> 3</li>
																							<li><strong>Rok budowy:</strong> 2005</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>Apartament na sprzedaż Mokotów CRYSTAL HOUSE / WWW.CRYSTALHOUSE.PL Crystal House S.A. poleca: Apartament w jednym z najwyżej cenionych apartamentowców w Warszawie. LOKALIZACJA:...</p>
																															<p class="flags">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/parking.png" alt="Parking" title="Parking" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/balcony.png" alt="Balkon" title="Balkon" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/internet.png" alt="Internet" title="Internet" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/elevator.png" alt="Winda" title="Winda" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/security.png" alt="Ochrona" title="Ochrona" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/foreign-schools.png" alt="Szkoły zagraniczne w pobliżu" title="Szkoły zagraniczne w pobliżu" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/shopcentre.png" alt="Centra handlowe w pobliżu" title="Centra handlowe w pobliżu" data-tooltip="1" data-placement="bottom">
																					</p>
																				</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 2366/3389/OMS 
											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				1 900 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="2366/3389/OMS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="29331" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-mokotow/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/apartment/apartament-sprzedaz-saska-kepa/">
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2015/04/45986411-370x215.jpg" width="370" alt="">
																									</a>
											</span>
											<figcaption><span>Nowa oferta | Polecana</span></figcaption>										</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/apartment/apartament-sprzedaz-saska-kepa/">Apartament na sprzedaż Saska Kępa !!!</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																							<li><strong>Ulica:</strong> Meksykańska</li>
																							<li><strong>Powierzchnia:</strong> 82.5 m<sup>2</sup></li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Dzielnica:</strong> Praga-Południe</li>
																							<li><strong>Ilość pokoi:</strong> 2</li>
																							<li><strong>Rok budowy:</strong> 2008</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>Apartament sprzedaż Saska Kępa ul. Meksykańska UNIKATOWY APARTAMENT NA SASKIEJ KĘPIE!!! LOKALIZACJA: Serce Saskiej Kępy, ul. Meksykańska 8. Okolica ciesząca się ogromnym...</p>
																															<p class="flags">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/parking.png" alt="Parking" title="Parking" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/balcony.png" alt="Balkon" title="Balkon" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/aircondition.png" alt="Klimatyzacja" title="Klimatyzacja" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/elevator.png" alt="Winda" title="Winda" data-tooltip="1" data-placement="bottom">
																						<img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/security.png" alt="Ochrona" title="Ochrona" data-tooltip="1" data-placement="bottom">
																					</p>
																				</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 2346/3389/OMS 
											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				1 200 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="2346/3389/OMS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="28754" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/apartment/apartament-sprzedaz-saska-kepa/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->											</section>
					<!-- .widget.offers-list -->

										<div class="pagination">
						<ul>
																						<li class="disabled"><a href="javascript:void(0);">&laquo;</a></li>
							
															<li class="disabled"><a href="javascript:void(0);">&lsaquo;</a></li>
							
														<li class="active"><a href="javascript:void(0);">1</a></li>
														<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&page=2">2</a></li>
														<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&page=3">3</a></li>
														<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&page=4">4</a></li>
														<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&page=5">5</a></li>
														<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&page=6">6</a></li>
														<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&page=7">7</a></li>
														<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&page=8">8</a></li>
														<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&page=9">9</a></li>
														<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&page=10">10</a></li>
							
															<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&page=2">&rsaquo;</a></li>
							
															<li><a href="http://crystalhouse.pl/szukaj/?is_recommended=1&page=10">&raquo;</a></li>
													</ul>
					</div>
									
								</div>
				<!-- .col-md-9 -->
				<div class="col-md-3">
					<div class="widget offers-list-filter">
						<h2>Znajdź nieruchomość</h2>
						<form method="get" action="http://crystalhouse.pl/szukaj/?" name="offers_filter" data-form-values='{"country": "Polska", "offer": "buy"}'>
														<div class="form-group">
								<input type="text" name="q"placeholder="słowo kluczowe">							</div>
							<!-- .form-group -->																												<div class="form-group" data-form-fieldset="offer">
								<label>Chcę</label>
								<div class="row inline-radios">
									<div class="col-xs-6" data-form-fieldset="offer_buy">
										<label><input type="radio" name="offer" value="buy"> kupić</label>
									</div>
									<!-- .col-xs-6 -->

									<div class="col-xs-6" data-form-fieldset="offer_rent">
										<label><input type="radio" name="offer" value="rent"> wynająć</label>
									</div>
									<!-- .col-xs-6 -->
								</div>
								<!-- .inline-checkboxes -->
							</div>
							<!-- .form-group -->														<div class="form-group" data-form-fieldset="currency">
								<div class="row">
									<div class="col-md-4"><label style="margin-top: 2px;">Waluta</label></div>
									<div class="col-md-8">
										<div class="inline-checkboxes currencies">
																					<label><input type="radio" name="currency" value="PLN" checked="checked"> PLN</label>
																					<label><input type="radio" name="currency" value="USD"> USD</label>
																					<label><input type="radio" name="currency" value="EUR"> EUR</label>
																				</div>
										<!-- .inline-checkboxes -->
									</div>
								</div>
								<!-- .row -->
							</div>
							<!-- .form-group -->							
							<div data-group="offer_type_all">
																<div class="form-group" data-form-fieldset="rent_type" style="display: none;">
									<label>Rodzaj wynajmu</label>
									<select name="rent_type" disabled="disabled"><option value="12">Wynajem długoterminowy</option><option value="10">Wynajem krótkoterminowy</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="type">
									<label>Typ nieruchomości</label>
									<select name="type"><option value="" selected="selected">Wszystkie</option><option value="apartment">Apartamenty</option><option value="house">Domy</option><option value="lot">Działki</option><option value="business">Oferty komercyjne</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="business_type" style="display: none;">
									<label>Typ nieruchomości</label>
									<select name="business_type"><option value="" selected="selected">Wszystkie</option><option value="office">Biuro</option><option value="premise">Lokal usługowy</option><option value="commercial_object">Obiekt handlowy</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="premise_type" style="display: none;">
									<label>Przeznaczenie</label>
									<select name="business_premise_type" disabled="disabled"><option value="" selected="selected">Wszystkie</option><option value="commerce">Handlowy/usługowy</option><option value="gastronomy">Gastronomia</option><option value="production">Produkcja</option><option value="other">Inny</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="market_type">
									<label>Rodzaj rynku</label>
									<div class="row inline-checkboxes">
																			<div class="col-xs-6" data-form-fieldset="market_type_secondary">
											<label><input type="radio" name="market_type[]" value="secondary"> wtórny</label>
										</div>
										<!-- .col-xs-6 -->
																			<div class="col-xs-6" data-form-fieldset="market_type_primary">
											<label><input type="radio" name="market_type[]" value="primary"> pierwotny</label>
										</div>
										<!-- .col-xs-6 -->
																		</div>
									<!-- .inline-radios -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="lot_type" style="display: none;">
									<label>Typ działki</label>
									<select name="lot_type" disabled="disabled"><option value="" selected="selected">Wszystkie</option><option value="construction">Budowlana</option><option value="recreational">Rekreacyjna</option><option value="agricultural">Rolna</option><option value="investment">Inwestycyjna</option><option value="industrial">Przemysłowa</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="area">
									<label><span data-type-label="default">Powierzchnia</span> <span data-type-label="house" style="display: none;">Powierzchnia domu</span></label>
									<div class="row">
										<div class="col-xs-6">
											<select name="area_from" style="text-transform: none;"><option value="">OD</option><option value="">0</option><option value="50">50m&#178;</option><option value="75">75m&#178;</option><option value="100">100m&#178;</option><option value="150">150m&#178;</option><option value="200">200m&#178;</option><option value="250">250m&#178;</option></select>										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<select name="area_to" style="text-transform: none;"><option value="">DO</option><option value="50">50m&#178;</option><option value="75">75m&#178;</option><option value="100">100m&#178;</option><option value="150">150m&#178;</option><option value="200">200m&#178;</option><option value="250">250m&#178;</option><option value="400">400m&#178;</option><option value="_">400m&#178;+</option></select>										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="area_lot" style="display: none;">
									<label>Powierzchnia działki</label>
									<div class="row">
										<div class="col-xs-6">
											<select name="area_lot_from" disabled="disabled" style="text-transform: none;"><option value="">OD</option><option value="">0</option><option value="500">500m&#178;</option><option value="1000">1.000m&#178;</option><option value="2000">2.000m&#178;</option><option value="3000">3.000m&#178;</option><option value="4000">4.000m&#178;</option><option value="5000">5.000m&#178;</option></select>										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<select name="area_lot_to" disabled="disabled" style="text-transform: none;"><option value="">DO</option><option value="500">500m&#178;</option><option value="1000">1.000m&#178;</option><option value="2000">2.000m&#178;</option><option value="3000">3.000m&#178;</option><option value="4000">4.000m&#178;</option><option value="5000">5.000m&#178;</option><option value="10000">10.000m&#178;</option><option value="20000">20.000m&#178;</option><option value="_">20.000m&#178;+</option></select>										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="price">
									<label>Cena</label>
									<div class="row">
										<div class="col-xs-6">
											<select name="price_from"><option value="">Od</option><option value="">0 PLN</option><option value="500000">500.000 PLN</option><option value="1000000">1.000.000 PLN</option><option value="2000000">2.000.000 PLN</option><option value="3000000">3.000.000 PLN</option><option value="4000000">4.000.000 PLN</option><option value="5000000">5.000.000 PLN</option></select>										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<select name="price_to"><option value="">Do</option><option value="1000000">1.000.000 PLN</option><option value="2000000">2.000.000 PLN</option><option value="3000000">3.000.000 PLN</option><option value="4000000">4.000.000 PLN</option><option value="5000000">5.000.000 PLN</option><option value="_">5.000.000+ PLN</option></select>										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="price_m2">
									<label>Cena/m2</label>
									<div class="row">
										<div class="col-xs-6">
											<select name="price_m2_from"><option value="">Od</option><option value="">0 PLN</option><option value="1000">1.000 PLN</option><option value="2000">2.000 PLN</option><option value="3000">3.000 PLN</option><option value="4000">4.000 PLN</option><option value="5000">5.000 PLN</option></select>										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<select name="price_m2_to"><option value="">Do</option><option value="1000">1.000 PLN</option><option value="2000">2.000 PLN</option><option value="3000">3.000 PLN</option><option value="4000">4.000 PLN</option><option value="5000">5.000 PLN</option><option value="_">5.000+ PLN</option></select>										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="province" style="display: none;">
									<label>Województwo</label>
									<select name="province" disabled="disabled"><option value="" selected="selected">Wszystkie</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="city">
									<label>Miasto</label>
									<select name="city"><option value="">Wszystkie</option><option value=" Le Castellet"> Le Castellet</option><option value="BENIDORM">BENIDORM</option><option value="Bielawa">Bielawa</option><option value="Cannes">Cannes</option><option value="Chojnów">Chojnów</option><option value="Chyliczki">Chyliczki</option><option value="Costa Blanca">Costa Blanca</option><option value="Costa Del Sol">Costa Del Sol</option><option value="Costa Del Sol, Marbella">Costa Del Sol, Marbella</option><option value="Cote d'Azur">Cote d'Azur</option><option value="Dołhobyczów">Dołhobyczów</option><option value="Dziekanów Polski">Dziekanów Polski</option><option value="Gdańsk">Gdańsk</option><option value="Gdynia">Gdynia</option><option value="Habdzin">Habdzin</option><option value="Hel">Hel</option><option value="Hornówek">Hornówek</option><option value="Izabelin">Izabelin</option><option value="Jabłonna">Jabłonna</option><option value="Jastarnia">Jastarnia</option><option value="Jastrzębie">Jastrzębie</option><option value="Józefosław">Józefosław</option><option value="Józefów">Józefów</option><option value="Jurata">Jurata</option><option value="Kampinos">Kampinos</option><option value="Kazimierz Dolny">Kazimierz Dolny</option><option value="Kępa Oborska">Kępa Oborska</option><option value="Kobyłka">Kobyłka</option><option value="Koczargi Stare">Koczargi Stare</option><option value="Kołobrzeg">Kołobrzeg</option><option value="Komorów">Komorów</option><option value="Konstancin-Jeziorna">Konstancin-Jeziorna</option><option value="Kościelisko">Kościelisko</option><option value="Kotorydz">Kotorydz</option><option value="Kraków">Kraków</option><option value="Kruklanki">Kruklanki</option><option value="Książnik">Książnik</option><option value="Kwitajny">Kwitajny</option><option value="Lanckorona">Lanckorona</option><option value="Lasocin">Lasocin</option><option value="Le Castellet">Le Castellet</option><option value="Lipków">Lipków</option><option value="Lisewo">Lisewo</option><option value="Lwówek Śląski">Lwówek Śląski</option><option value="Łomianki">Łomianki</option><option value="Łódź">Łódź</option><option value="Magdalenka">Magdalenka</option><option value="Marbella">Marbella</option><option value="Marki">Marki</option><option value="Michałowice">Michałowice</option><option value="Nadarzyn">Nadarzyn</option><option value="Natolin">Natolin</option><option value="Nicea">Nicea</option><option value="Obory">Obory</option><option value="Otwock">Otwock</option><option value="Pęcice Małe">Pęcice Małe</option><option value="Piaseczno">Piaseczno</option><option value="Podkowa Leśna">Podkowa Leśna</option><option value="Pruszków">Pruszków</option><option value="Raciborsko">Raciborsko</option><option value="Radość">Radość</option><option value="Radzymin">Radzymin</option><option value="Reguły">Reguły</option><option value="Rusiec">Rusiec</option><option value="Serock">Serock</option><option value="Sękocin Nowy">Sękocin Nowy</option><option value="Sękocin-Las">Sękocin-Las</option><option value="Skop">Skop</option><option value="Słoneczny Brzeg">Słoneczny Brzeg</option><option value="Słoneczny Brzeg Sunny Beach">Słoneczny Brzeg Sunny Beach</option><option value="Stara Wieś">Stara Wieś</option><option value="Stare Babice">Stare Babice</option><option value="Strzembowo">Strzembowo</option><option value="Toruń">Toruń</option><option value="Unieście">Unieście</option><option value="Walendów">Walendów</option><option value="Warszawa">Warszawa</option><option value="Wejherowo">Wejherowo</option><option value="Wiązowna">Wiązowna</option><option value="Władysławowo">Władysławowo</option><option value="Zakopane">Zakopane</option><option value="Zawada">Zawada</option><option value="Ząbki">Ząbki</option><option value="Zgorzelec">Zgorzelec</option><option value="Zielonka">Zielonka</option><option value="Żółwin">Żółwin</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="district">
									<label>Dzielnica</label>
									<select name="district"><option value="" selected="selected">Wszystkie</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="developer">
									<label>Developer</label>
									<select name="developer"><option value="" selected="selected">Wszystkie</option><option value="Bavaria Development">Bavaria Development</option><option value="BBI Development NFI">BBI Development NFI</option><option value="Catalina Group">Catalina Group</option><option value="Delpha Investments">Delpha Investments</option><option value="Dom Development">Dom Development</option><option value="Dom Development S.A.">Dom Development S.A.</option><option value="Dor Group">Dor Group</option><option value="Echo Investments">Echo Investments</option><option value="Fenix Group">Fenix Group</option><option value="Filadelfia Sp. z.o.o.">Filadelfia Sp. z.o.o.</option><option value="Grupa Inwestycyjna BEL">Grupa Inwestycyjna BEL</option><option value="Juvenes">Juvenes</option><option value="LD Sp. z.o.o.">LD Sp. z.o.o.</option><option value="Marvipol S.A">Marvipol S.A</option><option value="Myoni">Myoni</option><option value="Orco">Orco</option><option value="Parkowa Łazienki Sp. z.o.o">Parkowa Łazienki Sp. z.o.o</option><option value="Parkowa Łazienki Sp. z.o.o.">Parkowa Łazienki Sp. z.o.o.</option><option value="Polnord">Polnord</option><option value="Shiraz Sp. z o.o.">Shiraz Sp. z o.o.</option><option value="SM Pax">SM Pax</option><option value="SM Vistula">SM Vistula</option><option value="Tacit Investment">Tacit Investment</option><option value="Wan S.A.">Wan S.A.</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="investment">
									<label>Inwestycja</label>
									<select name="investment"><option value="" selected="selected">Wszystkie</option><option value=" OGRODY SHIRAZ"> OGRODY SHIRAZ</option><option value=" VILLA MONACO"> VILLA MONACO</option><option value="Belvedere Residence">Belvedere Residence</option><option value="Cosmopolitan">Cosmopolitan</option><option value="Cosmopolitan Twarda 4">Cosmopolitan Twarda 4</option><option value="Górnośląska 7A">Górnośląska 7A</option><option value="Hill Park Apartments">Hill Park Apartments</option><option value="Hoża 50">Hoża 50</option><option value="KAZIMIERZOWSKA RESIDENCE">KAZIMIERZOWSKA RESIDENCE</option><option value="Koszykowa 49A">Koszykowa 49A</option><option value="Łowicka Residence">Łowicka Residence</option><option value="Madalińskiego">Madalińskiego</option><option value="Melody House">Melody House</option><option value="Mondrian House">Mondrian House</option><option value="Morskie Oko">Morskie Oko</option><option value="Narbutta Residence">Narbutta Residence</option><option value="Narubtta Residence">Narubtta Residence</option><option value="Noakowskiego 16">Noakowskiego 16</option><option value="Parkowa">Parkowa</option><option value="Patria">Patria</option><option value="Przy Skoczni">Przy Skoczni</option><option value="Przy Wiśle">Przy Wiśle</option><option value="REZYDENCJA FOKSAL">REZYDENCJA FOKSAL</option><option value="Rezydencja Jaśminowa">Rezydencja Jaśminowa</option><option value="Rezydencja Królewska">Rezydencja Królewska</option><option value="Rezydencja Łowicka">Rezydencja Łowicka</option><option value="Rezydencja Maltańska">Rezydencja Maltańska</option><option value="Rezydencja Mokotów">Rezydencja Mokotów</option><option value="REZYDENCJA OPERA">REZYDENCJA OPERA</option><option value="Rezydencja Przy Skoczni">Rezydencja Przy Skoczni</option><option value="Śniegockiej Residence">Śniegockiej Residence</option><option value="Solec Residence">Solec Residence</option><option value="Szucha Residence">Szucha Residence</option><option value="Tagore">Tagore</option><option value="Villa Jantar">Villa Jantar</option><option value="Willa Wilanów">Willa Wilanów</option><option value="Zielony Mokotów">Zielony Mokotów</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="rooms">
									<label>Ilość pokoi</label>
									<div class="row">
										<div class="col-xs-6">
											<select name="rooms_from"><option value="">Od</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select>										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<select name="rooms_to"><option value="">Do</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="_">5+</option></select>										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="bathrooms">
									<label>Ilość łazienek</label>
									<div class="row">
										<div class="col-xs-6">
											<select name="bathrooms_from"><option value="">Od</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select>										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<select name="bathrooms_to"><option value="">Do</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="_">5+</option></select>										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="built_year">
									<label>Rok budowy</label>
									<div class="row">
										<div class="col-xs-6">
											<input type="text" name="built_year_from"placeholder="Od" maxlength="4">										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<input type="text" name="built_year_to"placeholder="Do" maxlength="4">										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="center_distance">
									<label>Odległość od centrum</label>
									<select name="center_distance"><option value="" selected="selected">Wszystkie</option><option value="<=1">do 1 km</option><option value="<=2">do 2 km</option><option value="<=5">do 5 km</option><option value="<=10">do 10 km</option><option value=">10">powyżej 10 km</option></select>								</div>
								<!-- .form-group -->																								<div class="form-group" data-form-fieldset="attributes">
									<label>Atrybuty</label>
									<div class="row attributes" data-form-fieldset="attributes_short">
																			<div class="col-xs-6">
																					<label><input type="checkbox" name="attr[]" value="parking"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/parking.png" alt="parking"></em> Parking</label>
																					<label><input type="checkbox" name="attr[]" value="reception"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/reception.png" alt="reception"></em> Portier</label>
																					<label><input type="checkbox" name="attr[]" value="balcony"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/balcony.png" alt="balcony"></em> Balkon</label>
																					<label><input type="checkbox" name="attr[]" value="aircondition"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/aircondition.png" alt="aircondition"></em> Klimatyzacja</label>
																				</div>
										<!-- .col-xs-6 -->
																			<div class="col-xs-6">
																					<label><input type="checkbox" name="attr[]" value="internet"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/internet.png" alt="internet"></em> Internet</label>
																					<label><input type="checkbox" name="attr[]" value="utility-room"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/utility-room.png" alt="utility-room"></em> Schowek</label>
																					<label><input type="checkbox" name="attr[]" value="elevator"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/elevator.png" alt="elevator"></em> Winda</label>
																					<label><input type="checkbox" name="attr[]" value="security"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/security.png" alt="security"></em> Ochrona</label>
																				</div>
										<!-- .col-xs-6 -->
																		</div>
									<!-- .row -->

																		<div class="attributes border" data-form-fieldset="attributes_long">
																				<label><input type="checkbox" name="attr[]" value="foreign-schools"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/foreign-schools.png" alt="foreign-schools"></em> Szkoły zagraniczne w pobliżu</label>
																				<label><input type="checkbox" name="attr[]" value="shopcentre"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/shopcentre.png" alt="shopcentre"></em> Centra handlowe w pobliżu</label>
																			</div>
									<!-- .row -->
																	</div>
								<!-- .form-group -->
																								<div class="form-group" data-form-fieldset="attributes" data-form-fieldset-more="attributes_business" style="display: none;">
									<label>Atrybuty</label>
									<div class="row attributes">
										<div class="col-xs-6">
																						<label><input type="checkbox" name="attr[]" value="has-tenant"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/has-tenant.png" alt="has-tenant"></em> Z najemcą</label>
										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->
								<div class="form-group text-center">
																		
									<input type="hidden" name="order">
									<input type="submit" value="Szukaj" class="btn search"> 
									<a href="http://crystalhouse.pl/szukaj/?" class="btn">Usuń filtry</a>								</div>
								<!-- .form-group -->
							</div>
							<!-- [data-group] -->
						</form>
					</div>
					<!-- .widget.offers-list-filter -->
				</div>
				<!-- .col-md-3 -->
			</div>
			<!-- .row -->
		</div>
		<!-- .container -->
	</div>
	<!-- #content -->


		<section class="home-section popular-entries hidden-print">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6 about">
					<h2>Najczęściej wyszukiwane</h2>
					<i class="icon-search"></i>
				</div>
				<!-- .col-md-3 -->

									<div class="col-md-3 col-sm-6"><h4>Sprzedaż</h4><nav><ul><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-srodmiescie/" title="Luksusowy apartament Śródmieście">Luksusowy apartament Śródmieście</a></li><li><a href="http://crystalhouse.pl/house/dom-saska-kepa-2/" title="Ekskluzywny Dom Saska Kępa">Ekskluzywny Dom Saska Kępa</a></li><li><a href="http://crystalhouse.pl/house/dom-konstancin-jeziorna-4/" title="Dom Konstancin Jeziorna">Dom Konstancin Jeziorna</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-bielany/" title="Luksusowy apartament Warszawa Bielany">Luksusowy apartament Warszawa Bielany</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowa-nieruchomosc-warszawa-mokotow/" title="Luksusowa nieruchomość Warszawa Mokotów">Luksusowa nieruchomość Warszawa Mokotów</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-wola-okopowa/" title="Luksusowy Apartament Warszawa Wola – Okopowa">Luksusowy Apartament Warszawa Wola – Okopowa</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-mokotow/" title="Luksusowy apartament Warszawa Mokotów">Luksusowy apartament Warszawa Mokotów</a></li><li><a href="http://crystalhouse.pl/house/nowoczesny-dom-w-komorowie/" title="Nowoczesny dom w Komorowie">Nowoczesny dom w Komorowie</a></li><li><a href="http://crystalhouse.pl/apartment/apartament-warszawa-mokotow-3/" title="Luksusowy Apartament Warszawa Mokotów">Luksusowy Apartament Warszawa Mokotów</a></li><li><a href="http://crystalhouse.pl/house/luksusowy-dom-warszawa-2/" title="Luksusowy dom Warszawa Wawer">Luksusowy dom Warszawa Wawer</a></li><li><a href="http://crystalhouse.pl/apartment/ekskluzywny-apartament-mokotow/" title="Ekskluzywny apartament Mokotów">Ekskluzywny apartament Mokotów</a></li></ul></nav></div>									<div class="col-md-3 col-sm-6"><h4>Wynajem</h4><nav><ul><li><a href="http://crystalhouse.pl/apartment/ekskluzywny-apartament-warszawa-powisle/" title="Ekskluzywny apartament Warszawa Powiśle">Ekskluzywny apartament Warszawa Powiśle</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-srodmiescie-2/" title="Luksusowy apartament Warszawa Śródmieście">Luksusowy apartament Warszawa Śródmieście</a></li><li><a href="http://crystalhouse.pl/apartment/ekskluzywny-apartament-warszawa-srodmiescie/" title="Ekskluzywny apartament Warszawa Śródmieście">Ekskluzywny apartament Warszawa Śródmieście</a></li><li><a href="http://crystalhouse.pl/house/ekskluzywny-dom-konstancin-jeziorna/" title="Ekskluzywny dom Konstancin-Jeziorna">Ekskluzywny dom Konstancin-Jeziorna</a></li><li><a href="http://crystalhouse.pl/house/luksusowa-willa-warszawa/" title="Luksusowa Willa Warszawa">Luksusowa Willa Warszawa</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-saska-kepa/" title="Luksusowy apartament Saska Kępa">Luksusowy apartament Saska Kępa</a></li><li><a href="http://crystalhouse.pl/apartment/apartament-srodmiescie/" title="Apartament Śródmieście">Apartament Śródmieście</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-centrum/" title="Luksusowy apartament Warszawa centrum">Luksusowy apartament Warszawa centrum</a></li><li><a href="http://crystalhouse.pl/apartment/ekskluzywny-apartament-mokotow/" title="Ekskluzywny apartament Mokotów">Ekskluzywny apartament Mokotów</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-wilanow/" title="Luksusowy apartament Warszawa Wilanów">Luksusowy apartament Warszawa Wilanów</a></li><li><a href="http://crystalhouse.pl/apartment/penthouse-zoliborz/" title="Penthouse Żoliborz">Penthouse Żoliborz</a></li></ul></nav></div>									<div class="col-md-3 col-sm-6"><h4>Inwestycje apartamentowe</h4><nav><ul><li><a href="http://crystalhouse.pl/apartment/rezydencja-opera/" title="Rezydencja Opera">Rezydencja Opera</a></li><li><a href="http://crystalhouse.pl/apartment/morskie-oko/" title="Morskie Oko">Morskie Oko</a></li><li><a href="http://crystalhouse.pl/apartment/melody-house/" title="Melody House">Melody House</a></li><li><a href="http://crystalhouse.pl/apartment/mondrian-house/" title="Mondrian House">Mondrian House</a></li><li><a href="http://crystalhouse.pl/apartment/narbutta-residence/" title="Narbutta Residence">Narbutta Residence</a></li><li><a href="http://crystalhouse.pl/apartment/patria/" title="Patria">Patria</a></li><li><a href="http://crystalhouse.pl/apartment/madalinskiego/" title="Madalińskiego">Madalińskiego</a></li><li><a href="http://crystalhouse.pl/apartment/rezydencja-krolewska/" title="Rezydencja Królewska">Rezydencja Królewska</a></li><li><a href="http://crystalhouse.pl/apartment/wisle/" title="Przy Wiśle">Przy Wiśle</a></li><li><a href="http://crystalhouse.pl/apartment/skoczni/" title="Przy Skoczni">Przy Skoczni</a></li></ul></nav></div>							</div>
			<!-- .row -->

						<div data-expandable>
				<div class="row">
											<div class="col-md-3 col-sm-6"><h4>Wakacyjne</h4><nav><ul><li><a href="http://crystalhouse.pl/house/luksusowa-willa-z-basenem/" title="LUKSUSOWA WILLA Z BASENEM">LUKSUSOWA WILLA Z BASENEM</a></li><li><a href="http://crystalhouse.pl/house/dom-w-hiszpanii-costa-blanca/" title="Dom w Hiszpanii, Costa Blanca">Dom w Hiszpanii, Costa Blanca</a></li><li><a href="http://crystalhouse.pl/house/francjale-castellet-2/" title="Francja,Le Castellet">Francja,Le Castellet</a></li><li><a href="http://crystalhouse.pl/house/costa-del-sol-marbella-marbella-4/" title="Costa Del Sol, Marbella, Marbella">Costa Del Sol, Marbella, Marbella</a></li><li><a href="http://crystalhouse.pl/house/nowoczesna-willa-cannes/" title="Nowoczesna willa, Cannes">Nowoczesna willa, Cannes</a></li><li><a href="http://crystalhouse.pl/house/willa-z-basenem-lazurowe-wybrzeze/" title="Willa z basenem, Lazurowe Wybrzeże">Willa z basenem, Lazurowe Wybrzeże</a></li><li><a href="http://crystalhouse.pl/apartment/cannes-cannes-4/" title="Cannes, Cannes">Cannes, Cannes</a></li><li><a href="http://crystalhouse.pl/apartment/bulgaria-sloneczny-brzeg/" title="Bułgaria, Słoneczny Brzeg">Bułgaria, Słoneczny Brzeg</a></li><li><a href="http://crystalhouse.pl/house/francja-cote-dazur-saint-tropez/" title="Francja, Cote d’Azur, Saint Tropez">Francja, Cote d’Azur, Saint Tropez</a></li><li><a href="http://crystalhouse.pl/house/costa-del-sol-marbella-marbella/" title="Costa Del Sol, Marbella, Marbella">Costa Del Sol, Marbella, Marbella</a></li></ul></nav></div>											<div class="col-md-3 col-sm-6"><h4>Lokale komercyjne</h4><nav><ul><li><a href="http://crystalhouse.pl/business/lokal-z-najemca-lodz/" title="Lokal z najemcą Łódź">Lokal z najemcą Łódź</a></li><li><a href="http://crystalhouse.pl/business/lokal-z-najemca-wroclaw/" title="Lokal z najemcą Wrocław 9,5% rentowność">Lokal z najemcą Wrocław 9,5% rentowność</a></li><li><a href="http://crystalhouse.pl/business/lokal-z-najemca-sieciowym-gdansk-2/" title="Lokal z najemcą sieciowym Gdańsk – 8,6% rentowność">Lokal z najemcą sieciowym Gdańsk – 8,6% rentowność</a></li><li><a href="http://crystalhouse.pl/business/lokal-z-najemca-sieciowym-warszawa/" title="Lokal z najemcą sieciowym Warszawa Rondo Wiatraczna">Lokal z najemcą sieciowym Warszawa Rondo Wiatraczna</a></li><li><a href="http://crystalhouse.pl/business/lokal-z-najemca-warszawa/" title="Lokal z najemcą Warszawa">Lokal z najemcą Warszawa</a></li><li><a href="http://crystalhouse.pl/business/warszawa-karola-chodkiewicza-5/" title="Warszawa, Karola Chodkiewicza">Warszawa, Karola Chodkiewicza</a></li><li><a href="http://crystalhouse.pl/business/warszawa-bialy-kamien/" title="Warszawa, Biały Kamień">Warszawa, Biały Kamień</a></li><li><a href="http://crystalhouse.pl/business/warszawa-leszno/" title="Warszawa, Leszno">Warszawa, Leszno</a></li><li><a href="http://crystalhouse.pl/business/warszawa-al-wincentego-witosa-4/" title="Warszawa, al. Wincentego Witosa">Warszawa, al. Wincentego Witosa</a></li></ul></nav></div>											<div class="col-md-3 col-sm-6"><h4>Popularne lokalizacje</h4><nav><ul><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Warszawa&district=Mokot%C3%B3w" title="Warszawa-Mokotów">Warszawa-Mokotów</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Konstancin-Jeziorna" title="Konstancin-Jeziorna">Konstancin-Jeziorna</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Warszawa&district=%C5%9Ar%C3%B3dmie%C5%9Bcie" title="Warszawa-Śródmieście">Warszawa-Śródmieście</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Warszawa&district=Wilan%C3%B3w" title="Warszawa-Wilanów">Warszawa-Wilanów</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Komor%C3%B3w" title="Komorów">Komorów</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Cannes" title="Cannes">Cannes</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Costa+Del+Sol%2C+Marbella" title="COSTA DEL SOL, Marbella">COSTA DEL SOL, Marbella</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Warszawa&district=Ursyn%C3%B3w" title="Warszawa-Ursynów">Warszawa-Ursynów</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Warszawa&district=Praga-Po%C5%82udnie" title="Warszawa-Praga Południe">Warszawa-Praga Południe</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Warszawa&district=%C5%BBoliborz" title="Warszawa- Żoliborz">Warszawa- Żoliborz</a></li></ul></nav></div>											<div class="col-md-3 col-sm-6"><h4>Popularne nieruchomości</h4><nav><ul><li><a href="http://crystalhouse.pl/house/luksusowy-dom-sprzedaz-mokotow/" title="Luksusowy dom sprzedaż Mokotów">Luksusowy dom sprzedaż Mokotów</a></li><li><a href="http://crystalhouse.pl/house/nowoczesny-dom-w-komorowie/" title="Nowoczesny dom w Komorowie">Nowoczesny dom w Komorowie</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-mokotow/" title="Luksusowy apartament Warszawa Mokotów">Luksusowy apartament Warszawa Mokotów</a></li><li><a href="http://crystalhouse.pl/apartment/apartament-warszawa-mokotow-3/" title="Luksusowy Apartament Warszawa Mokotów">Luksusowy Apartament Warszawa Mokotów</a></li><li><a href="http://crystalhouse.pl/house/luksusowy-dom-wesola/" title="Luksusowy Dom Wesoła">Luksusowy Dom Wesoła</a></li><li><a href="http://crystalhouse.pl/house/palac-konstancin-jeziorna/" title="Pałac Konstancin Jeziorna">Pałac Konstancin Jeziorna</a></li><li><a href="http://crystalhouse.pl/house/luksusowa-rezydencja/" title="Luksusowa Rezydencja">Luksusowa Rezydencja</a></li><li><a href="http://crystalhouse.pl/house/luksusowy-dom-konstancin-jeziorna/" title="Luksusowy dom Konstancin Jeziorna">Luksusowy dom Konstancin Jeziorna</a></li><li><a href="http://crystalhouse.pl/house/luksusowa-willa-stare-bielany/" title="Luksusowa willa Stare Bielany">Luksusowa willa Stare Bielany</a></li><li><a href="http://crystalhouse.pl/house/dom-konstancin-jeziorna/" title="Dom Konstancin-Jeziorna">Dom Konstancin-Jeziorna</a></li></ul></nav></div>											<div class="col-md-3 col-sm-6"><h4>Beliebte Ferienwohnungen</h4><nav><ul><li><a href="http://crystalhouse.pl/house/luksusowy-dom-sprzedaz-mokotow/" title="Luksusowy dom sprzedaż Mokotów">Luksusowy dom sprzedaż Mokotów</a></li><li><a href="http://crystalhouse.pl/house/nowoczesny-dom-w-komorowie/" title="Nowoczesny dom w Komorowie">Nowoczesny dom w Komorowie</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-mokotow/" title="Luksusowy apartament Warszawa Mokotów">Luksusowy apartament Warszawa Mokotów</a></li><li><a href="http://crystalhouse.pl/apartment/apartament-warszawa-mokotow-3/" title="Luksusowy Apartament Warszawa Mokotów">Luksusowy Apartament Warszawa Mokotów</a></li><li><a href="http://crystalhouse.pl/house/luksusowy-dom-wesola/" title="Luksusowy Dom Wesoła">Luksusowy Dom Wesoła</a></li><li><a href="http://crystalhouse.pl/house/palac-konstancin-jeziorna/" title="Pałac Konstancin Jeziorna">Pałac Konstancin Jeziorna</a></li><li><a href="http://crystalhouse.pl/house/luksusowa-rezydencja/" title="Luksusowa Rezydencja">Luksusowa Rezydencja</a></li><li><a href="http://crystalhouse.pl/house/luksusowy-dom-konstancin-jeziorna/" title="Luksusowy dom Konstancin Jeziorna">Luksusowy dom Konstancin Jeziorna</a></li><li><a href="http://crystalhouse.pl/house/luksusowa-willa-stare-bielany/" title="Luksusowa willa Stare Bielany">Luksusowa willa Stare Bielany</a></li><li><a href="http://crystalhouse.pl/house/dom-konstancin-jeziorna/" title="Dom Konstancin-Jeziorna">Dom Konstancin-Jeziorna</a></li></ul></nav></div>									</div>
				<!-- .row -->
			</div>
			<!-- [data-expandable] -->
			
			<div class="expander">
				<span><a href="#" data-expand data-collapse-label="Zwiń">Rozwiń</a></span>
			</div>
			<!-- .expander -->
		</div>
		<!-- .container -->
	</section>
	<!-- .popular-entries -->
	<div id="footer" class="hidden-print">
		<div class="container">
			<div class="row hidden-xs">
				<h3 class="slogan"><strong>Luxury</strong> Realty Estate Agency</h3>
			</div>
			<!-- .row -->

			<div class="row">
				<div class="col-md-3 col-sm-12 col-sm-12">
					<a href="http://crystalhouse.pl/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/logo-dark.png" alt=""></a>
				</div>
				<!-- .col-md-3 -->

				<div class="col-md-3 col-sm-4 col-xs-6">
					<p>ul. Belwederska 36/38D/99 <br>00-594 Warszawa</p>
				</div>
				<!-- .col-md-3 -->

				<div class="col-md-3 col-sm-4 col-xs-6">
					<p class="big">Telefon<br><strong>+48 22 856 78 60</strong></p>
				</div>
				<!-- .col-md-3 -->

				<div class="col-md-3 col-sm-4 col-xs-12">
					<p class="big">Email<br><strong><a href="mailto:biuro@crystalhouse.pl">biuro@crystalhouse.pl</a></strong></p>

										<ul class="socials">
												<li><a href="https://www.facebook.com/CrystalHousepl/?ref=aymt_homepage_panel"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/socials/facebook.png" alt="Facebook"></a></li>																		<li><a href="https://www.youtube.com/channel/UCCUjgifsjADB5yLsBxlCEnQ"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/socials/youtube.png" alt="Youtube"></a></li>					</ul>				</div>
				<!-- .col-md-3 -->
			</div>
			<!-- .row -->
		</div>
		<!-- .container -->

		<div class="copyrights">
			<div class="container">
				<p>&copy; 2016 <a href="http://crystalhouse.pl/">CrystalHouse</a> | <a href="http://roogmedia.pl/" style="cursor: default; opacity: 1;">by Roogmedia</a></p>
			</div>
			<!-- .container -->
		</div>
		<!-- .copyrights -->
	</div>
	<!-- #footer -->

	<script type='text/javascript' src='http://crystalhouse.pl/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.50.0-2014.02.05'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"http:\/\/crystalhouse.pl\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Trwa wysy\u0142anie...","cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='http://crystalhouse.pl/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=3.8.1'></script>
<script type='text/javascript' src='http://crystalhouse.pl/wp-includes/js/wp-embed.min.js?ver=4.5.2'></script>
		<!-- Form filters values -->
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/form-filters_pl.js"></script>

	<!-- CDN libraries -->
	<script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-538a578c3e089f8a"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=false&amp;language=pl"></script>

	<!-- jQuery -->
	<!--<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery-1.11.1.min.js"></script>-->
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery.easing.1.3.min.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery.scrollTo.min.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery.sticky.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery.jcarousel.min.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery.cookie.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery.scrollintoview.min.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery.resizeimagetoparent.min.js"></script>

	<!-- Bootstrap -->
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/bootstrap.min.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/bootstrap-hover-dropdown.min.js"></script>

	<!-- FancyBox -->
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/fancyBox2/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/fancyBox2/jquery.fancybox.pack.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/fancyBox2/helpers/jquery.fancybox-media.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/fancyBox2/helpers/jquery.fancybox-thumbs.js"></script>

	<!-- Gmap3 -->
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/gmap3.min.js"></script>

	<!-- Widgets -->
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/app/home-theatre.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/app/offer-gallery-carousel.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/app/offers-carousel.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/app/offers-list-map.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/app/popular-entries.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/app/wishlist.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/offers-filters.js"></script>
	<!--<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/lib.js"></script>-->

	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/app.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/smoothscroll.js"></script>
</body>
</html>
<!-- Dynamic page generated in 1.094 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2016-07-01 02:06:32 -->

								<div class="form-group" data-form-fieldset="lot_type"<?php if($disabled) : ?> style="display: none;"<?php endif; ?>>
									<label><?php echo chtof_label('offer_lot-type'); ?></label>
									<?php
										$lot_types = get_field_object(chtof_id('offer_lot-type'));
										$lot_types = $lot_types['choices'];

										echo chofl_select('lot_type', array_merge(array(
											'' => __('Wszystkie', 'chtheme')
										), $lot_types), null, $disabled ? array('disabled' => 'disabled') : array());
									?>
								</div>
								<!-- .form-group -->
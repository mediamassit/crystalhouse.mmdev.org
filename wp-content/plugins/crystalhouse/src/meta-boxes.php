<?php

// Offers meta boxes
function register_offers_meta_boxes($meta_boxes) {
	$meta_boxes[] = array(
		'title' => 'Media',
		'pages' => array('offer_apartment', 'offer_business', 'offer_house', 'offer_lot'),
		'context' => 'normal',
		'priority' => 'low',
		'autosave' => true,
		'fields' => array(
			// Gallery
			array(
				'name' => __('Galeria', 'chtheme'),
				'id' => "offer_gallery",
				'type' => 'plupload_image',
				'max_file_uploads' => 30,
			),
			// Movie URL
			array(
				'name'  => __('URL filmu', 'chtheme'),
				'id'    => "offer_movie-url",
				'type'  => 'url'
			),
			// 360 view URL
			array(
				'name'  => __('URL wirtualnej wycieczki', 'chtheme'),
				'id'    => "offer_360view",
				'type'  => 'url'
			),
		)
	);

	return $meta_boxes;
}
add_filter('rwmb_meta_boxes', 'register_offers_meta_boxes');
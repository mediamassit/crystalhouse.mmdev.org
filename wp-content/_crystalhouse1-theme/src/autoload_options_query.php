<?php

$options = array(
	'wpcf-custom-types',
	'wpcf_post_relationship',
	'wpcf-custom-taxonomies',
	'widget_polylang',
	'widget_calendar',
	'preload_cache_counter',
	'_transient_timeout_get_locations_tree:tree_pl',
	'_transient_get_locations_tree:tree_pl',
	'_transient_timeout_get_locations_tree:json_pl',
	'_transient_get_locations_tree:json_pl',
	'_transient_timeout_chtof_all_fields_pl',
	'_transient_chtof_all_fields_pl',
	'_transient_timeout_get_offers_developers_pl',
	'_transient_get_offers_developers_pl',
	'_transient_timeout_get_offers_investment_groups_pl',
	'_transient_get_offers_investment_groups_pl',
	'_transient_timeout_cth_get_top_searches_widgets_pl',
	'_transient_cth_get_top_searches_widgets_pl'
);
$query = "UPDATE wp_options SET autoload = 'yes' WHERE option_name IN ('".implode("', '", $options)."') AND autoload = 'no'";

if(isset($_GET['display'])) {
	header('Content-type: text/plain; charset=utf-8');
	echo $query;
}
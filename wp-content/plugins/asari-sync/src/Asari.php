<?php

class Asari {
	private $menu_id;
	private $capability;

	/**
	 * Checks whether synchronization is needed
	 * @return bool
	 * @static
	 */
	static public function syncIsNeeded() {
		return false;
		$status = new Asari_Status;

		return (int) $status->get('queue_size') > 0;
	}

	/**
	 * Returns path to given plugin directory
	 * @param string $name
	 * @return string
	 * @static
	 */
	static public function dir($name = null) {
		switch($name) {
			default: return dirname(__DIR__);
			case 'tmp': return dirname(__DIR__).'/tmp';
			case 'logs': return dirname(__DIR__).'/logs';
			case 'src': return dirname(__DIR__).'/src';
			case 'web': return dirname(__DIR__).'/web';
			case 'templates': return dirname(__DIR__).'/web/templates';
		}
	}

	/**
	 * Returns current URL with modified query
	 * @param array $query
	 * @return string
	 * @static
	 */
	static public function url($query = array()) {
		$url = ($_SERVER['HTTPS'] ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

		if(sizeof($query) > 0) {
			$data = parse_url($url);
			parse_str($data['query'], $old_query);

			$data['query'] = http_build_query(array_merge($old_query, $query));

			return http_build_url($data);
		}

		return $url;
	}

	/**
	 * Runs given process
	 * @param string $className
	 * @return Unirest_HttpResponse
	 * @static
	 */
	static public function runProcess($className) {
		return Unirest::get(plugins_url().'/asari-sync/web/run-process.php?name='.$className);
	}

	/**
	 * __construct
	 */
	public function __construct() {
		$this->capability = apply_filters('asarisync_cap', 'manage_options');

		load_plugin_textdomain('asarisync', false, '/asarisync/lang');

		add_action('admin_menu', array(&$this, 'add_admin_menu'));
		add_action('admin_menu', array(&$this, 'add_admin_menu_notification'));
		add_action('admin_enqueue_scripts', array(&$this, 'add_admin_enqueues'));
		//add_action('shutdown', array(&$this, 'forceQueueUpdate'));

		if(is_admin()) {
			$options_page = new Asari_Page_Options;
		}
	}

	public function add_admin_menu() {
		$label = __('Sync with Asari', 'asarisync');

		// If sync is needed
		if(self::syncIsNeeded()) {
			$label .= ' <span class="update-plugins count-1"><span class="update-count">Sync</span></span>';
		}

		$management_page = new Asari_Page_Management;
		$this->menu_id = add_management_page(__('Synchronize with Asari', 'asarisync'), $label, $this->capability, 'asarisync', array(&$management_page, 'render') );
	}

	public function add_admin_menu_notification($test) {
		if(!self::syncIsNeeded()) {
			return;
		}

		global $menu;

		foreach($menu as &$item) {
			if($item[2] === 'tools.php' AND strpos($item[0], '<span') === false) {
				$item[0] .= " <span class='update-plugins count-1'><span class='update-count'>Sync</span></span>";
			}
		}
	}

	public function add_admin_enqueues() {
		wp_enqueue_script('asarisync-script', plugins_url('web/scripts.js', dirname(__FILE__)));
		wp_enqueue_style('asarisync-style', plugins_url('web/style.css', dirname(__FILE__)));
	}

	/**
	 * Force to update queue if it's expired
	 * @return void
	 */
	public function forceQueueUpdate() {
		if(class_exists('Asari_Status', true)) {
			$status = new Asari_Status;
			$lastQueueUpdate = $status->get('last_queue_update');

			if(is_null($lastQueueUpdate) || (time()-$lastQueueUpdate) >= 1*60*60) {
				self::runProcess('Asari_Process_UpdateQueue');
			}
		}
	}
}
<?php

/**
 * Langs shortcode.
 * @param string $lang
 * @return string
 */
function chtheme_langs_shortcode($atts, $content = null) {
	$atts = shortcode_atts( array(
		'lang' => null
	), $atts);

	extract($atts);

	$lang = explode(',', $lang);

	ob_start();

	if(sizeof($lang) > 0) {
?><p class="langs">
	<?php foreach($lang as $slug) : ?>
	<img src="<?php bloginfo('template_url'); ?>/images/icons/flags/<?php echo $slug; ?>.png" alt="<?php echo strtoupper($slug); ?>">
	<?php endforeach; ?>
</p><?php
	}

	return ob_get_clean();
}
add_shortcode('ch_langs', 'chtheme_langs_shortcode');

function chtheme_ask_for_price_shortcode($atts, $content = null) {
	$atts = shortcode_atts( array(
		'offer' => null,
		'subject' => null
	), $atts);

	extract($atts);

	ob_start();
?><a href="#" title="<?php echo $content; ?>" data-toggle="modal" data-target="#modal-contact-offer" data-modal-form-offer="<?php if(strlen(trim($offer)) > 0) : ?><?php echo $offer; ?><?php endif; ?>" data-modal-form-subject="<?php if(strlen(trim($subject)) > 0) : ?><?php echo $subject; ?><?php endif; ?>"><?php echo $content; ?></a>
<?php
	return ob_get_clean();
}
add_shortcode('ch_ask_for_price', 'chtheme_ask_for_price_shortcode');

/**
 * Home section shortcode.
 * @param string $class
 * @return string
 */
function chtheme_home_section_shortcode($atts, $content = null) {
	$atts = shortcode_atts( array(
		'class' => null
	), $atts);

	extract($atts);

	ob_start();

?><section class="home-section<?php if(strlen($class) > 0) { echo ' '.$class; } ?>">
	<?php echo do_shortcode(strip_newlines($content)); ?>
</section>
<!-- .home-section -->
<?php
	return ob_get_clean();
}
add_shortcode('ch_home_section', 'chtheme_home_section_shortcode');

/**
 * Home theatre slide shortcode.
 * @param string $img The URL to image
 * @param string $url The link URL (optional)
 * @param string $title The slide title
 * @param float|int $price The price value (optional)
 * @param int $offer_id The offer ID to get URL, title and price from (optional)
 * @return string
 */
function chtheme_home_slide_shortcode($atts) {
	$atts = shortcode_atts( array(
		'img' => null,
		'url' => null,
		'title' => null,
		'price' => null,
		'offer_id' => null,
		'width' => null,
		'height' => null
	), $atts);

	extract($atts);

	// Get slide data from offer
	if(!is_null($atts['offer_id'])) {
		$offer = get_post(pll_get_post($atts['offer_id']));

		$title = $offer->post_title;
		$field = get_field('offer_price_primary', $offer->ID);
		$url = get_permalink($offer->ID);
		
		if($field) {
			$price = $field;
		}
	}

	if($price) {
		//$price = chtof_price($price);
		if(cht_offer_is_for_short_rent($offer->ID)) { // Short-term rent price
			$price = chtof_price($price, 0, true, '<small>%s</small>') . '<small> / ' . mb_strtoupper(__('Doba', 'chtheme')) . '</small>';
		}
		elseif(cht_offer_is_for_long_rent($offer->ID)) { // Long-term rent price
			$price = chtof_price($price, 0, true, '<small>%s</small>') . '<small> / ' . mb_strtoupper(__('Miesiąc', 'chtheme')) . '</small>';
		}
		else { // Standard sale price
			$price = chtof_price($price, 0, true, '<small>%s</small>');
		}
	}

	if(!$img || !$title) {
		return '';
	}

	ob_start();
?>

<figure>
	<?php if($url) : ?><a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><?php endif; ?><img src="<?php echo $img; ?>" alt="<?php echo $title; ?>"<?php if($width) : ?> width="<?php echo $width; ?>"<?php endif; ?><?php if($height) : ?> height="<?php echo $height; ?>"<?php endif; ?>><?php if($url) : ?></a><?php endif; ?>
	<figcaption>
		<div class="container">
			<p><?php if($price) : ?><span class="price"><?php echo $price; ?></span> <?php endif; ?><?php if($url) : ?><a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><?php endif; ?><?php echo $title; ?><?php if($url) : ?></a><?php endif; ?></p>
		</div>
		<!-- .container -->
	</figcaption>
</figure>
<?php
	return ob_get_clean();
}
add_shortcode('ch_home_slide', 'chtheme_home_slide_shortcode');

/**
 * Offers carousel shortcode.
 * @param string $title
 * @param string $url
 * @param string $ids
 * @param string $type
 * @param string $category
 * @param int $limit
 * @return string
 */
function chtheme_offers_carousel_shortcode($atts) {
	$atts = shortcode_atts(array(
		'title' => '', // Carousel title
		'url' => '', // Carousel URL
		'ids' => null, // Offers IDs
		'type' => null, // Offer type
		'category' => null, // Offer category
		'filters' => null, // Custom field value eg. "offer_rooms > 2", "offer_exclusive = 1"
		'limit' => 10,
		'class' => null
	), $atts);

	extract($atts);

	// Set limit to the limit
	if((int) $limit > 30) {
		$limit = 30;
	}

	// If IDs of offers where provided
	if(strlen($ids) > 0) {
		$args = array();

		$offers = new WP_Query(array(
			'post_type' => array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business'),
			'post__in' => array_map('intval', explode(',', $ids))
		));
	}
	else {
		$post_type = strlen($type) > 0 ? explode(',', $type) : array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business');
		$categories = strlen($category) > 0 ? array_map('pll_get_term', explode(',', $category)) : null;

		if(strlen($filters) > 0) parse_str($filters, $offers_filters);
		else $offers_filters = array();

		// Split attributes
		if(strlen(@$offers_filters['attr'])) {
			$offers_filters['attr'] = explode(',', $offers_filters['attr']);
		}

		$config = Cht_Offers_List_Config::init($_GET);
		$config->setPostType($post_type)
			->setCategory($categories)
			->setFilters($offers_filters)
			->setLimit((int) $limit);
		$list = new Cht_Offers_List($config);
		$offers = $list->fetchAll();

		//$offers = cht_get_offers($offers_filters, $post_type, (int) $limit, $categories, $sections);
	}

	ob_start();

	if(sizeof($offers->posts) > 0) {
?><div class="widget offers-carousel container<?php if($class) : ?> <?php echo $class; ?><?php endif; ?>">
			<div class="header">
				<h2><?php if(strlen($url) > 0) : ?><a href="<?php echo $url; ?>"><?php endif; ?><?php echo $title; ?><?php if(strlen($url) > 0) : ?></a><?php endif; ?></h2>
				
				<div class="toolbar">
					<?php if(strlen($url) > 0) : ?><span class="links"><a href="<?php echo $url; ?>"><?php _e('Zobacz wszystkie', 'chtheme'); ?></a></span><?php endif; ?>
					<span class="carousel-controller">
						<a href="javascript:void(0);" class="prev"><?php _e('Poprzednie', 'chtheme'); ?></a> 
						<a href="javascript:void(0);" class="next"><?php _e('Następne', 'chtheme'); ?></a>
					</span>
				</div>
				<!-- .toolbar -->
			</div>
			<!-- .header -->

			<section class="entries">
				<div class="row">
					<?php
						while($offers->have_posts()) : $offers->the_post();
							$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(370, 215));
					?>
					<div class="col-md-6 col-lg-4">
						<div class="offer">
							<?php get_template_part('src/partials/offers-list/slots/thumbnail'); ?>

							<article>
								<div class="body">
									<h4><a href="<?php echo get_permalink($post->ID); ?>"><?php the_title(); ?></a></h4>
									<ul class="fields">
										<li><strong><?php echo chtof_label('offer_location_city', $post->ID); ?>:</strong> <?php the_field('offer_location_city', $post->ID); ?></li>
										<li><strong><?php echo chtof_label('offer_location_district', $post->ID); ?>:</strong> <?php the_field('offer_location_district', $post->ID); ?></li>
									</ul>
									<?php get_template_part('src/partials/offers-list/slots/price'); ?>
									<p class="meta">
										<span class="left" title="<?php echo chtof_label('offer_property_area-primary'); ?>" data-tooltip="1" data-placement="bottom"><i class="icon-offer-area"></i> <?php if((int) get_field('offer_property_area-primary', $post->ID) > 0) : ?><?php the_field('offer_property_area-primary', $post->ID); ?> m<sup>2</sup><?php else : _e('b/d', 'chtheme'); endif; ?></span>
										<span class="right" title="<?php echo chtof_label('offer_rooms_bedrooms'); ?>" data-tooltip="1" data-placement="bottom"><i class="icon-offer-bedroom"></i> <?php if((int) get_field('offer_rooms_bedrooms', $post->ID) > 0) : ?><?php the_field('offer_rooms_bedrooms', $post->ID); ?><?php else : _e('b/d', 'chtheme'); endif; ?></span>
									</p>
								</div>
								<!-- .body -->

								<?php get_template_part('src/partials/offers-list/slots/side'); ?>
							</article>
						</div>
						<!-- .offer -->
					</div>
					<!-- .col-md-4 -->
					<?php endwhile; ?>
				</div>
				<!-- .row -->
			</section>
			<!-- .entries -->
		</div>
		<!-- .widget.offers-carousel --><?php
	}

	wp_reset_postdata();

	return ob_get_clean();
}
add_shortcode('ch_offers_carousel', 'chtheme_offers_carousel_shortcode');

/**
 * Home featured offers wrapper shortcode.
 * @return string
 */
function chtheme_home_featured_shortcode($atts, $content = null) {
	ob_start();
?>
		<div class="widget offers-featured container">
			<div class="row">
				<?php echo do_shortcode(strip_newlines($content)); ?>
			</div>
			<!-- .row -->
		</div>
		<!-- .offers-featured -->
<?php
	return ob_get_clean();
}
add_shortcode('ch_home_featured', 'chtheme_home_featured_shortcode');

/**
 * Home featured offers column shortcode.
 * @param bool $big
 * @return string
 */
function chtheme_home_featured_col_shortcode($atts, $content = null) {
	ob_start();

	if(!isset($atts['big'])) {
?>
		<div class="col-md-3 col-sm-6">
			<?php echo do_shortcode(strip_newlines($content)); ?>
		</div>
		<!-- .col-md-3 -->
<?php
	}
	else {
?>
		<div class="col-md-6 col-sm-12">
			<?php echo do_shortcode(strip_newlines($content)); ?>
		</div>
		<!-- .col-md-6 -->
<?php
	}

	return ob_get_clean();
}
add_shortcode('ch_home_featured_col', 'chtheme_home_featured_col_shortcode');

/**
 * Home featured offer shortcode.
 * @param string $img
 * @param string $url
 * @param string $title
 * @param int $offer_id
 * @return string
 */
function chtheme_home_featured_offer_shortcode($atts, $content = null) {
	$atts = shortcode_atts( array(
		'img' => null,
		'url' => null,
		'title' => null,
		'offer_id' => null,
		'class' => null
	), $atts);

	extract($atts);

	// Get slide data from offer
	if(!is_null($atts['offer_id'])) {
		$offer = get_post(pll_get_post($atts['offer_id']));

		$title = $offer->post_title;
		$price = get_field('offer_price_primary', $offer->ID);
		$url = get_permalink($offer->ID);
		
		if($price) {
			//$price = $field;

			if(cht_offer_is_for_short_rent($offer->ID)) { // Short-term rent price
				$price = chtof_price($price, 0, true, '<small>%s</small>') . '<small> / ' . mb_strtoupper(__('Doba', 'chtheme')) . '</small>';
			}
			elseif(cht_offer_is_for_long_rent($offer->ID)) { // Long-term rent price
				$price = chtof_price($price, 0, true, '<small>%s</small>') . '<small> / ' . mb_strtoupper(__('Miesiąc', 'chtheme')) . '</small>';
			}
			else { // Standard sale price
				$price = chtof_price($price, 0, true, '<small>%s</small>');
			}
		}
	}

	if($price) {
		$title .= ' - ' . $price;
	}

	ob_start();

	if(!$img || !$title) {
		return '';
	}
?>
					<article class="offer<?php if(isset($atts['big'])) : ?> big<?php endif; ?><?php if(isset($atts['class'])) : ?> <?php echo $atts['class']; ?><?php endif; ?>">
						<figure>
							<?php if(strlen($url) > 0) : ?><a href="<?php echo $url; ?>"><?php endif; ?><img src="<?php echo $img; ?>" alt=""><?php if(strlen($url) > 0) : ?></a><?php endif; ?>
							<?php if(strlen($title) > 0) : ?><figcaption><?php if(strlen($url) > 0) : ?><a href="<?php echo $url; ?>"><?php endif; ?><?php echo $title; ?><?php if(strlen($url) > 0) : ?></a><?php endif; ?></figcaption><?php endif; ?>
						</figure>
					</article>
					<!-- .offer -->
<?php
	return ob_get_clean();
}
add_shortcode('ch_home_featured_offer', 'chtheme_home_featured_offer_shortcode');
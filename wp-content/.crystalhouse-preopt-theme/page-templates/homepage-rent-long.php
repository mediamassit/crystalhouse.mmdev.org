<?php
/**
 * Template Name: Homepage - Wynajem długoterminowy
 */
	get_header();

	$image_slider = rwmb_meta('home_image_slider');

	$form_filters = cht_get_form_filters(true);
?>

	<div id="home-theatre">
		<div class="viewport">
			<a href="javascript:void(0);" class="prev"></a>
			<a href="javascript:void(0);" class="next"></a>
			<div class="dots"><ul></ul></div>

			<div class="slides">
				<?php echo do_shortcode(strip_newlines($image_slider)); ?>
			</div>
			<!-- .slides -->
		</div>
		<!-- .viewport -->
	</div>
	<!-- #home-theatre -->

	<section class="widget offers-home-filters">
		<div class="container">
			<form method="get" action="<?php echo get_term_link(cht_get_long_rent_category(), 'offer_category'); ?>" name="offers_filter" data-is-extendable="1" data-form-values='{"offer": "rent", "rent_type": "12", "country": "Polska"}'>
				<div class="row search-query">
					<div class="col-md-12">
						<label><?php echo chofl_input_text('q', array('placeholder' => __('wpisz nr oferty, miasto, ulicę', 'chtheme'))); ?></label>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3 side">
						<fieldset class="offer-currency" data-form-fieldset="currency">
							<label><?php _e('Waluta', 'chtheme'); ?></label>
							<?php
								echo chofl_select('currency', array_combine(chofl_currencies(false), chofl_currencies(false)));
							?>
						</fieldset>

					</div>
					<!-- .col-md-3 -->

					<div class="col-md-9 fields">
						<div class="col-md-4" data-form-fieldset="type">
							<fieldset>
								<label><?php _e('Typ nieruchomości', 'chtheme'); ?></label>
								<?php
									$post_types_select = array();

									foreach(array('offer_apartment', 'offer_house') as $type) {
										$post_types_select[str_replace('offer_', '', $type)] = chtof_type_label($type);
									}

									echo chofl_select('type', array_merge(array(
										'' => __('Wszystkie', 'chtheme')
									), $post_types_select));
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="city">
							<fieldset>
								<label><?php echo chtof_label(chtof_id('offer_location_city'), true); ?></label>
								<?php
									echo chofl_select('city', /*array(
										'' => __('Wszystkie', 'chtheme')
									) + */$form_filters['locations']['city']['default'], null, $disabled ? array('disabled' => 'disabled') : array());
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="district">
							<fieldset>
								<label><?php echo chtof_label(chtof_id('offer_location_district'), true); ?></label>
								<?php
									echo chofl_select('district', array(
										'' => __('Wszystkie', 'chtheme')
									));
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="rooms">
							<fieldset class="row">
								<label class="col-md-12"><?php echo chtof_label(chtof_id('offer_rooms_rooms'), true); ?></label>
								<div class="col-xs-6">
									<?php
										echo chofl_select('rooms_from', $form_filters['rooms']['default']['default']['from'], null);
									?>
								</div>
								<!-- .col-md-6 -->

								<div class="col-xs-6">
									<?php
										echo chofl_select('rooms_to', $form_filters['rooms']['default']['default']['to'], null);
									?>
								</div>
								<!-- .col-md-6 -->
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="price">
							<fieldset class="row">
								<label class="col-md-12"><?php echo chtof_label(chtof_id('offer_price_primary'), true); ?> <?php _e('za miesiąc', 'chtheme'); ?></label>
								<div class="col-xs-6">
									<?php
										echo chofl_select('price_from', $form_filters['price']['default']['buy']['pln']['from']);
									?>
								</div>
								<!-- .col-md-6 -->

								<div class="col-xs-6">
									<?php
										echo chofl_select('price_to', $form_filters['price']['default']['buy']['pln']['to']);
									?>
								</div>
								<!-- .col-md-6 -->
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4 pull-right submit">
							<fieldset class="row">
								<div class="col-md-6"></div>
								<div class="col-md-6 button" style="text-align: right;">
									<label>&nbsp;</label>
									<input type="submit" value="<?php _e('Szukaj', 'chtheme'); ?>" class="btn search">
								</div>
								<!-- .col-md-6 -->
							</fieldset>
						</div>
						<!-- .col-md-4 -->
					</div>
					<!-- .fields -->
				</div>
			</form>
		</div>
		<!-- .container -->
	</section>
	<!-- .offers-filter.home -->

	<?php echo do_shortcode(strip_newlines($post->post_content)); ?>

<?php get_footer(); ?>
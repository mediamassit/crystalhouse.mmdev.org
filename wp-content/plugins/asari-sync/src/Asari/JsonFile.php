<?php

class Asari_JsonFile {

	/**
	 * The name of queue.
	 * @var string
	 */
	protected $_name = 'filename';

	/**
	 * The prefix for filename.
	 * @var string
	 */
	protected $_filenamePrefix = 'json';

	/**
	 * The path to directory.
	 * @var string
	 */
	protected $_directory;

	/**
	 * Data to save.
	 * @var mixed
	 */
	protected $_data = array();

	/**
	 * __construct
	 */
	public function __construct() {
		$this->_directory = dirname(dirname(__DIR__)).'/tmp';

		if(!is_writeable($this->_directory)) {
			throw new RuntimeException('Plugin temp directory is not writeable');
		}

		$this->load();
	}

	/**
	 * Sets file data.
	 * @return Asari_JsonFile
	 */
	public function put($data) {
		$this->_data = $data;

		return $this;
	}

	/**
	 * Returns file data.
	 * @return mixed
	 */
	public function get() {
		return $this->_data;
	}

	/**
	 * Stores data in file.
	 * @throws RuntimeException
	 * @return Asari_JsonFile
	 */
	public function save() {
		$filename = $this->getFilename();
		$path = $this->_directory.'/'.$filename;

		if(file_exists($path)) {
			$path = realpath($path);
		}

		if(file_put_contents($path, @json_encode($this->_data)) === false) {
			//xdebug_var_dump($this->_directory.'/'.$filename, $this->_data, @json_encode($this->_data));
			throw new RuntimeException(sprintf('Cannot save data to "%s" file', $this->_name));
		}

		return true;
	}

	/**
	 * Returns file save time.
	 * @return int|null
	 */
	public function getSaveTime() {
		$filename = $this->getFilename();
		$time = @filemtime($this->_directory.'/'.$filename);

		return $time ? $time : null;
	}

	/**
	 * Removes file.
	 * @return bool
	 */
	public function remove() {
		return unlink($this->_directory.'/'.$filename);
	}

	/**
	 * Returns the URL to file.
	 * @return string
	 */
	public function getFileUrl() {
		// TODO: Generate it from $this->_directory ?
		return plugins_url().'/asari-sync/tmp/'.$this->getFilename();
	}

	/**
	 * Returns filename.
	 * @return string
	 */
	protected function getFilename() {
		return sprintf('%s_%s.js', $this->_filenamePrefix, $this->_name);
	}

	/**
	 * Loads stored data.
	 * @return mixed
	 */
	protected function load() {
		$filename = $this->getFilename();

		if(!file_exists($this->_directory.'/'.$filename) || !is_readable($this->_directory.'/'.$filename)) {
			return array();
		}

		$data = @json_decode(@file_get_contents($this->_directory.'/'.$filename), true);

		return $this->_data = $data ? $data : array();
	}

}
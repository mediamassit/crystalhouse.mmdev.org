<?php
/**
 * Template Name: Homepage
 */
	get_header();

	$image_slider = rwmb_meta('home_image_slider');
	cht_precache_slider_offers($image_slider);
	
	$form_filters = cht_get_form_filters(true);
?>

	<div id="home-theatre">
		<div class="viewport">
			<a href="javascript:void(0);" class="prev"></a>
			<a href="javascript:void(0);" class="next"></a>
			<div class="dots"><ul></ul></div>

			<div class="slides">
				<?php echo do_shortcode(strip_newlines($image_slider)); ?>
			</div>
			<!-- .slides -->
		</div>
		<!-- .viewport -->
	</div>
	<!-- #home-theatre -->

	<section class="widget offers-home-filters">
		<div class="container">
			<form method="get" action="<?php echo get_permalink(pll_get_post(642)); ?>" name="offers_filter" data-is-extendable="1" data-form-values='{"country": "Polska"}'>
				<div class="row search-query">
					<div class="col-md-12">
						<label><?php echo chofl_input_text('q', array('placeholder' => __('wpisz nr oferty, miasto, ulicę', 'chtheme'))); ?></label>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3 side">
						<fieldset class="offer-type inline-radios" data-form-fieldset="offer">
							<label><?php _e('Chcę kupić', 'chtheme'); ?> <input type="radio" name="offer" value="buy" checked="checked"></label>
							<label><?php _e('Chcę wynająć', 'chtheme'); ?> <input type="radio" name="offer" value="rent"></label>
						</fieldset>
						<fieldset class="offer-currency" data-form-fieldset="currency">
							<label><?php _e('Waluta', 'chtheme'); ?></label>
							<?php
								echo chofl_select('currency', array_combine(chofl_currencies(false), chofl_currencies(false)));
							?>
						</fieldset>
						<fieldset class="offer-currency offer-market_type" data-form-fieldset="market_type" data-form-extended="1" style="display: none;">
							<div class="inline-checkboxes">
							<?php
								$market_types = get_field_object(chtof_id('offer_market_type'));
								$market_types = $market_types['choices'];
								arsort($market_types);

								foreach($market_types as $id => $label) :
							?>
							<label><?php echo sprintf(__('Rynek %s', 'chtheme'), pll__($label)); ?> <input type="radio" name="market_type[]" value="<?php echo $id; ?>"<?php if($id === 'secondary') : ?> checked="checked"<?php endif; ?> disabled="disabled"></label>
							<?php endforeach; ?>
							</div>
						</fieldset>
					</div>
					<!-- .col-md-3 -->

					<div class="col-md-9 fields">
						<div class="col-md-4" data-form-fieldset="type">
							<fieldset>
								<label><?php _e('Typ nieruchomości', 'chtheme'); ?></label>
								<?php
									$post_types_select = array();

									foreach(array('offer_apartment', 'offer_house', 'offer_lot') as $type) {
										$post_types_select[str_replace('offer_', '', $type)] = chtof_type_label($type);
									}

									echo chofl_select('type', array_merge(array(
										'' => __('Wszystkie', 'chtheme')
									), $post_types_select));
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="rent_type" style="display: none;">
							<fieldset>
								<label><?php _e('Rodzaj wynajmu', 'chtheme'); ?></label>
								<?php
									echo chofl_select('rent_type', chtof_rent_types(), null, array('disabled' => 'disabled'));
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="lot_type" data-form-extended="1" style="display: none;">
							<fieldset>
								<label><?php echo chtof_label('offer_lot-type'); ?></label>
								<?php
									$lot_types = get_field_object(chtof_id('offer_lot-type'));
									$lot_types = $lot_types['choices'];

									echo chofl_select('lot_type', array_merge(array(
										'' => __('Wszystkie', 'chtheme')
									), $lot_types), null, isset($disabled) && $disabled ? array('disabled' => 'disabled') : array());
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="price">
							<fieldset class="row">
								<label class="col-md-12"><?php echo pll_current_language() !== 'pl' ? __('Zakres cen', 'chtheme') : chtof_label(chtof_id('offer_price_primary'), true); ?></label>
								<div class="col-xs-6">
									<?php
										echo chofl_select('price_from', $form_filters['price']['default']['buy']['pln']['from']);
									?>
								</div>
								<!-- .col-md-6 -->

								<div class="col-xs-6">
									<?php
										echo chofl_select('price_to', $form_filters['price']['default']['buy']['pln']['to']);
									?>
								</div>
								<!-- .col-md-6 -->
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="area">
							<fieldset class="row">
								<label class="col-md-12"><span data-type-label="default"><?php echo chtof_label('offer_property_area-primary'); ?></span> <span data-type-label="house" style="display: none;"><?php _e('Powierzchnia domu', 'chtheme'); ?></span></label>
								<div class="col-xs-6">
									<?php
										echo chofl_select('area_from', $form_filters['area']['default']['default']['m2']['from']);
									?>
								</div>
								<!-- .col-md-6 -->

								<div class="col-xs-6">
									<?php
										echo chofl_select('area_to', $form_filters['area']['default']['default']['m2']['from']);
									?>
								</div>
								<!-- .col-md-6 -->
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="province" data-form-extended="1" style="display: none;">
							<fieldset>
								<label><?php echo chtof_label(chtof_id('offer_location_province'), true); ?></label>
								<?php
									echo chofl_select('province', array(
										'' => __('Wszystkie', 'chtheme')
									), null, array('disabled' => 'disabled'));
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="city">
							<fieldset>
								<label><?php echo chtof_label(chtof_id('offer_location_city'), true); ?></label>
								<?php
									echo chofl_select('city', /*array(
										'' => __('Wszystkie', 'chtheme')
									) + */$form_filters['locations']['city']['default'], null, isset($disabled) && $disabled ? array('disabled' => 'disabled') : array());
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="district">
							<fieldset>
								<label><?php echo chtof_label(chtof_id('offer_location_district'), true); ?></label>
								<?php
									echo chofl_select('district', array(
										'' => __('Wszystkie', 'chtheme')
									));
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="built_year" data-form-extended="1" style="display: none;">
							<fieldset class="row">
								<label class="col-md-12"><?php echo chtof_label(chtof_id('offer_property_built-year'), true); ?></label>
								<div class="col-xs-6">
									<?php echo chofl_input_text('built_year_from', array('placeholder' => __('Od', 'chtheme'), 'maxlength' => 4, 'disabled' => 'disabled')); ?>
								</div>
								<!-- .col-md-6 -->

								<div class="col-xs-6">
									<?php echo chofl_input_text('built_year_to', array('placeholder' => __('Do', 'chtheme'), 'maxlength' => 4, 'disabled' => 'disabled')); ?>
								</div>
								<!-- .col-md-6 -->
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="area_lot" data-form-extended="1" style="display: none;">
							<fieldset class="row">
								<label class="col-md-12"><?php echo chtof_label(chtof_id('offer_property_area-lot'), true); ?></label>
								<div class="col-xs-6">
									<?php
										echo chofl_select('area_lot_from', $form_filters['area_lot']['m2']['from'], null, array('style' => 'text-transform: none;', 'disabled' => 'disabled'));
									?>
								</div>
								<!-- .col-md-6 -->

								<div class="col-xs-6">
									<?php
										echo chofl_select('area_lot_to', $form_filters['area_lot']['m2']['to'], null, array('style' => 'text-transform: none;', 'disabled' => 'disabled'));
									?>
								</div>
								<!-- .col-md-6 -->
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="center_distance" data-form-extended="1" style="display: none;">
							<fieldset>
								<label><?php echo chtof_label('offer_location_center-distance'); ?></label>
								<?php
									echo chofl_select('center_distance', array(
										'' => __('Wszystkie', 'chtheme'),
										'<=1' => __('do', 'chtheme').' 1 km',
										'<=2' => __('do', 'chtheme').' 2 km',
										'<=5' => __('do', 'chtheme').' 5 km',
										'<=10' => __('do', 'chtheme').' 10 km',
										'>10' => __('powyżej', 'chtheme').' 10 km'
									), null, array('disabled' => 'disabled'));
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="rooms" data-form-extended="1" style="display: none;">
							<fieldset class="row">
								<label class="col-md-12"><?php echo chtof_label(chtof_id('offer_rooms_rooms'), true); ?></label>
								<div class="col-xs-6">
									<?php
										echo chofl_select('rooms_from', $form_filters['rooms']['default']['default']['from'], null);
									?>
								</div>
								<!-- .col-md-6 -->

								<div class="col-xs-6">
									<?php
										echo chofl_select('rooms_to', $form_filters['rooms']['default']['default']['to'], null);
									?>
								</div>
								<!-- .col-md-6 -->
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<!--<div class="col-md-4" data-form-fieldset="bathrooms" data-form-extended="1" style="display: none;">
							<fieldset class="row">
								<label class="col-md-12"><?php echo chtof_label(chtof_id('offer_rooms_bathrooms'), true); ?></label>
								<div class="col-xs-6">
									<?php
										echo chofl_select('bathrooms_from', array(
											'' => __('Od', 'chtheme'),
											'1' => '1',
											'3' => '3',
											'5' => '5'
										), null, array('disabled' => 'disabled'));
									?>
								</div>
								<!-- .col-md-6

								<div class="col-xs-6">
									<?php
										echo chofl_select('bathrooms_to', array(
											'' => __('Do', 'chtheme'),
											'3' => '3',
											'5' => '5',
											' ' => '5+'
										), null, array('disabled' => 'disabled'));
									?>
								</div>
								<!-- .col-md-6
							</fieldset>
						</div>-->
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="developer" data-form-extended="1" style="display: none;">
							<fieldset>
								<label><?php _e('Developer', 'chtheme'); ?></label>
								<?php
									echo chofl_select('developer', array(
										'' => __('Wszystkie', 'chtheme')
									) + get_offers_developers(), null, array('disabled' => 'disabled'));
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="investment" data-form-extended="1" style="display: none;">
							<fieldset>
								<label><?php echo chtof_label('offer_investment_group'); ?></label>
								<?php
									echo chofl_select('investment', array(
										'' => __('Wszystkie', 'chtheme')
									) + get_offers_investment_groups(), null, array('disabled' => 'disabled'));
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4 pull-right submit">
							<fieldset class="row">
								<div class="col-md-6 closer">
									<label>&nbsp;</label>
									<a href="javascript:void(0);" class="advanced-search" data-toggle-extended="1"><?php _e('Zamknij', 'chtheme'); ?></a>
								</div>
								<!-- .col-md-6 -->

								<div class="col-md-6 button">
									<label>&nbsp;</label>
									<input type="submit" value="<?php _e('Szukaj', 'chtheme'); ?>" class="btn search">
								</div>
								<!-- .col-md-6 -->

								<div class="col-md-6 expander">
									<label>&nbsp;</label>
									<a href="javascript:void(0);" class="advanced-search" data-toggle-extended="1"><?php _e('Wyszukiwarka zaawansowana', 'chtheme'); ?></a>
								</div>
								<!-- .col-md-6 -->
							</fieldset>
						</div>
						<!-- .col-md-4 -->
					</div>
					<!-- .fields -->
				</div>
			</form>
		</div>
		<!-- .container -->
	</section>
	<!-- .offers-filter.home -->

	<?php echo do_shortcode(strip_newlines($post->post_content)); ?>

<?php get_footer(); ?>
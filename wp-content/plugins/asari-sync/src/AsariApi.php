<?php

class AsariApi {

	protected $_userId;
	protected $_token;
	protected $_devMode = true;

	public function __construct($userId, $token) {
		$this->_userId = (float) $userId;
		$this->_token = (string) $token;
	}

	public function get($url, $params) {
		$response = Unirest::get($url, array("Accept" => "application/json"), array_merge($params, array(
			"userId" => $this->_userId,
			"loginToken" => $this->_token
		)));

		if($response->code == 403) {
			throw new AsariApiException('Unauthorized access (403)');
		}

		if($response->code == 404) {
			throw new AsariApiException('Bad API call (404)');
		}

		if($response->code !== 200) {
			throw new AsariApiException(sprintf('Bad response code (%s)', $response->code));
		}

		if(!is_object($response->body)) {
			throw new AsariApiException('Response body not found');
		}

		if($response->body->success !== true) {
			throw new AsariApiException('API request failed');
		}

		return $response;
	}

	public function getListingsCount() {
		$response = $this->get("http://api.asariweb.pl/apiSite/list", array("limit" => 1));

		return (float) $response->body->totalCount;
	}

	public function getAllListings() {
		$step = 150;
		$offset = 0;
		$listings = array();

		$cycle = 0;
		$start = microtime(true);

		do {
			$response = $this->get("http://api.asariweb.pl/apiSite/list", array("offset" => $offset, "limit" => $step));
			$total_count = $response->body->totalCount;

			$offset += $step;
			$listings = array_merge($listings, $response->body->data);

			++$cycle;
		}
		while($offset <= $total_count);

		unset($step, $offset, $cycle, $start, $response, $total_count);
		return $listings;
	}

	public function extractListingsIdentifiers($listings) {
		$start = microtime(true);
		$ids = array();

		foreach($listings as $listing) {
			$ids[] = (object) array(
				'real' => $listing->id,
				'listing' => $listing->listingId
			);
		}

		unset($start, $listings);
		return $ids;
	}

	public function extractListingsRealIds($listings) {
		$ids = $this->extractListingsIdentifiers($listings);
		$array = array();

		foreach($ids as $id) {
			$array[] = $id->real;
		}

		unset($ids);
		return $array;
	}

	public function extractListingsIds($listings) {
		$ids = $this->extractListingsIdentifiers($listings);
		$array = array();

		foreach($ids as $id) {
			$array[] = $id->listing;
		}

		unset($ids);
		return $array;
	}

	public function getListing($realId) {
		$response = $this->get("http://api.asariweb.pl/apiSite/listing", array("id" => (float) $realId));

		return $response->body->data;
	}

}

class AsariApiException extends Exception {
}
<?php
	chp_clipboard_temp_ajax_end();

/**
 * Template Name: Page - Clipboard
 */
	get_header();

	$contact_form = rwmb_meta('contact_form');
	$offers = chp_clipboard_get_offers();

?>

	<div id="content">
		<div class="container">
			<?php get_template_part('src/partials/breadcrumbs'); ?>

			<div class="page-title">
				<h1><?php the_title(); ?></h1>
			</div>
			<!-- .page-header -->

			<div class="single-offer">
				<nav class="top-navigation hidden-print">
					<ul class="left sections">
						<li><a href="<?php echo get_permalink($post->ID); ?>?clear-clipboard"><em class="icon-close"></em> <?php _e('Wyczyść schowek', 'chtheme'); ?></a></li>
						<li><a href="javascript:void(0);" data-saveclipboard=""><em class="icon-download"></em> <?php _e('Zapisz schowek', 'chtheme'); ?></a></li>
					</ul>

					<ul class="right">
						<li><a href="javascript:print();"><em class="circle-button print"></em></a></li>
						<li><a class="addthis_button_compact" href="http://www.addthis.com/bookmark.php?v=300&amp;pubid=xa-538a578c3e089f8a" title="<?php _e('Udostępnij', 'chtheme'); ?>" target="_blank"><em class="circle-button share"></em></a></li>
					</ul>
				</nav>
				<!-- .top-navigation -->

				<div class="row divider divider-8-4">
					<div class="col-md-8 main">
						<?php if(is_object($offers) && $offers->have_posts()) : ?>
						<section class="widget offers-wishlist">
							<div class="row">
								<?php
								while ($offers->have_posts()) : $offers->the_post();
									$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(370, 215));
								?>
								<div class="col-sm-6">
									<article class="offer" style="min-height: 120px; margin: 15px auto;">
										<a href="javascript:void(0);" data-removefromclipboard="<?php echo $post->ID; ?>" class="remove">x</a>
										<figure>
											<a href="<?php echo get_permalink($post->ID); ?>">
												<?php if($thumb) :  ?>
												<img src="<?php echo $thumb[0]; ?>" alt="">
												<?php else : ?>
												<img src="<?php bloginfo('template_url'); ?>/tmp/thumb_370x215.jpg" width="370" alt="">
												<?php endif; ?>
											</a>
										</figure>
										<ul>
											<?php if(chtof_has('offer_asari_listing_id')) : ?>
												<li><strong><?php _e('Nr. oferty', 'chtheme'); ?>:</strong> <?php the_field('offer_asari_listing_id'); ?></li>
											<?php endif; ?>

											<?php if(chtof_has('offer_location_street')) : ?>
												<li><strong><?php _e('Ulica', 'chtheme'); ?>:</strong> <?php the_field('offer_location_street'); ?></li>
											<?php endif; ?>

											<?php if(chtof_has('offer_property_area-primary')) : ?>
												<li><strong><?php _e('Powierzchnia', 'chtheme'); ?>:</strong> <?php the_field('offer_property_area-primary'); ?> m<sup>2</sup></li>
											<?php endif; ?>

											<li>
												<strong><?php _e('Cena:', 'chtheme'); ?></strong>
												<?php if(cht_offer_is_for_long_rent()) : ?><strong><?php echo chtof_price(get_field('offer_price_primary')); ?> <small><?php _e('/ msc', 'chtheme'); ?></small></strong>
												<?php elseif(cht_offer_is_for_short_rent()) : ?><strong><?php echo chtof_price(get_field('offer_price_primary')); ?> <small><?php _e('/ dzień', 'chtheme'); ?></small></strong>
												<?php else : ?><strong><?php echo chtof_price(get_field('offer_price_primary')); ?></strong><?php endif; ?>
											</li>
										</ul>
									</article>
								</div>
								<!-- .col-md-6 -->
								<?php endwhile; ?>
							</div>
							<!-- .row -->
						</section>
						<!-- .widget.offers-wishlist -->
						<?php else : ?>
						<section class="page text">
							<p><?php _e('Schowek jest pusty.', 'chtheme'); ?></p>
						</section>
						<?php endif; ?>
					</div>
					<!-- .col-md-9 -->

					<div class="col-md-4 sidebar hidden-print">
						<section class="widget contact-broker">
							<?php echo do_shortcode($contact_form); ?>
						</section>
						<!-- .contact-broker -->
					</div>
					<!-- .col-md-3 -->
				</div>
				<!-- .row -->
			</div>
			<!-- .single-offer -->
		</div>
		<!-- .container -->
	</div>
	<!-- #content -->

	<?php get_template_part('src/partials/page-contact-form'); ?>

<?php get_footer(); ?>
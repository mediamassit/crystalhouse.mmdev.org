<?php

class Asari_Page_ManagementUpdateQueue {

	public function render() {
		$response = Asari::runProcess('Asari_Process_UpdateQueue');

		if($response->code === 200 && @$response->body->success === true) {
			return wp_redirect(admin_url('tools.php?page=asarisync'));
		}

		$error = sprintf('<strong>Queue update failed: %s.</strong> You will be redirected in 5 seconds.', $response->body->error);
		require Asari::dir('templates').'/error.php';
	}

}
<?php

	$form_filters = cht_get_form_filters(true);

?>								<div class="form-group" data-form-fieldset="bathrooms"<?php if(@$disabled) : ?> style="display: none;"<?php endif; ?>>
									<label><?php echo chtof_label('offer_rooms_bathrooms'); ?></label>
									<div class="row">
										<div class="col-xs-6">
											<?php
												echo chofl_select('bathrooms_from', $form_filters['rooms']['default']['default']['from'], null, @$disabled ? array('disabled' => 'disabled') : array());
											?>
										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<?php
												echo chofl_select('bathrooms_to', $form_filters['rooms']['default']['default']['to'], null, @$disabled ? array('disabled' => 'disabled') : array());
											?>
										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->
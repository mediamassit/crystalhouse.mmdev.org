								<?php
									$attributes = get_field_object(chtof_id('offer_attributes'));
									$attributes = $attributes['choices'];
									$long_attributes = array();

									foreach($attributes as $key => $label) {
										if(strlen($label) > 20) {
											$long_attributes[$key] = $label;
											unset($attributes[$key]);
										}
									}

									$attributes = array(
										array_slice($attributes, 0, floor(sizeof($attributes)/2)),
										array_slice($attributes, floor(sizeof($attributes)/2))
									);

									if(sizeof(@$attributes[0]) > 0) :
								?>
								<div class="form-group" data-form-fieldset="attributes">
									<label><?php echo chtof_label('offer_attributes'); ?></label>
									<div class="row attributes" data-form-fieldset="attributes_short">
									<?php foreach($attributes as $part) : ?>
										<div class="col-xs-6">
										<?php foreach($part as $key => $label) : ?>
											<label><input type="checkbox" name="attr[]" value="<?php echo $key; ?>"<?php if(@in_array($key, @$_GET['attr'])) : ?> checked="checked"<?php endif; ?>> <em><img src="<?php bloginfo('template_url'); ?>/images/icons/offer/<?php echo $key; ?>.png" alt="<?php echo $key; ?>"></em> <?php pll_e($label); ?></label>
										<?php endforeach; ?>
										</div>
										<!-- .col-xs-6 -->
									<?php endforeach; ?>
									</div>
									<!-- .row -->

									<?php if(!@$no_long_attributes && sizeof($long_attributes) > 0) : ?>
									<div class="attributes border" data-form-fieldset="attributes_long">
										<?php foreach($long_attributes as $key => $label) : ?>
										<label><input type="checkbox" name="attr[]" value="<?php echo $key; ?>"<?php if(@in_array($key, @$_GET['attr'])) : ?> checked="checked"<?php endif; ?>> <em><img src="<?php bloginfo('template_url'); ?>/images/icons/offer/<?php echo $key; ?>.png" alt="<?php echo $key; ?>"></em> <?php pll_e($label); ?></label>
										<?php endforeach; ?>
									</div>
									<!-- .row -->
									<?php endif; ?>
								</div>
								<!-- .form-group -->
								<?php endif; ?>
										<p class="price">
											<?php if((float) get_field('offer_price_primary') > 0) : ?>
												<?php if(cht_offer_is_for_short_rent()) : // Short-term rent price ?>
													<?php echo chtof_price(get_field('offer_price_primary'), 0, true, '<small>%s</small>'); ?><small> / <?php echo mb_strtoupper(__('Doba', 'chtheme')); ?></small>
												<?php elseif(cht_offer_is_for_long_rent()) : // Long-term rent price ?>
													<?php echo chtof_price(get_field('offer_price_primary'), 0, true, '<small>%s</small>'); ?><small> / <?php echo mb_strtoupper(__('Miesiąc', 'chtheme')); ?></small>
												<?php else : // Standard buy price ?>
													<?php echo chtof_price(get_field('offer_price_primary'), 0, true, '<small>%s</small>'); ?>
												<?php endif; ?>
											<?php else : ?>
												<a href="#" title="<?php _e('Zapytaj o cenę', 'chtheme'); ?>" data-toggle="modal" data-target="#modal-contact-offer" data-modal-form-offer="<?php echo get_field('offer_asari_listing_id'); ?>" data-modal-form-subject="<?php _e('Pytanie o cenę', 'chtheme'); ?>"><span style="font-size: 0.7em; color: #3e4d6b; text-transform: uppercase;"><?php _e('Zapytaj o cenę', 'chtheme'); ?></span></a>
											<?php endif; ?>
										</p>
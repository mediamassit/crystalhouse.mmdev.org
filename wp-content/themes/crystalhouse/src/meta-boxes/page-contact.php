<?php

function register_pagecontact_meta_boxes($meta_boxes) {
    if(!class_exists('RW_Meta_Box') ) return;

	// Gallery
    $meta_boxes = array(
		array(
			'title' => __('Formularz', 'chtheme'),
			'pages' => array('page'),
			'context' => 'normal',
			'priority' => 'high',
			'fields' => array(
				array(
					'name'  => '',
					'id'    => "contact_form",
					'type'  => 'textarea',
				)
			)
		),
		array(
			'title' => __('Mapa Google', 'chtheme'),
			'pages' => array('page'),
			'context' => 'normal',
			'priority' => 'high',
			'fields' => array(
				array(
					'name'  => __('Adres mapy', 'chtheme'),
					'id'    => "gmap_address",
					'type'  => 'text',
				),
				array(
					'name'  => __('Koordynaty GPS (lat,lng)', 'chtheme'),
					'id'    => "gmap_latlng",
					'type'  => 'text',
					'desc'  => __('Własność pola "Adres mapy" zostanie zignorowana.', 'chtheme')
				),
				array(
					'name'  => __('Przybliżenie mapy (zoom)', 'chtheme'),
					'id'    => "gmap_zoom",
					'type'  => 'number',
					'min'   => 0
				),
				array(
					'name'  => __('Tytuł markera', 'chtheme'),
					'id'    => "gmap_marker_title",
					'type'  => 'text'
				)
			)
		)
	);

	if(post_has_template('page-templates/page-contact.php')) {
		foreach($meta_boxes as $meta_box) {
			new RW_Meta_Box($meta_box);
		}
	}
}
add_action('admin_init', 'register_pagecontact_meta_boxes');
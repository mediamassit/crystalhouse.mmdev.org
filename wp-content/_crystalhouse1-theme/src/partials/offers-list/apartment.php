						<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
										<?php get_template_part('src/partials/offers-list/slots/thumbnail'); ?>
									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="<?php echo get_permalink($post->ID); ?>"><?php the_title(); ?></a></h4>

										<div class="row fields">
											<?php
												$fields = chtof_fields(array(
													'offer_location_city' => '%s',
													'offer_location_street' => '%s',
													'offer_property_area-primary' => 'chtof_area_formatter',
													'offer_location_district' => '%s',
													'offer_rooms_rooms' => '%s',
													'offer_rooms_bedrooms' => '%s',
													'offer_property_built-year' => '%s'
												));

												$chunks = array_slice(array_chunk($fields, 3, true), 0, 2);

												foreach($chunks as $chunk) :
											?>
											<ul class="col-md-6">
											<?php foreach($chunk as $key => $value) : ?>
												<li><strong><?php echo chtof_label($key); ?>:</strong> <?php echo chtof_value($value, $key); ?></li>
											<? endforeach; ?>
											</ul>
											<?php endforeach; ?>
										</div>
										<!-- .row -->

										<div class="row description">
											<p><?php echo get_the_excerpt(); ?></p>
											<?php get_template_part('src/partials/offers-list/slots/attributes'); ?>
										</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong><?php echo chtof_label('offer_asari_listing_id'); ?>:</strong> <?php echo get_field('offer_asari_listing_id'); ?> 
											<?php edit_post_link(__('Edytuj ofertę', 'chtheme'), '&nbsp;&nbsp;'); ?></p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
										<?php get_template_part('src/partials/offers-list/slots/price'); ?>
									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

							<?php get_template_part('src/partials/offers-list/slots/side'); ?>
						</div>
						<!-- .offer -->
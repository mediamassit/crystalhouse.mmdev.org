<?php

function register_pageclipboard_meta_boxes($meta_boxes) {
    if(!class_exists('RW_Meta_Box') ) return;

	// Gallery
    $meta_boxes = array(
		array(
			'title' => __('Formularz', 'chtheme'),
			'pages' => array('page'),
			'context' => 'normal',
			'priority' => 'high',
			'fields' => array(
				array(
					'name'  => '',
					'id'    => "contact_form",
					'type'  => 'textarea',
					'rows'	=> 15
				)
			)
		)
	);

	if(post_has_template('page-templates/page-clipboard.php')) {
		foreach($meta_boxes as $meta_box) {
			new RW_Meta_Box($meta_box);
		}
	}
}
add_action('admin_init', 'register_pageclipboard_meta_boxes');
					<div class="widget offers-list-filter">
						<h2><?php _e('Znajdź nieruchomość', 'chtheme'); ?></h2>
						<form method="get" action="<?php echo chtof_remf_url(); ?>" name="offers_filter" data-form-values='{"offer": "buy", "type": "lot", "country": "Polska"}'>
							<?php include locate_template('src/partials/filters/slots/q.php'); ?>
							<?php include locate_template('src/partials/filters/slots/keyword.php'); ?>
							<?php include locate_template('src/partials/filters/slots/currency.php'); ?>

							<div data-group="offer_type_lot">
								<?php chtof_get_slot('src/partials/filters/slots/lot_type'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/area'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/area_lot', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/price'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/price_m2'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/province'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/city'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/district'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/center_distance'); ?>

								<div class="form-group text-center">
									<?php get_template_part('src/partials/filters/slots/submit'); ?>
								</div>
								<!-- .form-group -->
							</div>
							<!-- [data-group] -->
						</form>
					</div>
					<!-- .widget.offers-list-filter -->
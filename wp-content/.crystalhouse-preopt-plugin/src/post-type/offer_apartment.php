<?php

function offer_apartment_custom_post_type() {

	$labels = array(
		'name'                => _x( 'Apartamenty', 'Post Type General Name', 'chtheme' ),
		'singular_name'       => _x( 'Apartament', 'Post Type Singular Name', 'chtheme' ),
		'menu_name'           => __( 'Apartamenty', 'chtheme' ),
		'parent_item_colon'   => __( 'Nadrzędny:', 'chtheme' ),
		'all_items'           => __( 'Wszystkie', 'chtheme' ),
		'view_item'           => __( 'Zobacz', 'chtheme' ),
		'add_new_item'        => __( 'Dodaj nowy apartament', 'chtheme' ),
		'add_new'             => __( 'Dodaj nowy', 'chtheme' ),
		'edit_item'           => __( 'Edytuj apartament', 'chtheme' ),
		'update_item'         => __( 'Aktualizuj apartament', 'chtheme' ),
		'search_items'        => __( 'Szukaj', 'chtheme' ),
		'not_found'           => __( 'Nie odnaleziono', 'chtheme' ),
		'not_found_in_trash'  => __( 'Nie odnaleziono w koszu', 'chtheme' ),
	);
	$rewrite = array(
		'slug'                => 'apartment',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'apartament', 'chtheme' ),
		'description'         => '',
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
		'taxonomies'          => array( 'offer_category', 'offer_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-admin-network',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);
	register_post_type( 'offer_apartment', $args );
	add_post_type_support( 'offer_apartment', 'excerpt' );
}
add_action( 'init', 'offer_apartment_custom_post_type', 0 );
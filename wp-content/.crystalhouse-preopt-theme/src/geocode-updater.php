<?php

set_time_limit(0);

define('WP_USE_THEMES', false);
global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header;
require('../../../../wp-load.php');

if(@$_GET['role'] !== 'admin') die('Access denied.');

$posts = new WP_Query(array(
	'posts_per_page' => -1,
	'post_type' => array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business')/*,
	'meta_query' => array(
		array('key' => 'offer_location_latlng', 'value' => ' ')
	)*/
));

// Update geolocation if needed
/*$log = fopen('./_geocodes.txt', 'w');

if($posts->have_posts()) :
	$i = 1;

	while($posts->have_posts()) : $posts->the_post();
		$lat_lng = get_field('offer_location_latlng');
		$address = cht_get_offer_gmap_address($post);

		if(strlen(trim($lat_lng)) <= 0 && strlen($address) > 0) :
			$location = cht_get_gmap_location($address);

			if(@is_array(@$location)) {
				$new_value = @$location['lat'].', '.@$location['lng'];
				update_field('field_538f218b53068', $new_value, $post->ID);
				update_field('field_538f218b53068', $new_value, pll_get_post($post->ID, 'en'));
			}

			usleep(1500000);
		endif;

		fwrite($log, sprintf('%s. %s', $i, get_the_title()) . "\r\n");
		++$i;
	endwhile;
endif;

fclose($log);*/

?><html>
<head>
	<meta charset="utf-8">
	<title>Geocode updater</title>
	<style>
body, table {
	font-family: Arial, sans-serif;
	font-size: 13px;
	color: #444;
	text-align: left;
	border-spacing: 0;
    border-collapse: separate;
}

body {
	background: #e5e5e5;
}

table {
	width: 80%;
	max-width: 1280px;
	margin: 100px auto;
	background: #fff;
	box-shadow: 1px 1px 12px rgba(0,0,0,0.1);
}

td, th {
	padding: 12px 20px;
	border-bottom: 1px solid #ddd;
}

th {
	border-bottom: 2px solid #bbb;
}
	</style>
</head>
<body>
<table>
	<tr>
		<th>#</th>
		<th>Title</th>
		<th>Post type</th>
		<th>LatLng</th>
		<th>Address</th>
	</tr>
<?php if($posts->have_posts()) : ?>
	<?php
		$i = 1;

		while($posts->have_posts()) : $posts->the_post();
			$lat_lng = get_field('offer_location_latlng');
			
			if(strlen(trim($lat_lng)) <= 0) :
	?>
	<tr>
		<td><?php echo $i; ?></td>
		<td><?php echo the_title(); ?> (<a href="<?php echo get_permalink($post->ID); ?>">v</a> / <a href="<?php echo admin_url('post.php?post='.$post->ID.'&action=edit'); ?>">e</a>)</td>
		<td><?php echo $post->post_type; ?></td>
		<td style="white-space: nowrap;"><?php var_export(get_field('offer_location_latlng')); ?></td>
		<td><?php echo cht_get_offer_gmap_address($post); ?></td>
	<tr>
	<?php
				++$i;
			endif;
		endwhile;
	?>
<?php endif; ?>
</table>
</body>
</html>
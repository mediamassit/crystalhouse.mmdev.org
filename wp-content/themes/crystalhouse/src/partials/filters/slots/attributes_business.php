								<div class="form-group" data-form-fieldset="attributes" data-form-fieldset-more="attributes_business"<?php if($disabled) : ?> style="display: none;"<?php endif; ?>>
									<label><?php echo chtof_label('offer_attributes'); ?></label>
									<div class="row attributes">
										<div class="col-xs-6">
											<?php
												$key = 'has-tenant';
												$label = chtof_label('offer_business_premise-has-tenant');
											?>
											<label><input type="checkbox" name="attr[]" value="<?php echo $key; ?>"<?php if(@in_array($key, @$_GET['attr'])) : ?> checked="checked"<?php endif; ?>> <em><img src="<?php bloginfo('template_url'); ?>/images/icons/offer/<?php echo $key; ?>.png" alt="<?php echo $key; ?>"></em> <?php echo $label; ?></label>
										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->
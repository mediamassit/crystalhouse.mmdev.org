<?php

class Asari_Log {

	protected $_directory;
	protected $_createdAt;
	protected $_prefix = 'log';
	protected $_log = array();

	public function __construct($prefix = null) {
		$this->_createdAt = time();
		$this->_directory = Asari::dir('logs');

		if(!is_writeable($this->_directory)) {
			throw new RuntimeException('Plugin logs directory is not writeable');
		}

		if(!is_null($prefix)) {
			$this->_prefix = (string) $prefix;
		}
	}

	public function load($filename) {
		$path = $this->_directory.'/'.$filename;

		if(!file_exists($path)) {
			return null;
		}

		if(!($file = @file_get_contents($path))) {
			return;
		}

		$lines = explode("\r\n", $file);

		foreach($lines as $line) {
			$parts = explode(' | ', $line);
			$this->_log[] = array(
				'time' => @strtotime(@$parts[0]),
				'message' => @$parts[1]
			);
		}

		$this->_createdAt = null; // TODO
		$this->_prefix = reset(explode('_', $filename));
	}

	public function log($message) {
		$this->_log[] = array(
			'time' => time(),
			'message' => (string) $message
		);

		return $this->save();
	}

	public function remove() {
		$filename = $this->getFilename();

		return unlink($this->_directory.'/'.$filename);
	}

	public function getMessages() {
		return $this->_log;
	}

	public function getLastMessage() {
		return end($this->_log);
	}

	public function getFilename() {
		return sprintf('%s_%s.log', $this->_prefix, date('Y-m-d_H-i-s', $this->_createdAt));
	}

	protected function save() {
		$filename = $this->getFilename();

		if(@file_put_contents($this->_directory.'/'.$filename, $this->getFileContents()) === false) {
			throw new RuntimeException(sprintf('Cannot save log to "%s" file', $filename));
		}

		return true;
	}

	protected function getFileContents() {
		$lines = array();

		foreach($this->_log as $message) {
			$lines[] = sprintf('%s | %s', date('Y/m/d H:i:s', $message['time']), $message['message']);
		}

		return implode("\r\n", $lines);
	}

}
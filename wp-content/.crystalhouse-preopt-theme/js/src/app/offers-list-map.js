(function($) {
	var defaults = {
			markers: [],
			map: {
				options: {
					zoom: 13,
					center: new google.maps.LatLng(52.290822, 20.955107),
					disableDefaultUI: true
				}
			}
		},
		methods = {
			init: function(options) {
				var options = $.extend(true, {}, defaults, options);

				return this.each(function() {
					// Initiate map
					$(this).find('.map').bind('init-map', function() {
						if($(this).is('.loaded')) {
							return false;
						}

						$(this).html('');
						$(this).gmap3({
							map: options.map,
							marker: {
								values: options.markers,
								options: {
									icon: THEME_URL+"/images/icons/gmap-pin.png"
								},
								events: {
									click: function(marker, event, context) {
										var overlay = $(this).gmap3({ get: { name:"overlay" } });
										var icons = {
											default: THEME_URL+"/images/icons/gmap-pin.png",
											active: THEME_URL+"/images/icons/gmap-pin-active.png"
										};

										// Hide overlay and set default icon
										if(overlay) {
											if(marker.position === overlay.getPosition() && !$(overlay.getDOMElement()).is(':hidden')) {
												marker.setIcon(icons.default);
												overlay.hide();
												return;
											}

											$(this).gmap3({ get: { name:"marker", all: true, callback: function(markers) {
												$.each(markers, function(i, marker){
													marker.setIcon(icons.default);
												});
											}}});
											overlay.hide();
										}

										// Show overlay and set active icon
										marker.setIcon(icons.active);
										var new_overlay = $(this).gmap3({
											overlay: {
												latLng: marker.position,
												options: {
													content: context.data.content,
													offset: {
														y: -32,
														x: 20
													}
												}
											}
										});

										var $self = $(this);
										var o = $(this).gmap3({ get: { name:"overlay" } });

										// Close infobox
										$(o.getDOMElement()).find('[data-closeinfobox]').click(function() {
											$self.gmap3({ get: { name:"marker", all: true, callback: function(markers) {
												$.each(markers, function(i, marker){
													marker.setIcon(icons.default);
												});
											}}});
											o.hide();
										});
									}
								}
							}
						});

						$(this).addClass('loaded');
					});

					// Setup open map
					$('[data-open-gmap]').each(function() {
						var $map = $($(this).attr('data-open-gmap'));

						if($map.length <= 0) return true;

						$(this).prop('label-close', $(this).attr('data-close-label'));
						$(this).prop('label-open', $(this).text());

						$(this).click(function() {
							var $map = $($(this).attr('data-open-gmap'));
							var $self = $(this);

							if(!$map.is('.closed')) {
								$map.slideUp(400, 'easeInOutQuart', function() {
									$map.addClass('closed');
									$self.text($self.prop('label-open'));
								});
							}
							else {
								$map.find('.map').trigger('init-map');
								$map.slideDown(400, 'easeInOutQuart', function() {
									$map.removeClass('closed');
									$self.text($self.prop('label-close'));
								});
							}

							return false;
						});
					});
				});
			}
	};

	// Offers list map widget
	$.fn.offersListMap = function(method) {
		if(typeof $.fn.gmap3 !== 'function') {
			$.error('gmap3 plugin was not found');
		}

		if(methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if(typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		}
		else {
			$.error('Method '+method+' does not exist on jQuery.offersListMap');
		}
	};
})(jQuery);
<?php

class Cht_Image_Watermark {
	protected $_path;
	protected $_image;

	/**
	 * Watermark given image.
	 * @param string $path
	 */
	public function __construct($path) {
		$this->_path = (string) $path;
		$this->_image = $this->loadImage($this->_path);
		$this->_watermark = $this->loadWatermark();
	}

	/**
	 * Adds watermark to loaded image.
	 */
	public function apply() {
		$margin = array(15, 15);

		imagecopy($this->_image, $this->_watermark, imagesx($this->_image)-imagesx($this->_watermark)-$margin[0], imagesy($this->_image)-imagesy($this->_watermark)-$margin[1], 0, 0, imagesx($this->_watermark), imagesy($this->_watermark));
	}

	/**
	 * Loads given image.
	 * @param string $path
	 * @return resource
	 */
	protected function loadImage($path) {
		$extension = $this->getExtension($path);

		switch($extension) {
			default:
				return @imagecreatefromjpeg($path);
				break;
			case 'png':
				return @imagecreatefrompng($path);
				break;
			case 'gif':
				return @imagecreatefromgif($path);
				break;
		}
	}

	/**
	 * Returns image.
	 * @return resource
	 */
	public function getImage() {
		return $this->_image;
	}

	/**
	 * Loads watermark image.
	 * @return resource
	 */
	protected function loadWatermark() {
		return $this->loadImage(get_template_directory().'/images/watermark.png');
	}

	/**
	 * Returns watermark image.
	 * @return resource
	 */
	public function getWatermark() {
		return $this->_watermark;
	}

	/**
	 * Saves image to given path.
	 * @param string $path
	 * @return bool
	 */
	public function saveImageTo($path) {
		switch($this->getExtension($this->_path)) {
			default:
				return imagejpeg($this->_image, $path);
				break;
			case 'png':
				return imagepng($this->_image, $path);
				break;
			case 'gif':
				return imagegif($this->_image, $path);
				break;
		}
	}

	/**
	 * Extract file extension from given path.
	 * @param string $path
	 * @return string
	 */
	protected function getExtension($path) {
		return @end(@explode('.', $path));
	}
}


/**
 * Returns URL to watermarked version of given image (generates image if needed).
 * @param string $url
 * @return string
 */
function cht_watermark_image($url) {
	$wp_path = '.';
	$path = preg_replace('#^'.preg_quote(get_bloginfo('wpurl')).'#', $wp_path, $url);

	$dirname = dirname($path);
	$filename = basename($path);
	$name = @reset(@explode('.', $filename));
	$extension = @end(@explode('.', $filename));

	// Generate URL and path
	$watermarked_filename = sprintf('%sw.%s', $name, $extension);
	$watermarked_path = sprintf('%s/%s', $dirname, $watermarked_filename);
	$watermarked_url = preg_replace('#'.preg_quote($filename).'$#', $watermarked_filename, $url);

	if(!file_exists($watermarked_path)) {
		$iw = new Cht_Image_Watermark($path);
		$iw->apply();
		$save = $iw->saveImageTo($watermarked_path);

		if(!$save) {
			return $url;
		}
	}

	return $watermarked_url;
}
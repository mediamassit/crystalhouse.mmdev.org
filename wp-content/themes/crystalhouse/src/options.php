<?php

if (!class_exists('Redux_Framework_chtheme_config')) {

    class Redux_Framework_chtheme_config {

        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if (!class_exists('ReduxFramework')) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if (  true == Redux_Helpers::isTheme(__FILE__) ) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }

        }

        public function initSettings() {

            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            //$this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        public function setSections() {
			/**
			 * Content
			 */
			$this->sections[] = array(
				'title' => __('Treść', 'chtheme'),
				'icon' => ' el-icon-wrench',
				'fields' => array(
					array(
						'id' => 'opt-theme-headline',
						'type' => 'text',
						'title' => __('Nagłówek', 'chtheme')
					)/*,
					array(
						'id' => 'opt-theme-clipboard-page',
						'type' => 'select',
						'title' => __('Strona schowka', 'chtheme'),
						'data' => 'pages',
						'args' => array(
							'numberposts' => -1,
							'parent' => 0
						)
					)*/
				)
			);

			/**
			 * Contact data
			 */
			$this->sections[] = array(
				'title' => __('Kontakt', 'chtheme'),
				'icon' => 'el-icon-envelope',
				'fields' => array(
					array(
						'id' => 'opt-contact-address',
						'type' => 'textarea',
						'rows' => 3,
						'title' => __('Adres', 'chtheme')
					),
					array(
						'id' => 'opt-contact-phone',
						'type' => 'textarea',
						'rows' => 2,
						'title' => __('Numer telefonu', 'chtheme'),
						'subtitle' => __('Kolejne numery w nowej linii.', 'chtheme')
					),
					array(
						'id' => 'opt-contact-email',
						'type' => 'text',
						'title' => __('Adres email', 'chtheme'),
						'validate' => 'email'
					),
					array(
						'id' => 'opt-contact-skype',
						'type' => 'text',
						'title' => __('Skype', 'chtheme')
					),
					array(
						'id' => 'section-socials',
						'type' => 'section',
						'title' => __('Serwisy społecznościowe', 'chtheme'),
						'indent' => false
                    ),
						array(
							'id' => 'opt-contact-socials-pinterest',
							'type' => 'text',
							'title' => __('Pinterest', 'chtheme'),
							'validate' => 'url'
						),
						array(
							'id' => 'opt-contact-socials-facebook',
							'type' => 'text',
							'title' => __('Facebook', 'chtheme'),
							'validate' => 'url'
						),
						array(
							'id' => 'opt-contact-socials-google_plus',
							'type' => 'text',
							'title' => __('Google+', 'chtheme'),
							'validate' => 'url'
						),
						array(
							'id' => 'opt-contact-socials-twitter',
							'type' => 'text',
							'title' => __('Twitter', 'chtheme'),
							'validate' => 'url'
						),
						array(
							'id' => 'opt-contact-socials-youtube',
							'type' => 'text',
							'title' => __('Youtube', 'chtheme'),
							'validate' => 'url'
						),
					array(
						'id' => 'section-socials',
						'type' => 'section',
						'indent' => false,
						'required' => array('section-media-checkbox', "=", 1),
					)
				)
			);

			/**
			 * Subdomains
			 */
			$domain = cht_get_domain();
			$this->sections[] = array(
				'title' => __('Subdomeny', 'chtheme'),
				'icon' => ' el-icon-link',
				'fields' => array(
					array(
						'id' => 'opt-subdomains',
						'type' => 'textarea',
						'rows' => 10,
						'title' => __('Mapa', 'chtheme'),
						'desc' => '<strong>W formacie:</strong><br><em>subdomena : sciezka<br>subdomena2 : sciezka2</em><br><br><strong>Przykład</strong>:<br><em>zespol : nasz-zespol</em><br><br><small>skieruje</small> "<span style="color: red;"><strong>zespol</strong></span>.'.$domain.'" <small>na</small> "'.$domain.'/<span style="color: red;"><strong>nasz-zespol</strong></span>"'
					)
				)
			);

			/**
			 * Google maps
			 */
			$domain = cht_get_domain();
			$this->sections[] = array(
				'title' => __('Mapy Google', 'chtheme'),
				'icon' => ' el-icon-map-marker',
				'fields' => array(
					array(
						'id' => 'opt-gmap-single',
						'type' => 'checkbox',
						'title' => __('Widok oferty', 'chtheme'), 
						'subtitle' => __('Odznaczenie skutkuje usunięciem mapy z widoku oferty danego typu.', 'chtheme'),
						'options' => array(
							'offer_apartment' => __('Apartament', 'chtheme'),
							'offer_house' => __('Dom', 'chtheme'),
							'offer_lot' => __('Działka', 'chtheme'),
							'offer_business' => __('Oferta komercyjna', 'chtheme')
						),
						'default'  => array(
							'offer_apartment' => '1',
							'offer_house' => '1',
							'offer_lot' => '1',
							'offer_business' => '1'
						)
					),
					array(
						'id' => 'opt-gmap-archive',
						'type' => 'checkbox',
						'title' => __('Widok listy ofert', 'chtheme'), 
						'subtitle' => __('Odznaczenie skutkuje usunięciem oferty danego typu z mapy nieruchomości w listach ofert.', 'chtheme'),
						'options' => array(
							'offer_apartment' => __('Apartament', 'chtheme'),
							'offer_house' => __('Dom', 'chtheme'),
							'offer_lot' => __('Działka', 'chtheme'),
							'offer_business' => __('Oferta komercyjna', 'chtheme')
						),
						'default'  => array(
							'offer_apartment' => '1',
							'offer_house' => '1',
							'offer_lot' => '1',
							'offer_business' => '1'
						)
					),
					/*array(
						'id' => 'section-gmap-address_pattern',
						'type' => 'section',
						'title' => __('Wzorce adresów dla markerów mapy Google', 'chtheme'),
						'subtitle' => __('<strong>Dostępne zmienne:</strong> %country <em>kraj</em> &nbsp;&middot;&nbsp; %province <em>województwo</em> &nbsp;&middot;&nbsp; %city <em>miasto</em> &nbsp;&middot;&nbsp; %street <em>ulica</em> &nbsp;&middot;&nbsp; %district <em>dzielnica</em>', 'chtheme'),
						'indent' => false
                    ),
						array(
							'id' => 'opt-gmap-address_pattern-apartment',
							'type' => 'text',
							'title' => __('Apartament', 'chtheme'),
							'default' => '%country, %city, %street'
						),
						array(
							'id' => 'opt-gmap-address_pattern-house',
							'type' => 'text',
							'title' => __('Dom', 'chtheme'),
							'default' => '%country, %city, %street'
						),
						array(
							'id' => 'opt-gmap-address_pattern-lot',
							'type' => 'text',
							'title' => __('Działka', 'chtheme'),
							'default' => '%country, %city, %street'
						),
						array(
							'id' => 'opt-gmap-address_pattern-business',
							'type' => 'text',
							'title' => __('Oferta komercyjna', 'chtheme'),
							'default' => '%country, %city, %street'
						),
					array(
						'id' => 'section-gmap-address_pattern',
						'type' => 'section',
						'indent' => false
                    ),*/
				)
			);

			/**
			 * Contact forms
			 */
			/*$fields = array();
			$contact_forms = array(
				'opt-theme-cf7-static-page-footer' => __('Stopka strony statycznej', 'chtheme'),
				'opt-theme-cf7-offer-broker' => __('Kontakt z agentem', 'chtheme'),
				'opt-theme-cf7-offer-seek' => __('Poszukuję', 'chtheme'),
				'opt-theme-cf7-offer-submit' => __('Zgłaszam', 'chtheme'),
			);
			$pll_langs = pll_languages_list();

			foreach($contact_forms as $key => $label) {
				foreach($pll_langs as $lang) {
					$fields[] = array(
						'id' => $key.'-'.$lang,
						'type' => 'select',
						'title' => strtoupper($lang).': '.$label,
						'data' => 'posts',
						'args' => array(
							'post_type' => array('wpcf7_contact_form'),
							'numberposts' => -1
						)
					);
				}
			}

			$this->sections[] = array(
				'title' => __('Formularze', 'chtheme'),
				'icon' => ' el-icon-check',
				'fields' => $fields
			);

			unset($fields, $contact_forms, $pll_langs);*/

			/**
			 * System info
			 */
			session_start();

			$fields = array();
			$fields[] = array(
				'id' => 'opt-sys___',
				'type' => 'raw',
				'title' => '',
				'content' => '<table class="form-table"><tbody>'
			);

			if(session_id()) {
				$fields[] = array(
					'id' => 'opt-sys___',
					'type' => 'raw',
					'title' => '',
					'content' => sprintf('<tr><th scope="row"><div class="redux_field_th">%s:</div></th><td> <td><fieldset>%s</fieldset></td></tr>', __('Sesja PHP', 'chtheme'), session_id())
				);
			}

			//$exchange_rates = chp_get_nbp_exchange_rates();
			$rates = '';

			if(sizeof($exchange_rates) > 0) {
				foreach($exchange_rates as $label => $rate) {
					if(in_array($label, array('USD', 'EUR'), true)) {
						$rates .= sprintf('<strong>%s:</strong> <em>%s</em>; ', $label, $rate);
					}
				}

				date_default_timezone_set(get_option('timezone_string')); // CHECK IF IT WON'T MESS OTHER DATES

				$rates_update_time = chp_get_nbp_exchange_rates_update_time();
				$rates .= sprintf('<br><small>(ostatnia aktualizacja: %s minut temu<small> - %s</small>)</small>', ceil((time()-$rates_update_time)/60), date('d/m/Y H:i:s', $rates_update_time));
			}

			$fields[] = array(
				'id' => 'opt-sys___',
				'type' => 'raw',
				'title' => '',
				'content' => sprintf('<tr><th scope="row"><div class="redux_field_th">%s:</div></th><td> <td><fieldset>%s</fieldset></td></tr>', __('Kursy walut', 'chtheme'), strlen($rates) > 0 ? $rates : __('b/d', 'chtheme'))
			);

			$fields[] = array(
				'id' => 'opt-sys___',
				'type' => 'raw',
				'title' => '',
				'content' => '</tbody></table>'
			);

			$this->sections[] = array(
				'title' => __('Systemowe', 'chtheme'),
				'icon' => ' el-icon-dashboard',
				'fields' => $fields
			);
		}

        public function setHelpTabs() {
        }

        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'          => 'chtheme_options',            // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
                'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
                'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
                'menu_title'        => __('Witryna', 'chtheme'),
                'page_title'        => __('Ustawienia witryny', 'chtheme'),
                
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' => '', // Must be defined to add google fonts to the typography module
                
                'async_typography'  => false,                    // Use a asynchronous font on the front end or font string
                'admin_bar'         => true,                    // Show the panel pages on the admin bar
                'global_variable'   => '',                      // Set a different name for your global variable other than the opt_name
                'dev_mode'          => false,                    // Show the time the page took to load, etc
                'customizer'        => true,                    // Enable basic customizer support
                
                // OPTIONAL -> Give you extra features
                'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
                'menu_icon'         => 'dashicons-admin-appearance',                      // Specify a custom URL to an icon
                'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
                'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
                'page_slug'         => '_options',              // Page slug used to denote the panel
                'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
                'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
                'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.
                
                // CAREFUL -> These options are for advanced use only
                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
                
                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'system_info'           => false, // REMOVE

                // HINTS
                'hints' => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'         => 'light',
                        'shadow'        => true,
                        'rounded'       => false,
                        'style'         => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show'          => array(
                            'effect'        => 'slide',
                            'duration'      => '500',
                            'event'         => 'mouseover',
                        ),
                        'hide'      => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
                        ),
                    ),
                )
            );
        }

    }
    
    global $reduxConfig;
    $reduxConfig = new Redux_Framework_chtheme_config();
}

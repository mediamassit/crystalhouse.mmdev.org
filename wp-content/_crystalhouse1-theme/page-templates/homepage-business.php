
<?php
/**
 * Template Name: Homepage - Komercja
 */
	get_header();

	$image_slider = rwmb_meta('home_image_slider');

	$form_filters = cht_get_form_filters(true);
?>

	<div id="home-theatre">
		<div class="viewport">
			<a href="javascript:void(0);" class="prev"></a>
			<a href="javascript:void(0);" class="next"></a>
			<div class="dots"><ul></ul></div>

			<div class="slides">
				<?php echo do_shortcode(strip_newlines($image_slider)); ?>
			</div>
			<!-- .slides -->
		</div>
		<!-- .viewport -->
	</div>
	<!-- #home-theatre -->

	<section class="widget offers-home-filters">
		<div class="container">
			<form method="get" action="<?php echo get_post_type_archive_link('offer_business'); ?>" name="offers_filter" data-is-extendable="1" data-form-values='{"type": "business", "country": "Polska"}'>
				<div class="row search-query">
					<div class="col-md-12">
						<label><?php echo chofl_input_text('q', array('placeholder' => __('wpisz nr oferty, miasto, ulicę', 'chtheme'))); ?></label>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3 side">
						<fieldset class="offer-type inline-radios" data-form-fieldset="offer">
							<label><?php _e('Chcę kupić', 'chtheme'); ?> <input type="radio" name="offer" value="buy" checked="checked"></label>
							<label><?php _e('Chcę wynająć', 'chtheme'); ?> <input type="radio" name="offer" value="rent"></label>
						</fieldset>
						<fieldset class="offer-currency" data-form-fieldset="currency">
							<label><?php _e('Waluta', 'chtheme'); ?></label>
							<?php
								echo chofl_select('currency', array_combine(chofl_currencies(false), chofl_currencies(false)));
							?>
						</fieldset>
					</div>
					<!-- .col-md-3 -->

					<div class="col-md-9 fields">
						<div class="col-md-4" data-form-fieldset="business_type">
							<fieldset>
								<label><?php echo chtof_label('offer_business_type'); ?></label>
								<?php
									$business_types = get_field_object(chtof_id('offer_business_type'));
									$business_types = $business_types['choices'];

									echo chofl_select('business_type', array_merge(array(
										'' => __('Wszystkie', 'chtheme')
									), $business_types));
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="premise_type" style="display: none;">
							<fieldset>
								<label><?php echo chtof_label('offer_business_premise-type'); ?></label>
								<?php
									$business_types = get_field_object(chtof_id('offer_business_premise-type'));
									$business_types = $business_types['choices'];

									echo chofl_select('business_premise_type', array_merge(array(
										'' => __('Wszystkie', 'chtheme')
									), $business_types), null, array('disabled' => 'disabled'));
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="city">
							<fieldset>
								<label><?php echo chtof_label(chtof_id('offer_location_city'), true); ?></label>
								<?php
									echo chofl_select('city', /*array(
										'' => __('Wszystkie', 'chtheme')
									) + */$form_filters['locations']['city']['default'], null, $disabled ? array('disabled' => 'disabled') : array());
								?>
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="area">
							<fieldset class="row">
								<label class="col-md-12"><span data-type-label="default"><?php echo chtof_label('offer_property_area-primary'); ?></span> <span data-type-label="house" style="display: none;"><?php _e('Powierzchnia domu', 'chtheme'); ?></span></label>
								<div class="col-xs-6">
									<?php
										echo chofl_select('area_from', $form_filters['area']['default']['default']['m2']['from']);
									?>
								</div>
								<!-- .col-md-6 -->

								<div class="col-xs-6">
									<?php
										echo chofl_select('area_to', $form_filters['area']['default']['default']['m2']['from']);
									?>
								</div>
								<!-- .col-md-6 -->
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4" data-form-fieldset="price">
							<fieldset class="row">
								<label class="col-md-12"><?php echo pll_current_language() !== 'pl' ? __('Zakres cen', 'chtheme') : chtof_label(chtof_id('offer_price_primary'), true); ?></label>
								<div class="col-xs-6">
									<?php
										echo chofl_select('price_from', $form_filters['price']['default']['buy']['pln']['from']);
									?>
								</div>
								<!-- .col-md-6 -->

								<div class="col-xs-6">
									<?php
										echo chofl_select('price_to', $form_filters['price']['default']['buy']['pln']['to']);
									?>
								</div>
								<!-- .col-md-6 -->
							</fieldset>
						</div>
						<!-- .col-md-4 -->

						<div class="col-md-4 pull-right submit">
							<fieldset class="row">
								<div class="col-md-6">
									<label>&nbsp;</label>
									<input type="submit" value="<?php _e('Szukaj', 'chtheme'); ?>" class="btn search">
								</div>
								<!-- .col-md-6 -->
							</fieldset>
						</div>
						<!-- .col-md-4 -->
					</div>
					<!-- .fields -->
				</div>

				<input type="hidden" name="type" value="business">
			</form>
		</div>
		<!-- .container -->
	</section>
	<!-- .offers-filter.home -->

	<?php echo do_shortcode(strip_newlines($post->post_content)); ?>

<?php get_footer(); ?>
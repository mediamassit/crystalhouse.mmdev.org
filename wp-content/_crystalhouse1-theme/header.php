<?php

$clipboard_page_id = pll_get_post(CLIPBOARD_PAGE_ID);
$search_page_id = pll_get_post(642);

?><!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php wp_title(' - ', true, 'right'); ?><?php bloginfo('title'); ?></title>
	<link rel="stylesheet" media="screen,print" href="<?php bloginfo('template_url'); ?>/css/style.css">
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico"/>
	<?php wp_head(); ?>
	<script>
	var THEME_URL = "<?php bloginfo('template_url'); ?>",
		SITE_DOMAIN = '<?php echo cht_get_domain(); ?>',
		WISHLIST_API_GET = '<?php echo get_permalink($clipboard_page_id); ?>?ajax&get',
		WISHLIST_API_PUT = '<?php echo get_permalink($clipboard_page_id); ?>?ajax&put',
		WISHLIST_API_REMOVE = '<?php echo get_permalink($clipboard_page_id); ?>?ajax&remove',
		WISHLIST_API_CLEAR = '<?php echo get_permalink($clipboard_page_id); ?>?ajax&clear',
		WISHLIST_API_SAVE = '<?php echo get_permalink($clipboard_page_id); ?>?ajax&save',
		addthis_config = {
			"ui_language": "<?php echo pll_current_language(); ?>"
		},
		FANCYBOX_LANG = {
			error: '<p class="fancybox-error"><?php _e('Nie udało się załadować.<br>Spróbuj ponownie.', 'chtheme'); ?></p>',
			closeBtn: '<a title="<?php _e('Zamknij', 'chtheme'); ?>" class="fancybox-item fancybox-close" href="javascript:;"></a>',
			next: '<a title="<?php _e('Następny', 'chtheme'); ?>" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
			prev: '<a title="<?php _e('Poprzedni', 'chtheme'); ?>" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
		};
	</script>
	<style>
	.widget.offers-home-filters form .submit .closer {
		display: none;
		text-align: right;
	}

		.widget.offers-home-filters form .submit .closer a {
			margin-top: 14px;
			color: #fff;
			font-weight: 700;
			font-size: 1em;
		}

	.widget.offers-home-filters form.extended .submit .closer {
		display: block;
	}

	.widget.offers-home-filters form.extended .submit .expander {
		display: none;
	}

	.widget.contact-broker figure img {
		max-width: 180px;
	}

	.widget.offers-featured .offer.page figure figcaption {
		width: auto;
		font-weight: 400;
		background: #5fbcce;
		bottom: 10px;
		left: auto;
		right: 0;
	}

	.widget.carousel-gallery .viewport {
		/*max-width: 800px;*/
		max-height: 450px;
		margin: 0 auto;
	}

	.widget.carousel-gallery .slides figure img {
		width: auto;
	}

	/*.widget.contact-broker figure {
		width: 80%;
	}

		.widget.contact-broker figure img {
			width: 100%;
		}
	
	@media (max-width: 992px) {
		.widget.contact-broker figure {
			width: auto;
		}

			.widget.contact-broker figure img {
				width: auto;
			}
	}*/

	#top .main-navigation ul li {
		margin-right: 2.15%;
	}

	@media (max-width: 1280px) {
		#top .main-navigation ul li {
			margin-right: 1.95%;
		}
	}

	@media (max-width: 1220px) {
		#top .main-navigation ul li {
			margin-right: 0.9%;
		}
	}

	@media (max-width: 1100px) {
		#top .main-navigation ul li {
			margin-right: 0;
		}
	}

	@media (max-width: 768px) {
		section.page.team .team-member figure {
			width: auto;
			display: block;
		}

		section.page.team .team-member figure img {
			width: auto;
		}
	}

	@media (max-width: 480px) {
		section.page.team .team-member figure {
			float: none;
		}
	}
	</style>
</head>

<body data-scrollto-append="100">

	<div id="cookie-notifier">
		<p><?php printf(pll__('cookie_notifier'), get_bloginfo('home')); ?> <a href="javascript:void(0);" class="close-x" data-close-cookie-notifier></a></p>
	</div>

	<div id="top" data-sticky-height="100" class="hidden-print">
		<div class="header">
			<div class="container">
				<div class="col-md-2 logo">
					<h1><a href="<?php echo pll_home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="<?php bloginfo('title'); ?>"></a></h1>
				</div>
				<!-- .logo -->

				<div class="col-md-10 right">
					<nav class="tray">
						<ul class="contact">
							<?php if(has_opt('opt-contact-phone')) : ?><li><span class="hidden-md">T:</span> <a href="tel:<?php echo reset(explode("\r\n", get_opt('opt-contact-phone'))); ?>"><?php echo get_opt('opt-contact-phone'); ?></a></li><?php endif; ?>
							<li><span class="hidden-md">E:</span> <a href="mailto:<?php echo get_opt('opt-contact-email'); ?>"><?php echo get_opt('opt-contact-email'); ?></a></li>
							<li><a href="skype:<?php echo get_opt('opt-contact-skype'); ?>"><em class="icon-skype"></em> <span class="hidden-md"><?php _e('Call me', 'chtheme'); ?></span></a></li>
							<li><a href="#" data-toggle="modal" data-target="#modal-contact"><em class="icon-message"></em> <span class="hidden-md"><?php _e('Wyślij wiadomość', 'chtheme'); ?></span></a></li>
							<li></li>
						</ul>
						<!-- .contact -->

						<ul class="switch">
							<li class="currency">
								<a href="javascript:void(0);" data-toggle="dropdown"><?php echo chofl_currency(); ?></a>
								<ul class="options" role="menu">
									<?php foreach(chofl_currencies(true) as $currency) : ?>
									<li><a href="<?php echo chtof_mod_url(array('_set_session_currency' => $currency)); ?>"><?php echo $currency; ?></a></li>
									<?php endforeach; ?>
								</ul>
							</li>
							<li class="language">
								<?php
									$langs = pll_the_languages(array('raw' => 1, 'force_home' => 0));
									$current_lang = null;

									foreach($langs as $key => $lang) {
										if($lang['current_lang'] === true) {
											$current_lang = $lang;
											unset($langs[$key]);
										}
									}
								?>
								<a href="javascript:void(0);" data-toggle="dropdown"><img src="<?php bloginfo('template_url'); ?>/images/icons/flags/<?php echo $current_lang['slug']; ?>.png" alt=""> <?php echo $current_lang['slug']; ?></a>
								<ul class="options" role="menu">
									<?php foreach($langs as $lang) : ?>
									<li><a href="<?php echo $lang['url']; ?>"><img src="<?php bloginfo('template_url'); ?>/images/icons/flags/<?php echo $lang['slug']; ?>.png" alt=""> <?php echo $lang['slug']; ?></a></li>
									<?php endforeach; ?>
								</ul>
							</li>
						</ul>
						<!-- .switch -->
					</nav>
					<!-- .tray -->

					<div class="middle">
						<h2><?php echo get_opt('opt-theme-headline'); ?></h2>

						<div class="side">
							<form method="get" action="<?php echo get_permalink($search_page_id); ?>" class="search-form">
								<input type="text" name="q" placeholder="<?php _e('wpisz nr oferty, miasto, ulicę', 'chtheme'); ?>">
								<input type="submit" value="<?php _e('Szukaj', 'chtheme'); ?>">
							</form>
							<!-- .search-form -->

							<?php $clipboard = chp_clipboard_get_offers(); ?>
							<div class="clipboard<?php if(is_object($clipboard) && $clipboard->found_posts <= 0) : ?> empty<?php endif; ?>">
								<a href="<?php echo get_permalink($clipboard_page_id); ?>" data-toggle="wishlist"><span><?php _e('Schowek', 'chtheme'); ?></span> <em><?php if(is_object($clipboard) && $clipboard->found_posts > 0) : ?><?php echo $clipboard->found_posts; ?><?php endif; ?></em></a>

								<div class="widget wishlist-dropdown">
									<h3><?php _e('Twoje oferty', 'chtheme'); ?></h3>

									<section class="offers">
									<?php
										if(is_object($clipboard) && $clipboard->have_posts()) :
											while($clipboard->have_posts()) :
												$clipboard->the_post();
									?>
										<?php get_template_part('src/partials/clipboard-offer'); ?>
									<?php
											endwhile;
											wp_reset_query();
										endif;
									?>
									</section>

									<div class="toolbar">
										<ul>
											<li><a href="javascript:void(0);" data-clearclipboard="1"><em class="icon-close"></em> <?php _e('Wyczyść schowek', 'chtheme'); ?></a></li>
											<li><a href="javascript:void(0);" data-saveclipboard="1"><em class="icon-download"></em> <?php _e('Zapisz schowek', 'chtheme'); ?></a></li>
											<li class="more"><a href="<?php echo get_permalink($clipboard_page_id); ?>"><?php _e('Chcę obejrzeć', 'chtheme'); ?></a></li>
										</ul>
									</div>
								</div>
								<!-- .widget.wishlist-popup -->
							</div>
							<!-- .clipboard -->
						</div>
						<!-- .side -->
					</div>
					<!-- .middle -->
				</div>
				<!-- .right -->

				<div class="col-md-8 mobile-right">
					<div class="row">
						<ul class="switch">
							<li class="currency">
								<a href="javascript:void(0);" data-toggle="dropdown"><?php echo chofl_currency(); ?></a>
								<ul class="options" role="menu">
									<?php foreach(chofl_currencies(true) as $currency) : ?>
									<li><a href="<?php echo chtof_mod_url(array('_set_session_currency' => $currency)); ?>"><?php echo $currency; ?></a></li>
									<?php endforeach; ?>
								</ul>
							</li>
							<li class="language">
								<?php
									$langs = pll_the_languages(array('raw' => 1, 'force_home' => 0));
									$current_lang = null;

									foreach($langs as $key => $lang) {
										if($lang['current_lang'] === true) {
											$current_lang = $lang;
											unset($langs[$key]);
										}
									}
								?>
								<a href="javascript:void(0);" data-toggle="dropdown"><img src="<?php echo $current_lang['flag']; ?>" alt=""> <?php echo $current_lang['slug']; ?></a>
								<ul class="options" role="menu">
									<?php foreach($langs as $lang) : ?>
									<li><a href="<?php echo $lang['url']; ?>"><img src="<?php echo $lang['flag']; ?>" alt=""> <?php echo $lang['slug']; ?></a></li>
									<?php endforeach; ?>
								</ul>
							</li>
							<li class="language">
								<a href="<?php echo get_permalink($clipboard_page_id); ?>" style="background-image: none; width: auto;"><?php _e('Schowek', 'chtheme'); ?></a>
							</li>
						</ul>
						<!-- .switch -->
					</div>
					<!-- .row -->

					<div class="row">
						<?php wp_nav_menu(array('theme_location' => 'main-menu', 'walker' => new cht_Main_Navigation_Select_Walker, 'items_wrap' => '<form method="get" action="#"><select onchange="if(this.value) window.location.href=this.value">%3$s</select></form>')); ?>
					</div>
					<!-- .row -->
				</div>
				<!-- .mobile-right -->
			</div>
			<!-- .container -->
		</div>
		<!-- .header -->

		<nav class="main-navigation<?php if(strpos(@basename(get_page_template()), 'homepage') !== 0) : ?> static<?php endif; ?>">
			<div class="container">
				<?php wp_nav_menu(array('theme_location' => 'main-menu', 'walker' => new cht_Main_Navigation_Walker)); ?>
			</div>
			<!-- .container -->
		</nav>
		<!-- .main-navigation -->
	</div>
	<!-- #top -->

	<?php get_template_part('src/partials/modals/modal-clipboard'); ?>
	<?php get_template_part('src/partials/modals/modal-clipboard-saved'); ?>
	<?php get_template_part('src/partials/modals/modal-clipboard-clean'); ?>
	<?php get_template_part('src/partials/modals/modal-contact'); ?>
	<?php get_template_part('src/partials/modals/modal-contact-offer'); ?>
	<?php get_template_part('src/partials/modals/modal-contact-subject'); ?>
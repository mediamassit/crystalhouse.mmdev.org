<?php

function cht_cf7() {
	wpcf7_add_shortcode('broker_email', 'wpcf7_broker_email_shortcode_handler', true);
	wpcf7_add_shortcode('offer_id', 'wpcf7_offer_id_shortcode_handler', true);
	wpcf7_add_shortcode('clipboard_offers', 'wpcf7_clipboard_shortcode_handler', true);
}
add_action('init', 'cht_cf7', 11);

function wpcf7_broker_email_shortcode_handler($tag) {
	global $post;

	$broker = get_field('offer_broker', $post->ID);

	if($broker && strlen(trim($broker['user_email'])) > 0) {
		return '<input type="hidden" name="broker_email" value="'.$broker['ID'].'" readonly="readonly">';
	}

	return '';
}

function wpcf7_offer_id_shortcode_handler($tag) {
	global $post;

	if(strlen(get_field('offer_asari_listing_id', $post->ID)) > 0) {
		return '<input type="hidden" name="offer_id" value="'.get_field('offer_asari_listing_id', $post->ID).'" readonly="readonly">';
	}

	return '';
}

function wpcf7_clipboard_shortcode_handler($tag) {
	$offers = chp_clipboard_get();

	foreach($offers as $key => $value) {
		$offers[$key] = get_field('offer_asari_listing_id', (int) $value);
	}

	if(sizeof($offers) > 0) {
		return '<input type="hidden" name="clipboard_offers" value="'.implode(', ', $offers).'" readonly="readonly">';
	}

	return '';
}

function wpcf7_broker_email_replace() {
	if(isset($_POST['broker_email'])) {
		$user = get_userdata((int) $_POST['broker_email']);
		$_POST['broker_email'] = $user->user_email;
	}
}
add_action('init', 'wpcf7_broker_email_replace');
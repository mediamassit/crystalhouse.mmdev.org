<?php

function get_filter_numeric_meta_query($value) {
	if($value{0} === '>') {

	}
	elseif($value{0} === '<') {

	}
	elseif($value{0} === '>=') {

	}
	elseif($value{0} === '<=') {

	}
}

function cht_get_offers($filters) {
	$post_type = array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business');

	$args = array(
		'numberposts' => 10,
		'post_type' => $post_type,
		'meta_query' => array(
			'relation' => 'AND',
				array(
					'key' => 'location',
					'value' => 'Melbourne',
					'compare' => 'LIKE'
				),
				array(
					'key' => 'location',
					'value' => 'Sydney',
					'compare' => 'LIKE'
				)
		)
	);

	// Price
	if() {
		$filters['price']['offer_price_primary'];
	}
 
	return new WP_Query($args);
			'offer_price_primary' => 'field_5388ebd0b8a18',
			'offer_price_m2' => 'field_538f1948a5930',
			'offer_property_area-primary' => 'field_5388eb80b8a17',
			'offer_flag' => 'field_5388eecc36b44',
			'offer_location_street' => 'field_5388ea267b006',
			'offer_location_district' => 'field_5388ea0a7b005',
			'offer_location_city' => 'field_5388e9f37b004',
			'offer_location_province' => 'field_5388e9bb7b003',
			'offer_location_center-distance' => 'field_538f210253067',
			'offer_asari_listing_id' => 'field_538a4d5314c87',
			'offer_attributes' => 'field_5388edc1294f0',
			'offer_property_built-year' => 'field_538a4c47d8d63',
			'offer_property_floors-floor' => 'field_538a4c5ad8d64',
			'offer_property_floors-all' => 'field_538a4c73d8d65',
			'offer_property_area-lot' => 'field_5388f9aab8011',
			'offer_lot-type' => 'field_5388f5a5a78d4',

			'offer_investment' => 'field_5388f727a9637',
			'offer_investment_developer' => 'field_5388f80da9639',
			'offer_investment_completion-date' => 'field_5388f824a963a',

			'offer_business_type' => 'field_5388fada52fbf',
			'offer_business_premise-type' => 'field_5388fb6452fc0',
			'offer_business_premise-has-tenant' => 'field_5388fbc152fc1',

			'offer_rooms_rooms' => 'field_5388f8b2472dd',
			'offer_rooms_bathrooms' => 'field_5388f8cc472de',

			'offer_market_type' => 'field_5388f4dade353'
}
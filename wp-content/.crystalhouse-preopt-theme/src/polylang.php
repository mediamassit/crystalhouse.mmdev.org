<?php

function chtheme_polylang() {
	// Register ACF fields labels
	$fields = chtof_all_fields();

	foreach($fields as $name => $id) {
		pll_register_string($id, chtof_label($id, true), 'plugin ACF');

		// Register choices
		$field = get_field_object($id);

		if(@is_array(@$field['choices'])) {
			foreach($field['choices'] as $value => $label) {
				pll_register_string($id.' '.$value, $label, 'plugin ACF');
			}
		}
	}

	// Register cookie notifier string
	pll_register_string('cookie_notifier', 'cookie_notifier', 'chtheme');
}
add_action('admin_init', 'chtheme_polylang');
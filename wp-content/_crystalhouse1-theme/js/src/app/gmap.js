(function($) {
	var defaults = {
			markers: [],
			map: {
				zoom: 15,
				center: new google.maps.LatLng(52.2315717, 20.8898496),
				styles: [
					{"stylers":
						[
							{ "hue": "#005eff" },
							{ "lightness": 13 }
						]
					}
				]
			}
		},
		maps = [],
		methods = {
			init: function(options) {
				var options = $.extend(true, {}, defaults, options);

				return this.each(function() {
					var elm = $(this).get(0);

					function init() {
						var map = new google.maps.Map(elm, options.map);

						for(var i = 0; i < options.markers.length; ++i) {
							options.markers[i] = $.extend(true, {}, {
								position: options.map.center,
								icon: '/images/spotlight-poi-blue.png',
								map: map
							}, options.markers[i]);

							options.markers[i].object = new google.maps.Marker(options.markers[i]);
						}

						maps.push({
							element: elm,
							map: map
						});
					}

					google.maps.event.addDomListener(window, 'load', init);
				});
			},
			maps: function() {
				return maps;
			}
		};

	// GoogleMap plugin
	$.fn.gmap = function(method) {
		if(typeof google !== 'object' || typeof google.maps !== 'object') {
			$.error('Google Maps API was not found');
		}

		if(methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if(typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		}
		else {
			$.error('Method '+method+' does not exist on jQuery.gmap');
		}
	};

})(jQuery);
							<div class="form-group" data-form-fieldset="measure">
								<div class="row">
									<div class="col-md-4"><label style="margin-top: 2px;"><?php _e('Miara', 'chtheme'); ?></label></div>
									<div class="col-md-8">
										<div class="inline-checkboxes currencies">
										<?php
											foreach(array('sqm' => 'm<sup>2</sup>', 'sqft' => 'sqft') as $value => $label) :
												$active_measure = 'sqm';

												if(isset($_GET['measure'])) {
													$active_measure = @$_GET['measure'];
												}
										?>
											<label><input type="radio" name="measure" value="<?php echo $value; ?>"<?php if($value == $active_measure) : ?> checked="checked"<?php endif; ?>> <?php echo $label; ?></label>
										<?php endforeach; ?>
										</div>
										<!-- .inline-checkboxes -->
									</div>
								</div>
								<!-- .row -->
							</div>
							<!-- .form-group -->
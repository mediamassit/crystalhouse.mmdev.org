var __filters = {
	"locations": {
		"country": {
			"__placeholder": "Wszystkie",
			"Polska": "Polska",
			"Hiszpania": "Hiszpania",
			"Francja": "Francja",
			"polska": "polska",
			"Bu\u0142garia": "Bu\u0142garia",
			"France": "France"
		},
		"province": {
			"Polska": {
				"__placeholder": "Wszystkie",
				"Dolno\u015bl\u0105skie": "Dolno\u015bl\u0105skie",
				"Kujawsko-Pomorskie": "Kujawsko-Pomorskie",
				"Lubelskie": "Lubelskie",
				"\u0141\u00f3dzkie": "\u0141\u00f3dzkie",
				"Ma\u0142opolskie": "Ma\u0142opolskie",
				"Mazowickie": "Mazowickie",
				"mazowieckie": "mazowieckie",
				"Mazowieckie": "Mazowieckie",
				"Pomorskie": "Pomorskie",
				"Warmi\u0144sko-Mazurskie": "Warmi\u0144sko-Mazurskie",
				"Zachodniopomorskie": "Zachodniopomorskie"
			},
			"Hiszpania": {
				"__placeholder": "Wszystkie"
			},
			"Francja": {
				"__placeholder": "Wszystkie"
			},
			"polska": {
				"__placeholder": "Wszystkie",
				"pomorskie": "pomorskie"
			},
			"Bu\u0142garia": {
				"__placeholder": "Wszystkie"
			},
			"France": {
				"__placeholder": "Wszystkie"
			},
			"": {
				"__placeholder": "Wszystkie",
				"Mazowieckie": "Mazowieckie"
			}
		},
		"city": {
			"Mazowieckie": {
				"__placeholder": "Wszystkie",
				"Bielawa": "Bielawa",
				"Chojn\u00f3w": "Chojn\u00f3w",
				"Chyliczki": "Chyliczki",
				"Dziekan\u00f3w Polski": "Dziekan\u00f3w Polski",
				"Habdzin": "Habdzin",
				"Horn\u00f3wek": "Horn\u00f3wek",
				"Izabelin": "Izabelin",
				"Jastrz\u0119bie": "Jastrz\u0119bie",
				"J\u00f3zefos\u0142aw": "J\u00f3zefos\u0142aw",
				"J\u00f3zef\u00f3w": "J\u00f3zef\u00f3w",
				"Kampinos": "Kampinos",
				"K\u0119pa Oborska": "K\u0119pa Oborska",
				"Koby\u0142ka": "Koby\u0142ka",
				"Koczargi Stare": "Koczargi Stare",
				"Komor\u00f3w": "Komor\u00f3w",
				"Konstancin-Jeziorna": "Konstancin-Jeziorna",
				"Kotorydz": "Kotorydz",
				"Lasocin": "Lasocin",
				"Lipk\u00f3w": "Lipk\u00f3w",
				"\u0141omianki": "\u0141omianki",
				"Magdalenka": "Magdalenka",
				"Marki": "Marki",
				"Micha\u0142owice": "Micha\u0142owice",
				"Nadarzyn": "Nadarzyn",
				"Natolin": "Natolin",
				"Obory": "Obory",
				"Otwock": "Otwock",
				"P\u0119cice Ma\u0142e": "P\u0119cice Ma\u0142e",
				"Piaseczno": "Piaseczno",
				"Podkowa Le\u015bna": "Podkowa Le\u015bna",
				"Podkowa-Le\u015bna": "Podkowa-Le\u015bna",
				"Pruszk\u00f3w": "Pruszk\u00f3w",
				"Radzymin": "Radzymin",
				"Regu\u0142y": "Regu\u0142y",
				"Rusiec": "Rusiec",
				"Serock": "Serock",
				"S\u0119kocin Nowy": "S\u0119kocin Nowy",
				"S\u0119kocin-Las": "S\u0119kocin-Las",
				"Stara Wie\u015b": "Stara Wie\u015b",
				"Stare Babice": "Stare Babice",
				"Strzembowo": "Strzembowo",
				"Walend\u00f3w": "Walend\u00f3w",
				"Warszawa": "Warszawa",
				"Z\u0105bki": "Z\u0105bki",
				"Zielonka": "Zielonka",
				"\u017b\u00f3\u0142win": "\u017b\u00f3\u0142win"
			},
			"": {
				"__placeholder": "Wszystkie",
				" Le Castellet": " Le Castellet",
				"Cannes": "Cannes",
				"Costa Blanca": "Costa Blanca",
				"Costa Del Sol": "Costa Del Sol",
				"Costa Del Sol, Marbella": "Costa Del Sol, Marbella",
				"Cote d'Azur": "Cote d'Azur",
				"Le Castellet": "Le Castellet",
				"Nicea": "Nicea",
				"Rado\u015b\u0107": "Rado\u015b\u0107",
				"S\u0142oneczny Brzeg": "S\u0142oneczny Brzeg",
				"S\u0142oneczny Brzeg Sunny Beach": "S\u0142oneczny Brzeg Sunny Beach",
				"Warszawa": "Warszawa"
			},
			"Warmi\u0144sko-Mazurskie": {
				"__placeholder": "Wszystkie",
				"Kruklanki": "Kruklanki",
				"Kruty\u0144": "Kruty\u0144",
				"Ksi\u0105\u017cnik": "Ksi\u0105\u017cnik",
				"Lisewo": "Lisewo",
				"Skop": "Skop"
			},
			"Pomorskie": {
				"__placeholder": "Wszystkie",
				"Gda\u0144sk": "Gda\u0144sk",
				"Gdynia": "Gdynia",
				"Hel": "Hel",
				"Jastarnia": "Jastarnia",
				"Jurata": "Jurata",
				"\u0141eba": "\u0141eba",
				"Wejherowo": "Wejherowo",
				"W\u0142adys\u0142awowo": "W\u0142adys\u0142awowo"
			},
			"pomorskie": {
				"__placeholder": "Wszystkie",
				"Gda\u0144sk": "Gda\u0144sk"
			},
			"Mazowickie": {
				"__placeholder": "Wszystkie",
				"Komor\u00f3w": "Komor\u00f3w"
			},
			"Ma\u0142opolskie": {
				"__placeholder": "Wszystkie",
				"Ko\u015bcielisko": "Ko\u015bcielisko",
				"Krak\u00f3w": "Krak\u00f3w",
				"Lanckorona": "Lanckorona",
				"Raciborsko": "Raciborsko",
				"Zakopane": "Zakopane",
				"Zawada": "Zawada"
			},
			"Kujawsko-Pomorskie": {
				"__placeholder": "Wszystkie",
				"Toru\u0144": "Toru\u0144"
			},
			"Lubelskie": {
				"__placeholder": "Wszystkie",
				"Do\u0142hobycz\u00f3w": "Do\u0142hobycz\u00f3w",
				"Kazimierz Dolny": "Kazimierz Dolny"
			},
			"\u0141\u00f3dzkie": {
				"__placeholder": "Wszystkie",
				"\u0141\u00f3d\u017a": "\u0141\u00f3d\u017a"
			},
			"Dolno\u015bl\u0105skie": {
				"__placeholder": "Wszystkie",
				"Lw\u00f3wek \u015al\u0105ski": "Lw\u00f3wek \u015al\u0105ski",
				"Zgorzelec": "Zgorzelec"
			},
			"Zachodniopomorskie": {
				"__placeholder": "Wszystkie",
				"Ko\u0142obrzeg": "Ko\u0142obrzeg"
			},
			"mazowieckie": {
				"__placeholder": "Wszystkie",
				"Warszawa": "Warszawa"
			},
			"default": {
				"__placeholder": "Wszystkie",
				" Le Castellet": " Le Castellet",
				"Bielawa": "Bielawa",
				"Cannes": "Cannes",
				"Chojn\u00f3w": "Chojn\u00f3w",
				"Chyliczki": "Chyliczki",
				"Costa Blanca": "Costa Blanca",
				"Costa Del Sol": "Costa Del Sol",
				"Costa Del Sol, Marbella": "Costa Del Sol, Marbella",
				"Cote d'Azur": "Cote d'Azur",
				"Do\u0142hobycz\u00f3w": "Do\u0142hobycz\u00f3w",
				"Dziekan\u00f3w Polski": "Dziekan\u00f3w Polski",
				"Gda\u0144sk": "Gda\u0144sk",
				"Gdynia": "Gdynia",
				"Habdzin": "Habdzin",
				"Hel": "Hel",
				"Horn\u00f3wek": "Horn\u00f3wek",
				"Izabelin": "Izabelin",
				"Jastarnia": "Jastarnia",
				"Jastrz\u0119bie": "Jastrz\u0119bie",
				"J\u00f3zefos\u0142aw": "J\u00f3zefos\u0142aw",
				"J\u00f3zef\u00f3w": "J\u00f3zef\u00f3w",
				"Jurata": "Jurata",
				"Kampinos": "Kampinos",
				"Kazimierz Dolny": "Kazimierz Dolny",
				"K\u0119pa Oborska": "K\u0119pa Oborska",
				"Koby\u0142ka": "Koby\u0142ka",
				"Koczargi Stare": "Koczargi Stare",
				"Ko\u0142obrzeg": "Ko\u0142obrzeg",
				"Komor\u00f3w": "Komor\u00f3w",
				"Konstancin-Jeziorna": "Konstancin-Jeziorna",
				"Ko\u015bcielisko": "Ko\u015bcielisko",
				"Kotorydz": "Kotorydz",
				"Krak\u00f3w": "Krak\u00f3w",
				"Kruklanki": "Kruklanki",
				"Kruty\u0144": "Kruty\u0144",
				"Ksi\u0105\u017cnik": "Ksi\u0105\u017cnik",
				"Lanckorona": "Lanckorona",
				"Lasocin": "Lasocin",
				"Le Castellet": "Le Castellet",
				"Lipk\u00f3w": "Lipk\u00f3w",
				"Lisewo": "Lisewo",
				"Lw\u00f3wek \u015al\u0105ski": "Lw\u00f3wek \u015al\u0105ski",
				"\u0141eba": "\u0141eba",
				"\u0141omianki": "\u0141omianki",
				"\u0141\u00f3d\u017a": "\u0141\u00f3d\u017a",
				"Magdalenka": "Magdalenka",
				"Marki": "Marki",
				"Micha\u0142owice": "Micha\u0142owice",
				"Nadarzyn": "Nadarzyn",
				"Natolin": "Natolin",
				"Nicea": "Nicea",
				"Obory": "Obory",
				"Otwock": "Otwock",
				"P\u0119cice Ma\u0142e": "P\u0119cice Ma\u0142e",
				"Piaseczno": "Piaseczno",
				"Podkowa Le\u015bna": "Podkowa Le\u015bna",
				"Podkowa-Le\u015bna": "Podkowa-Le\u015bna",
				"Pruszk\u00f3w": "Pruszk\u00f3w",
				"Raciborsko": "Raciborsko",
				"Rado\u015b\u0107": "Rado\u015b\u0107",
				"Radzymin": "Radzymin",
				"Regu\u0142y": "Regu\u0142y",
				"Rusiec": "Rusiec",
				"Serock": "Serock",
				"S\u0119kocin Nowy": "S\u0119kocin Nowy",
				"S\u0119kocin-Las": "S\u0119kocin-Las",
				"Skop": "Skop",
				"S\u0142oneczny Brzeg": "S\u0142oneczny Brzeg",
				"S\u0142oneczny Brzeg Sunny Beach": "S\u0142oneczny Brzeg Sunny Beach",
				"Stara Wie\u015b": "Stara Wie\u015b",
				"Stare Babice": "Stare Babice",
				"Strzembowo": "Strzembowo",
				"Toru\u0144": "Toru\u0144",
				"Walend\u00f3w": "Walend\u00f3w",
				"Warszawa": "Warszawa",
				"Wejherowo": "Wejherowo",
				"W\u0142adys\u0142awowo": "W\u0142adys\u0142awowo",
				"Zakopane": "Zakopane",
				"Zawada": "Zawada",
				"Z\u0105bki": "Z\u0105bki",
				"Zgorzelec": "Zgorzelec",
				"Zielonka": "Zielonka",
				"\u017b\u00f3\u0142win": "\u017b\u00f3\u0142win"
			}
		},
		"district": {
			"Konstancin-Jeziorna": {
				"__placeholder": "Wszystkie",
				"Chylice": "Chylice",
				"Klarysew": "Klarysew",
				"Konstancin": "Konstancin",
				"Kr\u00f3lewska G\u00f3ra": "Kr\u00f3lewska G\u00f3ra"
			},
			"Bielawa": {
				"__placeholder": "Wszystkie"
			},
			"Costa Del Sol, Marbella": {
				"__placeholder": "Wszystkie"
			},
			"Cannes": {
				"__placeholder": "Wszystkie"
			},
			"Skop": {
				"__placeholder": "Wszystkie"
			},
			"Otwock": {
				"__placeholder": "Wszystkie"
			},
			"Warszawa": {
				"__placeholder": "Wszystkie",
				"Anin": "Anin",
				"Bemowo": "Bemowo",
				"Bia\u0142o\u0142\u0119ka": "Bia\u0142o\u0142\u0119ka",
				"Bielany": "Bielany",
				"Konstancin": "Konstancin",
				"Mokot\u00f3w": "Mokot\u00f3w",
				"Ochota": "Ochota",
				"Powi\u015ble": "Powi\u015ble",
				"Praga-P\u00f3\u0142noc": "Praga-P\u00f3\u0142noc",
				"Praga-Po\u0142udnie": "Praga-Po\u0142udnie",
				"\u015ar\u00f3dmie\u015bcie": "\u015ar\u00f3dmie\u015bcie",
				"Targ\u00f3wek": "Targ\u00f3wek",
				"Ursus": "Ursus",
				"Ursyn\u00f3w": "Ursyn\u00f3w",
				"Wawer": "Wawer",
				"Weso\u0142a": "Weso\u0142a",
				"Wilan\u00f3w": "Wilan\u00f3w",
				"Wola": "Wola",
				"WOLA": "WOLA",
				"W\u0142ochy": "W\u0142ochy",
				"\u017boliborz": "\u017boliborz"
			},
			"Pruszk\u00f3w": {
				"__placeholder": "Wszystkie"
			},
			"K\u0119pa Oborska": {
				"__placeholder": "Wszystkie"
			},
			"Gdynia": {
				"__placeholder": "Wszystkie"
			},
			"Komor\u00f3w": {
				"__placeholder": "Wszystkie"
			},
			"Lipk\u00f3w": {
				"__placeholder": "Wszystkie"
			},
			"Gda\u0144sk": {
				"__placeholder": "Wszystkie"
			},
			"S\u0119kocin Nowy": {
				"__placeholder": "Wszystkie"
			},
			"Piaseczno": {
				"__placeholder": "Wszystkie",
				"Zalesie Dolne": "Zalesie Dolne"
			},
			"Lasocin": {
				"__placeholder": "Wszystkie"
			},
			"Nicea": {
				"__placeholder": "Wszystkie"
			},
			"Marki": {
				"__placeholder": "Wszystkie"
			},
			"Jurata": {
				"__placeholder": "Wszystkie"
			},
			"S\u0142oneczny Brzeg Sunny Beach": {
				"__placeholder": "Wszystkie"
			},
			"Z\u0105bki": {
				"__placeholder": "Wszystkie"
			},
			"Natolin": {
				"__placeholder": "Wszystkie"
			},
			"Podkowa Le\u015bna": {
				"__placeholder": "Wszystkie"
			},
			"Kampinos": {
				"__placeholder": "Wszystkie"
			},
			" Le Castellet": {
				"__placeholder": "Wszystkie"
			},
			"Nadarzyn": {
				"__placeholder": "Wszystkie"
			},
			"Walend\u00f3w": {
				"__placeholder": "Wszystkie"
			},
			"Cote d'Azur": {
				"__placeholder": "Wszystkie"
			},
			"Zakopane": {
				"__placeholder": "Wszystkie"
			},
			"J\u00f3zefos\u0142aw": {
				"__placeholder": "Wszystkie"
			},
			"\u0141eba": {
				"__placeholder": "Wszystkie"
			},
			"Izabelin": {
				"__placeholder": "Wszystkie"
			},
			"Toru\u0144": {
				"__placeholder": "Wszystkie"
			},
			"Kazimierz Dolny": {
				"__placeholder": "Wszystkie"
			},
			"Chojn\u00f3w": {
				"__placeholder": "Wszystkie"
			},
			"Magdalenka": {
				"__placeholder": "Wszystkie"
			},
			"J\u00f3zef\u00f3w": {
				"__placeholder": "Wszystkie",
				"Michalin": "Michalin"
			},
			"Lanckorona": {
				"__placeholder": "Wszystkie"
			},
			"Radzymin": {
				"__placeholder": "Wszystkie"
			},
			"Stare Babice": {
				"__placeholder": "Wszystkie"
			},
			"Raciborsko": {
				"__placeholder": "Wszystkie"
			},
			"Micha\u0142owice": {
				"__placeholder": "Wszystkie"
			},
			"Chyliczki": {
				"__placeholder": "Wszystkie"
			},
			"Do\u0142hobycz\u00f3w": {
				"__placeholder": "Wszystkie"
			},
			"S\u0119kocin-Las": {
				"__placeholder": "Wszystkie"
			},
			"Koby\u0142ka": {
				"__placeholder": "Wszystkie"
			},
			"Serock": {
				"__placeholder": "Wszystkie"
			},
			"Costa Del Sol": {
				"__placeholder": "Wszystkie"
			},
			"\u0141\u00f3d\u017a": {
				"__placeholder": "Wszystkie"
			},
			"\u0141omianki": {
				"__placeholder": "Wszystkie",
				"D\u0105browa Le\u015bna": "D\u0105browa Le\u015bna"
			},
			"Hel": {
				"__placeholder": "Wszystkie"
			},
			"Ko\u015bcielisko": {
				"__placeholder": "Wszystkie"
			},
			"Stara Wie\u015b": {
				"__placeholder": "Wszystkie"
			},
			"Lw\u00f3wek \u015al\u0105ski": {
				"__placeholder": "Wszystkie"
			},
			"Regu\u0142y": {
				"__placeholder": "Wszystkie"
			},
			"Horn\u00f3wek": {
				"__placeholder": "Wszystkie"
			},
			"Kruty\u0144": {
				"__placeholder": "Wszystkie"
			},
			"Zawada": {
				"__placeholder": "Wszystkie"
			},
			"Obory": {
				"__placeholder": "Wszystkie"
			},
			"Ksi\u0105\u017cnik": {
				"__placeholder": "Wszystkie"
			},
			"P\u0119cice Ma\u0142e": {
				"__placeholder": "Wszystkie"
			},
			"Jastrz\u0119bie": {
				"__placeholder": "Wszystkie"
			},
			"Le Castellet": {
				"__placeholder": "Wszystkie"
			},
			"Podkowa-Le\u015bna": {
				"__placeholder": "Wszystkie"
			},
			"Krak\u00f3w": {
				"__placeholder": "Wszystkie"
			},
			"Wejherowo": {
				"__placeholder": "Wszystkie"
			},
			"Lisewo": {
				"__placeholder": "Wszystkie"
			},
			"Rusiec": {
				"__placeholder": "Wszystkie"
			},
			"Zielonka": {
				"__placeholder": "Wszystkie"
			},
			"Habdzin": {
				"__placeholder": "Wszystkie"
			},
			"Kruklanki": {
				"__placeholder": "Wszystkie"
			},
			"Dziekan\u00f3w Polski": {
				"__placeholder": "Wszystkie"
			},
			"Ko\u0142obrzeg": {
				"__placeholder": "Wszystkie"
			},
			"Strzembowo": {
				"__placeholder": "Wszystkie"
			},
			"Costa Blanca": {
				"__placeholder": "Wszystkie"
			},
			"Kotorydz": {
				"__placeholder": "Wszystkie"
			},
			"Koczargi Stare": {
				"__placeholder": "Wszystkie"
			},
			"\u017b\u00f3\u0142win": {
				"__placeholder": "Wszystkie"
			},
			"Rado\u015b\u0107": {
				"__placeholder": "Wszystkie"
			},
			"S\u0142oneczny Brzeg": {
				"__placeholder": "Wszystkie"
			},
			"W\u0142adys\u0142awowo": {
				"__placeholder": "Wszystkie",
				"Ch\u0142apowo": "Ch\u0142apowo"
			},
			"Jastarnia": {
				"__placeholder": "Wszystkie",
				"Jurata": "Jurata"
			},
			"Zgorzelec": {
				"__placeholder": "Wszystkie",
				"Ujazd": "Ujazd"
			}
		}
	},
	"price": {
		"default": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"_": "5.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD"
					},
					"to": {
						"__placeholder": "Do",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"_": "1.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"_": "1.250.000+ EUR"
					}
				}
			},
			"rent_short": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"100": "100 PLN",
						"200": "200 PLN",
						"300": "300 PLN",
						"400": "400 PLN",
						"500": "500 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"100": "100 PLN",
						"200": "200 PLN",
						"300": "300 PLN",
						"400": "400 PLN",
						"500": "500 PLN",
						"_": "500+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"20": "20 USD",
						"40": "40 USD",
						"60": "60 USD",
						"80": "80 USD",
						"100": "100 USD"
					},
					"to": {
						"__placeholder": "Do",
						"20": "20 USD",
						"40": "40 USD",
						"60": "60 USD",
						"80": "80 USD",
						"100": "100 USD",
						"_": "100+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"25": "25 EUR",
						"50": "50 EUR",
						"75": "75 EUR",
						"100": "100 EUR",
						"125": "125 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"25": "25 EUR",
						"50": "50 EUR",
						"75": "75 EUR",
						"100": "100 EUR",
						"125": "125 EUR",
						"_": "125+ EUR"
					}
				}
			},
			"rent_long": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"3000": "3.000 PLN",
						"4000": "4.000 PLN",
						"5000": "5.000 PLN",
						"10000": "10.000 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"3000": "3.000 PLN",
						"4000": "4.000 PLN",
						"5000": "5.000 PLN",
						"10000": "10.000 PLN",
						"_": "10.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"500": "500 USD",
						"750": "750 USD",
						"1000": "1.000 USD",
						"2000": "2.000 USD"
					},
					"to": {
						"__placeholder": "Do",
						"500": "500 USD",
						"750": "750 USD",
						"1000": "1.000 USD",
						"2000": "2.000 USD",
						"_": "2.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"750": "750 EUR",
						"1000": "1.000 EUR",
						"1250": "1.250 EUR",
						"2500": "2.500 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"750": "750 EUR",
						"1000": "1.000 EUR",
						"1250": "1.250 EUR",
						"2500": "2.500 EUR",
						"_": "2.500+ EUR"
					}
				}
			}
		},
		"apartment": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"20000000": "20.000.000 PLN",
						"_": "20.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD"
					},
					"to": {
						"__placeholder": "Do",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"8000000": "8.000.000 USD",
						"_": "8.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"5000000": "5.000.000 EUR",
						"_": "5.000.000+ EUR"
					}
				}
			}
		},
		"house": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"15000000": "15.000.000 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"15000000": "15.000.000 PLN",
						"20000000": "20.000.000 PLN",
						"_": "20.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"6000000": "6.000.000 USD"
					},
					"to": {
						"__placeholder": "Do",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"6000000": "6.000.000 USD",
						"8000000": "8.000.000 USD",
						"_": "8.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"3500000": "3.500.000 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"3500000": "3.500.000 EUR",
						"5000000": "5.000.000 EUR",
						"_": "5.000.000+ EUR"
					}
				}
			}
		},
		"lot": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"_": "10.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD"
					},
					"to": {
						"__placeholder": "Do",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"_": "1.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"_": "2.500.000+ EUR"
					}
				}
			}
		},
		"business": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"15000000": "15.000.000 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"15000000": "15.000.000 PLN",
						"20000000": "20.000.000 PLN",
						"_": "20.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"6000000": "6.000.000 USD"
					},
					"to": {
						"__placeholder": "Do",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"6000000": "6.000.000 USD",
						"8000000": "8.000.000 USD",
						"_": "8.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"3500000": "3.500.000 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"3500000": "3.500.000 EUR",
						"5000000": "5.000.000 EUR",
						"_": "5.000.000+ EUR"
					}
				}
			}
		}
	},
	"price_m2": {
		"default": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"1000": "1.000 PLN",
						"2000": "2.000 PLN",
						"3000": "3.000 PLN",
						"4000": "4.000 PLN",
						"5000": "5.000 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"1000": "1.000 PLN",
						"2000": "2.000 PLN",
						"3000": "3.000 PLN",
						"4000": "4.000 PLN",
						"5000": "5.000 PLN",
						"_": "5.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"200": "200 USD",
						"400": "400 USD",
						"600": "600 USD",
						"800": "800 USD",
						"1000": "1.000 USD"
					},
					"to": {
						"__placeholder": "Do",
						"200": "200 USD",
						"400": "400 USD",
						"600": "600 USD",
						"800": "800 USD",
						"_": "800+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"250": "250 EUR",
						"500": "500 EUR",
						"750": "750 EUR",
						"1000": "1.000 EUR",
						"1250": "1.250 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"250": "250 EUR",
						"500": "500 EUR",
						"750": "750 EUR",
						"1000": "1.000 EUR",
						"1250": "1.250 EUR",
						"_": "1.250+ EUR"
					}
				}
			}
		}
	},
	"area": {
		"default": {
			"default": {
				"m2": {
					"from": {
						"__placeholder": "OD",
						"0": "0",
						"50": "50m&#178;",
						"75": "75m&#178;",
						"100": "100m&#178;",
						"150": "150m&#178;",
						"200": "200m&#178;",
						"250": "250m&#178;"
					},
					"to": {
						"__placeholder": "DO",
						"50": "50m&#178;",
						"75": "75m&#178;",
						"100": "100m&#178;",
						"150": "150m&#178;",
						"200": "200m&#178;",
						"250": "250m&#178;",
						"400": "400m&#178;",
						"_": "400m&#178;+"
					}
				}
			}
		},
		"house": {
			"buy": {
				"m2": {
					"from": {
						"__placeholder": "OD",
						"0": "0",
						"100": "100m&#178;",
						"200": "200m&#178;",
						"300": "300m&#178;",
						"400": "400m&#178;",
						"500": "500m&#178;",
						"750": "750m&#178;",
						"1000": "1.000m&#178;"
					},
					"to": {
						"__placeholder": "DO",
						"200": "200m&#178;",
						"300": "300m&#178;",
						"400": "400m&#178;",
						"500": "500m&#178;",
						"750": "750m&#178;",
						"1000": "1.000m&#178;",
						"2000": "2.000m&#178;",
						"_": "2.000m&#178;+"
					}
				}
			}
		},
		"lot": {
			"buy": {
				"m2": {
					"from": {
						"__placeholder": "OD",
						"0": "0",
						"500": "500m&#178;",
						"1000": "1.000m&#178;",
						"2000": "2.000m&#178;",
						"3000": "3.000m&#178;",
						"4000": "4.000m&#178;",
						"5000": "5.000m&#178;",
						"10000": "10.000m&#178;",
						"50000": "50.000m&#178;"
					},
					"to": {
						"__placeholder": "DO",
						"500": "500m&#178;",
						"1000": "1.000m&#178;",
						"2000": "2.000m&#178;",
						"3000": "3.000m&#178;",
						"4000": "4.000m&#178;",
						"5000": "5.000m&#178;",
						"10000": "10.000m&#178;",
						"50000": "50.000m&#178;",
						"100000": "100.000m&#178;",
						"_": "100.000m&#178;+"
					}
				}
			}
		}
	},
	"rooms": {
		"default": {
			"default": {
				"from": {
					"__placeholder": "Od",
					"1": "1",
					"2": "2",
					"3": "3",
					"4": "4",
					"5": "5"
				},
				"to": {
					"__placeholder": "Do",
					"1": "1",
					"2": "2",
					"3": "3",
					"4": "4",
					"5": "5",
					"_": "5+"
				}
			}
		}
	},
	"area_lot": {
		"m2": {
			"from": {
				"__placeholder": "OD",
				"0": "0",
				"500": "500m&#178;",
				"1000": "1.000m&#178;",
				"2000": "2.000m&#178;",
				"3000": "3.000m&#178;",
				"4000": "4.000m&#178;",
				"5000": "5.000m&#178;"
			},
			"to": {
				"__placeholder": "DO",
				"500": "500m&#178;",
				"1000": "1.000m&#178;",
				"2000": "2.000m&#178;",
				"3000": "3.000m&#178;",
				"4000": "4.000m&#178;",
				"5000": "5.000m&#178;",
				"10000": "10.000m&#178;",
				"20000": "20.000m&#178;",
				"_": "20.000m&#178;+"
			}
		}
	}
};
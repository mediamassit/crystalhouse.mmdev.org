								<div class="form-group" data-form-fieldset="built_year"<?php if(@$disabled) : ?> style="display: none;"<?php endif; ?>>
									<label><?php echo chtof_label('offer_property_built-year'); ?></label>
									<div class="row">
										<div class="col-xs-6">
											<?php echo chofl_input_text('built_year_from', array('placeholder' => __('Od', 'chtheme'), 'maxlength' => 4)); ?>
										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<?php echo chofl_input_text('built_year_to', array('placeholder' => __('Do', 'chtheme'), 'maxlength' => 4)); ?>
										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->
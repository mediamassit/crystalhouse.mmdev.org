				<div class="col-md-9">
				<?php if($offers->have_posts()) : ?>
					<?php include(locate_template('src/partials/offers-list/slots/switches.php')); ?>
					<?php include(locate_template('src/partials/offers-list/slots/google-map.php')); ?>
					<?php include(locate_template('src/partials/offers-list/slots/list.php')); ?>
				<?php else : ?>
					<section class="page text">
						<p><?php _e('Brak ofert.', 'chtheme'); ?></p>
					</section>
				<?php endif; ?>

				<?php //echo '<pre>'.$offers->request.'</pre>'; ?>
				</div>
				<!-- .col-md-9 -->
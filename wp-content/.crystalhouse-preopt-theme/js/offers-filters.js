var CHT_DEV_MODE = 0;

function getFilterProp(obj, path) {
	var parts = path.split('.');

	for(var i = 0; i < parts.length; ++i) {
		if(!obj || !obj.hasOwnProperty(parts[i])) {
			return undefined;
		}
		
		obj = obj[parts[i]];
	}

	return obj;
}

function getFilterProps(obj, paths) {
	for(var i in paths) {
		var prop = getFilterProp(obj, paths[i]);

		if(typeof(prop) !== 'undefined') {
			CHT_DEV_MODE === 1 ? console.log('Path matched: '+paths[i]) : null;
			return prop;
		}
		else {
			CHT_DEV_MODE === 1 ? console.log('Path looked up: '+paths[i]) : null;
		}
	}

	return undefined;
}

function hasFilterProp(obj, path) {
	return typeof(getFilterProps(obj, path)) !== 'undefined';
}

var QueryString = function () {
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");

	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
		// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;
		// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	} 

	return query_string;
}();

var OffersFilters = (function($) {
	return {
		change_options: function($selects, options) {
			function need_change($select, options) {
				var first = { vals: [], labels: [] },
					second = { vals: [], labels: [] };

				$select.find('option').each(function() {
					if($(this).val() !== '') {
						first.vals.push($(this).val());
						first.labels.push($(this).text());
					}
				});

				$.each(options, function(val, label) {
					if(val !== '__placeholder') {
						second.labels.push(label);
						second.vals.push(val);
					}
				});

				if(first.vals.length !== second.vals.length) {
					return true;
				}

				return !($(first.vals).not(second.vals).length == 0 && $(first.vals).not(second.vals).length == 0/* &&
					$(first.labels).not(second.labels).length == 0 && $(first.labels).not(second.labels).length == 0*/);
			}

			$selects.each(function () {
				var $select = $(this);

				if(typeof($select.prop('original-select-content')) == 'undefined') {
					$select.prop('original-select-content', $select.html());
				}

				if(!need_change($select, options)) {
					return;
				}

				$select.html('');

				if(typeof(options['__placeholder']) !== 'undefined') {
					$select.append('<option value="">'+options['__placeholder']+'</option>');
				}

				$.each(options, function(key, value) {
					if(key !== '__placeholder') {
						$select.append('<option value="'+key+'">'+value+'</option>');

						if(QueryString[$select.attr('name')] === encodeURI(key)) {
							$select.find('option:last').attr('selected', true);
						}
					}
				});
			});
		},

		restore_options: function($selects) {
			$selects.each(function () {
				var $select = $(this);

				if(typeof($select.prop('original-select-content')) != 'undefined') {
					if($select.html() !== $select.prop('original-select-content')) $select.html($select.prop('original-select-content'));
				}
			});
		},

		get_value: function($input) {
			if($input.is('input[type=radio]') || $input.is('input[type=checkbox]')) {
				return $input.closest('form').find('input[type='+$input.attr('type')+'][name='+$input.attr('name')+']:checked').val();
			}

			return $input.val();
		},

		fiv: function($form, input_name) {
			var fields = $form.serializeArray();
			var value = null;

			for(var key in fields) {
				if(fields[key].name == input_name) {
					return fields[key].value;
				}
			}

			// Check for values in "data-form-values" if not found in form
			var form_values = eval('(function() { return ' + $form.attr('data-form-values') + '; })();');

			if(typeof(form_values) == 'object') {
				for(var key in form_values) {
					if(key == input_name) {
						return form_values[key];
					}
				}
			}
		},

		fls: function($fieldset, show_or_hide) {
			if(show_or_hide == true) {
				$fieldset.show().find('input,select').attr('disabled', false);
			}
			else {
				$fieldset.hide().find('input,select').attr('disabled', true);
			}
		}
	};
})(jQuery);

(function($) {
	// Change order
	$('.offers-list-switches select[name=order]').change(function() {
		$('.offers-list-filter [name=order]').val($(this).val());
	}).trigger('change');
	$('.offers-list-switches select[name=order]').change(function() {
		$('.offers-list-filter form').submit();
	});

	// Prevent sending empty queries
	$('form[name=offers_filter]').submit(function() {
		$('input,select').each(function() {
			if($(this).val() === '') {
				$(this).attr('name', '');
			}
		});
	});
})(jQuery);

(function($) {
	// List side filters
	$('form[name=offers_filter]').change(function() {
		var of = OffersFilters,
			fs = __filters,
			post_type = of.fiv($(this), 'type'),
			offer_type = of.fiv($(this), 'offer'),
			rent_type = of.fiv($(this), 'rent_type'),
			market_type = of.fiv($(this), 'market_type[]'),
			country = of.fiv($(this), 'country'),
			province = of.fiv($(this), 'province'),
			city = of.fiv($(this), 'city'),
			currency = of.fiv($(this), 'currency');

		// Extended search
		if($(this).is('[data-is-extendable]')) {
			// If is extended
			of.fls($(this).find('[data-form-extended]'), $(this).hasClass('extended'));
		}

		if(typeof($(this).prop('serialized_form')) != 'undefined' && $(this).serialize() === $(this).prop('serialized_form')) {
			return;
		}

		/*
		 * Show/hide filters
		 */
		switch(offer_type) {
			case 'buy':
				of.fls($(this).find('[data-form-fieldset="rent_type"]'), false);
				of.fls($(this).find('[data-form-fieldset="area"]'), true);
				of.fls($(this).find('[data-form-fieldset="price"]'), true);
				of.fls($(this).find('[data-form-fieldset="city"]'), true);
				of.fls($(this).find('[data-form-fieldset="district"]'), true);
				of.fls($(this).find('[data-form-fieldset="attributes_short"]'), true);

				// Market type
				of.fls($(this).find('[data-form-fieldset="market_type"]'), offer_type !== 'rent' && $.inArray(post_type, ['house', 'apartment']) >= 0);
				var market_type = of.fiv($(this), 'market_type[]');

				// Lot type
				of.fls($(this).find('[data-form-fieldset="lot_type"]'), post_type === 'lot');

				// Area lot
				of.fls($(this).find('[data-form-fieldset="area_lot"]'), offer_type === 'buy' && post_type == 'house' && market_type === 'secondary');

				// Price / m2
				of.fls($(this).find('[data-form-fieldset="price_m2"]'),
					//offer_type === 'buy' && post_type == 'lot' || 
					($.inArray(post_type, ['house', 'apartment']) >= 0 && market_type === 'primary')
				);

				// Province
				of.fls($(this).find('[data-form-fieldset="province"]'), post_type == 'lot');

				// Developer
				of.fls($(this).find('[data-form-fieldset="developer"]'), ($.inArray(post_type, ['house', 'apartment']) >= 0 && market_type === 'primary') || of.fiv($(this), 'is_investment') == 1);

				// Rooms
				of.fls($(this).find('[data-form-fieldset="rooms"]'), $.inArray(post_type, ['house', 'apartment']) >= 0);

				// Bathrooms
				of.fls($(this).find('[data-form-fieldset="bathrooms"]'), $.inArray(post_type, ['house', 'apartment']) >= 0 && market_type === 'secondary');

				// Built year
				of.fls($(this).find('[data-form-fieldset="built_year"]'), $.inArray(post_type, ['house', 'apartment']) >= 0 && market_type === 'secondary');

				// Center distance
				//of.fls($(this).find('[data-form-fieldset="center_distance"]'), market_type !== 'primary');

				// Investment
				of.fls($(this).find('[data-form-fieldset="investment"]'), (offer_type === 'buy' && $.inArray(post_type, ['house', 'apartment']) >= 0 && market_type === 'primary') || of.fiv($(this), 'is_investment') == 1);

				// Attributes
				of.fls($(this).find('[data-form-fieldset="attributes"]'), $.inArray(post_type, ['house', 'apartment']) >= 0);

				// Attributes (long)
				of.fls($(this).find('[data-form-fieldset="attributes_long"]'), $.inArray(post_type, ['house', 'apartment']) >= 0 && market_type === 'secondary');

				// Lot is available
				$(this).find('[name=type] option[value="lot"]').show().attr('disabled', false);
				break;
			case 'rent':
				of.fls($(this).find('[data-form-fieldset="rent_type"]'), true);
				of.fls($(this).find('[data-form-fieldset="market_type"]'), false);
				of.fls($(this).find('[data-form-fieldset="lot_type"]'), false);
				of.fls($(this).find('[data-form-fieldset="area"]'), false);
				of.fls($(this).find('[data-form-fieldset="area_lot"]'), false);
				of.fls($(this).find('[data-form-fieldset="price"]'), true);
				of.fls($(this).find('[data-form-fieldset="price_m2"]'), false);
				of.fls($(this).find('[data-form-fieldset="province"]'), false);
				of.fls($(this).find('[data-form-fieldset="city"]'), true);
				of.fls($(this).find('[data-form-fieldset="district"]'), true);
				of.fls($(this).find('[data-form-fieldset="developer"]'), false);
				of.fls($(this).find('[data-form-fieldset="investment"]'), false);
				of.fls($(this).find('[data-form-fieldset="rooms"]'), post_type !== 'business' /*true*/);
				of.fls($(this).find('[data-form-fieldset="bathrooms"]'), post_type !== 'business' /*true*/);
				of.fls($(this).find('[data-form-fieldset="built_year"]'), false);
				of.fls($(this).find('[data-form-fieldset="center_distance"]'), false);
				of.fls($(this).find('[data-form-fieldset="attributes"]'), true);
				of.fls($(this).find('[data-form-fieldset="attributes_short"]'), post_type !== 'business' /*true*/);
				of.fls($(this).find('[data-form-fieldset="attributes_long"]'), false);

				if(post_type == 'house' || post_type == 'business') {
					$(this).find('[name=rent_type]').val($(this).find('[name=rent_type] option:not([disabled])').val())
						.find('option[value!="12"]').hide().attr('disabled', true);
				}
				else {
					$(this).find('[name=rent_type] option[value!="12"]').show().attr('disabled', false);
				}

				// Lot is disabled
				if($(this).find('[name=type] option[value="lot"]').is(':checked')) $(this).find('[name=type]').val(null);
				$(this).find('[name=type] option[value="lot"]').hide().attr('disabled', true);
				break;
		}

		// Business fields
		of.fls($(this).find('[data-form-fieldset="business_type"]'), post_type == 'business');
		of.fls($(this).find('[data-form-fieldset-more="attributes_business"]'), post_type == 'business');
		of.fls($(this).find('[data-form-fieldset="premise_type"]'), post_type == 'business' && of.fiv($(this), 'business_type') == 'premise');

		if(post_type === 'house') {
			$(this).find('[data-type-label=default]').hide();
			$(this).find('[data-type-label=house]').show();
		}
		else {
			$(this).find('[data-type-label=default]').show();
			$(this).find('[data-type-label=house]').hide();
		}

		/*
		 * Change filters value
		 */
		// TODO: Load cities based on province (if visible, if not load all cities)
		// TODO: House can't be rented short-term
		var offer_slug = offer_type;

		if(offer_type === 'rent') {
			if(of.fiv($(this), 'rent_type') == 10) offer_slug = 'rent_short';
			else offer_slug = 'rent_long';
		}

		// Locations
			// Countries
			if($(this).find('[name=country]').length > 0) {
				if(typeof(fs.locations.country) != 'undefined') of.change_options($(this).find('[name=country]'), fs.locations.country);
			}

			// Provinces
			var country = of.fiv($(this), 'country');
			if($(this).find('[name=province]').length > 0 && country && typeof(fs.locations.province[country]) != 'undefined') {
				of.change_options($(this).find('[name=province]'), fs.locations.province[country]);
			}
			else of.restore_options($(this).find('[name=province]'));

			// Cities
			var province = of.fiv($(this), 'province');
			if($(this).find('[name=city]').length > 0 && province && typeof(fs.locations.city[province]) != 'undefined') {
				of.change_options($(this).find('[name=city]'), fs.locations.city[province]);
			}
			else {
				of.restore_options($(this).find('[name=city]'));
			}

			// Districts
			var city = of.fiv($(this), 'city');
			if($(this).find('[name=district]').length > 0 && city && typeof(fs.locations.district[city]) != 'undefined') {
				of.change_options($(this).find('[name=district]'), fs.locations.district[city]);
			}
			else of.restore_options($(this).find('[name=district]'));


		// Prices
		if($(this).find('[name=price_from],[name=price_to]').length > 0) {
			/*if(typeof(fs.price[post_type]) !== 'undefined') var values = fs.price[post_type][offer_slug][currency.toLowerCase()];
			else var values = fs.price.default[offer_slug][currency.toLowerCase()];*/
			var values = getFilterProps(__filters, ['price.'+post_type+'.'+offer_slug+'.'+currency.toLowerCase(), 'price.default.'+offer_slug+'.'+currency.toLowerCase()]);

			if(typeof(values) !== 'undefined') {
				of.change_options($(this).find('[name=price_from]'), values.from);
				of.change_options($(this).find('[name=price_to]'), values.to);
			}
			else {
				of.restore_options($(this).find('[name=price_from]'));
				of.restore_options($(this).find('[name=price_to]'));
			}
		}

		// Prices / m2
		if($(this).find('[name=price_m2_from],[name=price_m2_to]').length > 0 && offer_type == 'buy') {
			/*if(typeof(fs.price_m2[post_type]) !== 'undefined') var values = fs.price_m2[post_type][offer_slug][currency.toLowerCase()];
			else var values = fs.price_m2.default[offer_slug][currency.toLowerCase()];*/
			var values = getFilterProps(__filters, ['price_m2.'+post_type+'.'+offer_slug+'.'+currency.toLowerCase(), 'price_m2.default.'+offer_slug+'.'+currency.toLowerCase()]);

			if(typeof(values) !== 'undefined') {
				of.change_options($(this).find('[name=price_m2_from]'), values.from);
				of.change_options($(this).find('[name=price_m2_to]'), values.to);
			}
			else {
				of.restore_options($(this).find('[name=price_m2_from]'));
				of.restore_options($(this).find('[name=price_m2_to]'));
			}
		}

		// Areas
		if($(this).find('[name=area_from],[name=area_to]').length > 0) {
			/*if(typeof(fs.area[post_type]) !== 'undefined' && typeof(fs.area[post_type][offer_slug]) !== 'undefined') {
				var values = fs.area[post_type][offer_slug]['m2'];*/
			var values = getFilterProps(__filters, ['area.'+post_type+'.'+offer_slug+'.m2', 'area.default.'+offer_slug+'.m2']);

			if(typeof(values) !== 'undefined') {
				of.change_options($(this).find('[name=area_from]'), values.from);
				of.change_options($(this).find('[name=area_to]'), values.to);
			}
			else {
				of.restore_options($(this).find('[name=area_from]'));
				of.restore_options($(this).find('[name=area_to]'));
			}
		}

		// Rooms
		/*if($(this).find('[name=rooms_from],[name=rooms_to]').length > 0) {
			if(typeof(fs.rooms[post_type]) !== 'undefined') {
				var values = fs.rooms[post_type][offer_slug];

				of.change_options($(this).find('[name=rooms_from]'), values.from);
				of.change_options($(this).find('[name=rooms_to]'), values.to);
			}
			else {
				of.restore_options($(this).find('[name=rooms_from]'));
				of.restore_options($(this).find('[name=rooms_to]'));
			}
		}*/

		// Extended search
		if($(this).is('[data-is-extendable]')) {
			if(!$(this).hasClass('extended')) {
				of.fls($(this).find('[data-form-extended]'), false);
			}
		}

		// Save form to verify if it was modified
		$(this).prop('serialized_form', $(this).serialize());
	});
})(jQuery);
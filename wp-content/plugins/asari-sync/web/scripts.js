(function($) {
	function formatTimer(secs) {
		var hours = parseInt(secs / 3600) % 24;
		var minutes = parseInt(secs / 60) % 60;
		var seconds = parseInt(secs % 60, 10);

		return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
	}

	$.fn.ascTimer = function() {
		function timer($self) {
			var time = parseInt($self.attr('data-time'));

			$self.attr('data-time', time+1);
			$self.html(formatTimer(time));
		}

		return this.each(function() {
			var $self = $(this);
				timer($self);

			setInterval(function() {
				timer($self);
			}, 1000);
        });
	}

	$.fn.ascProcessing = function(options) {
		$self = $(this);

		var interval = setInterval(function() {
			$.ajax({
				url: options.status_url,
				dataType: 'json',
				cache: false
			}).done(function(data) {
				$('body').find('[data-syncdata-queueupdatedate]').html(data.last_queue_update_string);
				$('body').find('[data-syncdata-queuesize]').html(data.queue_size);

				var data = data.processes[options.process_name];

				if(data.running === true) {
					$self.find('[data-processname]').html(data.label);
					$self.find('[data-lastaction]').html(data.last_message);
					$self.find('[data-progressbar]').find('.uk-progress-bar').css('width', data.progress+'%').html(data.progress+'%');
					$self.find('[data-syncprocess-started]').html(data.started_at_string);
					//$self.find('[data-syncprocess-runtime]').attr('data-time', data.processing_time);
					$self.find('[data-syncprocess-estimatedtime]').html(formatTimer(data.estimated_time));
					$self.find('[data-syncprocess-done]').html(data.items_done_count);
					$self.find('[data-syncprocess-scheduled]').html(data.items_count);

					if(data.messages.length > 0) {
						$self.find('.asc-log').find('ul').html('');

						for(var i = 0; i < data.messages.length; ++i) {
							$self.find('.asc-log').find('ul').append('<li><time>'+data.messages[i].time_string+'</time> '+data.messages[i].message+'</li>');
						}

						$self.find('.asc-log').find('ul').scrollTop($self.find('.asc-log').find('ul').prop("scrollHeight"));
					}
				}
				else {
					if(typeof(interval) !== 'undefined') clearInterval(interval);
					window.location.replace(options.redirect_url);
				}
			});
		}, 1000);
	}

	$(document).ready(function() {
		$('[data-time]').ascTimer();
	});
})(jQuery);
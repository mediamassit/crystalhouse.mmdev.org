<?php

function offer_lot_custom_post_type() {

	$labels = array(
		'name'                => _x( 'Działki', 'Post Type General Name', 'chtheme' ),
		'singular_name'       => _x( 'Działka', 'Post Type Singular Name', 'chtheme' ),
		'menu_name'           => __( 'Działki', 'chtheme' ),
		'parent_item_colon'   => __( 'Nadrzędny:', 'chtheme' ),
		'all_items'           => __( 'Wszystkie', 'chtheme' ),
		'view_item'           => __( 'Zobacz', 'chtheme' ),
		'add_new_item'        => __( 'Dodaj nową działkę', 'chtheme' ),
		'add_new'             => __( 'Dodaj nową', 'chtheme' ),
		'edit_item'           => __( 'Edytuj działkę', 'chtheme' ),
		'update_item'         => __( 'Aktualizuj działkę', 'chtheme' ),
		'search_items'        => __( 'Szukaj', 'chtheme' ),
		'not_found'           => __( 'Nie odnaleziono', 'chtheme' ),
		'not_found_in_trash'  => __( 'Nie odnaleziono w koszu', 'chtheme' ),
	);
	$rewrite = array(
		'slug'                => 'lot',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'dzialka', 'chtheme' ),
		'description'         => '',
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'          => array( 'offer_category', 'offer_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-clipboard',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);
	register_post_type( 'offer_lot', $args );
	add_post_type_support( 'offer_lot', 'excerpt' );

}
add_action( 'init', 'offer_lot_custom_post_type', 0 );
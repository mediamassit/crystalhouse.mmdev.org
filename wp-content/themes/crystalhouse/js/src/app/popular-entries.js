(function($) {

	$.fn.popularEntries = function(options) {
		return this.each(function() {
			$(this).find('[data-expandable]').hide();
			$(this).find('.expander').show();
			$(this).addClass('collapsed');

			$(this).prop('expander-label-expand', $(this).find('[data-expand]').text());
			$(this).prop('expander-label-collapse', $(this).find('[data-expand]').attr('data-collapse-label'));

			$(this).find('[data-expand]').click(function() {
				var $w = $(this).closest('.popular-entries');
				var $a = $(this);

				if($w.is('.collapsed')) {
					$w.find('[data-expandable]').slideDown(400, 'easeInOutCirc', function() {
						$a.html($w.prop('expander-label-collapse'));
						$w.removeClass('collapsed');
					});
				}
				else {
					$w.find('[data-expandable]').slideUp(400, 'easeInOutCirc', function() {
						$a.html($w.prop('expander-label-expand'));
						$w.addClass('collapsed');
					});
				}

				return false;
			});
		});
	};

})(jQuery);
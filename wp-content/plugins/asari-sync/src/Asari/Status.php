<?php

class Asari_Status {
	protected $_status = array(
		'last_queue_update' => null,
		'last_queue_update_string' => null,
		'queue_size' => null,
		'processes' => array(),
		'last_error' => null
	);

	/**
	 * __construct
	 */
	public function __construct() {
		$this->load();
	}

	/**
	 * Gathers informations about Asari
	 * @return void
	 */
	public function load() {
		// Loads queue data
		$queue = new Asari_Queue;

		$this->_status['last_queue_update'] = $queue->getSaveTime();
		$this->_status['last_queue_update_string'] = date('Y/m/d H:i:s', $this->_status['last_queue_update']);
		$this->_status['queue_size'] = sizeof($queue->get());

		// Loads processes
		$this->loadProcess('Asari_Process_UpdateQueue');
		$this->loadProcess('Asari_Process_Synchronize');
	}

	/**
	 * Returns status info
	 * @param string $key
	 * @return mixed
	 */
	public function get($key) {
		return @$this->_status[$key];
	}

	/**
	 * Checks whether status info exists
	 * @param string $key
	 * @return bool
	 */
	public function has($key) {
		return array_key_exists($key, $this->_status) && !is_null(@$this->_status[$key]);
	}

	/**
	 * Returns processes' statuses
	 * @return array
	 */
	public function getProcesses() {
		return $this->_status['processes'];
	}

	/**
	 * Returns running process
	 * @return array|null
	 */
	public function getRunningProcess() {
		//return reset($this->_status['processes']);
		if(sizeof($this->_status['processes']) > 0) {
			foreach($this->_status['processes'] as $process) {
				if($process['running'] === true) {
					return $process;
				}
			}
		}

		return null;
	}

	/**
	 * Checks whether any process is running
	 * @return bool
	 */
	public function isProcessing() {
		return is_array($this->getRunningProcess());
	}

	/**
	 * Returns URL to JSON status file
	 * @return string
	 */
	public function getJsonFileUrl() {
		// TODO: Generate it from $this->_directory ?
		return plugins_url().'/asari-sync/web/status.php';
	}

	/**
	 * Converts status data to array
	 * @return array
	 */
	public function toArray() {
		return $this->_status;
	}

	/**
	 * Appends given process data to status
	 * @param string $name
	 */
	protected function loadProcess($name) {
		$process = new $name;
		$status = $process->getStatus()->get();
		
		$log = new Asari_Log;
		$log->load($status['log_filename']);

		$process_status = &$this->_status['processes'][$process->getName()];

		$process_status = $status;
		$process_status['messages'] = $log->getMessages();

		foreach($process_status['messages'] as &$message) {
			$message['time_string'] = date('Y/m/d H:i:s', $message['time']);
		}

		$process_status['messages'] = array_slice($process_status['messages'], -50, 50);

		$last_message = end($process_status['messages']);
		$process_status['last_message'] = is_array($last_message) ? $last_message['message'] : null;
	}
}
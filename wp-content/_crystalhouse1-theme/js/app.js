(function($) {
	function positionModal($modal) {
		if($modal.is(':hidden')) {
			var mh = $modal.show().find('.modal-dialog').height();
			$modal.hide();
		}
		else {
			var mh = $modal.find('.modal-dialog').height();
		}

		if(mh >= $(window).height()) {
			$modal.find('.modal-dialog').css('top', '');
		}
		else {
			$modal.find('.modal-dialog').css('top', ($(window).height()-mh) / 2);
		}
	}

	$(document).ready(function() {
		// Home filters search query
		$('.offers-home-filters').each(function() {
			var $sq = $(this).find('.search-query');

			function position() {
				$sq.css({
					width: $sq.closest('.offers-home-filters').find('.side').outerWidth()-15,
					marginTop: '-'+$sq.outerHeight()+'px'
				});
			}

			function bind() {
				$sq.addClass('floating');
				position();

				$(window).bind('resize.position-search-query', function() {
					position();
				});
			}

			function unbind() {
				$sq.removeClass('floating').css({
					width: 'auto',
					marginTop: 'auto'
				});

				$(window).unbind('resize.position-search-query');
			}

			function init() {
				$(window).width() > 992 ? bind() : unbind();
			}

			$(window).resize(function() {
				init();
			});

			init();
		});

		// Sticky #top
		$('#top').sticky({ className: 'sticky' });
		$(window).resize(function() {
			var $wrapper = $('#top').parent();

			$wrapper.css('height', $('#top').height());
			//$('#top').sticky('update');
		});

		// Set active flag in main menu
		$('.main-navigation').each(function() {
			$(this).find('a[href="'+window.location.href.split('?')[0]+'"]:not(.active)').addClass('active');
			$(this).find('.dropdown-menu li.active').each(function() {
				$(this).closest('.dropdown-menu').closest('li').addClass('active');
			});
		});

		// Setup widgets & components
		$('#home-theatre').homeTheatre();
		$('.offers-carousel').offersCarousel();
		$('.widget.carousel-gallery').offerGalleryCarousel();
		$('.popular-entries').popularEntries();
		$(window).offersWishlist();

		// Tooltips
		$('[data-tooltip]').each(function() {
			$(this).tooltip({
				placement: $(this).attr('data-placement') ? $(this).attr('data-placement') : $(window).width() > 768 ? 'right' : 'left',
				container: 'body',
				delay: 300
			});
		});

		// Target links
		function scrollto($elm) {
			var $elm = $($elm);

			if($elm.length <= 0) return true;

			var mt = $('#top').attr('data-sticky-height') ? parseInt($('#top').attr('data-sticky-height')) : $('#top').height();

			if($(window).width() <= 992) mt = $('#top').height();

			$(document).scrollTo($elm, 600, { offset: mt*-1 });
		}

		$('[data-section]').each(function() {
			if($(this).attr('data-section') == '#') return;

			$(this).bind('click.target', function(e) {
				scrollto($(this).attr('data-section'));
				return false;
			});
		});

		// Tabs
		$('[data-target-tab]').each(function() {
			$(this)
				.unbind('click.target')
				.bind('click.tab', function(e) {
					var $tab = $($(this).attr('data-target-tab'));

					if($tab.length <= 0) return false;

					$tab.parent().find('.tab.active').removeClass('active');
					$tab.addClass('active');

					scrollto($(this).attr('href'));

					return false;
				});
		});

		// Cookie notifier
		$('#cookie-notifier').each(function() {
			var $self = $(this);

			if($.cookie('ch-cn') != 1) {
				$self.show();
			}

			$(this).find('[data-close-cookie-notifier]').click(function() {
				$.cookie('ch-cn', 1, { expires: 30, path: '/' });

				$self.fadeOut(400);
				return false;
			});
		});

		// Custom checkboxes and radio buttons
		$('.offers-list-filter,.offers-home-filters').find('input[type=checkbox],input[type=radio]').each(function() {
			var $label = $(this).closest('label');

			if($label.length <= 0) {
				return;
			}

			if($(this).is('[type=checkbox]')) {
				$(this).hide().change(function() {
					if($(this).is(':checked')) {
						$label.removeClass('not-active').addClass('active');
					}
					else {
						$label.removeClass('active').addClass('not-active');
					}
				});
			}
			else if($(this).is('[type=radio]')) {
				$(this).hide().change(function() {
					$(this).closest('form').find('input[name="'+$(this).attr('name')+'"]:radio:checked').closest('label').removeClass('not-active').addClass('active');
					$(this).closest('form').find('input[name="'+$(this).attr('name')+'"]:radio:not(:checked)').closest('label').removeClass('active').addClass('not-active');
				});
			}
		}).trigger('change');

		// Refresh clipboard list
		$('.wishlist-dropdown').trigger('wishlist-refresh');

		// Setup form custom fields filler
		$('[data-modal-form-offer]').click(function() {
			$('#modal-contact-offer').find('input[name=offer_number]').val($(this).attr('data-modal-form-offer'));
		});

		$('[data-form-subject]').click(function() {
			$('#modal-contact-subject').find('input[name=subject]').val($(this).attr('data-form-subject'));
		});

		// Modals
		$('.modal.vertical-middle').each(function() {
			positionModal($(this));
		});

		$(window).resize(function() {
			$('.modal.vertical-middle').each(function() {
				positionModal($(this));
			});
		});

		// Extended search
		$('form[name="offers_filter"]').find('[data-toggle-extended]').click(function() {
			$(this).closest('form').toggleClass('extended').trigger('change');
			$('.widget.offers-home-filters').scrollintoview({ duration: "normal" });
			return false;
		});

		// Setup fancyBox
		$(".fancybox").fancybox({
			loop: false,
			margin: 10,
			nextEffect: 'fade',
			prevEffect: 'fade',
			helpers	: {
				thumbs	: {
					width: 50,
					height: 50
				}
			},
			tpl: {
				error: FANCYBOX_LANG.error,
				closeBtn: FANCYBOX_LANG.closeBtn,
				next: FANCYBOX_LANG.next,
				prev: FANCYBOX_LANG.prev
			}
		});
		$('.fancybox-media').fancybox({
			helpers: {
				media: {
					youtube: { params: { autoplay: 0 } }
				}
			},
			width: 640,
			height: 400,
			aspectRatio: true
		});

		$(window).trigger('resize');
	});
})(jQuery);
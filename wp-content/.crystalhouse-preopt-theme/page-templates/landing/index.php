<?php

$landing_image = reset(rwmb_meta('landing_image', 'type=file'));

?><html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php the_title(); ?></title>
	<link rel="stylesheet" media="screen,print" href="<?php bloginfo('template_url'); ?>/page-templates/landing/landing.css">
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico"/>
	<?php wp_head(); ?>
</head>
<body>

	<div id="landing">
		<div class="top">
			<h1><a href="<?php echo pll_home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo-dark.png" alt="<?php bloginfo('title'); ?>"></a></h1>
			<h2><?php echo get_opt('opt-theme-headline'); ?></h2>
		</div>
		<!-- .top -->

		<div class="content">
			<div class="left">
				<?php if(is_array($landing_image)) : ?><figure><img src="<?php echo $landing_image['url']; ?>" alt=""></figure><?php endif; ?>

				<div class="contact">
					<ul>
						<li><strong><?php _e('Telefon', 'chtheme'); ?>:</strong> <?php echo reset(explode("\r\n", get_opt('opt-contact-phone'))); ?></li>
						<li><strong><?php _e('Email', 'chtheme'); ?>:</strong> <a href="mailto:<?php echo get_opt('opt-contact-email'); ?>"><?php echo get_opt('opt-contact-email'); ?></a></li>
						<li><strong><?php _e('WWW', 'chtheme'); ?>:</strong> <a href="<?php echo pll_home_url(); ?>"><?php echo reset(explode('/', preg_replace('#^https?://#', '', get_bloginfo('home')))); ?></a></li>
					</ul>

					<ul>
						<li><?php _e('Crystal House S.A.', 'chtheme'); ?><br><?php echo preg_replace("#\r\n#", '<br>', get_opt('opt-contact-address')); ?></li>
					</ul>
				</div>
				<!-- .contact -->
			</div>
			<!-- .left -->

			<div class="right">
				<?php echo apply_filters('the_content', $post->post_content); ?>
			</div>
			<!-- .right -->
		</div>
		<!-- .content -->
	</div>
	<!-- #landing -->

	 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	 <script>
(function($) {
	$(window).bind('resize', function() {
		if($(this).width() > 980) {
			$('#landing .content .right p.bottom').addClass('js');
			$('#landing .content .right').css('min-height', $('#landing .content').height());
		}

		// Position slide
		$('#landing .content .left figure').each(function() {
			var $figure = $(this),
				$img = $(this).find('img');

			if($img.width() > $figure.width()) {
				$img.css('margin-left', '-' + (($img.width()-$figure.width())/2) + 'px');
			}
		});

		// Position landing
		$('#landing').css('margin-top', '0px');

		if($('#landing').height() < $(this).height()) {
			$('#landing').css('margin-top', (($(this).height()-$('#landing').height())/2.25) + 'px');
		}
	});
	$(window).load(function() {
		$(window).trigger('resize');
	});
})(jQuery);
	 </script>
</body>
</html>
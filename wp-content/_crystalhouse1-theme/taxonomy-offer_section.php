<?php

get_header();

$section = array(pll_get_term(84));

include locate_template('src/filters/temp_head.php');

?>

	<div id="content">
		<div class="container">
			<?php get_template_part('src/partials/breadcrumbs'); ?>
			<?php chtof_get_slot('src/partials/filters/navigation_tabs', array('no_buy' => true, 'no_rent' => true)); ?>
			<?php include locate_template('src/partials/filters/page_title.php'); ?>

			<div class="row">
				<?php include locate_template('src/partials/filters/list.php'); ?>

				<div class="col-md-3">
					<div class="widget offers-list-filter">
						<h2><?php _e('Znajdź nieruchomość', 'chtheme'); ?></h2>
						<form method="get" action="<?php echo chtof_remf_url(); ?>" name="offers_filter">
							<?php get_template_part('src/partials/filters/slots/q'); ?>
							<?php get_template_part('src/partials/filters/slots/keyword'); ?>
							<?php get_template_part('src/partials/filters/slots/currency'); ?>

							<div data-group="offer_type_apartment-house">
								<?php chtof_get_slot('src/partials/filters/slots/type', array('types' => array('offer_apartment', 'offer_house'), 'all_disabled' => true)); ?>
								<?php get_template_part('src/partials/filters/slots/developer'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/area'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/price'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/city', array('location_city' => $location_city)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/district', array('locations' => $locations)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/attributes', array('no_long_attributes' => true)); ?>


								<div class="form-group text-center">
									<input type="hidden" name="offer" value="buy">
									<input type="hidden" name="market_type[]" value="primary">
									<?php get_template_part('src/partials/filters/slots/submit'); ?>
								</div>
								<!-- .form-group -->
							</div>
							<!-- [data-group] -->
						</form>
					</div>
					<!-- .widget.offers-list-filter -->
				</div>
				<!-- .col-md-3 -->
			</div>
			<!-- .row -->
		</div>
		<!-- .container -->
	</div>
	<!-- #content -->

<?php get_footer(); ?>
<?php

function register_pagelanding_meta_boxes($meta_boxes) {
    if(!class_exists('RW_Meta_Box') ) return;

	// Gallery
    $meta_boxes = array(
		array(
			'title' => __('Landing', 'chtheme'),
			'pages' => array('page'),
			'context' => 'normal',
			'priority' => 'high',
			'fields' => array(
				/*array(
					'name'  => __('Cena', 'chtheme'),
					'id'    => "landing_price",
					'type'  => 'text',
				),
				array(
					'name'  => __('URL', 'chtheme'),
					'id'    => "landing_url",
					'type'  => 'text'
				),
				array(
					'name' => __('Obraz', 'chtheme'),
					'id' => "landing_image",
					'type' => 'thickbox_image'
				),*/
				array(
					'name' => __('Obraz', 'chtheme'),
					'id' => "landing_image",
					'type' => 'image_advanced',
					'max_file_uploads' => 1
				)
			)
		)
	);

	if(post_has_template('page-templates/page-landing.php')) {
		foreach($meta_boxes as $meta_box) {
			new RW_Meta_Box($meta_box);
		}
	}
}
add_action('admin_init', 'register_pagelanding_meta_boxes');
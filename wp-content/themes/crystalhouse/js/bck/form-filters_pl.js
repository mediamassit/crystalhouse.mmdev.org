var __filters = {
	price: {
		default: {
			buy: {
				pln: {
					from: {'__placeholder': 'Od', '0': '0 PLN', '1000000': '1.000.000 PLN', '3000000': '3.000.000 PLN', '5000000': '5.000.000 PLN'},
					to: {'__placeholder': 'Do', '1000000': '1.000.000 PLN', '3000000': '3.000.000', '5000000': '5.000.000 PLN', ' ': '5.000.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': 'Od', '0': '0 USD', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD'},
					to: {'__placeholder': 'Do', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD', ' ': '800.000+ USD'}
				},
				eur: {
					from: {'__placeholder': 'Od', '0': '0 EUR', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR'},
					to: {'__placeholder': 'Do', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR', ' ': '1.250.000+ EUR'},
				}
			},
			rent_short: {
				pln: {
					from: {'__placeholder': 'Od', '0': '0 PLN', '200': '200 PLN', '400': '400 PLN'},
					to: {'__placeholder': 'Do', '200': '200 PLN', '400': '400 PLN', ' ': '400+ PLN'}
				},
				usd: {
					from: {'__placeholder': 'Od', '0': '0 USD', '40': '40 USD', '100': '100 USD'},
					to: {'__placeholder': 'Do', '40': '40 USD', '100': '100 USD', ' ': '100+ USD'}
				},
				eur: {
					from: {'__placeholder': 'Od', '0': '0 EUR', '50': '50 EUR', '100': '100 EUR'},
					to: {'__placeholder': 'Do', '50': '50 EUR', '100': '100 EUR', ' ': '100+ EUR'}
				}
			},
			rent_long: {
				pln: {
					from: {'__placeholder': 'Od', '0': '0 PLN', '10000': '10.000 PLN', '20000': '20.000 PLN', '50000': '50.000 PLN'},
					to: {'__placeholder': 'Do', '10000': '10.000 PLN', '20000': '20.000 PLN', '50000': '50.000 PLN', ' ': '50.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': 'Od', '0': '0 USD', '2000': '2.000 USD', '4000': '4.000 USD', '7000': '7.000 USD'},
					to: {'__placeholder': 'Do', '2000': '2.000 USD', '4000': '4.000 USD', '7000': '7.000 USD', ' ': '7.000+ USD'}
				},
				eur: {
					from: {'__placeholder': 'Od', '0': '0 EUR', '1250': '1.250 EUR', '2500': '2.500 EUR', '5000': '5.000 EUR'},
					to: {'__placeholder': 'Do', '1250': '1.250 EUR', '2500': '2.500 EUR', '5000': '5.000 EUR', ' ': '5.000+ EUR'}
				}
			}
		},
		apartment: {
			buy: {
				pln: {
					from: {'__placeholder': 'Od', '0': '0 PLN', '1000000': '1.000.000 PLN', '3000000': '3.000.000 PLN', '5000000': '5.000.000 PLN'},
					to: {'__placeholder': 'Do', '1000000': '1.000.000 PLN', '3000000': '3.000.000', '5000000': '5.000.000 PLN', ' ': '5.000.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': 'Od', '0': '0 USD', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD'},
					to: {'__placeholder': 'Do', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD', ' ': '800.000+ USD'}
				},
				eur: {
					from: {'__placeholder': 'Od', '0': '0 EUR', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR'},
					to: {'__placeholder': 'Do', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR', ' ': '1.250.000+ EUR'},
				}
			},
			rent_short: {
				pln: {
					from: {'__placeholder': 'Od', '0': '0 PLN', '200': '200 PLN', '400': '400 PLN'},
					to: {'__placeholder': 'Do', '200': '200 PLN', '400': '400 PLN', ' ': '400+ PLN'}
				},
				usd: {
					from: {'__placeholder': 'Od', '0': '0 USD', '40': '40 USD', '100': '100 USD'},
					to: {'__placeholder': 'Do', '40': '40 USD', '100': '100 USD', ' ': '100+ USD'}
				},
				eur: {
					from: {'__placeholder': 'Od', '0': '0 EUR', '50': '50 EUR', '100': '100 EUR'},
					to: {'__placeholder': 'Do', '50': '50 EUR', '100': '100 EUR', ' ': '100+ EUR'}
				}
			},
			rent_long: {
				pln: {
					from: {'__placeholder': 'Od', '0': '0 PLN', '10000': '10.000 PLN', '20000': '20.000 PLN', '50000': '50.000 PLN'},
					to: {'__placeholder': 'Do', '10000': '10.000 PLN', '20000': '20.000 PLN', '50000': '50.000 PLN', ' ': '50.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': 'Od', '0': '0 USD', '2000': '2.000 USD', '4000': '4.000 USD', '7000': '7.000 USD'},
					to: {'__placeholder': 'Do', '2000': '2.000 USD', '4000': '4.000 USD', '7000': '7.000 USD', ' ': '7.000+ USD'}
				},
				eur: {
					from: {'__placeholder': 'Od', '0': '0 EUR', '1250': '1.250 EUR', '2500': '2.500 EUR', '5000': '5.000 EUR'},
					to: {'__placeholder': 'Do', '1250': '1.250 EUR', '2500': '2.500 EUR', '5000': '5.000 EUR', ' ': '5.000+ EUR'}
				}
			}
		},
		house: {
			buy: {
				pln: {
					from: {'__placeholder': 'Od', '0': '0 PLN', '1000000': '1.000.000 PLN', '3000000': '3.000.000 PLN', '5000000': '5.000.000 PLN'},
					to: {'__placeholder': 'Do', '1000000': '1.000.000 PLN', '3000000': '3.000.000', '5000000': '5.000.000 PLN', ' ': '5.000.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': 'Od', '0': '0 USD', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD'},
					to: {'__placeholder': 'Do', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD', ' ': '800.000+ USD'}
				},
				eur: {
					from: {'__placeholder': 'Od', '0': '0 EUR', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR'},
					to: {'__placeholder': 'Do', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR', ' ': '1.250.000+ EUR'},
				}
			},
			rent_short: {
				pln: {
					from: {'__placeholder': 'Od', '0': '0 PLN', '200': '200 PLN', '400': '400 PLN'},
					to: {'__placeholder': 'Do', '200': '200 PLN', '400': '400 PLN', ' ': '400+ PLN'}
				},
				usd: {
					from: {'__placeholder': 'Od', '0': '0 USD', '40': '40 USD', '100': '100 USD'},
					to: {'__placeholder': 'Do', '40': '40 USD', '100': '100 USD', ' ': '100+ USD'}
				},
				eur: {
					from: {'__placeholder': 'Od', '0': '0 EUR', '50': '50 EUR', '100': '100 EUR'},
					to: {'__placeholder': 'Do', '50': '50 EUR', '100': '100 EUR', ' ': '100+ EUR'}
				}
			},
			rent_long: {
				pln: {
					from: {'__placeholder': 'Od', '0': '0 PLN', '10000': '10.000 PLN', '20000': '20.000 PLN', '50000': '50.000 PLN'},
					to: {'__placeholder': 'Do', '10000': '10.000 PLN', '20000': '20.000 PLN', '50000': '50.000 PLN', ' ': '50.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': 'Od', '0': '0 USD', '2000': '2.000 USD', '4000': '4.000 USD', '7000': '7.000 USD'},
					to: {'__placeholder': 'Do', '2000': '2.000 USD', '4000': '4.000 USD', '7000': '7.000 USD', ' ': '7.000+ USD'}
				},
				eur: {
					from: {'__placeholder': 'Od', '0': '0 EUR', '1250': '1.250 EUR', '2500': '2.500 EUR', '5000': '5.000 EUR'},
					to: {'__placeholder': 'Do', '1250': '1.250 EUR', '2500': '2.500 EUR', '5000': '5.000 EUR', ' ': '5.000+ EUR'}
				}
			}
		},
		lot: {
			buy: {
				pln: {
					from: {'__placeholder': 'Od', '0': '0 PLN', '1000000': '1.000.000 PLN', '4000000': '4.000.000 PLN', '6000000': '6.000.000 PLN'},
					to: {'__placeholder': 'Do', '1000000': '1.000.000 PLN', '4000000': '4.000.000', '5000000': '6.000.000 PLN', ' ': '6.000.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': 'Od', '0': '0 USD', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD'},
					to: {'__placeholder': 'Do', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD', ' ': '800.000+ USD'}
				},
				eur: {
					from: {'__placeholder': 'Od', '0': '0 EUR', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR'},
					to: {'__placeholder': 'Do', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR', ' ': '1.250.000+ EUR'},
				}
			}
		}
	},
	price_m2: {
		default: {
			buy: {
				pln: {
					from: {'__placeholder': 'Od', '0': '0 PLN', '500': '1.000 PLN', '3000': '3.000 PLN'},
					to: {'__placeholder': 'Do', '500': '1.000 PLN', '3000': '3.000 PLN', ' ': '3.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': 'Od', '0': '0 USD', '100': '100 USD', '700': '700 USD'},
					to: {'__placeholder': 'Do', '100': '100 USD', '700': '700 USD', ' ': '700+ USD'}
				},
				eur: {
					from: {'__placeholder': 'Od', '0': '0 EUR', '120': '120 EUR', '750': '750 EUR'},
					to: {'__placeholder': 'Do', '120': '120 EUR', '750': '750 EUR', ' ': '750+ EUR'}
				}
			}
		}
	},
	area: {
		apartment: {
			buy: {
				m2: {
					from: {'__placeholder': 'Od', '0': '0', '100': '100m2', '200': '200m2', '400': '400m2'},
					to: {'__placeholder': 'Do', '100': '100m2', '200': '200m2', '400': '400m2', ' ': '400m2+'}
				}
			},
			rent_long: {
				m2: {
					from: {'__placeholder': 'Od', '0': '0', '100': '100m2', '200': '200m2', '400': '400m2'},
					to: {'__placeholder': 'Do', '100': '100m2', '200': '200m2', '400': '400m2', ' ': '400m2+'}
				}
			}
		},
		house: {
			buy: {
				m2: {
					from: {'__placeholder': 'Od', '0': '0', '100': '100m2', '300': '300m2', '500': '500m2'},
					to: {'__placeholder': 'Do', '100': '100m2', '300': '300m2', '500': '500m2', ' ': '500m2+'}
				}
			},
			rent_long: {
				m2: {
					from: {'__placeholder': 'Od', '0': '0', '100': '100m2', '200': '200m2', '400': '400m2'},
					to: {'__placeholder': 'Do', '100': '100m2', '200': '200m2', '400': '400m2', ' ': '400m2+'}
				}
			}
		},
		lot: {
			buy: {
				m2: {
					from: {'__placeholder': 'Od', '0': '0', '1000': '1.000m2', '5000': '5.000m2', '10000': '10.000m2'},
					to: {'__placeholder': 'Do', '1000': '1.000m2', '5000': '5.000m2', '10000': '10.000m2', ' ': '10.000m2+'}
				}
			}
		}
	},
	rooms: {
		default: {
			buy: {
				from: {'__placeholder': 'Od', '1': '1', '3': '3', '5': '5'},
				to: {'__placeholder': 'Do', '3': '3', '5': '5', ' ': '5+'}
			},
			rent_short: {
				from: {'__placeholder': 'Od', '1': '1', '3': '3', '5': '5'},
				to: {'__placeholder': 'Do', '3': '3', '5': '5', ' ': '5+'}
			},
			rent_long: {
				from: {'__placeholder': 'Od', '1': '1', '3': '3', '5': '5'},
				to: {'__placeholder': 'Do', '3': '3', '5': '5', ' ': '5+'}
			}
		},
		apartment: {
			buy: {
				from: {'__placeholder': 'Od', '1': '1', '3': '3', '5': '5'},
				to: {'__placeholder': 'Do', '3': '3', '5': '5', ' ': '5+'}
			},
			rent_short: {
				from: {'__placeholder': 'Od', '1': '1', '2': '2'},
				to: {'__placeholder': 'Do', '2': '2', ' ': '2+'}
			},
			rent_long: {
				from: {'__placeholder': 'Od', '1': '1', '3': '3', '5': '5'},
				to: {'__placeholder': 'Do', '3': '3', '5': '5', ' ': '5+'}
			}
		},
	},
	house_lot_area: {
		m2: {
			from: {'__placeholder': 'Od', '0': '0', '500': '500m2', '1000': '1.000m2', '3000': '3.000m2'},
			to: {'__placeholder': 'Do', '500': '500m2', '1000': '1.000m2', '3000': '3.000m2', ' ': '3.000m2+'}
		}
	}
};
(function($) {

	$.fn.offerGalleryCarousel = function(options) {
		function positionSlide($img) {
			var $viewport = $img.closest('.viewport');
				$img.css('width', 'auto').css('margin-top', 0);

			if($img.width() > $viewport.width()) {
				$img.css('width', '100%');
			}

			if($img.height() < $viewport.height()) {
				$img.css('margin-top', (($viewport.height() - $img.height())/2)+'px');
			}
		}

		return this.each(function() {
			var $self = $(this);

			// Setup carousel connector
			var connector = function(itemNavigation, carouselStage) {
				return carouselStage.jcarousel('items').eq(itemNavigation.index());
			};

			// Setup carousel
			var carouselStage = $(this).jcarousel({
				list: '.slides',
				animation: {
					duration: 600,
					easing: 'easeInOutCirc',
				}
			})
				.on('jcarousel:targetin', 'figure', function() {
					$(this).prevAll().length === 0 ? $self.find('.prev').hide() : $self.find('.prev').show();
					$(this).nextAll().length === 0 ? $self.find('.next').hide() : $self.find('.next').show();
				});

			// Setup carousel navigation
			var carouselNavigation = $(this).find('.navigation .carousel').jcarousel();

			carouselNavigation.jcarousel('items').each(function() {
				var item = $(this);
				var target = connector(item, carouselStage);

				item
					.on('jcarouselcontrol:active', function() {
						carouselNavigation.jcarousel('scrollIntoView', this);
						item.addClass('active');
					})
					.on('jcarouselcontrol:inactive', function() {
						item.removeClass('active');
			 		})
					.jcarouselControl({
						target: target,
						carousel: carouselStage
					});
			});

			// Set fixed widths for slides and slides layer
			$self.find('.slides').css('width', '2000em');
			$self.find('.slides figure').css('width', $self.find('.viewport').width());
			
			// Setup controller
			$self.find('.prev').click(function() {
				$self.jcarousel('scroll', '-=1');
				return false;
			});
			$self.find('.next').click(function() {
				$self.jcarousel('scroll', '+=1');
				return false;
			});

			 // Setup controls for the navigation carousel
			$(this).find('.nav-prev')
				.on('jcarouselcontrol:inactive', function() {
					$(this).addClass('inactive');
				})
				.on('jcarouselcontrol:active', function() {
					$(this).removeClass('inactive');
				})
				.jcarouselControl({
					target: '-=1'
				});

			$(this).find('.nav-next')
				.on('jcarouselcontrol:inactive', function() {
					$(this).addClass('inactive');
				})
				.on('jcarouselcontrol:active', function() {
					$(this).removeClass('inactive');
				})
				.jcarouselControl({
					target: '+=1'
				});

			// Hide/show controllers
			$self.jcarousel('visible').prevAll().length === 0 ? $self.find('.prev').hide() : $self.find('.prev').show();
			$self.jcarousel('visible').nextAll().length === 0 ? $self.find('.next').hide() : $self.find('.next').show();

			// Position slides
			//$self.find('.slides figure img').resizeToParent({ parent: '.viewport' });
			$self.find('.slides figure img').load(function() {
				positionSlide($(this));
			});

			$(window).resize(function() {
				// Set fixed width for navigation thumbs
				$self.find('.navigation .carousel ul li').css('width', ($self.find('.navigation .carousel').width()/5)-10);

				// Set fixed width for slides
				$self.find('.slides figure').css('width', $self.find('.viewport').width());

				// Position slide
				$self.find('.slides figure img').each(function() {
					positionSlide($(this));
				});

				// Refresh carousel
				$self.jcarousel('reload');
			});
		});
	};

})(jQuery);
								<div class="form-group" data-form-fieldset="province"<?php if($disabled) : ?> style="display: none;"<?php endif; ?>>
									<label><?php echo chtof_label('offer_location_province'); ?></label>
									<?php
										echo chofl_select('province', array(
											'' => __('Wszystkie', 'chtheme')
										), null, $disabled ? array('disabled' => 'disabled') : array());
									?>
								</div>
								<!-- .form-group -->
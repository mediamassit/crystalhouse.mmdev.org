<?php
	get_header();

	$created_date = (int) get_field('offer_asari_listing_created-date');
	$updated_date = (int) get_field('offer_asari_listing_updated-date');
	$map_latlng = sizeof(explode(',', get_field('offer_location_latlng'))) == 2 ? explode(',', get_field('offer_location_latlng')) : null;
	$gallery = rwmb_meta('offer_gallery', Array('type' => 'plupload_image'), $post->ID);
	$video_url = rwmb_meta('offer_movie-url', Array('type' => 'raw'), $post->ID);
	$view_360_url = rwmb_meta('offer_360view', Array('type' => 'raw'), $post->ID);

	$gmap_visible = array_key_exists($post->post_type, get_opt('opt-gmap-single'));
	$gmap_address = cht_get_offer_gmap_address($post);
	$gmap_latlng = get_field('offer_location_latlng');
?>

	<div id="content">
		<div class="container">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php get_template_part('src/partials/breadcrumbs'); ?>

			<div class="page-meta">
				<p>
					<?php _e('Opublikowane', 'chtheme'); ?> <strong><?php echo date('d.m.Y', $created_date > 0 ? $created_date : get_the_time('U')); ?></strong>
					<?php if($updated_date > 0) : ?> / <?php _e('ostatnia aktualizacja', 'chtheme'); ?> <strong><?php echo date('d.m.Y', $updated_date); ?></strong><?php endif; ?>
					/ <?php edit_post_link(__('Edytuj ofertę', 'chtheme')); ?>
				</p>
			</div>
			<!-- .post-meta -->

			<div class="page-title">
				<h1>
					<?php if(strlen(get_field('offer_asari_listing_id')) > 0) : ?><span class="side-note"><?php _e('Numer oferty', 'chtheme'); ?> <strong><?php the_field('offer_asari_listing_id'); ?></strong></span> <?php endif; ?>
					<?php the_title(); ?>
				</h1>
			</div>
			<!-- .page-header -->

			<div class="single-offer">
				<nav class="top-navigation hidden-print">
					<ul class="left sections">
						<li><a href="#description" data-target-tab="#offer-tab-1"><em class="icon-description"></em> <?php _e('Opis', 'chtheme'); ?></a></li>
						<?php if($gmap_visible) : ?><li><a href="#map" data-target-tab="#offer-tab-1"><em class="icon-map"></em> <?php _e('Mapa', 'chtheme'); ?></a></li><?php endif; ?>
						<?php if(sizeof($gallery) > 0) : ?><li><a href="#gallery" data-target-tab="#offer-tab-1"><em class="icon-gallery"></em> <?php _e('Zdjęcia', 'chtheme'); ?></a></li><?php endif; ?>
						<?php if(strlen(trim(get_field('offer_offers_table'))) > 0) : ?><li><a href="#offer" data-target-tab="#offer-tab-2"><em class="icon-offers"></em> <?php _e('Oferty', 'chtheme'); ?></a></li><?php endif; ?>
						<?php if(strlen($view_360_url) > 0) : ?><li><a href="<?php echo $view_360_url; ?>" target="_blank"><em class="icon-360"></em> <?php _e('Wirtualna wycieczka', 'chtheme'); ?></a></li><?php endif; ?>
						<?php if(strlen($video_url) > 0) : ?><li><a href="<?php echo $video_url; ?>" class="fancybox-media"><em class="icon-movie"></em> <?php _e('Film', 'chtheme'); ?></a></li><?php endif; ?>
					</ul>

					<ul class="right">
						<li><a href="javascript:void(0);" data-addtoclipboard="<?php echo $post->ID; ?>"><em class="circle-button clipboard"></em> <span><?php _e('Dodaj do schowka', 'chtheme'); ?></span></a></li>
						<li><a href="javascript:print();"><em class="circle-button print"></em></a></li>
						<li><a class="addthis_button_compact" href="http://www.addthis.com/bookmark.php?v=300&amp;pubid=xa-538a578c3e089f8a" title="<?php _e('Udostępnij', 'chtheme'); ?>" target="_blank"><em class="circle-button share"></em></a></li>
					</ul>
				</nav>
				<!-- .top-navigation -->

				<div class="row divider">
					<div class="col-md-9 main">
						<div class="tab active" id="offer-tab-1">
							<?php if(sizeof($gallery) > 0) : ?>
							<section class="widget carousel-gallery" id="gallery">
								<h2><?php _e('Zdjęcia', 'chtheme'); ?></h2>
								<div class="viewport">
									<a href="javascript:void(0);" class="prev"></a>
									<a href="javascript:void(0);" class="next"></a>

									<div class="slides">
										<?php
											foreach($gallery as $id => $image) :
												$img = wp_get_attachment_image_src($id, Array(870, 440));
										?>
										<figure><a href="<?php echo cht_watermark_image($image['full_url']); ?>" data-fancybox-group="gallery" class="fancybox"><img src="<?php echo cht_watermark_image($img[0]); ?>" width="<?php echo $img[1]; ?>" alt=""></a></figure>
										<?php endforeach; ?>
									</div>
									<!-- .slides -->
								</div>
								<!-- .viewport -->

								<nav class="navigation">
									<a href="javascript:void(0);" class="nav-prev"></a>
									<a href="javascript:void(0);" class="nav-next"></a>

									<div class="carousel">
										<ul>
											<?php
												foreach($gallery as $id => $image) :
													$img = wp_get_attachment_image_src($id, Array(150, 115));
											?>
											<li><a href="javascript:void(0);"><img src="<?php echo $img[0]; ?>" width="150" height="115" alt=""></a></li>
											<?php endforeach; ?>
										</ul>
									</div>
									<!-- .carousel -->
								</nav>
								<!-- .navigation -->
							</section>
							<!-- .carousel-gallery -->
							<?php endif; ?>

							<section class="widget text page" id="description">
								<h2><?php _e('Opis', 'chtheme'); ?></h2>
								<?php if(strlen(get_the_content()) > 0) : ?>
									<?php the_content(); ?>
								<?php else : ?>
									<?php _e('Brak opisu.', 'chtheme'); ?>
								<?php endif; ?>
							</section>
							<!-- .text -->

							<?php if($gmap_visible) : ?>
							<section class="widget google-map" id="map">
								<h2><?php _e('Mapa', 'chtheme'); ?></h2>
								<div class="map"></div>
							</section>
							<!-- .google-map -->
							<?php endif; ?>
						</div>
						<!-- .tab -->

						<?php if(strlen(trim(get_field('offer_offers_table'))) > 0) : ?>
						<div class="tab" id="offer-tab-2">
							<section class="widget text page" id="offer">
								<h2><?php _e('Oferty', 'chtheme'); ?></h2>
								<?php echo do_shortcode(get_field('offer_offers_table')); ?>
							</section>
							<!-- .text -->
						</div>
						<!-- .tab -->
						<?php endif; ?>
					</div>
					<!-- .col-md-9 -->

					<div class="col-md-3 sidebar">
						<?php get_template_part('src/partials/offer-meta/price') ?>

						<?php
							switch($post->post_type) {
								default:
									get_template_part('src/partials/offer-meta/default');
									break;
								case 'offer_lot':
									get_template_part('src/partials/offer-meta/lot');
									break;
								case 'offer_business':
									get_template_part('src/partials/offer-meta/business');
									break;
							}
						?>

						<?php get_template_part('src/partials/offer-contact-broker') ?>
					</div>
					<!-- .col-md-3 -->
				</div>
				<!-- .row -->
			</div>
			<!-- .single-offer -->
			<?php endwhile; endif; ?>
		</div>
		<!-- .container -->
	</div>
	<!-- #content -->

	<?php get_template_part('src/partials/page-contact-form'); ?>

	<?php if($gmap_visible) : ?><script>
		(function($) {
			$(window).load(function() {
				$('.google-map .map').gmap3({
					map: {
						<?php if(strlen(trim($gmap_latlng)) <= 0) : ?>address: "<?php echo $gmap_address; ?>",<?php endif; ?>
						options: {
							zoom: 15,
							<?php if(strlen(trim($gmap_latlng)) > 0) : ?>center: new google.maps.LatLng(<?php echo $gmap_latlng; ?>),<?php endif; ?>
							styles: [{"stylers": [{ "hue": "#005eff" }, { "lightness": 13 }]}],
							scrollwheel: false
						}
					},
					marker: {
						<?php if(strlen(trim($gmap_latlng)) <= 0) : ?>address: "<?php echo $gmap_address; ?>",<?php endif; ?>
						<?php if(strlen(trim($gmap_latlng)) > 0) : ?>latLng: [<?php echo $gmap_latlng; ?>],<?php endif; ?>
						options: {
							icon: THEME_URL+'/images/spotlight-poi-blue.png',
							title: "<?php the_title(); ?>"
						}
					}
				});
			});
		})(jQuery);
	</script><?php endif; ?>

<?php get_footer(); ?>
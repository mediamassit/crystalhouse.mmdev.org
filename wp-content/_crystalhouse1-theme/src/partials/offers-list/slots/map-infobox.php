<?php

// Setup thumbnail
$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(370, 215));
$thumb = is_array($thumb) ? '<img src="'.$thumb[0].'" width="'.$thumb[1].'" alt="">' : '<img src="'.get_bloginfo('template_url').'/tmp/thumb_370x215.jpg" alt="">';

// Setup price
if(cht_offer_is_for_long_rent()) {
	$price = '<strong>'.chtof_price(get_field('offer_price_primary')).' <small>'.__('/ msc', 'chtheme').'</small></strong>';
}
elseif(cht_offer_is_for_short_rent()) {
	$price = '<strong>'.chtof_price(get_field('offer_price_primary')).' <small>'.__('/ dzień', 'chtheme').'</small></strong>';
}
else {
	$price = '<strong>'.chtof_price(get_field('offer_price_primary')).'</strong>';
}

?><div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="<?php echo get_permalink($post->ID); ?>"><?php echo $thumb; ?></a></figure><?php if(chtof_has('offer_location_street')) : ?><p><strong><?php echo chtof_label('offer_location_street'); ?>:</strong> <?php echo get_field('offer_location_street'); ?></p><?php endif; ?><?php if(chtof_has('offer_property_area-primary')) : ?><p><strong><?php echo chtof_label('offer_property_area-primary'); ?>:</strong> <?php echo get_field('offer_property_area-primary'); ?>m<sup>2</sup></p><?php endif; ?><?php if(chtof_has('offer_price_primary')) : ?><p class="price"><strong><?php echo chtof_label('offer_price_primary'); ?>:</strong> <strong><?php echo $price; ?></strong></p><?php endif; ?> <a href="<?php echo get_permalink($post->ID); ?>" class="more">&raquo;</a></div>
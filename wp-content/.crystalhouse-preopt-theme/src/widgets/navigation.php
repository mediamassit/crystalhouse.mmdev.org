<?php

class cht_WP_Nav_Menu_Widget extends WP_Nav_Menu_Widget {
	function widget($args, $instance) {
		// Get menu
		$nav_menu = ! empty( $instance['nav_menu'] ) ? wp_get_nav_menu_object( $instance['nav_menu'] ) : false;

		if(!$nav_menu)
			return;

		/** This filter is documented in wp-includes/default-widgets.php */
		$instance['title'] = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

		echo $args['before_widget'];

		if ( !empty($instance['title']) )
			echo $args['before_title'] . $instance['title'] . $args['after_title'];

		//wp_nav_menu( array( 'fallback_cb' => '', 'menu' => $nav_menu ) );
		$items = wp_get_nav_menu_items($nav_menu);

		echo $args['before_list'];
		echo '<ul>';

		foreach($items as $item) {
			printf('<li><a href="%s" title="%s">%s</a></li>', $item->url, $item->title, $item->title);
		}

		echo '</ul>';
		echo $args['after_list'];

		echo $args['after_widget'];
	}
}

function cht_register_nav_widget() { 
	unregister_widget('WP_Widget_Calendar');
	register_widget('cht_WP_Nav_Menu_Widget');
}
add_action('widgets_init', 'cht_register_nav_widget', 1);
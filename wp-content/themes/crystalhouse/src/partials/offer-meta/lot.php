						<section class="widget offer-meta">
							<?php
								// Location meta
								$meta = chtof_meta_fields('offer_location_', array('offer_location_latlng', 'offer_location_center-distance', 'offer_location_country'));

								if(sizeof($meta) > 0) :
							?>
							<ul>
								<?php foreach($meta as $field) : ?>
								<li><strong><?php echo chtof_label($field['name']); ?></strong>
									<span><?php echo chtof_value(get_field($field['name']), $field['name']); ?></span></li>
								<?php endforeach; ?>
							</ul>
							<?php endif; ?>

							<ul>
								<?php if(chtof_has('offer_lot-type')) :	?>
								<li><strong><?php echo chtof_label('offer_lot-type'); ?></strong>
									<span><?php echo chtof_value(get_field('offer_lot-type'), 'offer_lot-type'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_property_area-primary')) : ?>
								<li><strong><?php echo chtof_label('offer_property_area-primary'); ?></strong>
									<span><?php the_field('offer_property_area-primary'); ?> m<sup>2</sup> / <?php echo cht_sqm2feet(get_field('offer_property_area-primary')); ?> sqft</span></li>
								<?php endif; ?>
							</ul>

							<?php
								// Additional fields
								if(chtof_has('offer_additional-fields')) :
									$additional = explode("\r\n", trim(get_field('offer_additional-fields')));
							?>
							<ul>
								<?php
									foreach($additional as $line) :
										$field = explode(':', $line);

										if(sizeof($field) === 2) :
								?>
								<li><strong><?php echo trim(@$field[0]); ?></strong>
									<span><?php echo trim(@$field[1]); ?></span></li>
								<?php
										endif;
									endforeach;
								?>
							</ul>
							<?php endif; ?>
							
							<?php get_template_part('src/partials/offers-list/slots/attributes'); ?>
						</section>
						<!-- .offer-meta -->
<?php

function cht_prepend_placeholder($array) {
	return array_merge(array('__placeholder' => __('Wszystkie', 'chtheme')), $array);
}

function cht_get_form_filters($return = false) {
	$locations_tree = cht_get_locations_tree(true);

	// Default cities
	$locations_tree['city']['default'] = array();

	foreach($locations_tree['city'] as $province => $cities) {
		$locations_tree['city']['default'] = array_merge($locations_tree['city']['default'], $cities);
	}

	$locations_tree['city']['default'] = array_unique($locations_tree['city']['default']);

	setlocale(LC_COLLATE, 'pl_PL.UTF-8');
	asort($locations_tree['city']['default'], SORT_LOCALE_STRING);

	$locations_tree['country'] = array_merge(array('__placeholder' => __('Wszystkie', 'chtheme')), $locations_tree['country']);
	$locations_tree['province'] = array_map('cht_prepend_placeholder', $locations_tree['province']);
	$locations_tree['city'] = array_map('cht_prepend_placeholder', $locations_tree['city']);
	$locations_tree['district'] = array_map('cht_prepend_placeholder', $locations_tree['district']);

	$filters = array(

		'locations' => $locations_tree,

		// Prices
		'price' => array(

			// Default values
			'default' => array(
				'buy' => array(
					'pln' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 PLN', 
							'500000' => '500.000 PLN', 
							'1000000' => '1.000.000 PLN', 
							'2000000' => '2.000.000 PLN',
							'3000000' => '3.000.000 PLN',
							'4000000' => '4.000.000 PLN',
							'5000000' => '5.000.000 PLN'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'1000000' => '1.000.000 PLN', 
							'2000000' => '2.000.000 PLN',
							'3000000' => '3.000.000 PLN',
							'4000000' => '4.000.000 PLN',
							'5000000' => '5.000.000 PLN',
							'_' => '5.000.000+ PLN'
						)
					),
					'usd' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'),
							'0' => '0 USD', 
							'100000' => '100.000 USD', 
							'200000' => '200.000 USD', 
							'300000' => '300.000 USD',
							'500000' => '500.000 USD',
							'750000' => '750.000 USD',
							'1000000' => '1.000.000 USD'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'),
							'200000' => '200.000 USD', 
							'300000' => '300.000 USD',
							'500000' => '500.000 USD',
							'750000' => '750.000 USD',
							'1000000' => '1.000.000 USD',
							'_' => '1.000.000+ USD'
						)
					),
					'eur' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 EUR', 
							'125000' => '125.000 EUR', 
							'250000' => '250.000 EUR', 
							'500000' => '500.000 EUR',
							'750000' => '750.000 EUR',
							'1000000' => '1.000.000 EUR',
							'1250000' => '1.250.000 EUR'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'250000' => '250.000 EUR', 
							'500000' => '500.000 EUR',
							'750000' => '750.000 EUR',
							'1000000' => '1.000.000 EUR',
							'1250000' => '1.250.000 EUR',
							'_' => '1.250.000+ EUR'
						)
					)
				),
				'rent_short' => array(
					'pln' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 PLN', 
							'100' => '100 PLN', 
							'200' => '200 PLN',
							'300' => '300 PLN',
							'400' => '400 PLN',
							'500' => '500 PLN'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'100' => '100 PLN', 
							'200' => '200 PLN',
							'300' => '300 PLN',
							'400' => '400 PLN',
							'500' => '500 PLN',
							'_' => '500+ PLN'
						)
					),
					'usd' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 USD', 
							'20' => '20 USD', 
							'40' => '40 USD',
							'60' => '60 USD',
							'80' => '80 USD',
							'100' => '100 USD'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'20' => '20 USD', 
							'40' => '40 USD',
							'60' => '60 USD',
							'80' => '80 USD',
							'100' => '100 USD',
							'_' => '100+ USD'
						)
					),
					'eur' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 EUR', 
							'25' => '25 EUR', 
							'50' => '50 EUR',
							'75' => '75 EUR',
							'100' => '100 EUR',
							'125' => '125 EUR'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'25' => '25 EUR', 
							'50' => '50 EUR',
							'75' => '75 EUR',
							'100' => '100 EUR',
							'125' => '125 EUR',
							'_' => '125+ EUR'
						)
					)
				),

				'rent_long' => array(
					'pln' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 PLN', 
							'3000' => '3.000 PLN', 
							'4000' => '4.000 PLN', 
							'5000' => '5.000 PLN',
							'10000' => '10.000 PLN'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'3000' => '3.000 PLN', 
							'4000' => '4.000 PLN', 
							'5000' => '5.000 PLN',
							'10000' => '10.000 PLN',
							'_' => '10.000+ PLN'
						)
					),
					'usd' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 USD', 
							'500' => '500 USD', 
							'750' => '750 USD', 
							'1000' => '1.000 USD',
							'2000' => '2.000 USD'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'500' => '500 USD', 
							'750' => '750 USD', 
							'1000' => '1.000 USD',
							'2000' => '2.000 USD',
							'_' => '2.000+ USD'
						)
					),
					'eur' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 EUR', 
							'750' => '750 EUR', 
							'1000' => '1.000 EUR', 
							'1250' => '1.250 EUR',
							'2500' => '2.500 EUR'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'750' => '750 EUR', 
							'1000' => '1.000 EUR', 
							'1250' => '1.250 EUR',
							'2500' => '2.500 EUR',
							'_' => '2.500+ EUR'
						)
					)
				)
			),

			// Apartment post type
			'apartment' => array(
				'buy' => array(
					'pln' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 PLN', 
							'500000' => '500.000 PLN', 
							'1000000' => '1.000.000 PLN', 
							'2000000' => '2.000.000 PLN',
							'3000000' => '3.000.000 PLN',
							'4000000' => '4.000.000 PLN',
							'5000000' => '5.000.000 PLN',
							'10000000' => '10.000.000 PLN'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'1000000' => '1.000.000 PLN', 
							'2000000' => '2.000.000 PLN',
							'3000000' => '3.000.000 PLN',
							'4000000' => '4.000.000 PLN',
							'5000000' => '5.000.000 PLN',
							'10000000' => '10.000.000 PLN',
							'20000000' => '20.000.000 PLN',
							'_' => '20.000.000+ PLN'
						)
					),
					'usd' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'),
							'0' => '0 USD', 
							'100000' => '100.000 USD', 
							'200000' => '200.000 USD', 
							'300000' => '300.000 USD',
							'500000' => '500.000 USD',
							'750000' => '750.000 USD',
							'1000000' => '1.000.000 USD',
							'4000000' => '4.000.000 USD'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'),
							'200000' => '200.000 USD', 
							'300000' => '300.000 USD',
							'500000' => '500.000 USD',
							'750000' => '750.000 USD',
							'1000000' => '1.000.000 USD',
							'4000000' => '4.000.000 USD',
							'8000000' => '8.000.000 USD',
							'_' => '8.000.000+ USD'
						)
					),
					'eur' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 EUR', 
							'125000' => '125.000 EUR', 
							'250000' => '250.000 EUR', 
							'500000' => '500.000 EUR',
							'750000' => '750.000 EUR',
							'1000000' => '1.000.000 EUR',
							'1250000' => '1.250.000 EUR',
							'2500000' => '2.500.000 EUR'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'250000' => '250.000 EUR', 
							'500000' => '500.000 EUR',
							'750000' => '750.000 EUR',
							'1000000' => '1.000.000 EUR',
							'1250000' => '1.250.000 EUR',
							'2500000' => '2.500.000 EUR',
							'5000000' => '5.000.000 EUR',
							'_' => '5.000.000+ EUR'
						)
					)
				)
			),

			// House post type
			'house' => array(
				'buy' => array(
					'pln' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 PLN', 
							'500000' => '500.000 PLN', 
							'1000000' => '1.000.000 PLN', 
							'2000000' => '2.000.000 PLN',
							'3000000' => '3.000.000 PLN',
							'4000000' => '4.000.000 PLN',
							'5000000' => '5.000.000 PLN',
							'10000000' => '10.000.000 PLN',
							'15000000' => '15.000.000 PLN'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'1000000' => '1.000.000 PLN', 
							'2000000' => '2.000.000 PLN',
							'3000000' => '3.000.000 PLN',
							'4000000' => '4.000.000 PLN',
							'5000000' => '5.000.000 PLN',
							'10000000' => '10.000.000 PLN',
							'15000000' => '15.000.000 PLN',
							'20000000' => '20.000.000 PLN',
							'_' => '20.000.000+ PLN'
						)
					),
					'usd' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'),
							'0' => '0 USD', 
							'100000' => '100.000 USD', 
							'200000' => '200.000 USD', 
							'300000' => '300.000 USD',
							'500000' => '500.000 USD',
							'750000' => '750.000 USD',
							'1000000' => '1.000.000 USD',
							'4000000' => '4.000.000 USD',
							'6000000' => '6.000.000 USD'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'),
							'200000' => '200.000 USD', 
							'300000' => '300.000 USD',
							'500000' => '500.000 USD',
							'750000' => '750.000 USD',
							'1000000' => '1.000.000 USD',
							'4000000' => '4.000.000 USD',
							'6000000' => '6.000.000 USD',
							'8000000' => '8.000.000 USD',
							'_' => '8.000.000+ USD'
						)
					),
					'eur' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 EUR', 
							'125000' => '125.000 EUR', 
							'250000' => '250.000 EUR', 
							'500000' => '500.000 EUR',
							'750000' => '750.000 EUR',
							'1000000' => '1.000.000 EUR',
							'1250000' => '1.250.000 EUR',
							'2500000' => '2.500.000 EUR',
							'3500000' => '3.500.000 EUR'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'250000' => '250.000 EUR', 
							'500000' => '500.000 EUR',
							'750000' => '750.000 EUR',
							'1000000' => '1.000.000 EUR',
							'1250000' => '1.250.000 EUR',
							'2500000' => '2.500.000 EUR',
							'3500000' => '3.500.000 EUR',
							'5000000' => '5.000.000 EUR',
							'_' => '5.000.000+ EUR'
						)
					)
				)
			),

			// Lot post type
			'lot' => array(
				'buy' => array(
					'pln' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 PLN', 
							'500000' => '500.000 PLN', 
							'1000000' => '1.000.000 PLN', 
							'2000000' => '2.000.000 PLN',
							'3000000' => '3.000.000 PLN',
							'4000000' => '4.000.000 PLN',
							'5000000' => '5.000.000 PLN',
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'1000000' => '1.000.000 PLN', 
							'2000000' => '2.000.000 PLN',
							'3000000' => '3.000.000 PLN',
							'4000000' => '4.000.000 PLN',
							'5000000' => '5.000.000 PLN',
							'10000000' => '10.000.000 PLN',
							'_' => '10.000.000+ PLN'
						)
					),
					'usd' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'),
							'0' => '0 USD', 
							'100000' => '100.000 USD', 
							'200000' => '200.000 USD', 
							'300000' => '300.000 USD',
							'500000' => '500.000 USD',
							'750000' => '750.000 USD',
							'1000000' => '1.000.000 USD'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'),
							'200000' => '200.000 USD', 
							'300000' => '300.000 USD',
							'500000' => '500.000 USD',
							'750000' => '750.000 USD',
							'1000000' => '1.000.000 USD',
							'_' => '1.000.000+ USD'
						)
					),
					'eur' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 EUR', 
							'125000' => '125.000 EUR', 
							'250000' => '250.000 EUR', 
							'500000' => '500.000 EUR',
							'750000' => '750.000 EUR',
							'1000000' => '1.000.000 EUR',
							'1250000' => '1.250.000 EUR',
							'2500000' => '2.500.000 EUR',
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'250000' => '250.000 EUR', 
							'500000' => '500.000 EUR',
							'750000' => '750.000 EUR',
							'1000000' => '1.000.000 EUR',
							'1250000' => '1.250.000 EUR',
							'2500000' => '2.500.000 EUR',
							'_' => '2.500.000+ EUR'
						)
					)
				)
			),

			// Lot post type
			'business' => array(
				'buy' => array(
					'pln' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 PLN', 
							'500000' => '500.000 PLN', 
							'1000000' => '1.000.000 PLN', 
							'2000000' => '2.000.000 PLN',
							'3000000' => '3.000.000 PLN',
							'4000000' => '4.000.000 PLN',
							'5000000' => '5.000.000 PLN',
							'10000000' => '10.000.000 PLN',
							'15000000' => '15.000.000 PLN'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'1000000' => '1.000.000 PLN', 
							'2000000' => '2.000.000 PLN',
							'3000000' => '3.000.000 PLN',
							'4000000' => '4.000.000 PLN',
							'5000000' => '5.000.000 PLN',
							'10000000' => '10.000.000 PLN',
							'15000000' => '15.000.000 PLN',
							'20000000' => '20.000.000 PLN',
							'_' => '20.000.000+ PLN'
						)
					),
					'usd' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'),
							'0' => '0 USD', 
							'100000' => '100.000 USD', 
							'200000' => '200.000 USD', 
							'300000' => '300.000 USD',
							'500000' => '500.000 USD',
							'750000' => '750.000 USD',
							'1000000' => '1.000.000 USD',
							'4000000' => '4.000.000 USD',
							'6000000' => '6.000.000 USD'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'),
							'200000' => '200.000 USD', 
							'300000' => '300.000 USD',
							'500000' => '500.000 USD',
							'750000' => '750.000 USD',
							'1000000' => '1.000.000 USD',
							'4000000' => '4.000.000 USD',
							'6000000' => '6.000.000 USD',
							'8000000' => '8.000.000 USD',
							'_' => '8.000.000+ USD'
						)
					),
					'eur' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 EUR', 
							'125000' => '125.000 EUR', 
							'250000' => '250.000 EUR', 
							'500000' => '500.000 EUR',
							'750000' => '750.000 EUR',
							'1000000' => '1.000.000 EUR',
							'1250000' => '1.250.000 EUR',
							'2500000' => '2.500.000 EUR',
							'3500000' => '3.500.000 EUR'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'250000' => '250.000 EUR', 
							'500000' => '500.000 EUR',
							'750000' => '750.000 EUR',
							'1000000' => '1.000.000 EUR',
							'1250000' => '1.250.000 EUR',
							'2500000' => '2.500.000 EUR',
							'3500000' => '3.500.000 EUR',
							'5000000' => '5.000.000 EUR',
							'_' => '5.000.000+ EUR'
						)
					)
				)
			)
		),

		// Price for m2
		'price_m2' => array(

			// Default values
			'default' => array(
				'buy' => array(
					'pln' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 PLN', 
							'1000' => '1.000 PLN', 
							'2000' => '2.000 PLN', 
							'3000' => '3.000 PLN',
							'4000' => '4.000 PLN',
							'5000' => '5.000 PLN'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'1000' => '1.000 PLN', 
							'2000' => '2.000 PLN', 
							'3000' => '3.000 PLN',
							'4000' => '4.000 PLN',
							'5000' => '5.000 PLN',
							'_' => '5.000+ PLN'
						)
					),
					'usd' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 USD', 
							'200' => '200 USD', 
							'400' => '400 USD', 
							'600' => '600 USD', 
							'800' => '800 USD', 
							'1000' => '1.000 USD'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'200' => '200 USD', 
							'400' => '400 USD', 
							'600' => '600 USD', 
							'800' => '800 USD', 
							'_' => '800+ USD'
						)
					),
					'eur' => array(
						'from' => array(
							'__placeholder' => __('Od', 'chtheme'), 
							'0' => '0 EUR', 
							'250' => '250 EUR', 
							'500' => '500 EUR',
							'750' => '750 EUR',
							'1000' => '1.000 EUR',
							'1250' => '1.250 EUR'
						),
						'to' => array(
							'__placeholder' => __('Do', 'chtheme'), 
							'250' => '250 EUR', 
							'500' => '500 EUR',
							'750' => '750 EUR',
							'1000' => '1.000 EUR',
							'1250' => '1.250 EUR',
							'_' => '1.250+ EUR'
						)
					)
				)
			)
		),

		// Areas
		'area' => array(

			// Apartment post type
			'default' => array(
				'default' => array(
					'm2' => array(
						'from' => array(
							'__placeholder' => strtoupper(__('Od', 'chtheme')), 
							'0' => '0', 
							'50' => '50m&#178;', 
							'75' => '75m&#178;', 
							'100' => '100m&#178;',
							'150' => '150m&#178;',
							'200' => '200m&#178;',
							'250' => '250m&#178;'
						),
						'to' => array(
							'__placeholder' => strtoupper(__('Do', 'chtheme')), 
							'50' => '50m&#178;', 
							'75' => '75m&#178;', 
							'100' => '100m&#178;',
							'150' => '150m&#178;',
							'200' => '200m&#178;',
							'250' => '250m&#178;',
							'400' => '400m&#178;',
							'_' => '400m&#178;+'
						)
					)
				)
			),

			// House post type
			'house' => array(
				'buy' => array(
					'm2' => array(
						'from' => array(
							'__placeholder' => strtoupper(__('Od', 'chtheme')), 
							'0' => '0', 
							'100' => '100m&#178;',
							'200' => '200m&#178;',
							'300' => '300m&#178;',
							'400' => '400m&#178;',
							'500' => '500m&#178;',
							'750' => '750m&#178;',
							'1000' => '1.000m&#178;'
						),
						'to' => array(
							'__placeholder' => strtoupper(__('Do', 'chtheme')), 
							'200' => '200m&#178;',
							'300' => '300m&#178;',
							'400' => '400m&#178;',
							'500' => '500m&#178;',
							'750' => '750m&#178;',
							'1000' => '1.000m&#178;',
							'2000' => '2.000m&#178;',
							'_' => '2.000m&#178;+'
						)
					)
				)
			),

			// Lot post type
			'lot' => array(
				'buy' => array(
					'm2' => array(
						'from' => array(
							'__placeholder' => strtoupper(__('Od', 'chtheme')), 
							'0' => '0', 
							'500' => '500m&#178;',
							'1000' => '1.000m&#178;',
							'2000' => '2.000m&#178;',
							'3000' => '3.000m&#178;',
							'4000' => '4.000m&#178;',
							'5000' => '5.000m&#178;',
							'10000' => '10.000m&#178;',
							'50000' => '50.000m&#178;'
						),
						'to' => array(
							'__placeholder' => strtoupper(__('Do', 'chtheme')), 
							'500' => '500m&#178;',
							'1000' => '1.000m&#178;',
							'2000' => '2.000m&#178;',
							'3000' => '3.000m&#178;',
							'4000' => '4.000m&#178;',
							'5000' => '5.000m&#178;',
							'10000' => '10.000m&#178;',
							'50000' => '50.000m&#178;',
							'100000' => '100.000m&#178;',
							'_' => '100.000m&#178;+'
						)
					)
				)
			)
		),

		/*'area_lot' => array(
			'default' => array(
				'buy' => array(
					'm2' => array(
						'from' => array(
							'__placeholder' => strtoupper(__('Od', 'chtheme')), 
							'0' => '0', 
							'500' => '500m&#178;', 
							'1000' => '1.000m&#178;', 
							'3000' => '3.000m&#178;'
						),
						'to' => array(
							'__placeholder' => strtoupper(__('Do', 'chtheme')),
							'500' => '500m&#178;',
							'1000' => '1.000m&#178;',
							'3000' => '3.000m&#178;',
							'_' => '3.000m&#178;+'
						)
					)
				)
			)
		),*/

		// Number of rooms
		'rooms' => array(

			// Default values
			'default' => array(
				'default' => array(
					'from' => array(
						'__placeholder' => __('Od', 'chtheme'), 
						'1' => '1', 
						'2' => '2',
						'3' => '3',
						'4' => '4',
						'5' => '5'
					),
					'to' => array(
						'__placeholder' => __('Do', 'chtheme'), 
						'1' => '1', 
						'2' => '2',
						'3' => '3',
						'4' => '4', 
						'5' => '5', 
						'_' => '5+'
					)
				)
			)
		),

		// Lot area values
		'area_lot' => array(
			'm2' => array(
				'from' => array(
					'__placeholder' => strtoupper(__('Od', 'chtheme')), 
					'0' => '0', 
					'500' => '500m&#178;', 
					'1000' => '1.000m&#178;', 
					'2000' => '2.000m&#178;', 
					'3000' => '3.000m&#178;',
					'4000' => '4.000m&#178;', 
					'5000' => '5.000m&#178;'
				),
				'to' => array(
					'__placeholder' => strtoupper(__('Do', 'chtheme')), 
					'500' => '500m&#178;', 
					'1000' => '1.000m&#178;', 
					'2000' => '2.000m&#178;', 
					'3000' => '3.000m&#178;',
					'4000' => '4.000m&#178;', 
					'5000' => '5.000m&#178;',
					'10000' => '10.000m&#178;',
					'20000' => '20.000m&#178;',
					'_' => '20.000m&#178;+'
				)
			)
		)
	);

	return $return ? $filters : sprintf('var __filters = %s;', cht_json_prettify(json_encode($filters)));

	ob_start();
?>var __filters = {
	price: {
		default: {
			buy: {
				pln: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 PLN', '1000000': '1.000.000 PLN', '3000000': '3.000.000 PLN', '5000000': '5.000.000 PLN'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '1000000': '1.000.000 PLN', '3000000': '3.000.000 PLN', '5000000': '5.000.000 PLN', '_': '5.000.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 USD', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD', '_': '800.000+ USD'}
				},
				eur: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 EUR', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR', '_': '1.250.000+ EUR'},
				}
			},
			rent_short: {
				pln: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 PLN', '200': '200 PLN', '400': '400 PLN'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '200': '200 PLN', '400': '400 PLN', '_': '400+ PLN'}
				},
				usd: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 USD', '40': '40 USD', '100': '100 USD'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '40': '40 USD', '100': '100 USD', '_': '100+ USD'}
				},
				eur: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 EUR', '50': '50 EUR', '100': '100 EUR'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '50': '50 EUR', '100': '100 EUR', '_': '100+ EUR'}
				}
			},
			rent_long: {
				pln: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 PLN', '10000': '10.000 PLN', '20000': '20.000 PLN', '50000': '50.000 PLN'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '10000': '10.000 PLN', '20000': '20.000 PLN', '50000': '50.000 PLN', '_': '50.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 USD', '2000': '2.000 USD', '4000': '4.000 USD', '7000': '7.000 USD'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '2000': '2.000 USD', '4000': '4.000 USD', '7000': '7.000 USD', '_': '7.000+ USD'}
				},
				eur: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 EUR', '1250': '1.250 EUR', '2500': '2.500 EUR', '5000': '5.000 EUR'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '1250': '1.250 EUR', '2500': '2.500 EUR', '5000': '5.000 EUR', '_': '5.000+ EUR'}
				}
			}
		},
		apartment: {
			buy: {
				pln: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 PLN', '1000000': '1.000.000 PLN', '3000000': '3.000.000 PLN', '5000000': '5.000.000 PLN'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '1000000': '1.000.000 PLN', '3000000': '3.000.000', '5000000': '5.000.000 PLN', '_': '5.000.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 USD', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD', '_': '800.000+ USD'}
				},
				eur: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 EUR', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR', '_': '1.250.000+ EUR'},
				}
			},
			rent_short: {
				pln: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 PLN', '200': '200 PLN', '400': '400 PLN'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '200': '200 PLN', '400': '400 PLN', '_': '400+ PLN'}
				},
				usd: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 USD', '40': '40 USD', '100': '100 USD'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '40': '40 USD', '100': '100 USD', '_': '100+ USD'}
				},
				eur: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 EUR', '50': '50 EUR', '100': '100 EUR'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '50': '50 EUR', '100': '100 EUR', '_': '100+ EUR'}
				}
			},
			rent_long: {
				pln: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 PLN', '10000': '10.000 PLN', '20000': '20.000 PLN', '50000': '50.000 PLN'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '10000': '10.000 PLN', '20000': '20.000 PLN', '50000': '50.000 PLN', '_': '50.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 USD', '2000': '2.000 USD', '4000': '4.000 USD', '7000': '7.000 USD'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '2000': '2.000 USD', '4000': '4.000 USD', '7000': '7.000 USD', '_': '7.000+ USD'}
				},
				eur: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 EUR', '1250': '1.250 EUR', '2500': '2.500 EUR', '5000': '5.000 EUR'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '1250': '1.250 EUR', '2500': '2.500 EUR', '5000': '5.000 EUR', '_': '5.000+ EUR'}
				}
			}
		},
		house: {
			buy: {
				pln: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 PLN', '1000000': '1.000.000 PLN', '3000000': '3.000.000 PLN', '5000000': '5.000.000 PLN'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '1000000': '1.000.000 PLN', '3000000': '3.000.000', '5000000': '5.000.000 PLN', '_': '5.000.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 USD', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD', '_': '800.000+ USD'}
				},
				eur: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 EUR', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR', '_': '1.250.000+ EUR'},
				}
			},
			rent_short: {
				pln: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 PLN', '200': '200 PLN', '400': '400 PLN'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '200': '200 PLN', '400': '400 PLN', '_': '400+ PLN'}
				},
				usd: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 USD', '40': '40 USD', '100': '100 USD'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '40': '40 USD', '100': '100 USD', '_': '100+ USD'}
				},
				eur: {
					from: {'__placeholder': 'Od', '0': '0 EUR', '50': '50 EUR', '100': '100 EUR'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '50': '50 EUR', '100': '100 EUR', '_': '100+ EUR'}
				}
			},
			rent_long: {
				pln: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 PLN', '10000': '10.000 PLN', '20000': '20.000 PLN', '50000': '50.000 PLN'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '10000': '10.000 PLN', '20000': '20.000 PLN', '50000': '50.000 PLN', '_': '50.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 USD', '2000': '2.000 USD', '4000': '4.000 USD', '7000': '7.000 USD'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '2000': '2.000 USD', '4000': '4.000 USD', '7000': '7.000 USD', '_': '7.000+ USD'}
				},
				eur: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 EUR', '1250': '1.250 EUR', '2500': '2.500 EUR', '5000': '5.000 EUR'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '1250': '1.250 EUR', '2500': '2.500 EUR', '5000': '5.000 EUR', '_': '5.000+ EUR'}
				}
			}
		},
		lot: {
			buy: {
				pln: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 PLN', '1000000': '1.000.000 PLN', '4000000': '4.000.000 PLN', '6000000': '6.000.000 PLN'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '1000000': '1.000.000 PLN', '4000000': '4.000.000', '5000000': '6.000.000 PLN', '_': '6.000.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 USD', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '300000': '300.000 USD', '500000': '500.000 USD', '800000': '800.000 USD', '_': '800.000+ USD'}
				},
				eur: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 EUR', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '250000': '250.000 EUR', '750000': '750.000 EUR', '1250000': '1.250.000 EUR', '_': '1.250.000+ EUR'},
				}
			}
		}
	},
	price_m2: {
		default: {
			buy: {
				pln: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 PLN', '500': '1.000 PLN', '3000': '3.000 PLN'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '500': '1.000 PLN', '3000': '3.000 PLN', '_': '3.000+ PLN'}
				},
				usd: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 USD', '100': '100 USD', '700': '700 USD'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '100': '100 USD', '700': '700 USD', '_': '700+ USD'}
				},
				eur: {
					from: {'__placeholder': '<?php _e('Od', 'chtheme'); ?>', '0': '0 EUR', '120': '120 EUR', '750': '750 EUR'},
					to: {'__placeholder': '<?php _e('Do', 'chtheme'); ?>', '120': '120 EUR', '750': '750 EUR', '_': '750+ EUR'}
				}
			}
		}
	},
	area: {
		apartment: {
			buy: {
				m2: {
					from: {'__placeholder': '<?php echo strtoupper(__('Od', 'chtheme')); ?>', '0': '0', '100': '100m&#178;', '200': '200m&#178;', '400': '400m&#178;'},
					to: {'__placeholder': '<?php echo strtoupper(__('Do', 'chtheme')); ?>', '100': '100m&#178;', '200': '200m&#178;', '400': '400m&#178;', '_': '400m&#178;+'}
				}
			},
			rent_long: {
				m2: {
					from: {'__placeholder': '<?php echo strtoupper(__('Od', 'chtheme')); ?>', '0': '0', '100': '100m&#178;', '200': '200m&#178;', '400': '400m&#178;'},
					to: {'__placeholder': '<?php echo strtoupper(__('Do', 'chtheme')); ?>', '100': '100m&#178;', '200': '200m&#178;', '400': '400m&#178;', '_': '400m&#178;+'}
				}
			}
		},
		house: {
			buy: {
				m2: {
					from: {'__placeholder': '<?php echo strtoupper(__('Od', 'chtheme')); ?>', '0': '0', '100': '100m&#178;', '300': '300m&#178;', '500': '500m&#178;'},
					to: {'__placeholder': '<?php echo strtoupper(__('Do', 'chtheme')); ?>', '100': '100m&#178;', '300': '300m&#178;', '500': '500m&#178;', '_': '500m&#178;+'}
				}
			},
			rent_long: {
				m2: {
					from: {'__placeholder': '<?php echo strtoupper(__('Od', 'chtheme')); ?>', '0': '0', '100': '100m&#178;', '200': '200m&#178;', '400': '400m&#178;'},
					to: {'__placeholder': '<?php echo strtoupper(__('Do', 'chtheme')); ?>', '100': '100m&#178;', '200': '200m&#178;', '400': '400m&#178;', '_': '400m&#178;+'}
				}
			}
		},
		lot: {
			buy: {
				m2: {
					from: {'__placeholder': '<?php echo strtoupper(__('Od', 'chtheme')); ?>', '0': '0', '1000': '1.000m&#178;', '5000': '5.000m&#178;', '10000': '10.000m&#178;'},
					to: {'__placeholder': '<?php echo strtoupper(__('Do', 'chtheme')); ?>', '1000': '1.000m&#178;', '5000': '5.000m&#178;', '10000': '10.000m&#178;', '_': '10.000m&#178;+'}
				}
			}
		}
	},
	rooms: {
		default: {
			buy: {
				from: {'__placeholder': '<?php echo strtoupper(__('Od', 'chtheme')); ?>', '1': '1', '3': '3', '5': '5'},
				to: {'__placeholder': '<?php echo strtoupper(__('Do', 'chtheme')); ?>', '3': '3', '5': '5', '_': '5+'}
			},
			rent_short: {
				from: {'__placeholder': '<?php echo strtoupper(__('Od', 'chtheme')); ?>', '1': '1', '3': '3', '5': '5'},
				to: {'__placeholder': '<?php echo strtoupper(__('Do', 'chtheme')); ?>', '3': '3', '5': '5', '_': '5+'}
			},
			rent_long: {
				from: {'__placeholder': '<?php echo strtoupper(__('Od', 'chtheme')); ?>', '1': '1', '3': '3', '5': '5'},
				to: {'__placeholder': '<?php echo strtoupper(__('Do', 'chtheme')); ?>', '3': '3', '5': '5', '_': '5+'}
			}
		},
		apartment: {
			buy: {
				from: {'__placeholder': '<?php echo strtoupper(__('Od', 'chtheme')); ?>', '1': '1', '3': '3', '5': '5'},
				to: {'__placeholder': '<?php echo strtoupper(__('Do', 'chtheme')); ?>', '3': '3', '5': '5', '_': '5+'}
			},
			rent_short: {
				from: {'__placeholder': '<?php echo strtoupper(__('Od', 'chtheme')); ?>', '1': '1', '2': '2'},
				to: {'__placeholder': '<?php echo strtoupper(__('Do', 'chtheme')); ?>', '2': '2', '_': '2+'}
			},
			rent_long: {
				from: {'__placeholder': '<?php echo strtoupper(__('Od', 'chtheme')); ?>', '1': '1', '3': '3', '5': '5'},
				to: {'__placeholder': '<?php echo strtoupper(__('Do', 'chtheme')); ?>', '3': '3', '5': '5', '_': '5+'}
			}
		},
	},
	house_lot_area: {
		m2: {
			from: {'__placeholder': '<?php echo strtoupper(__('Od', 'chtheme')); ?>', '0': '0', '500': '500m&#178;', '1000': '1.000m&#178;', '3000': '3.000m&#178;'},
			to: {'__placeholder': '<?php echo strtoupper(__('Do', 'chtheme')); ?>', '500': '500m&#178;', '1000': '1.000m&#178;', '3000': '3.000m&#178;', '_': '3.000m&#178;+'}
		}
	},
	district: {
		<?php foreach($districts as $city => $list) : ?> 
		"<?php echo $city; ?>": {
			'__placeholder': '<?php echo __('Wszystkie', 'chtheme'); ?>',
			<?php foreach($list as $value) : ?>
			"<?php echo $value; ?>": "<?php echo $value; ?>",
			<?php endforeach; ?>
		}<?php if(end(array_keys($districts)) !== $city) : ?>,<?php endif; ?>
		<?php endforeach; ?> 
	}
};<?php
	return ob_get_clean();
}
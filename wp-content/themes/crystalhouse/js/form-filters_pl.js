var __filters = {
	"locations": {
		"country": {
			"__placeholder": "Wszystkie",
			"Polska": "Polska",
			"Francja": "Francja",
			"Hiszpania": "Hiszpania",
			"Bu\u0142garia": "Bu\u0142garia",
			"France": "France"
		},
		"province": {
			"Polska": {
				"__placeholder": "Wszystkie",
				"Dolno\u015bl\u0105skie": "Dolno\u015bl\u0105skie",
				"kujawsko-pomorskie": "kujawsko-pomorskie",
				"Kujawsko-Pomorskie": "Kujawsko-Pomorskie",
				"Lubelskie": "Lubelskie",
				"\u0141\u00f3dzkie": "\u0141\u00f3dzkie",
				"Ma\u0142opolskie": "Ma\u0142opolskie",
				"Mazowickie": "Mazowickie",
				"mazowieckie": "mazowieckie",
				"Mazowieckie": "Mazowieckie",
				"Pomorskie": "Pomorskie",
				"Warmi\u0144sko-Mazurskie": "Warmi\u0144sko-Mazurskie",
				"Zachodniopomorskie": "Zachodniopomorskie"
			},
			"Francja": {
				"__placeholder": "Wszystkie",
				"Mazowieckie": "Mazowieckie"
			},
			"Hiszpania": {
				"__placeholder": "Wszystkie",
				"Mazowieckie": "Mazowieckie"
			},
			"Bu\u0142garia": {
				"__placeholder": "Wszystkie"
			},
			"France": {
				"__placeholder": "Wszystkie"
			},
			"": {
				"__placeholder": "Wszystkie",
				"Mazowieckie": "Mazowieckie"
			}
		},
		"city": {
			"Mazowieckie": {
				"__placeholder": "Wszystkie",
				"BENIDORM": "BENIDORM",
				"Bielawa": "Bielawa",
				"B\u0142onie": "B\u0142onie",
				"Cannes": "Cannes",
				"Chyliczki": "Chyliczki",
				"Horn\u00f3wek": "Horn\u00f3wek",
				"Izabelin": "Izabelin",
				"Izabelin C": "Izabelin C",
				"Jab\u0142onna": "Jab\u0142onna",
				"Jastrz\u0119bie": "Jastrz\u0119bie",
				"J\u00f3zefos\u0142aw": "J\u00f3zefos\u0142aw",
				"J\u00f3zef\u00f3w": "J\u00f3zef\u00f3w",
				"Kampinos": "Kampinos",
				"K\u0119pa Oborska": "K\u0119pa Oborska",
				"Koby\u0142ka": "Koby\u0142ka",
				"Koczargi Stare": "Koczargi Stare",
				"Komor\u00f3w": "Komor\u00f3w",
				"Konstancin-Jeziorna": "Konstancin-Jeziorna",
				"Kotorydz": "Kotorydz",
				"Lasocin": "Lasocin",
				"Lipk\u00f3w": "Lipk\u00f3w",
				"\u0141omianki": "\u0141omianki",
				"Magdalenka": "Magdalenka",
				"Marbella": "Marbella",
				"Marki": "Marki",
				"Micha\u0142owice": "Micha\u0142owice",
				"Nadarzyn": "Nadarzyn",
				"Natolin": "Natolin",
				"Nicea": "Nicea",
				"Obory": "Obory",
				"Otwock": "Otwock",
				"O\u017car\u00f3w Mazowiecki": "O\u017car\u00f3w Mazowiecki",
				"P\u0119cice Ma\u0142e": "P\u0119cice Ma\u0142e",
				"Piaseczno": "Piaseczno",
				"Podkowa Le\u015bna": "Podkowa Le\u015bna",
				"Pruszk\u00f3w": "Pruszk\u00f3w",
				"Radzymin": "Radzymin",
				"Regu\u0142y": "Regu\u0142y",
				"Rusiec": "Rusiec",
				"Saint Tropez": "Saint Tropez",
				"Serock": "Serock",
				"S\u0119kocin Nowy": "S\u0119kocin Nowy",
				"S\u0119kocin-Las": "S\u0119kocin-Las",
				"Solec": "Solec",
				"Stara Wie\u015b": "Stara Wie\u015b",
				"Stare Babice": "Stare Babice",
				"Strach\u00f3wka": "Strach\u00f3wka",
				"Strzembowo": "Strzembowo",
				"Strzyku\u0142y": "Strzyku\u0142y",
				"Walend\u00f3w": "Walend\u00f3w",
				"Warszawa": "Warszawa",
				"Wawer": "Wawer",
				"Wi\u0105zowna": "Wi\u0105zowna",
				"Z\u0105bki": "Z\u0105bki",
				"Zielonka": "Zielonka",
				"\u017b\u00f3\u0142win": "\u017b\u00f3\u0142win"
			},
			"Warmi\u0144sko-Mazurskie": {
				"__placeholder": "Wszystkie",
				"Kruklanki": "Kruklanki",
				"Ksi\u0105\u017cnik": "Ksi\u0105\u017cnik",
				"Lisewo": "Lisewo",
				"Olsztyn": "Olsztyn",
				"Skop": "Skop"
			},
			"": {
				"__placeholder": "Wszystkie",
				" Le Castellet": " Le Castellet",
				"BENIDORM": "BENIDORM",
				"Cannes": "Cannes",
				"Cap D'Antibes": "Cap D'Antibes",
				"Costa Blanca": "Costa Blanca",
				"Costa Del Sol": "Costa Del Sol",
				"Costa Del Sol, Marbella": "Costa Del Sol, Marbella",
				"Cote d'Azur": "Cote d'Azur",
				"Kwitajny": "Kwitajny",
				"Le Castellet": "Le Castellet",
				"Marbella": "Marbella",
				"Nicea": "Nicea",
				"Podkowa Le\u015bna": "Podkowa Le\u015bna",
				"Saint Tropez": "Saint Tropez",
				"S\u0142oneczny Brzeg": "S\u0142oneczny Brzeg",
				"S\u0142oneczny Brzeg Sunny Beach": "S\u0142oneczny Brzeg Sunny Beach",
				"Warszawa": "Warszawa",
				"Wi\u0105zowna": "Wi\u0105zowna"
			},
			"Kujawsko-Pomorskie": {
				"__placeholder": "Wszystkie",
				"Toru\u0144": "Toru\u0144",
				"Wierzbiczany": "Wierzbiczany"
			},
			"Pomorskie": {
				"__placeholder": "Wszystkie",
				"Gda\u0144sk": "Gda\u0144sk",
				"Hel": "Hel",
				"Jastarnia": "Jastarnia",
				"Jurata": "Jurata",
				"Wejherowo": "Wejherowo",
				"W\u0142adys\u0142awowo": "W\u0142adys\u0142awowo"
			},
			"Mazowickie": {
				"__placeholder": "Wszystkie",
				"Komor\u00f3w": "Komor\u00f3w"
			},
			"Zachodniopomorskie": {
				"__placeholder": "Wszystkie",
				"Ko\u0142obrzeg": "Ko\u0142obrzeg",
				"Unie\u015bcie": "Unie\u015bcie"
			},
			"kujawsko-pomorskie": {
				"__placeholder": "Wszystkie",
				"Wierzbiczany": "Wierzbiczany"
			},
			"Ma\u0142opolskie": {
				"__placeholder": "Wszystkie",
				"Ko\u015bcielisko": "Ko\u015bcielisko",
				"Krak\u00f3w": "Krak\u00f3w",
				"Lanckorona": "Lanckorona",
				"Raciborsko": "Raciborsko",
				"Zakopane": "Zakopane",
				"Zawada": "Zawada"
			},
			"Lubelskie": {
				"__placeholder": "Wszystkie",
				"Do\u0142hobycz\u00f3w": "Do\u0142hobycz\u00f3w",
				"Kazimierz Dolny": "Kazimierz Dolny",
				"Lipniak": "Lipniak"
			},
			"Dolno\u015bl\u0105skie": {
				"__placeholder": "Wszystkie",
				"Lw\u00f3wek \u015al\u0105ski": "Lw\u00f3wek \u015al\u0105ski",
				"Zgorzelec": "Zgorzelec"
			},
			"\u0141\u00f3dzkie": {
				"__placeholder": "Wszystkie",
				"\u0141\u00f3d\u017a": "\u0141\u00f3d\u017a"
			},
			"mazowieckie": {
				"__placeholder": "Wszystkie",
				"Warszawa": "Warszawa"
			},
			"default": {
				"__placeholder": "Wszystkie",
				" Le Castellet": " Le Castellet",
				"BENIDORM": "BENIDORM",
				"Bielawa": "Bielawa",
				"B\u0142onie": "B\u0142onie",
				"Cannes": "Cannes",
				"Cap D'Antibes": "Cap D'Antibes",
				"Chyliczki": "Chyliczki",
				"Costa Blanca": "Costa Blanca",
				"Costa Del Sol": "Costa Del Sol",
				"Costa Del Sol, Marbella": "Costa Del Sol, Marbella",
				"Cote d'Azur": "Cote d'Azur",
				"Do\u0142hobycz\u00f3w": "Do\u0142hobycz\u00f3w",
				"Gda\u0144sk": "Gda\u0144sk",
				"Hel": "Hel",
				"Horn\u00f3wek": "Horn\u00f3wek",
				"Izabelin": "Izabelin",
				"Izabelin C": "Izabelin C",
				"Jab\u0142onna": "Jab\u0142onna",
				"Jastarnia": "Jastarnia",
				"Jastrz\u0119bie": "Jastrz\u0119bie",
				"J\u00f3zefos\u0142aw": "J\u00f3zefos\u0142aw",
				"J\u00f3zef\u00f3w": "J\u00f3zef\u00f3w",
				"Jurata": "Jurata",
				"Kampinos": "Kampinos",
				"Kazimierz Dolny": "Kazimierz Dolny",
				"K\u0119pa Oborska": "K\u0119pa Oborska",
				"Koby\u0142ka": "Koby\u0142ka",
				"Koczargi Stare": "Koczargi Stare",
				"Ko\u0142obrzeg": "Ko\u0142obrzeg",
				"Komor\u00f3w": "Komor\u00f3w",
				"Konstancin-Jeziorna": "Konstancin-Jeziorna",
				"Ko\u015bcielisko": "Ko\u015bcielisko",
				"Kotorydz": "Kotorydz",
				"Krak\u00f3w": "Krak\u00f3w",
				"Kruklanki": "Kruklanki",
				"Ksi\u0105\u017cnik": "Ksi\u0105\u017cnik",
				"Kwitajny": "Kwitajny",
				"Lanckorona": "Lanckorona",
				"Lasocin": "Lasocin",
				"Le Castellet": "Le Castellet",
				"Lipk\u00f3w": "Lipk\u00f3w",
				"Lipniak": "Lipniak",
				"Lisewo": "Lisewo",
				"Lw\u00f3wek \u015al\u0105ski": "Lw\u00f3wek \u015al\u0105ski",
				"\u0141omianki": "\u0141omianki",
				"\u0141\u00f3d\u017a": "\u0141\u00f3d\u017a",
				"Magdalenka": "Magdalenka",
				"Marbella": "Marbella",
				"Marki": "Marki",
				"Micha\u0142owice": "Micha\u0142owice",
				"Nadarzyn": "Nadarzyn",
				"Natolin": "Natolin",
				"Nicea": "Nicea",
				"Obory": "Obory",
				"Olsztyn": "Olsztyn",
				"Otwock": "Otwock",
				"O\u017car\u00f3w Mazowiecki": "O\u017car\u00f3w Mazowiecki",
				"P\u0119cice Ma\u0142e": "P\u0119cice Ma\u0142e",
				"Piaseczno": "Piaseczno",
				"Podkowa Le\u015bna": "Podkowa Le\u015bna",
				"Pruszk\u00f3w": "Pruszk\u00f3w",
				"Raciborsko": "Raciborsko",
				"Radzymin": "Radzymin",
				"Regu\u0142y": "Regu\u0142y",
				"Rusiec": "Rusiec",
				"Saint Tropez": "Saint Tropez",
				"Serock": "Serock",
				"S\u0119kocin Nowy": "S\u0119kocin Nowy",
				"S\u0119kocin-Las": "S\u0119kocin-Las",
				"Skop": "Skop",
				"S\u0142oneczny Brzeg": "S\u0142oneczny Brzeg",
				"S\u0142oneczny Brzeg Sunny Beach": "S\u0142oneczny Brzeg Sunny Beach",
				"Solec": "Solec",
				"Stara Wie\u015b": "Stara Wie\u015b",
				"Stare Babice": "Stare Babice",
				"Strach\u00f3wka": "Strach\u00f3wka",
				"Strzembowo": "Strzembowo",
				"Strzyku\u0142y": "Strzyku\u0142y",
				"Toru\u0144": "Toru\u0144",
				"Unie\u015bcie": "Unie\u015bcie",
				"Walend\u00f3w": "Walend\u00f3w",
				"Warszawa": "Warszawa",
				"Wawer": "Wawer",
				"Wejherowo": "Wejherowo",
				"Wi\u0105zowna": "Wi\u0105zowna",
				"Wierzbiczany": "Wierzbiczany",
				"W\u0142adys\u0142awowo": "W\u0142adys\u0142awowo",
				"Zakopane": "Zakopane",
				"Zawada": "Zawada",
				"Z\u0105bki": "Z\u0105bki",
				"Zgorzelec": "Zgorzelec",
				"Zielonka": "Zielonka",
				"\u017b\u00f3\u0142win": "\u017b\u00f3\u0142win"
			}
		},
		"district": {
			"Warszawa": {
				"__placeholder": "Wszystkie",
				"Bemowo": "Bemowo",
				"Bia\u0142o\u0142\u0119ka": "Bia\u0142o\u0142\u0119ka",
				"Bielany": "Bielany",
				"Konstancin": "Konstancin",
				"Mi\u0119dzylesie": "Mi\u0119dzylesie",
				"Mokot\u00f3w": "Mokot\u00f3w",
				"Ochota": "Ochota",
				"Powi\u015ble": "Powi\u015ble",
				"Praga-P\u00f3\u0142noc": "Praga-P\u00f3\u0142noc",
				"Praga-Po\u0142udnie": "Praga-Po\u0142udnie",
				"\u015ar\u00f3dmie\u015bcie": "\u015ar\u00f3dmie\u015bcie",
				"Targ\u00f3wek": "Targ\u00f3wek",
				"Ursus": "Ursus",
				"Ursyn\u00f3w": "Ursyn\u00f3w",
				"Wawer": "Wawer",
				"Weso\u0142a": "Weso\u0142a",
				"Wilan\u00f3w": "Wilan\u00f3w",
				"W\u0142ochy": "W\u0142ochy",
				"Wola": "Wola",
				"WOLA": "WOLA",
				"\u017boliborz": "\u017boliborz"
			},
			"Konstancin-Jeziorna": {
				"__placeholder": "Wszystkie",
				"Chylice": "Chylice",
				"Konstancin": "Konstancin",
				"Kr\u00f3lewska G\u00f3ra": "Kr\u00f3lewska G\u00f3ra"
			},
			"Olsztyn": {
				"__placeholder": "Wszystkie"
			},
			"Le Castellet": {
				"__placeholder": "Wszystkie"
			},
			"Bielawa": {
				"__placeholder": "Wszystkie"
			},
			"Costa Del Sol, Marbella": {
				"__placeholder": "Wszystkie"
			},
			"Cannes": {
				"__placeholder": "Wszystkie"
			},
			"Skop": {
				"__placeholder": "Wszystkie"
			},
			"Otwock": {
				"__placeholder": "Wszystkie"
			},
			"Wierzbiczany": {
				"__placeholder": "Wszystkie"
			},
			"Pruszk\u00f3w": {
				"__placeholder": "Wszystkie"
			},
			"K\u0119pa Oborska": {
				"__placeholder": "Wszystkie"
			},
			"Zielonka": {
				"__placeholder": "Wszystkie"
			},
			"Podkowa Le\u015bna": {
				"__placeholder": "Wszystkie"
			},
			"Komor\u00f3w": {
				"__placeholder": "Wszystkie"
			},
			"Lipk\u00f3w": {
				"__placeholder": "Wszystkie"
			},
			"S\u0119kocin Nowy": {
				"__placeholder": "Wszystkie"
			},
			"Lasocin": {
				"__placeholder": "Wszystkie"
			},
			"Nicea": {
				"__placeholder": "Wszystkie"
			},
			"Marki": {
				"__placeholder": "Wszystkie"
			},
			"Jurata": {
				"__placeholder": "Wszystkie"
			},
			"S\u0142oneczny Brzeg": {
				"__placeholder": "Wszystkie"
			},
			"Z\u0105bki": {
				"__placeholder": "Wszystkie"
			},
			"Wawer": {
				"__placeholder": "Wszystkie"
			},
			"Natolin": {
				"__placeholder": "Wszystkie"
			},
			"Unie\u015bcie": {
				"__placeholder": "Wszystkie"
			},
			"Wi\u0105zowna": {
				"__placeholder": "Wszystkie",
				"Ko\u015bcielna": "Ko\u015bcielna"
			},
			"Kampinos": {
				"__placeholder": "Wszystkie"
			},
			" Le Castellet": {
				"__placeholder": "Wszystkie"
			},
			"Saint Tropez": {
				"__placeholder": "Wszystkie"
			},
			"Nadarzyn": {
				"__placeholder": "Wszystkie"
			},
			"Walend\u00f3w": {
				"__placeholder": "Wszystkie"
			},
			"Cote d'Azur": {
				"__placeholder": "Wszystkie"
			},
			"Zakopane": {
				"__placeholder": "Wszystkie"
			},
			"Izabelin": {
				"__placeholder": "Wszystkie"
			},
			"Kazimierz Dolny": {
				"__placeholder": "Wszystkie"
			},
			"Magdalenka": {
				"__placeholder": "Wszystkie"
			},
			"J\u00f3zef\u00f3w": {
				"__placeholder": "Wszystkie",
				"Michalin": "Michalin",
				"\u015awidry Ma\u0142e": "\u015awidry Ma\u0142e"
			},
			"Lanckorona": {
				"__placeholder": "Wszystkie"
			},
			"Radzymin": {
				"__placeholder": "Wszystkie"
			},
			"Stare Babice": {
				"__placeholder": "Wszystkie"
			},
			"Raciborsko": {
				"__placeholder": "Wszystkie"
			},
			"B\u0142onie": {
				"__placeholder": "Wszystkie"
			},
			"Micha\u0142owice": {
				"__placeholder": "Wszystkie"
			},
			"J\u00f3zefos\u0142aw": {
				"__placeholder": "Wszystkie"
			},
			"Chyliczki": {
				"__placeholder": "Wszystkie"
			},
			"Cap D'Antibes": {
				"__placeholder": "Wszystkie"
			},
			"Marbella": {
				"__placeholder": "Wszystkie"
			},
			"Strach\u00f3wka": {
				"__placeholder": "Wszystkie"
			},
			"Strzyku\u0142y": {
				"__placeholder": "Wszystkie"
			},
			"Do\u0142hobycz\u00f3w": {
				"__placeholder": "Wszystkie"
			},
			"Ko\u015bcielisko": {
				"__placeholder": "Wszystkie"
			},
			"S\u0119kocin-Las": {
				"__placeholder": "Wszystkie"
			},
			"Serock": {
				"__placeholder": "Wszystkie"
			},
			"Costa Del Sol": {
				"__placeholder": "Wszystkie"
			},
			"Krak\u00f3w": {
				"__placeholder": "Wszystkie",
				"Przegorza\u0142y": "Przegorza\u0142y"
			},
			"Piaseczno": {
				"__placeholder": "Wszystkie",
				"Zalesie Dolne": "Zalesie Dolne"
			},
			"Solec": {
				"__placeholder": "Wszystkie"
			},
			"\u0141omianki": {
				"__placeholder": "Wszystkie"
			},
			"Hel": {
				"__placeholder": "Wszystkie"
			},
			"Stara Wie\u015b": {
				"__placeholder": "Wszystkie"
			},
			"Lipniak": {
				"__placeholder": "Wszystkie"
			},
			"Lw\u00f3wek \u015al\u0105ski": {
				"__placeholder": "Wszystkie"
			},
			"Regu\u0142y": {
				"__placeholder": "Wszystkie"
			},
			"Horn\u00f3wek": {
				"__placeholder": "Wszystkie"
			},
			"Kruklanki": {
				"__placeholder": "Wszystkie"
			},
			"BENIDORM": {
				"__placeholder": "Wszystkie"
			},
			"Zawada": {
				"__placeholder": "Wszystkie"
			},
			"Obory": {
				"__placeholder": "Wszystkie"
			},
			"Ksi\u0105\u017cnik": {
				"__placeholder": "Wszystkie"
			},
			"P\u0119cice Ma\u0142e": {
				"__placeholder": "Wszystkie"
			},
			"Jastrz\u0119bie": {
				"__placeholder": "Wszystkie"
			},
			"Izabelin C": {
				"__placeholder": "Wszystkie"
			},
			"\u0141\u00f3d\u017a": {
				"__placeholder": "Wszystkie"
			},
			"Koby\u0142ka": {
				"__placeholder": "Wszystkie"
			},
			"Jab\u0142onna": {
				"__placeholder": "Wszystkie"
			},
			"Wejherowo": {
				"__placeholder": "Wszystkie"
			},
			"Lisewo": {
				"__placeholder": "Wszystkie"
			},
			"Rusiec": {
				"__placeholder": "Wszystkie"
			},
			"Gda\u0144sk": {
				"__placeholder": "Wszystkie"
			},
			"Ko\u0142obrzeg": {
				"__placeholder": "Wszystkie"
			},
			"Kwitajny": {
				"__placeholder": "Wszystkie"
			},
			"Toru\u0144": {
				"__placeholder": "Wszystkie"
			},
			"Strzembowo": {
				"__placeholder": "Wszystkie"
			},
			"O\u017car\u00f3w Mazowiecki": {
				"__placeholder": "Wszystkie"
			},
			"Costa Blanca": {
				"__placeholder": "Wszystkie"
			},
			"Kotorydz": {
				"__placeholder": "Wszystkie"
			},
			"Koczargi Stare": {
				"__placeholder": "Wszystkie"
			},
			"\u017b\u00f3\u0142win": {
				"__placeholder": "Wszystkie"
			},
			"S\u0142oneczny Brzeg Sunny Beach": {
				"__placeholder": "Wszystkie"
			},
			"W\u0142adys\u0142awowo": {
				"__placeholder": "Wszystkie",
				"Ch\u0142apowo": "Ch\u0142apowo"
			},
			"Jastarnia": {
				"__placeholder": "Wszystkie",
				"Jurata": "Jurata"
			},
			"Zgorzelec": {
				"__placeholder": "Wszystkie",
				"Ujazd": "Ujazd"
			}
		}
	},
	"price": {
		"default": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"_": "5.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD"
					},
					"to": {
						"__placeholder": "Do",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"_": "1.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"_": "1.250.000+ EUR"
					}
				}
			},
			"rent_short": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"100": "100 PLN",
						"200": "200 PLN",
						"300": "300 PLN",
						"400": "400 PLN",
						"500": "500 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"100": "100 PLN",
						"200": "200 PLN",
						"300": "300 PLN",
						"400": "400 PLN",
						"500": "500 PLN",
						"_": "500+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"20": "20 USD",
						"40": "40 USD",
						"60": "60 USD",
						"80": "80 USD",
						"100": "100 USD"
					},
					"to": {
						"__placeholder": "Do",
						"20": "20 USD",
						"40": "40 USD",
						"60": "60 USD",
						"80": "80 USD",
						"100": "100 USD",
						"_": "100+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"25": "25 EUR",
						"50": "50 EUR",
						"75": "75 EUR",
						"100": "100 EUR",
						"125": "125 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"25": "25 EUR",
						"50": "50 EUR",
						"75": "75 EUR",
						"100": "100 EUR",
						"125": "125 EUR",
						"_": "125+ EUR"
					}
				}
			},
			"rent_long": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"3000": "3.000 PLN",
						"4000": "4.000 PLN",
						"5000": "5.000 PLN",
						"10000": "10.000 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"3000": "3.000 PLN",
						"4000": "4.000 PLN",
						"5000": "5.000 PLN",
						"10000": "10.000 PLN",
						"_": "10.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"500": "500 USD",
						"750": "750 USD",
						"1000": "1.000 USD",
						"2000": "2.000 USD"
					},
					"to": {
						"__placeholder": "Do",
						"500": "500 USD",
						"750": "750 USD",
						"1000": "1.000 USD",
						"2000": "2.000 USD",
						"_": "2.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"750": "750 EUR",
						"1000": "1.000 EUR",
						"1250": "1.250 EUR",
						"2500": "2.500 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"750": "750 EUR",
						"1000": "1.000 EUR",
						"1250": "1.250 EUR",
						"2500": "2.500 EUR",
						"_": "2.500+ EUR"
					}
				}
			}
		},
		"apartment": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"20000000": "20.000.000 PLN",
						"_": "20.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD"
					},
					"to": {
						"__placeholder": "Do",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"8000000": "8.000.000 USD",
						"_": "8.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"5000000": "5.000.000 EUR",
						"_": "5.000.000+ EUR"
					}
				}
			}
		},
		"house": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"15000000": "15.000.000 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"15000000": "15.000.000 PLN",
						"20000000": "20.000.000 PLN",
						"_": "20.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"6000000": "6.000.000 USD"
					},
					"to": {
						"__placeholder": "Do",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"6000000": "6.000.000 USD",
						"8000000": "8.000.000 USD",
						"_": "8.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"3500000": "3.500.000 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"3500000": "3.500.000 EUR",
						"5000000": "5.000.000 EUR",
						"_": "5.000.000+ EUR"
					}
				}
			}
		},
		"lot": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"_": "10.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD"
					},
					"to": {
						"__placeholder": "Do",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"_": "1.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"_": "2.500.000+ EUR"
					}
				}
			}
		},
		"business": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"15000000": "15.000.000 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"15000000": "15.000.000 PLN",
						"20000000": "20.000.000 PLN",
						"_": "20.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"6000000": "6.000.000 USD"
					},
					"to": {
						"__placeholder": "Do",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"6000000": "6.000.000 USD",
						"8000000": "8.000.000 USD",
						"_": "8.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"3500000": "3.500.000 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"3500000": "3.500.000 EUR",
						"5000000": "5.000.000 EUR",
						"_": "5.000.000+ EUR"
					}
				}
			}
		}
	},
	"price_m2": {
		"default": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Od",
						"0": "0 PLN",
						"1000": "1.000 PLN",
						"2000": "2.000 PLN",
						"3000": "3.000 PLN",
						"4000": "4.000 PLN",
						"5000": "5.000 PLN"
					},
					"to": {
						"__placeholder": "Do",
						"1000": "1.000 PLN",
						"2000": "2.000 PLN",
						"3000": "3.000 PLN",
						"4000": "4.000 PLN",
						"5000": "5.000 PLN",
						"_": "5.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Od",
						"0": "0 USD",
						"200": "200 USD",
						"400": "400 USD",
						"600": "600 USD",
						"800": "800 USD",
						"1000": "1.000 USD"
					},
					"to": {
						"__placeholder": "Do",
						"200": "200 USD",
						"400": "400 USD",
						"600": "600 USD",
						"800": "800 USD",
						"_": "800+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Od",
						"0": "0 EUR",
						"250": "250 EUR",
						"500": "500 EUR",
						"750": "750 EUR",
						"1000": "1.000 EUR",
						"1250": "1.250 EUR"
					},
					"to": {
						"__placeholder": "Do",
						"250": "250 EUR",
						"500": "500 EUR",
						"750": "750 EUR",
						"1000": "1.000 EUR",
						"1250": "1.250 EUR",
						"_": "1.250+ EUR"
					}
				}
			}
		}
	},
	"area": {
		"default": {
			"default": {
				"m2": {
					"from": {
						"__placeholder": "OD",
						"0": "0",
						"50": "50m&#178;",
						"75": "75m&#178;",
						"100": "100m&#178;",
						"150": "150m&#178;",
						"200": "200m&#178;",
						"250": "250m&#178;"
					},
					"to": {
						"__placeholder": "DO",
						"50": "50m&#178;",
						"75": "75m&#178;",
						"100": "100m&#178;",
						"150": "150m&#178;",
						"200": "200m&#178;",
						"250": "250m&#178;",
						"400": "400m&#178;",
						"_": "400m&#178;+"
					}
				}
			}
		},
		"house": {
			"buy": {
				"m2": {
					"from": {
						"__placeholder": "OD",
						"0": "0",
						"100": "100m&#178;",
						"200": "200m&#178;",
						"300": "300m&#178;",
						"400": "400m&#178;",
						"500": "500m&#178;",
						"750": "750m&#178;",
						"1000": "1.000m&#178;"
					},
					"to": {
						"__placeholder": "DO",
						"200": "200m&#178;",
						"300": "300m&#178;",
						"400": "400m&#178;",
						"500": "500m&#178;",
						"750": "750m&#178;",
						"1000": "1.000m&#178;",
						"2000": "2.000m&#178;",
						"_": "2.000m&#178;+"
					}
				}
			}
		},
		"lot": {
			"buy": {
				"m2": {
					"from": {
						"__placeholder": "OD",
						"0": "0",
						"500": "500m&#178;",
						"1000": "1.000m&#178;",
						"2000": "2.000m&#178;",
						"3000": "3.000m&#178;",
						"4000": "4.000m&#178;",
						"5000": "5.000m&#178;",
						"10000": "10.000m&#178;",
						"50000": "50.000m&#178;"
					},
					"to": {
						"__placeholder": "DO",
						"500": "500m&#178;",
						"1000": "1.000m&#178;",
						"2000": "2.000m&#178;",
						"3000": "3.000m&#178;",
						"4000": "4.000m&#178;",
						"5000": "5.000m&#178;",
						"10000": "10.000m&#178;",
						"50000": "50.000m&#178;",
						"100000": "100.000m&#178;",
						"_": "100.000m&#178;+"
					}
				}
			}
		}
	},
	"rooms": {
		"default": {
			"default": {
				"from": {
					"__placeholder": "Od",
					"1": "1",
					"2": "2",
					"3": "3",
					"4": "4",
					"5": "5"
				},
				"to": {
					"__placeholder": "Do",
					"1": "1",
					"2": "2",
					"3": "3",
					"4": "4",
					"5": "5",
					"_": "5+"
				}
			}
		}
	},
	"area_lot": {
		"m2": {
			"from": {
				"__placeholder": "OD",
				"0": "0",
				"500": "500m&#178;",
				"1000": "1.000m&#178;",
				"2000": "2.000m&#178;",
				"3000": "3.000m&#178;",
				"4000": "4.000m&#178;",
				"5000": "5.000m&#178;"
			},
			"to": {
				"__placeholder": "DO",
				"500": "500m&#178;",
				"1000": "1.000m&#178;",
				"2000": "2.000m&#178;",
				"3000": "3.000m&#178;",
				"4000": "4.000m&#178;",
				"5000": "5.000m&#178;",
				"10000": "10.000m&#178;",
				"20000": "20.000m&#178;",
				"_": "20.000m&#178;+"
			}
		}
	}
};
								<div class="form-group" data-form-fieldset="district"<?php if($disabled) : ?> style="display: none;"<?php endif; ?>>
									<label><?php echo chtof_label('offer_location_district'); ?></label>
									<?php
										echo chofl_select('district', array(
											'' => __('Wszystkie', 'chtheme')
										), null, $disabled ? array('disabled' => 'disabled') : array());
									?>
								</div>
								<!-- .form-group -->
<?php

$offers_count = 10;
$locations = get_offers_locations();
$location_city = array('Warszawa');
$params = array();

if(in_array(@$_GET['order'], array('date_desc', 'date_asc', 'price_desc', 'price_asc'))) {
	$params['order'] = strtoupper(end(explode('_', $_GET['order'])));
	$params['orderby'] = reset(explode('_', $_GET['order']));
}
else {
	$params['order'] = 'DESC';
	$params['orderby'] = 'date';
}

if(!isset($post_types)) {
	switch(@$_GET['type']) {
		default:
			$post_types = array('offer_apartment', 'offer_house', 'offer_lot');
			break;
		case 'apartment':
			$post_types = array('offer_apartment');
			break;
		case 'house':
			$post_types = array('offer_house');
			break;
		case 'lot':
			$post_types = array('offer_lot');
			break;
	}
}

if(!isset($categories)) {
	$list = cht_get_offer_categories();

	switch(@$_GET['offer']) {
		default:
			$categories = array($list->buy);
			break;
		case 'rent':
			switch((int) @$_GET['rent_type']) {
				default:
					$categories = array(/*$list->short_rent, */$list->long_rent); // Show long-term rent by default
					break;
				case $list->long_rent:
					$categories = array((int) @$_GET['rent_type']);
					break;
				case $list->short_rent:
					$categories = array((int) @$_GET['rent_type']);
					break;
			}
			break;
	}

	unset($list);
}

if(get_query_var('page') > 0) {
	$_GET['page'] = get_query_var('page');
}

$offers = cht_get_offers(array_merge($_GET, $params), $post_types, $offers_count, $categories, $section);
$offers_map = cht_get_offers_for_map(array_merge($_GET, $params), $post_types, $offers_count, $categories, $section);
$current_page = ((int) $_GET['page'] > 0) ? (int) $_GET['page'] : 1;
$max_pages = $offers->max_num_pages;
$found_posts = $offers->found_posts;

$rent_types = chtof_rent_types();
<?php
/**
 * Plugin Name: CrystalHouse
 * Description: Provides structure for CrystalHouse website.
 * Version: 1.0
 * Author: 365grupa
 */

require __DIR__.'/src/taxonomies.php';
require __DIR__.'/src/roles.php';
require __DIR__.'/src/post-type/offer_apartment.php';
require __DIR__.'/src/post-type/offer_business.php';
require __DIR__.'/src/post-type/offer_house.php';
require __DIR__.'/src/post-type/offer_lot.php';
require __DIR__.'/src/meta-boxes.php';
require __DIR__.'/src/nbp-currencies.php';

function op_fix_timezone() {
	date_default_timezone_set(get_option('timezone_string'));
}
add_action('init', 'op_fix_timezone', 1);

function chplugin_start_session() {
	if(!session_id()){
		session_start();
	}

	// Set currency
	// @TODO Move it to cookies!
	if(isset($_GET['_set_session_currency']) && in_array($_GET['_set_session_currency'], chofl_currencies(false))) {
		$_SESSION['currency'] = $_GET['_set_session_currency'];
		wp_redirect(chtof_remf_url(array('_set_session_currency')));
		die;
	}
}
add_action('init', 'chplugin_start_session');
add_action('admin_init', 'chplugin_start_session');

/**
 * Load plugin textdomain.
 */
function chplugin_load_textdomain() {
	load_plugin_textdomain('chtheme', false, get_template_directory() . '/languages');
}
add_action('plugins_loaded', 'chplugin_load_textdomain');
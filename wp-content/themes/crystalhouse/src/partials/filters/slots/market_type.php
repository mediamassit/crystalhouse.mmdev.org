								<div class="form-group" data-form-fieldset="market_type">
									<label><?php echo chtof_label(chtof_id('offer_market_type'), true); ?></label>
									<div class="row inline-checkboxes">
									<?php
										$market_types = get_field_object(chtof_id('offer_market_type'));
										$market_types = $market_types['choices'];
										arsort($market_types);

										foreach($market_types as $id => $label) :
									?>
										<div class="col-xs-6" data-form-fieldset="market_type_<?php echo $id; ?>">
											<label><input type="radio" name="market_type[]" value="<?php echo $id; ?>"<?php if(@in_array($id, @$_GET['market_type'])) : ?> checked="checked"<?php endif; ?>> <?php pll_e($label); ?></label>
										</div>
										<!-- .col-xs-6 -->
									<?php endforeach; ?>
									</div>
									<!-- .inline-radios -->
								</div>
								<!-- .form-group -->
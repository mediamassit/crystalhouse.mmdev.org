<?php

	$form_filters = cht_get_form_filters(true);

?>								<div class="form-group" data-form-fieldset="area"<?php if(@$disabled) : ?> style="display: none;"<?php endif; ?>>
									<label><span data-type-label="default"><?php echo chtof_label('offer_property_area-primary'); ?></span> <span data-type-label="house" style="display: none;"><?php _e('Powierzchnia domu', 'chtheme'); ?></span></label>
									<div class="row">
										<div class="col-xs-6">
											<?php
												echo chofl_select('area_from', $form_filters['area']['default']['default']['m2']['from'], @$_GET['area_from'], @$disabled ? array('disabled' => 'disabled', 'style' => 'text-transform: none;') : array('style' => 'text-transform: none;'));
											?>
										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<?php
												echo chofl_select('area_to', $form_filters['area']['default']['default']['m2']['to'], @$_GET['area_to'], @$disabled ? array('disabled' => 'disabled', 'style' => 'text-transform: none;') : array('style' => 'text-transform: none;'));
											?>
										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->
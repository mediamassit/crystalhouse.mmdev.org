								<div class="form-group" data-form-fieldset="premise_type" style="display: none;">
									<label><?php echo chtof_label('offer_business_premise-type'); ?></label>
									<?php
										$business_types = get_field_object(chtof_id('offer_business_premise-type'));
										$business_types = $business_types['choices'];

										echo chofl_select('business_premise_type', array_merge(array(
											'' => __('Wszystkie', 'chtheme')
										), $business_types), null, array('disabled' => 'disabled'));
									?>
								</div>
								<!-- .form-group -->
<?php

	$form_filters = cht_get_form_filters(true);

?>								<div class="form-group" data-form-fieldset="area_lot"<?php if(@$disabled) : ?> style="display: none;"<?php endif; ?>>
									<label><?php echo chtof_label('offer_property_area-lot'); ?></label>
									<div class="row">
										<div class="col-xs-6">
											<?php
												echo chofl_select('area_lot_from', $form_filters['area_lot']['m2']['from'], null, @$disabled ? array('disabled' => 'disabled', 'style' => 'text-transform: none;') : array('style' => 'text-transform: none;'));
											?>
										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<?php
												echo chofl_select('area_lot_to', $form_filters['area_lot']['m2']['to'], null, @$disabled ? array('disabled' => 'disabled', 'style' => 'text-transform: none;') : array('style' => 'text-transform: none;'));
											?>
										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->
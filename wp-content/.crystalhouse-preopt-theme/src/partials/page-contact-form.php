	<div class="page-contact-form hidden-print">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<div class="col-md-6 about">
						<h4><?php _e('Zapraszamy do wypełnienia formularza kontaktowego', 'chtheme'); ?></h4>
					</div>
					<!-- .about -->

					<div class="col-md-6 form">
						<?php echo apply_filters('the_content', pll_current_language() === 'pl' ? '[contact-form-7 id="64"]' : '[contact-form-7 id="726"]'); ?>
					</div>
					<!-- .form -->
				</div>
				<!-- .col-md-9 -->

				<div class="col-md-3 address">
					<p class="big">Crystal House S.A.</p>
					<p><?php echo reset(explode("\r\n", get_opt('opt-contact-phone'))); ?><br><a href="mailto:<?php echo get_opt('opt-contact-email'); ?>"><?php echo get_opt('opt-contact-email'); ?></a></p>
				</div>
				<!-- .col-md-3 -->
			</div>
			<!-- .row -->
		</div>
		<!-- .container -->
	</div>
	<!-- .page-contact-form -->
<?php die(); ?><!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Kup Archives - CrystalHouseCrystalHouse</title>
	<link rel="stylesheet" media="screen,print" href="http://crystalhouse.pl/wp-content/themes/crystalhouse/css/style.css">
	<link rel="shortcut icon" href="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/favicon.ico"/>
	
<!-- This site is optimized with the Yoast SEO plugin v2.3.4 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="http://crystalhouse.pl/offer-category/kup/" />
<link rel="next" href="http://crystalhouse.pl/offer-category/kup/page/2/" />
<meta property="og:locale" content="pl_PL" />
<meta property="og:locale:alternate" content="en_US" />
<meta property="og:locale:alternate" content="de_DE" />
<meta property="og:type" content="object" />
<meta property="og:title" content="Kup Archives - CrystalHouse" />
<meta property="og:url" content="http://crystalhouse.pl/offer-category/kup/" />
<meta property="og:site_name" content="CrystalHouse" />
<meta name="twitter:card" content="summary"/>
<meta name="twitter:title" content="Kup Archives - CrystalHouse"/>
<meta name="twitter:domain" content="CrystalHouse"/>
<!-- / Yoast SEO plugin. -->


            <script type="text/javascript">//<![CDATA[
            // Google Analytics for WordPress by Yoast v4.3.5 | http://yoast.com/wordpress/google-analytics/
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-17189051-42']);
				            _gaq.push(['_trackPageview']);
            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
            //]]></script>
			<link rel="alternate" type="application/rss+xml" title="Kanał CrystalHouse &raquo; Kup Kategoria oferty" href="http://crystalhouse.pl/offer-category/kup/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/crystalhouse.pl\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.5.2"}};
			!function(a,b,c){function d(a){var c,d,e,f=b.createElement("canvas"),g=f.getContext&&f.getContext("2d"),h=String.fromCharCode;if(!g||!g.fillText)return!1;switch(g.textBaseline="top",g.font="600 32px Arial",a){case"flag":return g.fillText(h(55356,56806,55356,56826),0,0),f.toDataURL().length>3e3;case"diversity":return g.fillText(h(55356,57221),0,0),c=g.getImageData(16,16,1,1).data,d=c[0]+","+c[1]+","+c[2]+","+c[3],g.fillText(h(55356,57221,55356,57343),0,0),c=g.getImageData(16,16,1,1).data,e=c[0]+","+c[1]+","+c[2]+","+c[3],d!==e;case"simple":return g.fillText(h(55357,56835),0,0),0!==g.getImageData(16,16,1,1).data[0];case"unicode8":return g.fillText(h(55356,57135),0,0),0!==g.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='contact-form-7-css'  href='http://crystalhouse.pl/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=3.8.1' type='text/css' media='all' />
<script type='text/javascript' src='http://crystalhouse.pl/wp-includes/js/jquery/jquery.js?ver=1.12.3'></script>
<script type='text/javascript' src='http://crystalhouse.pl/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.0'></script>
<link rel='https://api.w.org/' href='http://crystalhouse.pl/wp-json/' />
<link rel="alternate" href="http://crystalhouse.pl/offer-category/kup/" hreflang="pl" />
<link rel="alternate" href="http://crystalhouse.pl/offer-category/buy/" hreflang="en" />
<link rel="alternate" href="http://crystalhouse.pl/offer-category/kaufen/" hreflang="de" />
<script type="text/javascript">
(function(url){
if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
var wfscr = document.createElement('script');
wfscr.type = 'text/javascript';
wfscr.async = true;
wfscr.src = url + '&r=' + Math.random();
(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
})('//crystalhouse.pl/wp-admin/admin-ajax.php?action=wordfence_logHuman&hid=E5B81EBB0A5DEB7AB4BC8C33215A0AE9'); 
</script>	<script>
	var THEME_URL = "http://crystalhouse.pl/wp-content/themes/crystalhouse",
		SITE_DOMAIN = 'crystalhouse.pl',
		WISHLIST_API_GET = 'http://crystalhouse.pl/twoj-schowek/?ajax&get',
		WISHLIST_API_PUT = 'http://crystalhouse.pl/twoj-schowek/?ajax&put',
		WISHLIST_API_REMOVE = 'http://crystalhouse.pl/twoj-schowek/?ajax&remove',
		WISHLIST_API_CLEAR = 'http://crystalhouse.pl/twoj-schowek/?ajax&clear',
		WISHLIST_API_SAVE = 'http://crystalhouse.pl/twoj-schowek/?ajax&save',
		addthis_config = {
			"ui_language": "pl"
		},
		FANCYBOX_LANG = {
			error: '<p class="fancybox-error">Nie udało się załadować.<br>Spróbuj ponownie.</p>',
			closeBtn: '<a title="Zamknij" class="fancybox-item fancybox-close" href="javascript:;"></a>',
			next: '<a title="Następny" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
			prev: '<a title="Poprzedni" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
		};
	</script>
	<style>
	.widget.offers-home-filters form .submit .closer {
		display: none;
		text-align: right;
	}

		.widget.offers-home-filters form .submit .closer a {
			margin-top: 14px;
			color: #fff;
			font-weight: 700;
			font-size: 1em;
		}

	.widget.offers-home-filters form.extended .submit .closer {
		display: block;
	}

	.widget.offers-home-filters form.extended .submit .expander {
		display: none;
	}

	.widget.contact-broker figure img {
		max-width: 180px;
	}

	.widget.offers-featured .offer.page figure figcaption {
		width: auto;
		font-weight: 400;
		background: #5fbcce;
		bottom: 10px;
		left: auto;
		right: 0;
	}

	.widget.carousel-gallery .viewport {
		/*max-width: 800px;*/
		max-height: 450px;
		margin: 0 auto;
	}

	.widget.carousel-gallery .slides figure img {
		width: auto;
	}

	/*.widget.contact-broker figure {
		width: 80%;
	}

		.widget.contact-broker figure img {
			width: 100%;
		}
	
	@media (max-width: 992px) {
		.widget.contact-broker figure {
			width: auto;
		}

			.widget.contact-broker figure img {
				width: auto;
			}
	}*/

	#top .main-navigation ul li {
		margin-right: 2.15%;
	}

	@media (max-width: 1280px) {
		#top .main-navigation ul li {
			margin-right: 1.95%;
		}
	}

	@media (max-width: 1220px) {
		#top .main-navigation ul li {
			margin-right: 0.9%;
		}
	}

	@media (max-width: 1100px) {
		#top .main-navigation ul li {
			margin-right: 0;
		}
	}

	@media (max-width: 768px) {
		section.page.team .team-member figure {
			width: auto;
			display: block;
		}

		section.page.team .team-member figure img {
			width: auto;
		}
	}

	@media (max-width: 480px) {
		section.page.team .team-member figure {
			float: none;
		}
	}
	</style>
</head>

<body data-scrollto-append="100">

	<div id="cookie-notifier">
		<p>Serwis używa cookies. Wyrażasz zgodę na używanie cookie, zgodnie z aktualnymi ustawieniami przeglądarki. Zapoznaj się z <a href="http://crystalhouse.pl">polityką cookies</a>. <a href="javascript:void(0);" class="close-x" data-close-cookie-notifier></a></p>
	</div>

	<div id="top" data-sticky-height="100" class="hidden-print">
		<div class="header">
			<div class="container">
				<div class="col-md-2 logo">
					<h1><a href="http://crystalhouse.pl/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/logo.png" alt="CrystalHouse"></a></h1>
				</div>
				<!-- .logo -->

				<div class="col-md-10 right">
					<nav class="tray">
						<ul class="contact">
							<li><span class="hidden-md">T:</span> <a href="tel:+48 22 856 78 60">+48 22 856 78 60</a></li>							<li><span class="hidden-md">E:</span> <a href="mailto:biuro@crystalhouse.pl">biuro@crystalhouse.pl</a></li>
							<li><a href="skype:d.przewocki"><em class="icon-skype"></em> <span class="hidden-md">Call me</span></a></li>
							<li><a href="#" data-toggle="modal" data-target="#modal-contact"><em class="icon-message"></em> <span class="hidden-md">Wyślij wiadomość</span></a></li>
							<li></li>
						</ul>
						<!-- .contact -->

						<ul class="switch">
							<li class="currency">
								<a href="javascript:void(0);" data-toggle="dropdown">PLN</a>
								<ul class="options" role="menu">
																		<li><a href="http://crystalhouse.pl/offer-category/kup/?currency=PLN&type=lot&city=Warszawa&center_distance=%3C%3D2&order=price_desc&_set_session_currency=USD">USD</a></li>
																		<li><a href="http://crystalhouse.pl/offer-category/kup/?currency=PLN&type=lot&city=Warszawa&center_distance=%3C%3D2&order=price_desc&_set_session_currency=EUR">EUR</a></li>
																	</ul>
							</li>
							<li class="language">
																<a href="javascript:void(0);" data-toggle="dropdown"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/flags/pl.png" alt=""> pl</a>
								<ul class="options" role="menu">
																		<li><a href="http://crystalhouse.pl/offer-category/buy/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/flags/en.png" alt=""> en</a></li>
																		<li><a href="http://crystalhouse.pl/offer-category/kaufen/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/flags/de.png" alt=""> de</a></li>
																		<li><a href="http://crystalhouse.pl/home-fr/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/flags/fr.png" alt=""> fr</a></li>
																	</ul>
							</li>
						</ul>
						<!-- .switch -->
					</nav>
					<!-- .tray -->

					<div class="middle">
						<h2><strong>Luxury</strong> Realty Estate Agency</h2>

						<div class="side">
							<form method="get" action="http://crystalhouse.pl/szukaj/" class="search-form">
								<input type="text" name="q" placeholder="wpisz nr oferty, miasto, ulicę">
								<input type="submit" value="Szukaj">
							</form>
							<!-- .search-form -->

														<div class="clipboard">
								<a href="http://crystalhouse.pl/twoj-schowek/" data-toggle="wishlist"><span>Schowek</span> <em></em></a>

								<div class="widget wishlist-dropdown">
									<h3>Twoje oferty</h3>

									<section class="offers">
																		</section>

									<div class="toolbar">
										<ul>
											<li><a href="javascript:void(0);" data-clearclipboard="1"><em class="icon-close"></em> Wyczyść schowek</a></li>
											<li><a href="javascript:void(0);" data-saveclipboard="1"><em class="icon-download"></em> Zapisz schowek</a></li>
											<li class="more"><a href="http://crystalhouse.pl/twoj-schowek/">Chcę obejrzeć</a></li>
										</ul>
									</div>
								</div>
								<!-- .widget.wishlist-popup -->
							</div>
							<!-- .clipboard -->
						</div>
						<!-- .side -->
					</div>
					<!-- .middle -->
				</div>
				<!-- .right -->

				<div class="col-md-8 mobile-right">
					<div class="row">
						<ul class="switch">
							<li class="currency">
								<a href="javascript:void(0);" data-toggle="dropdown">PLN</a>
								<ul class="options" role="menu">
																		<li><a href="http://crystalhouse.pl/offer-category/kup/?currency=PLN&type=lot&city=Warszawa&center_distance=%3C%3D2&order=price_desc&_set_session_currency=USD">USD</a></li>
																		<li><a href="http://crystalhouse.pl/offer-category/kup/?currency=PLN&type=lot&city=Warszawa&center_distance=%3C%3D2&order=price_desc&_set_session_currency=EUR">EUR</a></li>
																	</ul>
							</li>
							<li class="language">
																<a href="javascript:void(0);" data-toggle="dropdown"><img src="http://crystalhouse.pl/wp-content/plugins/polylang/flags/pl.png" alt=""> pl</a>
								<ul class="options" role="menu">
																		<li><a href="http://crystalhouse.pl/offer-category/buy/"><img src="http://crystalhouse.pl/wp-content/plugins/polylang/flags/us.png" alt=""> en</a></li>
																		<li><a href="http://crystalhouse.pl/offer-category/kaufen/"><img src="http://crystalhouse.pl/wp-content/plugins/polylang/flags/de.png" alt=""> de</a></li>
																		<li><a href="http://crystalhouse.pl/home-fr/"><img src="http://crystalhouse.pl/wp-content/plugins/polylang/flags/fr.png" alt=""> fr</a></li>
																	</ul>
							</li>
							<li class="language">
								<a href="http://crystalhouse.pl/twoj-schowek/" style="background-image: none; width: auto;">Schowek</a>
							</li>
						</ul>
						<!-- .switch -->
					</div>
					<!-- .row -->

					<div class="row">
						<div class="menu-main_menu_pl-container"><form method="get" action="#"><select onchange="if(this.value) window.location.href=this.value"><option value="http://crystalhouse.pl/">Strona główna</option>
<option value="http://crystalhouse.pl/offer-category/kup/">Kup</option>
<option value="http://crystalhouse.pl/sprzedaj/">Sprzedaj</option>
<option value="http://crystalhouse.pl/offer-category/wynajem/">Wynajmij<option value="http://crystalhouse.pl/offer-category/wynajem/">&nbsp;&nbsp;&nbsp;Lista nieruchomości</option>
<option value="http://crystalhouse.pl/strona-glowna/wynajem-dlugoterminowy/">&nbsp;&nbsp;&nbsp;Wynajem długoterminowy</option>
</option>
<option value="http://crystalhouse.pl/strona-glowna/komercja/">Komercja<option value="http://crystalhouse.pl/strona-glowna/komercja/">&nbsp;&nbsp;&nbsp;Lokale komercyjne</option>
<option value="/business">&nbsp;&nbsp;&nbsp;Lista nieruchomości</option>
</option>
<option value="http://crystalhouse.pl/szukaj/?keyword=wakacje">Inne nieruchomości<option value="http://crystalhouse.pl/szukaj/?keyword=wakacje">&nbsp;&nbsp;&nbsp;Nieruchomości wakacyjne</option>
<option value="http://crystalhouse.pl/szukaj/?keyword=woda">&nbsp;&nbsp;&nbsp;Nieruchomości nad wodą</option>
<option value="http://crystalhouse.pl/szukaj/?keyword=gory">&nbsp;&nbsp;&nbsp;Nieruchomości w górach</option>
<option value="http://crystalhouse.pl/szukaj/?keyword=historia">&nbsp;&nbsp;&nbsp;Nieruchomości historyczne</option>
<option value="http://crystalhouse.pl/szukaj/?keyword=hotel,pensjonat">&nbsp;&nbsp;&nbsp;Pensjonaty i hotele</option>
<option value="http://crystalhouse.pl/lot/">&nbsp;&nbsp;&nbsp;Działki</option>
</option>
<option value="http://crystalhouse.pl/uslugi/finansowanie/">Usługi<option value="http://crystalhouse.pl/uslugi/finansowanie/">&nbsp;&nbsp;&nbsp;Finansowanie</option>
<option value="http://crystalhouse.pl/uslugi/wykonczenie-wnetrz-fit-out/">&nbsp;&nbsp;&nbsp;Wykończenie wnętrz / Fit-out</option>
<option value="http://crystalhouse.pl/uslugi/zarzadzanie-nieruchomosciami/">&nbsp;&nbsp;&nbsp;Zarządzanie nieruchomościami</option>
<option value="http://crystalhouse.pl/uslugi/doradztwo-inwestycyjne/">&nbsp;&nbsp;&nbsp;Doradztwo inwestycyjne</option>
</option>
<option value="http://crystalhouse.pl/nasz-zespol/">O nas<option value="http://crystalhouse.pl/historia-firmy/">&nbsp;&nbsp;&nbsp;Historia firmy</option>
<option value="http://crystalhouse.pl/strategia-firmy/">&nbsp;&nbsp;&nbsp;Strategia firmy</option>
<option value="http://crystalhouse.pl/nasz-zespol/">&nbsp;&nbsp;&nbsp;Nasz zespół</option>
<option value="http://crystalhouse.pl/uslugi/ubezpieczenia/">&nbsp;&nbsp;&nbsp;Ubezpieczenia</option>
<option value="http://crystalhouse.pl/oferty-pracy/">&nbsp;&nbsp;&nbsp;Oferty pracy</option>
</option>
<option value="http://crystalhouse.pl/kontakt/">Kontakt</option>
</select></form></div>					</div>
					<!-- .row -->
				</div>
				<!-- .mobile-right -->
			</div>
			<!-- .container -->
		</div>
		<!-- .header -->

		<nav class="main-navigation static">
			<div class="container">
				<div class="menu-main_menu_pl-container"><ul id="menu-main_menu_pl-1" class="menu"><li><a href="http://crystalhouse.pl/" >Strona główna</a></li>
<li><a href="http://crystalhouse.pl/offer-category/kup/" >Kup</a></li>
<li><a href="http://crystalhouse.pl/sprzedaj/" >Sprzedaj</a></li>
<li><a href="http://crystalhouse.pl/offer-category/wynajem/"  data-toggle="dropdown" data-hover="dropdown">Wynajmij</a>
<ul class="dropdown-menu" role="menu">
	<li><a href="http://crystalhouse.pl/offer-category/wynajem/" >Lista nieruchomości</a></li>
	<li><a href="http://crystalhouse.pl/strona-glowna/wynajem-dlugoterminowy/" >Wynajem długoterminowy</a></li>
</ul>
</li>
<li><a href="http://crystalhouse.pl/strona-glowna/komercja/"  data-toggle="dropdown" data-hover="dropdown">Komercja</a>
<ul class="dropdown-menu" role="menu">
	<li><a href="http://crystalhouse.pl/strona-glowna/komercja/" >Lokale komercyjne</a></li>
	<li><a href="/business">Lista nieruchomości</a></li>
</ul>
</li>
<li><a href="http://crystalhouse.pl/szukaj/?keyword=wakacje"  data-toggle="dropdown" data-hover="dropdown">Inne nieruchomości</a>
<ul class="dropdown-menu" role="menu">
	<li><a href="http://crystalhouse.pl/szukaj/?keyword=wakacje" >Nieruchomości wakacyjne</a></li>
	<li><a href="http://crystalhouse.pl/szukaj/?keyword=woda" >Nieruchomości nad wodą</a></li>
	<li><a href="http://crystalhouse.pl/szukaj/?keyword=gory" >Nieruchomości w górach</a></li>
	<li><a href="http://crystalhouse.pl/szukaj/?keyword=historia" >Nieruchomości historyczne</a></li>
	<li><a href="http://crystalhouse.pl/szukaj/?keyword=hotel,pensjonat" >Pensjonaty i hotele</a></li>
	<li><a href="http://crystalhouse.pl/lot/" >Działki</a></li>
</ul>
</li>
<li><a href="http://crystalhouse.pl/uslugi/finansowanie/"  data-toggle="dropdown" data-hover="dropdown">Usługi</a>
<ul class="dropdown-menu" role="menu">
	<li><a href="http://crystalhouse.pl/uslugi/finansowanie/" >Finansowanie</a></li>
	<li><a href="http://crystalhouse.pl/uslugi/wykonczenie-wnetrz-fit-out/" >Wykończenie wnętrz / Fit-out</a></li>
	<li><a href="http://crystalhouse.pl/uslugi/zarzadzanie-nieruchomosciami/" >Zarządzanie nieruchomościami</a></li>
	<li><a href="http://crystalhouse.pl/uslugi/doradztwo-inwestycyjne/" >Doradztwo inwestycyjne</a></li>
</ul>
</li>
<li><a href="http://crystalhouse.pl/nasz-zespol/"  data-toggle="dropdown" data-hover="dropdown">O nas</a>
<ul class="dropdown-menu" role="menu">
	<li><a href="http://crystalhouse.pl/historia-firmy/" >Historia firmy</a></li>
	<li><a href="http://crystalhouse.pl/strategia-firmy/" >Strategia firmy</a></li>
	<li><a href="http://crystalhouse.pl/nasz-zespol/" >Nasz zespół</a></li>
	<li><a href="http://crystalhouse.pl/uslugi/ubezpieczenia/" >Ubezpieczenia</a></li>
	<li><a href="http://crystalhouse.pl/oferty-pracy/" >Oferty pracy</a></li>
</ul>
</li>
<li><a href="http://crystalhouse.pl/kontakt/" >Kontakt</a></li>
</ul></div>			</div>
			<!-- .container -->
		</nav>
		<!-- .main-navigation -->
	</div>
	<!-- #top -->

		<div class="modal clipboard vertical-middle fade" id="modal-clipboard" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="javascript:void(0);" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<div class="nav">
						<p><a href="http://crystalhouse.pl/twoj-schowek/" class="btn">Obejrzyj</a></p>
						<p>lub <strong><a href="javascript:void(0);" data-dismiss="modal" aria-hidden="true">Dodaj kolejne</a></strong></p>
					</div>
					<!-- .nav -->

					<div class="message">
						<p>Dodałeś nieruchomość do <a href="http://crystalhouse.pl/twoj-schowek/">schowka</a></p>
					</div>
					<!-- .message -->
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->		<div class="modal clipboard vertical-middle fade" id="modal-clipboard-saved" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="javascript:void(0);" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<div class="nav">
						<p style="margin-top: 25px;"><a href="http://crystalhouse.pl/twoj-schowek/" class="btn">Obejrzyj</a></p>
					</div>
					<!-- .nav -->

					<div class="message">
						<p>Zapisano schowek</p>
					</div>
					<!-- .message -->
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->		<div class="modal clipboard vertical-middle fade" id="modal-clipboard-clean" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="javascript:void(0);" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<div class="nav">
						<p style="margin-top: 25px;"><a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true" style="background-image: none; padding-right: 15px;">Zamknij</a></p>
					</div>
					<!-- .nav -->

					<div class="message">
						<p>Wyczyszczono schowek</p>
					</div>
					<!-- .message -->
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->		<div class="modal classic vertical-middle fade" id="modal-contact" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="#" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<h3>Wyślij wiadomość</h3>
<div class="wpcf7" id="wpcf7-f60-o1">
<div class="screen-reader-response"></div>
<form action="/offer-category/kup/?currency=PLN&#038;type=lot&#038;city=Warszawa&#038;center_distance=%3C%3D2&#038;order=price_desc#wpcf7-f60-o1" method="post" class="wpcf7-form form-horizontal" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="60" />
<input type="hidden" name="_wpcf7_version" value="3.8.1" />
<input type="hidden" name="_wpcf7_locale" value="pl_PL" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f60-o1" />
<input type="hidden" name="_wpnonce" value="c34566d764" />
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcf-name">Imię i nazwisko*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap name"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcf-name" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcf-email">Email*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" id="pcf-email" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcf-phone">Telefon*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap phone"><input type="text" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcf-phone" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcf-message">Wiadomość*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap message"><textarea name="message" cols="5" rows="5" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form-control" id="pcf-message" aria-required="true" aria-invalid="false"></textarea></span></div>
</div>
<div class="form-group">
<div class="col-sm-6 text-right"><br><input type="hidden" name="_wpcf7_captcha_challenge_captcha-260" value="701464406" /><img class="wpcf7-form-control wpcf7-captchac wpcf7-captcha-captcha-260" width="84" height="28" alt="captcha" src="http://crystalhouse.pl/wp-content/uploads/wpcf7_captcha/701464406.png" /></div>
<div class="col-sm-6"><label>przepisz tekst:</label><span class="wpcf7-form-control-wrap captcha-260"><input type="text" name="captcha-260" value="" size="40" class="wpcf7-form-control wpcf7-captchar form-control" aria-invalid="false" /></span></div>
</div>
<div class="form-group text-right">
<div class="col-sm-12"><input type="submit" value="Wyślij" class="wpcf7-form-control wpcf7-submit btn" /></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->		<div class="modal classic vertical-middle fade" id="modal-contact-offer" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="#" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<h3>Zadaj pytanie o nieruchomość</h3>
<div class="wpcf7" id="wpcf7-f732-o2">
<div class="screen-reader-response"></div>
<form action="/offer-category/kup/?currency=PLN&#038;type=lot&#038;city=Warszawa&#038;center_distance=%3C%3D2&#038;order=price_desc#wpcf7-f732-o2" method="post" class="wpcf7-form form-horizontal" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="732" />
<input type="hidden" name="_wpcf7_version" value="3.8.1" />
<input type="hidden" name="_wpcf7_locale" value="pl_PL" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f732-o2" />
<input type="hidden" name="_wpnonce" value="b66e3f140f" />
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfo-offer-number">Nr oferty:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap offer_number"><input type="text" name="offer_number" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcfo-offer-number" readonly="readonly" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfo-name">Imię i nazwisko*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap name"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcfo-name" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfo-email">Email*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" id="pcfo-email" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfo-phone">Telefon*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap phone"><input type="text" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcfo-phone" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfo-message">Wiadomość*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap message"><textarea name="message" cols="5" rows="5" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form-control" id="pcfo-message" aria-required="true" aria-invalid="false"></textarea></span></div>
</div>
<div class="form-group">
<div class="col-sm-6 text-right"><br><input type="hidden" name="_wpcf7_captcha_challenge_captcha-260" value="2017532663" /><img class="wpcf7-form-control wpcf7-captchac wpcf7-captcha-captcha-260" width="84" height="28" alt="captcha" src="http://crystalhouse.pl/wp-content/uploads/wpcf7_captcha/2017532663.png" /></div>
<div class="col-sm-6"><label>przepisz tekst:</label><span class="wpcf7-form-control-wrap captcha-260"><input type="text" name="captcha-260" value="" size="40" class="wpcf7-form-control wpcf7-captchar form-control" aria-invalid="false" /></span></div>
</div>
<div class="form-group text-right">
<div class="col-sm-12"><input type="submit" value="Wyślij" class="wpcf7-form-control wpcf7-submit btn" /></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->		<div class="modal classic vertical-middle fade" id="modal-contact-subject" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="#" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<h3>Wyślij wiadomość</h3>
<div class="wpcf7" id="wpcf7-f738-o3">
<div class="screen-reader-response"></div>
<form action="/offer-category/kup/?currency=PLN&#038;type=lot&#038;city=Warszawa&#038;center_distance=%3C%3D2&#038;order=price_desc#wpcf7-f738-o3" method="post" class="wpcf7-form form-horizontal" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="738" />
<input type="hidden" name="_wpcf7_version" value="3.8.1" />
<input type="hidden" name="_wpcf7_locale" value="pl_PL" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f738-o3" />
<input type="hidden" name="_wpnonce" value="f032dd70bb" />
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfos-name">Imię i nazwisko*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap name"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcfos-name" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfos-email">Email*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" id="pcfos-email" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfos-subject">Temat*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap subject"><input type="text" name="subject" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcfos-subject" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfos-phone">Telefon*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap phone"><input type="text" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="pcfos-phone" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-label" for="pcfos-message">Wiadomość*:</label>
<div class="col-sm-8"><span class="wpcf7-form-control-wrap message"><textarea name="message" cols="5" rows="5" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form-control" id="pcfo-message" aria-required="true" aria-invalid="false"></textarea></span></div>
</div>
<div class="form-group">
<div class="col-sm-6 text-right"><br><input type="hidden" name="_wpcf7_captcha_challenge_captcha-260" value="953602850" /><img class="wpcf7-form-control wpcf7-captchac wpcf7-captcha-captcha-260" width="84" height="28" alt="captcha" src="http://crystalhouse.pl/wp-content/uploads/wpcf7_captcha/953602850.png" /></div>
<div class="col-sm-6"><label>przepisz tekst:</label><span class="wpcf7-form-control-wrap captcha-260"><input type="text" name="captcha-260" value="" size="40" class="wpcf7-form-control wpcf7-captchar form-control" aria-invalid="false" /></span></div>
</div>
<div class="form-group text-right">
<div class="col-sm-12"><input type="submit" value="Wyślij" class="wpcf7-form-control wpcf7-submit btn" /></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->
	<div id="content">
		<div class="container">
						<div class="breadcrumbs hidden-print">
				<ul>
					<li>jesteś tutaj:</li>
					<li><a href="http://crystalhouse.pl/">CrystalHouse</a></li>
										<li class="active"><a href="http://crystalhouse.pl/offer-category/kup/">Kup</a></li>
									</ul>
			</div>
			<!-- .breadcrumbs -->						<div class="widget navigation-tabs">
				<ul>
											<li><a href="http://crystalhouse.pl/offer-category/kup/?currency=PLN&type=lot&city=Warszawa&center_distance=%3C%3D2&order=price_desc&offer=buy" class="active">Sprzedaż</a></li>
					
					
					<li><a href="#" data-toggle="modal" data-target="#modal-contact-subject" data-form-subject="Zgłaszam: ">Zgłaszam</a></li>
					<li><a href="#" data-toggle="modal" data-target="#modal-contact-subject" data-form-subject="Poszukuję: ">Poszukuję</a></li>
				</ul>
			</div>
			<!-- .widget.tab-buttons -->						<div class="page-title">
				<h2>Działki w kategorii: <strong>na sprzedaż</strong> <strong>(8 ofert)</strong></h2>
			</div>
			<!-- .page-title -->
			<div class="row">
								<div class="col-md-9">
														<div class="row widget offers-list-switches">
						<div class="col-md-5 left">
							<a href="#" class="button-googlemap" data-close-label="Zamknij mapę nieruchomości" data-open-gmap="#content .google-map">Pokaż mapę nieruchomości</a>						</div>
						<!-- .col-md-5 -->

						<div class="col-md-7 right">
							<ul class="inline-switch">
															<li><a href="http://crystalhouse.pl/offer-category/kup/?currency=PLN&type=lot&city=Warszawa&center_distance=%3C%3D2&order=price_desc&show_currency=PLN" class="active">PLN</a></li>
															<li><a href="http://crystalhouse.pl/offer-category/kup/?currency=PLN&type=lot&city=Warszawa&center_distance=%3C%3D2&order=price_desc&show_currency=USD">USD</a></li>
															<li><a href="http://crystalhouse.pl/offer-category/kup/?currency=PLN&type=lot&city=Warszawa&center_distance=%3C%3D2&order=price_desc&show_currency=EUR">EUR</a></li>
														</ul>

							<select name="order"><option value="date_desc">Najnowsze</option><option value="date_asc">Najstarsze</option><option value="price_desc" selected="selected">Cena najwyższa</option><option value="price_asc">Cena najniższa</option></select>						</div>
						<!-- .col-md-7 -->
					</div>
					<!-- .widget.offers-list-switches -->															<section class="widget google-map offers-list-map closed" style="display: none;">
						<div class="map"></div>
					</section>
					<!-- .widget.google-map -->

					<script>
						(function($) {
							$(window).load(function() {
								$('.widget.offers-list-map').offersListMap({
									map: {
										options: {
											zoom: 10,
											center: new google.maps.LatLng(52.2329379,21.0011941), // Warszawa
											disableDefaultUI: false,
											scrollwheel: false
										}
									},
									markers: [
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/luksusowa-rezydencja-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/05/25217218-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 570m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>19 270 779 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/luksusowa-rezydencja-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1785639, 20.9894679],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-tasmowa/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18782888.jpg" width="139" alt=""></a></figure><p><strong>Ulica:</strong> Taśmowa</p><p><strong>Powierzchnia:</strong> 6090m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>15 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-tasmowa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2298015, 21.038103],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartamenty-solec-residence-na-sprzedaz-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45845283w1-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Solec</p><p><strong>Powierzchnia:</strong> 119.95m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>11 979 175 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartamenty-solec-residence-na-sprzedaz-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2416808, 21.0840602],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-z-najemca-saska-kepa-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/03/INGBankLogoSquare-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 530m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>10 905 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-z-najemca-saska-kepa-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2224266, 21.2195512],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-ptasia-1-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918636-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ptasia (1)</p><p class="price"><strong>Cena:</strong> <strong><strong>10 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-ptasia-1-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/rezydencja-na-sprzedaz-warszawa-wawer/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18909704-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 500m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>10 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/rezydencja-na-sprzedaz-warszawa-wawer/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/luksusowy-dom-warszawa-anin/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18910766-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 500m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>10 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/luksusowy-dom-warszawa-anin/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-cosmopolitan-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/45166813-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 194m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>8 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-cosmopolitan-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1561298, 21.1116284],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/luksusowy-dom-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18910467-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 750m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>7 890 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/luksusowy-dom-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2920788, 21.0480613],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-z-najemca-warszawa-targowek/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36888831.jpg" width="278" alt=""></a></figure><p><strong>Ulica:</strong> Ludwika Kondratowicza</p><p><strong>Powierzchnia:</strong> 530m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>7 881 379 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-z-najemca-warszawa-targowek/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2010249, 21.0273894],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-ludowa-6/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/07/36w-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ludowa</p><p><strong>Powierzchnia:</strong> 289m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>7 800 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-ludowa-6/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1561298, 21.1116284],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/lot/dzialka-warszawa-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/26727561-370x215.jpg" width="370" alt=""></a></figure><p class="price"><strong>Cena:</strong> <strong><strong>7 700 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/lot/dzialka-warszawa-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/luksusowy-dom-wawer/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18909786-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 800m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>7 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/luksusowy-dom-wawer/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1561298, 21.1116284],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/27463554-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 600m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>7 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/02/73-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 166m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>6 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/luksusowa-willa-warszawa-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/10/18907429-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 500m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>6 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/luksusowa-willa-warszawa-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-w-inwestycji-cosmopolitan-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/09/393640181-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 163.27m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>6 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-w-inwestycji-cosmopolitan-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/26752089.jpg" width="161" alt=""></a></figure><p><strong>Powierzchnia:</strong> 540m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>6 300 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartamenty-na-sprzedaz-cosmopolitan/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/45159618-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 164m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>6 300 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartamenty-na-sprzedaz-cosmopolitan/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1742604, 21.0134179],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-al-lotnikow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18783376-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> al. Lotników</p><p><strong>Powierzchnia:</strong> 1000m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>6 100 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-al-lotnikow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2614481, 20.990342],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-gen-jozefa-zajaczka/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/27233202-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> gen. Józefa Zajączka</p><p><strong>Powierzchnia:</strong> 362m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>6 100 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-gen-jozefa-zajaczka/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2614481, 20.990342],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-gen-jozefa-zajaczka-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18803093-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> gen. Józefa Zajączka</p><p><strong>Powierzchnia:</strong> 362m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>6 100 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-gen-jozefa-zajaczka-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.235366, 21.0515964],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/penthouse-saska-kepa/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18803653-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Gruzińska</p><p><strong>Powierzchnia:</strong> 262m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>6 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/penthouse-saska-kepa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-sprzedaz-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/45167165-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 165m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>6 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-sprzedaz-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-na-sprzedaz-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18908729-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 350m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>5 750 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-na-sprzedaz-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/luksusowa-rezydencja-mokotow-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/01/18907559-370x215.jpg" width="370" alt=""></a></figure><p class="price"><strong>Cena:</strong> <strong><strong>5 700 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/luksusowa-rezydencja-mokotow-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/02/19-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 161.97m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>5 600 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2296756, 21.0122287],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/luksusowa-willa-stare-bielany/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/269833131-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 300m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>5 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/luksusowa-willa-stare-bielany/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1561298, 21.1116284],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/rezydencja-warszawa-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/110-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 600m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/rezydencja-warszawa-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/wyjatkowy-dom-marina-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18907454-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 352m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/wyjatkowy-dom-marina-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2933234, 20.928664],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/luksusowy-dom-warszawa-bielany/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/ELEWACJA-OGRODOWA1w-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 700m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/luksusowy-dom-warszawa-bielany/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartamenty-cosmopolitan-twarda-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/09/235uj2-min-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 136m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartamenty-cosmopolitan-twarda-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-z-basenem-na-sprzedaz-warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/164-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 430m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-z-basenem-na-sprzedaz-warszawa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/luksusowy-dom-marina-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18909064-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 330m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 850 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/luksusowy-dom-marina-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/luksusowa-nieruchomosc-anin/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18910745-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 526m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 850 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/luksusowa-nieruchomosc-anin/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2296756, 21.0122287],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-handlowy-na-sprzedaz-warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/11/41037523-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 344m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 850 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-handlowy-na-sprzedaz-warszawa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/warszawa-64/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/187825121-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 342m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 850 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/warszawa-64/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2313967, 20.9991199],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-wola/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18905869-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Sienna</p><p><strong>Powierzchnia:</strong> 213m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 800 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-wola/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2416808, 21.0840602],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-saska-kepa/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/26746522-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 265m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-saska-kepa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2296756, 21.0122287],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-z-najemca-warszawa-srodmiescie/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/03/pobrane.jpg" width="259" alt=""></a></figure><p><strong>Powierzchnia:</strong> 199m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 400 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-z-najemca-warszawa-srodmiescie/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2296756, 21.0122287],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-z-najemca-warszawa-srodmiescie-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/03/pobrane.jpg" width="259" alt=""></a></figure><p><strong>Powierzchnia:</strong> 199m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 400 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-z-najemca-warszawa-srodmiescie-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2010249, 21.0273894],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-ludowa-7/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/11/Ludowa-8-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ludowa</p><p><strong>Powierzchnia:</strong> 169m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 390 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-ludowa-7/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-na-sprzedaz-cosmopolitan-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/45184729-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 134m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 300 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-na-sprzedaz-cosmopolitan-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2286187, 21.0055835],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-emilii-plater-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18914995-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Emilii Plater</p><p class="price"><strong>Cena:</strong> <strong><strong>4 200 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-emilii-plater-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.193104, 20.9871044],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/penthouse-na-marinie-z-basenem/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/12/18683691w-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Sztormowa</p><p><strong>Powierzchnia:</strong> 204m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 200 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/penthouse-na-marinie-z-basenem/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1799062, 20.9478161],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-warszawa-ochota/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18907029-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 300m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 190 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-warszawa-ochota/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2286187, 21.0055835],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-emilii-plater/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918131-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Emilii Plater</p><p class="price"><strong>Cena:</strong> <strong><strong>4 107 840 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-emilii-plater/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2698349, 20.9752446],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-penthouse-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18798795-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Hanki Czaki</p><p><strong>Powierzchnia:</strong> 266m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 021 567 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-penthouse-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.193104, 20.9871044],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18803301-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Sztormowa</p><p><strong>Powierzchnia:</strong> 162.35m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-warszawa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.193104, 20.9871044],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/przepiekny-apartament/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18802790-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Sztormowa</p><p><strong>Powierzchnia:</strong> 162.35m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/przepiekny-apartament/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2266495, 21.0360237],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-powisle/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/11/42518759w-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Cecylii Śniegockiej</p><p><strong>Powierzchnia:</strong> 195m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-powisle/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-cosmopolitan/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/01/42902883-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 134.17m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>4 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-cosmopolitan/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-sprzedaz-warszawa-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18907478-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 490m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 999 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-sprzedaz-warszawa-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2012734, 21.0109725],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-al-niepodleglosci/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18805592-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> al. Niepodległości</p><p><strong>Powierzchnia:</strong> 100m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 990 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-al-niepodleglosci/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/ekskluzywny-dom-warszawa-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18909334-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 473m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 950 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/ekskluzywny-dom-warszawa-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2466798, 21.0189197],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-dobra/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/187832681-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Dobra</p><p><strong>Powierzchnia:</strong> 415m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 917 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-dobra/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.189942, 21.0243693],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-pulawska-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18916809-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Puławska</p><p><strong>Powierzchnia:</strong> 354m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 894 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-pulawska-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-apartament-na-sprzedaz/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/45162383-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 134m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 800 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-apartament-na-sprzedaz/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-22/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 135m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 777 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-22/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.191338, 21.0205633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-rezydencja-mokotow-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36697597-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Adama Naruszewicza</p><p><strong>Powierzchnia:</strong> 250m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 750 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-rezydencja-mokotow-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-24/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 136m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 732 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-24/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-sprzedaz/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/45167459-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 134m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 700 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-sprzedaz/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-23/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 131m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 675 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-23/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2933234, 20.928664],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-warszawa-bielany/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/12/21613420-370x215.jpg" width="370" alt=""></a></figure><p class="price"><strong>Cena:</strong> <strong><strong>3 600 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-warszawa-bielany/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-z-basenem-warszawa-wawer/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/05/25189446-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 341m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 590 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-z-basenem-warszawa-wawer/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2036656, 20.9524794],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-warszawa-szczesliwice/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18802633-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Drawska</p><p><strong>Powierzchnia:</strong> 270m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 590 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-warszawa-szczesliwice/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-6/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 148m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 590 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-6/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2261486, 21.013761],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-hoza-7/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/12/hoza50-31-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Hoża</p><p><strong>Powierzchnia:</strong> 122m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 512 600 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-hoza-7/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/ekskluzywny-dom-warszawa-wawer/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/26748625-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 400m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/ekskluzywny-dom-warszawa-wawer/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2933234, 20.928664],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/warszawa-105/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18912374-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 360m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/warszawa-105/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/ekskluzywny-dom-warszawa-anin/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/27154516-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 460m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/ekskluzywny-dom-warszawa-anin/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.186263, 21.030013],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-plycwianska/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/188040891-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Płyćwiańska</p><p><strong>Powierzchnia:</strong> 167m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-plycwianska/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-mokotow-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18909680-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 300m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-mokotow-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/domy-na-sprzedaz-warszawa-anin/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/122-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 560m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/domy-na-sprzedaz-warszawa-anin/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.167442, 21.0120413],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-na-sprzedaz-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18800844-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> al. Wyścigowa</p><p><strong>Powierzchnia:</strong> 208m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 490 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-na-sprzedaz-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.167442, 21.0120413],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-sluzew/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18802748.jpg" width="144" alt=""></a></figure><p><strong>Ulica:</strong> al. Wyścigowa</p><p><strong>Powierzchnia:</strong> 208m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 490 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-sluzew/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1845994, 21.0337579],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-leszczyny-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2012/09/1_z49048_20130206_105331-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Leszczyny</p><p><strong>Powierzchnia:</strong> 190m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 350 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-leszczyny-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2017172, 21.0053428],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-lowicka-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18805488-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Łowicka</p><p><strong>Powierzchnia:</strong> 166.94m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 322 106 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-lowicka-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-25/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 121m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 317 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-25/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-27/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 120m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 302 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-27/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2371268, 21.0014183],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-grzybowska/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18906277-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Grzybowska</p><p><strong>Powierzchnia:</strong> 193m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 300 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-grzybowska/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1958102, 21.0166351],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/mieszkanie-na-sprzedaz-warszawa-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/11/41489714-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Jana Czeczota</p><p><strong>Powierzchnia:</strong> 261m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 300 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/mieszkanie-na-sprzedaz-warszawa-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/rezydencja-na-sprzedaz-warszawa-anin/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/04/18906678-639x440w-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 600m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 300 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/rezydencja-na-sprzedaz-warszawa-anin/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2329397, 21.225198],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/warszawa-28/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/189113421-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 370m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 250 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/warszawa-28/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2261486, 21.013761],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-hoza-8/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/12/hoza50-31-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Hoża</p><p><strong>Powierzchnia:</strong> 112m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 249 300 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-hoza-8/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-warszawa-mokotow-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/01/1-1w-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 324m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 200 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-warszawa-mokotow-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-26/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 119m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 179 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-26/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2346655, 20.9907518],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-grzybowska-10/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18902653-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Grzybowska</p><p><strong>Powierzchnia:</strong> 152m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 150 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-grzybowska-10/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2416808, 21.0840602],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-z-najemca-saska-kepa/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/03/INGBankLogoSquare1-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 175m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 116 155 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-z-najemca-saska-kepa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2834463, 20.9519544],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-bielany/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/09/29159679-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Grębałowska</p><p><strong>Powierzchnia:</strong> 190m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 050 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-bielany/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2079221, 21.0448662],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-podchorazych/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/186840021-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Podchorążych</p><p><strong>Powierzchnia:</strong> 164m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>3 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-podchorazych/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1561298, 21.1116284],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/rodzinny-dom-warszawa-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18781292-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 420m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 950 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/rodzinny-dom-warszawa-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-7/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 162m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 931 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-7/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2693286, 20.9818745],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-z-najemca-warszawa-zoliborz/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/03/bank-bgz_big.png" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 147m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 920 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-z-najemca-warszawa-zoliborz/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1941146, 21.0201615],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-ignacego-krasickiego-5/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18906156-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ignacego Krasickiego</p><p><strong>Powierzchnia:</strong> 160m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-ignacego-krasickiego-5/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2416808, 21.0840602],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-z-najemca-na-sprzedaz/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/03/bank-pekao-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 148m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 837 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-z-najemca-na-sprzedaz/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2256758, 21.0115929],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-poznanska-5/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/1884-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Poznańska</p><p><strong>Powierzchnia:</strong> 153m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 822 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-poznanska-5/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1378544, 21.0291229],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/warszawa-67/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/187824361-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 551m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 800 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/warszawa-67/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1688552, 21.1070882],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/10/40529900-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Syta</p><p><strong>Powierzchnia:</strong> 249m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 750 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1730431, 21.0879921],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-stary-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/27231566-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Biedronki</p><p><strong>Powierzchnia:</strong> 186m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 700 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-stary-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2681289, 20.9588351],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-uslugowy-z-najemca-bielany/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36888440.jpg" width="278" alt=""></a></figure><p><strong>Powierzchnia:</strong> 140m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 700 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-uslugowy-z-najemca-bielany/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-6/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 160m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 694 500 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-6/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.191338, 21.0205633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-rezydencja-mokotow-warszawa-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36731943-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Adama Naruszewicza</p><p><strong>Powierzchnia:</strong> 143m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 681 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-rezydencja-mokotow-warszawa-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-na-sprzedaz-wawer/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/27236070.jpg" width="161" alt=""></a></figure><p><strong>Powierzchnia:</strong> 336m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 670 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-na-sprzedaz-wawer/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2416808, 21.0840602],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-saska-kepa-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/03/MG_2111-HDR3_Default-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 220m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 600 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-saska-kepa-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1561298, 21.1116284],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18781259-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 525m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 600 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2261486, 21.013761],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-hoza-9/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/37257324-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Hoża</p><p><strong>Powierzchnia:</strong> 92m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 568 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-hoza-9/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.193104, 20.9871044],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-marina-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2011/05/1_z30650_20110512_180048-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Sztormowa</p><p><strong>Powierzchnia:</strong> 118.5m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 550 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-marina-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-28/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 107m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 549 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-28/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-cosmopolitan/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/12/115-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 77.13m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 549 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-cosmopolitan/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2823819, 21.0653678],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/piekny-dom-na-sprzedaz-warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/04/24688659-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 350m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/piekny-dom-na-sprzedaz-warszawa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1561298, 21.1116284],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/luksusowy-dom-warszawa-wilanow-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/03/d5fa476355dd17359c8e2763ebaec4a92054543815243531-370x215.jpeg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 331.51m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/luksusowy-dom-warszawa-wilanow-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-na-sprzedaz-sadyba/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18780981-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 200m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-na-sprzedaz-sadyba/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2010249, 21.0273894],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-sprzedaz-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45554489-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ludowa</p><p><strong>Powierzchnia:</strong> 132m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-sprzedaz-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-29/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 86m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 420 170 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-29/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2060402, 20.9960948],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-bialy-kamien-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918395-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Biały Kamień</p><p><strong>Powierzchnia:</strong> 182.66m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 388 827 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-bialy-kamien-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-15/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 160m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 385 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-15/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2538503, 21.0000397],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-bonifraterska/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18689480-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Bonifraterska</p><p><strong>Powierzchnia:</strong> 124m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 350 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-bonifraterska/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-30/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 136m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 315 400 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-30/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2298015, 21.038103],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartamenty-solec-residence-na-sprzedaz/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45823845w1-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Solec</p><p><strong>Powierzchnia:</strong> 144.57m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 313 120 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartamenty-solec-residence-na-sprzedaz/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1561298, 21.1116284],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-wilanow-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/06/24501507w-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 180m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 300 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-wilanow-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/45164538-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 77m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 300 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2261486, 21.013761],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/ekskluzywne-mieszkanie-w-kamienicy/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/12/hoza50-32-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Hoża</p><p><strong>Powierzchnia:</strong> 80m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 283 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/ekskluzywne-mieszkanie-w-kamienicy/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2614481, 20.990342],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-gen-jozefa-zajaczka/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18698582-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> gen. Józefa Zajączka</p><p><strong>Powierzchnia:</strong> 159m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 250 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-gen-jozefa-zajaczka/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2247501, 20.9437823],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-jana-kazimierza-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18699693-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Jana Kazimierza</p><p><strong>Powierzchnia:</strong> 333.94m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 248 700 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-jana-kazimierza-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2416808, 21.0840602],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-na-sprzedaz-saska-kepa/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/26745124-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 200m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 200 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-na-sprzedaz-saska-kepa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2416808, 21.0840602],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-na-saskiej-kepie/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/03/26732990.jpg" width="280" alt=""></a></figure><p><strong>Powierzchnia:</strong> 220m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 200 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-na-saskiej-kepie/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2484825, 21.0133066],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-stare-miasto/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/27230148.jpg" width="143" alt=""></a></figure><p><strong>Ulica:</strong> Świętojańska</p><p><strong>Powierzchnia:</strong> 116m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 200 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-stare-miasto/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.189942, 21.0243693],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2012/01/6_007-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Puławska</p><p><strong>Powierzchnia:</strong> 170m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 200 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/cosmopolitan-apartamenty/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/45167803-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 77m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 200 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/cosmopolitan-apartamenty/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2010249, 21.0273894],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-park-morskie-oko/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/26734707.jpg" width="121" alt=""></a></figure><p><strong>Ulica:</strong> Ludowa</p><p><strong>Powierzchnia:</strong> 120m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 150 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-park-morskie-oko/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2017172, 21.0053428],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-lowicka-5/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18805474-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Łowicka</p><p><strong>Powierzchnia:</strong> 117.72m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 142 500 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-lowicka-5/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2365887, 20.9918303],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-krochmalna-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918556-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Krochmalna</p><p><strong>Powierzchnia:</strong> 178m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 100 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-krochmalna-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2360987, 21.0215288],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-okolnik/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18689360-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Okólnik</p><p><strong>Powierzchnia:</strong> 131.36m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 100 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-okolnik/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1941146, 21.0201615],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-ignacego-krasickiego-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/05/4_z55487_20130703_140039-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ignacego Krasickiego</p><p><strong>Powierzchnia:</strong> 115m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 100 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-ignacego-krasickiego-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1799062, 20.9478161],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-z-najemca-na-sprzedaz-warszawa-wlochy/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/06/46556428-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 162m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 096 326 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-z-najemca-na-sprzedaz-warszawa-wlochy/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-31/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 123m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 092 700 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-31/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2346655, 20.9907518],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-uslugowy-warszawa-centrum/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918693-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Grzybowska</p><p class="price"><strong>Cena:</strong> <strong><strong>2 060 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-uslugowy-warszawa-centrum/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2009348, 21.0389299],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-rezydencja-maltanska/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/39801024-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Jazgarzewska</p><p><strong>Powierzchnia:</strong> 143.72m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 050 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-rezydencja-maltanska/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2340161, 21.0303644],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-powislu-1/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/26638471-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Leona Kruczkowskiego</p><p><strong>Powierzchnia:</strong> 130m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-powislu-1/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2056476, 21.0324796],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-w-belvedere-residence/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/19709803.jpg" width="161" alt=""></a></figure><p><strong>Ulica:</strong> Sułkowicka</p><p><strong>Powierzchnia:</strong> 144m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-w-belvedere-residence/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/lot/dom-mokotow-sadyba/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/26726458-370x215.jpg" width="370" alt=""></a></figure><p class="price"><strong>Cena:</strong> <strong><strong>2 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/lot/dom-mokotow-sadyba/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.186263, 21.030013],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-plycwianska-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18900691-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Płyćwiańska</p><p><strong>Powierzchnia:</strong> 148m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-plycwianska-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2274784, 21.041126],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-adama-idzkowskiego/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18779184-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Adama Idźkowskiego</p><p><strong>Powierzchnia:</strong> 125m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-adama-idzkowskiego/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45379776-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 58m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>2 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.191338, 21.0205633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-rezydencja-mokotow-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36733986-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> naruszewicza</p><p><strong>Powierzchnia:</strong> 110m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 954 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-rezydencja-mokotow-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-3/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 155m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 930 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.245881, 20.9899861],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/ekskluzywny-apartament-centrum-warszawy/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/27229114-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> al. Jana Pawła II</p><p><strong>Powierzchnia:</strong> 140m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/ekskluzywny-apartament-centrum-warszawy/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-w-inwestycji-cosmopolitan/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/02/44-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 57.79m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-w-inwestycji-cosmopolitan/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-srodmiescie/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/45186514-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 54m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-srodmiescie/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/cosmopolitan-apartament-na-sprzedaz/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/45185250-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 57m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/cosmopolitan-apartament-na-sprzedaz/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2577603, 20.9945333],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-srodmiescie-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18803725-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Zygmunta Słomińskiego</p><p><strong>Powierzchnia:</strong> 160m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 870 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-srodmiescie-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2017172, 21.0053428],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-lowicka-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18805495-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Łowicka</p><p><strong>Powierzchnia:</strong> 98.24m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 866 560 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-lowicka-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.245881, 20.9899861],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-uzytkowy-warszawa-centrum/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/189188371.jpg" width="156" alt=""></a></figure><p><strong>Ulica:</strong> al. Jana Pawła II</p><p><strong>Powierzchnia:</strong> 183m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 830 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-uzytkowy-warszawa-centrum/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2298015, 21.038103],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/solec-residence/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45846882w1-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Solec</p><p><strong>Powierzchnia:</strong> 125.54m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 820 330 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/solec-residence/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2043435, 20.9965113],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-zaryna/"><img src="http://crystalhouse.pl/wp-content/uploads/2012/10/1_z50235_20121107_161344-370x215.png" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Żaryna</p><p><strong>Powierzchnia:</strong> 94m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 800 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-zaryna/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2010249, 21.0273894],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-ludowa-5/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18904234-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ludowa</p><p><strong>Powierzchnia:</strong> 115m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 800 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-ludowa-5/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-cosmopolitan-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/45166006-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 58m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 800 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-cosmopolitan-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.191338, 21.0205633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/ekskluzywny-apartament-rezydencja-mokotow-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36731944-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Adama Naruszewicza</p><p><strong>Powierzchnia:</strong> 140m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 769 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/ekskluzywny-apartament-rezydencja-mokotow-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/lot/dzialka-warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18906471-370x215.jpg" width="370" alt=""></a></figure><p class="price"><strong>Cena:</strong> <strong><strong>1 750 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/lot/dzialka-warszawa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2346655, 20.9907518],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-platinum-towers/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/12/2w1-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Grzybowska</p><p><strong>Powierzchnia:</strong> 92m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 748 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-platinum-towers/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/rezydencja-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36728846-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Naruszewicza</p><p><strong>Powierzchnia:</strong> 100m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 719 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/rezydencja-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.208688, 21.0477123],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-mokotow-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18778455-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Tadeusza Hołówki</p><p><strong>Powierzchnia:</strong> 94m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 700 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-mokotow-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2359676, 21.0203813],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-mikolaja-kopernika-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18803933.jpg" width="143" alt=""></a></figure><p><strong>Ulica:</strong> Mikołaja Kopernika</p><p><strong>Powierzchnia:</strong> 96m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 700 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-mikolaja-kopernika-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-sprzedaz-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/45165060-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 57m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 700 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-sprzedaz-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/agencja-nieruchomosci-warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/06/46912352-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 57m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 700 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/agencja-nieruchomosci-warszawa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-cybernetyki/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/03/inmedio-new.jpg" width="300" alt=""></a></figure><p><strong>Powierzchnia:</strong> 74m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 677 875 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-cybernetyki/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2266495, 21.0360237],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowe-apartamenty-na-sprzedaz-w-warszawie/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18905936-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Cecylii Śniegockiej</p><p><strong>Powierzchnia:</strong> 98m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 650 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowe-apartamenty-na-sprzedaz-w-warszawie/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2614481, 20.990342],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-gen-jozefa-zajaczka-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18698233-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> gen. Józefa Zajączka</p><p><strong>Powierzchnia:</strong> 134m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 650 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-gen-jozefa-zajaczka-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2900194, 20.9285812],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-warszawa-bielany/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36916736-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Jana Kasprowicza</p><p><strong>Powierzchnia:</strong> 87m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 650 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-warszawa-bielany/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2346655, 20.9907518],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-z-najemca-na-sprzedaz-warszawa-wola/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/03/LOGO-1Minute-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Grzybowska</p><p><strong>Powierzchnia:</strong> 66m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 640 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-z-najemca-na-sprzedaz-warszawa-wola/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2060402, 20.9960948],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-bialy-kamien/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918545-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Biały Kamień</p><p><strong>Powierzchnia:</strong> 123.25m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 611 864 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-bialy-kamien/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2030807, 20.9960449],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-karola-chodkiewicza-6/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918407-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Karola Chodkiewicza</p><p><strong>Powierzchnia:</strong> 119.33m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 604 989 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-karola-chodkiewicza-6/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2360987, 21.0215288],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-kamienica-ordynacka/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/11/41130144-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Okólnik</p><p><strong>Powierzchnia:</strong> 93m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 600 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-kamienica-ordynacka/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-sprzedaz-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/45185944-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p><p><strong>Powierzchnia:</strong> 53m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 600 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/cosmopolitan-warszawa-sprzedaz-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2298015, 21.038103],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartamenty-solec-residence-sprzedaz/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45846882w1-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Solec</p><p><strong>Powierzchnia:</strong> 103.73m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 555 950 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartamenty-solec-residence-sprzedaz/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1924656, 21.0103884],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-gornym-mokotowie/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/10/249432861-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Fryderyka Joliot-Curie</p><p><strong>Powierzchnia:</strong> 82m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 545 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-gornym-mokotowie/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2030807, 20.9960449],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-karola-chodkiewicza/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918544-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Karola Chodkiewicza</p><p><strong>Powierzchnia:</strong> 117.62m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 540 822 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-karola-chodkiewicza/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2247501, 20.9437823],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-jana-kazimierza-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18699697-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Jana Kazimierza</p><p><strong>Powierzchnia:</strong> 215.96m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 527 073 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-jana-kazimierza-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-4/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 105m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 522 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2261486, 21.013761],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-hoza/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/12/hoza50-32-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Hoża</p><p><strong>Powierzchnia:</strong> 136m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 506 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-hoza/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2326557, 21.0744424],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-al-stanow-zjednoczonych/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18697847-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> al. Stanów Zjednoczonych</p><p><strong>Powierzchnia:</strong> 210m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-al-stanow-zjednoczonych/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.3289879, 21.0076793],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-warszawa-tarchomin/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/27235625-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 350m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 500 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-warszawa-tarchomin/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-z-najemca-na-sprzedaz-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/03/inmedio-new1.jpg" width="300" alt=""></a></figure><p><strong>Powierzchnia:</strong> 62m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 459 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-z-najemca-na-sprzedaz-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2329397, 21.225198],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-wesola/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/04/44710320-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 200m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 450 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-wesola/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2010249, 21.0273894],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-ludowa-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/03/1_z52693_20130312_100423-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ludowa</p><p><strong>Powierzchnia:</strong> 79m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 420 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-ludowa-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1563299, 21.0575811],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-ursynow/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/27230522.jpg" width="161" alt=""></a></figure><p><strong>Ulica:</strong> Urwisko</p><p><strong>Powierzchnia:</strong> 81m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 400 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-ursynow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1561298, 21.1116284],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/lot/dzialka-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18906556-370x215.jpg" width="370" alt=""></a></figure><p class="price"><strong>Cena:</strong> <strong><strong>1 400 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/lot/dzialka-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2398627, 21.0012535],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-ptasia/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18912981-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ptasia</p><p class="price"><strong>Cena:</strong> <strong><strong>1 400 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-ptasia/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2296756, 21.0122287],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/elegancki-apartament-srodmiescie-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/09/28935995.jpg" width="161" alt=""></a></figure><p><strong>Powierzchnia:</strong> 83.12m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 399 667 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/elegancki-apartament-srodmiescie-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2296756, 21.0122287],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-centrum-warszawy/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/09/28934899.jpg" width="161" alt=""></a></figure><p><strong>Powierzchnia:</strong> 107.47m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 390 825 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-centrum-warszawy/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1561298, 21.1116284],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-wilanow-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/02/145-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 250m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 390 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-wilanow-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2296756, 21.0122287],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartamenty-warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/09/28935597.jpg" width="161" alt=""></a></figure><p><strong>Powierzchnia:</strong> 99.44m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 384 901 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartamenty-warszawa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2060402, 20.9960948],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-bialy-kamien-5/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918852-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Biały Kamień</p><p><strong>Powierzchnia:</strong> 134m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 383 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-bialy-kamien-5/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2296756, 21.0122287],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowe-apartamenty-warszawa-na-sprzedaz/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/09/28934756.jpg" width="161" alt=""></a></figure><p><strong>Powierzchnia:</strong> 104.17m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 371 645 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowe-apartamenty-warszawa-na-sprzedaz/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2060402, 20.9960948],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-bialy-kamien-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918397-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Biały Kamień</p><p><strong>Powierzchnia:</strong> 100.71m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 354 550 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-bialy-kamien-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2614481, 20.990342],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-gen-jozefa-zajaczka-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18698238-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> gen. Józefa Zajączka</p><p><strong>Powierzchnia:</strong> 110m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 350 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-gen-jozefa-zajaczka-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2256758, 21.0115929],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-poznanska-6/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/1884-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Poznańska</p><p><strong>Powierzchnia:</strong> 80m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 311 750 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-poznanska-6/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2296756, 21.0122287],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-srodmiescie-5/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/09/28935866.jpg" width="161" alt=""></a></figure><p><strong>Powierzchnia:</strong> 76.35m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 302 455 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-srodmiescie-5/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.167771, 21.0435303],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-nowoursynowska/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18684790-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Nowoursynowska</p><p><strong>Powierzchnia:</strong> 114.8m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 300 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-nowoursynowska/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2284576, 21.0155589],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-zurawia/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18915412.jpg" width="143" alt=""></a></figure><p><strong>Ulica:</strong> Żurawia</p><p class="price"><strong>Cena:</strong> <strong><strong>1 300 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-zurawia/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2248604, 21.0416537],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-powisle-warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18905175-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Fabryczna</p><p><strong>Powierzchnia:</strong> 83m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 300 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-powisle-warszawa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2598891, 20.9659241],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-sady-zoliborskie/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18687572.jpg" width="143" alt=""></a></figure><p><strong>Ulica:</strong> Sady Żoliborskie</p><p><strong>Powierzchnia:</strong> 82.48m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 290 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-sady-zoliborskie/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2012734, 21.0109725],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-al-niepodleglosci/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/188049351.jpg" width="144" alt=""></a></figure><p><strong>Ulica:</strong> al. Niepodległości</p><p><strong>Powierzchnia:</strong> 118m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 290 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-al-niepodleglosci/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2296756, 21.0122287],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/09/289360661.jpg" width="161" alt=""></a></figure><p><strong>Powierzchnia:</strong> 76.53m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 289 967 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2459929, 21.0747359],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-grochowska/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18916536-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Grochowska</p><p><strong>Powierzchnia:</strong> 175.4m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 283 928 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-grochowska/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2165128, 21.0414737],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-srodmiescie-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/12/41932658-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Szwoleżerów</p><p><strong>Powierzchnia:</strong> 112m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 280 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-srodmiescie-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2614386, 20.9895141],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-zoliborz/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/12/42092169-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> gen. Józefa Zajączka</p><p><strong>Powierzchnia:</strong> 92m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 280 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-zoliborz/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-wawer-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18906411-370x215.jpg" width="370" alt=""></a></figure><p class="price"><strong>Cena:</strong> <strong><strong>1 261 800 PLN</strong></strong></p> <a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-wawer-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1928575, 21.0484435],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/luksusowy-lokal-handlowy-warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918594-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> al. Wincentego Witosa</p><p><strong>Powierzchnia:</strong> 96m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 250 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/luksusowy-lokal-handlowy-warszawa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1928575, 21.0484435],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-al-wincentego-witosa/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18915310-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> al. Wincentego Witosa</p><p class="price"><strong>Cena:</strong> <strong><strong>1 250 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-al-wincentego-witosa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.165326, 21.076211],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/nowoczesny-apartament-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18803827-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Sarmacka</p><p><strong>Powierzchnia:</strong> 99m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 250 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/nowoczesny-apartament-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2577603, 20.9945333],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-zygmunta-slominskiego-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/10/35777535-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Zygmunta Słomińskiego</p><p><strong>Powierzchnia:</strong> 140m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 250 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-zygmunta-slominskiego-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-32/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 87m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 245 386 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-32/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2247501, 20.9437823],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-jana-kazimierza-5/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18698727.jpg" width="161" alt=""></a></figure><p><strong>Ulica:</strong> Jana Kazimierza</p><p><strong>Powierzchnia:</strong> 126.6m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 240 738 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-jana-kazimierza-5/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.183611, 21.018611],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-ksawerow-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18687282-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ksawerów</p><p><strong>Powierzchnia:</strong> 97.51m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 225 500 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-ksawerow-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/03/LOGO-1Minute-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 47m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 221 857 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-34/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 87m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 219 259 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-34/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2009348, 21.0389299],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-jazgarzewska-6/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18902368-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Jazgarzewska</p><p><strong>Powierzchnia:</strong> 65m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 215 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-jazgarzewska-6/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2598891, 20.9659241],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-sady-zoliborskie/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18902898.jpg" width="144" alt=""></a></figure><p><strong>Ulica:</strong> Sady Żoliborskie</p><p><strong>Powierzchnia:</strong> 80.1m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 215 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-sady-zoliborskie/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.191338, 21.0205633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-w-rezydencji-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/rezydencja-mokotow-wewnetrzny-dziedziniec8-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Adama Naruszewicza</p><p><strong>Powierzchnia:</strong> 92m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 212 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-w-rezydencji-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2030807, 20.9960449],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-uzytkowy-warszawa-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918408-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Karola Chodkiewicza</p><p><strong>Powierzchnia:</strong> 89.73m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 206 869 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-uzytkowy-warszawa-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.3289879, 21.0076793],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18782505-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 160m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 200 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1928846, 21.0225133],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/nowoczesny-apartament-warszawa-mokotow-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/02/1_z49244_20121002_202633-370x215.png" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Tyniecka</p><p><strong>Powierzchnia:</strong> 95m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 200 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/nowoczesny-apartament-warszawa-mokotow-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-wawer/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/11/41740205-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 200m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 200 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/dom-na-sprzedaz-warszawa-wawer/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-37/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 87m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 193 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-37/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1660986, 21.0835764],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18904155-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> al. Wilanowska</p><p><strong>Powierzchnia:</strong> 108m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 190 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.165326, 21.076211],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-warszawa-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18803057-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Sarmacka</p><p><strong>Powierzchnia:</strong> 99m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 180 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-warszawa-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-33/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 80m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 165 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-33/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2036109, 21.011942],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/28900282-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Antoniego Józefa Madalińskiego</p><p><strong>Powierzchnia:</strong> 100m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 150 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1608237, 21.0780113],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-wilanow-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/01/42699536-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Sarmacka</p><p><strong>Powierzchnia:</strong> 93.7m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 150 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-wilanow-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-35/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 81m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 140 969 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-35/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2298015, 21.038103],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-w-luksusowej-solec-residence/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45823842w1-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Solec</p><p><strong>Powierzchnia:</strong> 88.12m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 135 680 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-w-luksusowej-solec-residence/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2030807, 20.9960449],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-karola-chodkiewicza-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918409-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Karola Chodkiewicza</p><p><strong>Powierzchnia:</strong> 84.14m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 131 683 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-karola-chodkiewicza-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.183611, 21.018611],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-uslugowy-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918689-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Ksawerów</p><p class="price"><strong>Cena:</strong> <strong><strong>1 127 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-uslugowy-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2030807, 20.9960449],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-karola-chodkiewicza-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18918410-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Karola Chodkiewicza</p><p><strong>Powierzchnia:</strong> 83.66m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 125 227 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-karola-chodkiewicza-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-wawer/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/26728697-370x215.jpg" width="370" alt=""></a></figure><p class="price"><strong>Cena:</strong> <strong><strong>1 125 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-wawer/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-38/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 81m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 117 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-38/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2369677, 20.972054],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-na-sprzedaz/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/11/21058785-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Leszno</p><p><strong>Powierzchnia:</strong> 96m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 100 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-na-sprzedaz/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2301039, 21.1089661],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-saska-kepa-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/09/39947689-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Jana Nowaka-Jeziorańskiego</p><p><strong>Powierzchnia:</strong> 80.41m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 100 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-saska-kepa-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2298015, 21.038103],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-solec-residence-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45823842w1-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Solec</p><p><strong>Powierzchnia:</strong> 78.17m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 094 380 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-solec-residence-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1986108, 21.0464264],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/nowoczesny-apartament-mokotow-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/01/31906408w-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Bobrowiecka</p><p><strong>Powierzchnia:</strong> 93m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 089 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/nowoczesny-apartament-mokotow-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1641959, 20.9922496],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-klobucka/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18698471-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Kłobucka</p><p><strong>Powierzchnia:</strong> 91.2m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 088 107 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-klobucka/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2030944, 21.0155615],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-kazimierzowska/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18698810-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Kazimierzowska</p><p><strong>Powierzchnia:</strong> 69.73m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 050 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-kazimierzowska/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1986108, 21.0464264],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/mieszkanie-na-sprzedaz-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/08/1_z54513_20130531_151550-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Bobrowiecka</p><p><strong>Powierzchnia:</strong> 90m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 050 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/mieszkanie-na-sprzedaz-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1334047, 21.0660575],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-melchiora-wankowicza/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18700198.jpg" width="161" alt=""></a></figure><p><strong>Ulica:</strong> Melchiora Wańkowicza</p><p><strong>Powierzchnia:</strong> 112m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 036 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-melchiora-wankowicza/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1951226, 20.8837885],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/lokal-uslugowy-z-najemca-warszawa-ursus/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36888719.jpg" width="278" alt=""></a></figure><p><strong>Powierzchnia:</strong> 94m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 024 674 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/lokal-uslugowy-z-najemca-warszawa-ursus/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2116757, 20.921132],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-potrzebna/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18779803w-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Potrzebna</p><p><strong>Powierzchnia:</strong> 190m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>1 000 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-potrzebna/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1660986, 21.0835764],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/nowoczesny-apartament-warszawa-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/27231336-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> al. Wilanowska</p><p><strong>Powierzchnia:</strong> 90.56m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>980 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/nowoczesny-apartament-warszawa-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.191338, 21.0205633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-w-rezydencji-mokotow-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/rezydencja-mokotow-widok-od-ulicy-naruszewicza8-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Naruszewicza</p><p><strong>Powierzchnia:</strong> 72m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>971 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-w-rezydencji-mokotow-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1928575, 21.0484435],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-al-wincentego-witosa-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18914009-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> al. Wincentego Witosa</p><p class="price"><strong>Cena:</strong> <strong><strong>950 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-al-wincentego-witosa-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1424866, 21.0263071],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-filipiny-plaskowickiej/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18697781-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Filipiny Płaskowickiej</p><p><strong>Powierzchnia:</strong> 113.29m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>950 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-filipiny-plaskowickiej/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2298015, 21.038103],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartamenty-solec-residence/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45823842w1-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Solec</p><p><strong>Powierzchnia:</strong> 66.48m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>947 340 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartamenty-solec-residence/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2256758, 21.0115929],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-poznanska-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/1884-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Poznańska</p><p><strong>Powierzchnia:</strong> 67m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>945 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-poznanska-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-w-rezydencji-mokotow-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/rezydencja-mokotow-naroznik-ulic-naruszewicza-i-wejnerta__1_8-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Naruszewicza</p><p><strong>Powierzchnia:</strong> 72m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>943 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-w-rezydencji-mokotow-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2877061, 20.9748864],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/mieszkanie-na-sprzedaz-warszawa-bielany/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18905926-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Tczewska</p><p><strong>Powierzchnia:</strong> 74m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>940 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/mieszkanie-na-sprzedaz-warszawa-bielany/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-36/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 65m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>935 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-36/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-14/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 65m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>916 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-14/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-7/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 65m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>913 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-7/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2247501, 20.9437823],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-jana-kazimierza/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18699706-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Jana Kazimierza</p><p><strong>Powierzchnia:</strong> 103.5m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>908 781 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-jana-kazimierza/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2189106, 20.9976972],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-filtrowa/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18686736-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Filtrowa</p><p><strong>Powierzchnia:</strong> 72m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-filtrowa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1660986, 21.0835764],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-miasteczko-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18799561-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> al. Wilanowska</p><p><strong>Powierzchnia:</strong> 110m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>900 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-miasteczko-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-10/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 64m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>897 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-10/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2228513, 21.007506],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-koszykowa/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 65m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>893 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-koszykowa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2340435, 21.0308388],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/kamienica-na-sprzedaz/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18799254-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> al. 3 Maja</p><p><strong>Powierzchnia:</strong> 73m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>880 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/kamienica-na-sprzedaz/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2296756, 21.0122287],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-srodmiescie-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/09/20140902_1548048-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 62.82m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>860 452 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-srodmiescie-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.143753, 21.0362703],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-ursynow-5/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/12/21705939-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Polskie Drogi</p><p><strong>Powierzchnia:</strong> 100m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>860 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-ursynow-5/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2298015, 21.038103],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-solec-residence/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45823842w1-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Solec</p><p><strong>Powierzchnia:</strong> 60.72m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>850 080 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-solec-residence/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2521191, 20.9889892],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/mieszkanie-srodmiescie-sprzedaz/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18801103-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Stawki</p><p><strong>Powierzchnia:</strong> 68.8m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>850 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/mieszkanie-srodmiescie-sprzedaz/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2247629, 21.0313199],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/komfortowy-50m2-apartament-ul-gornoslaska-16-srodmiescie-przy-sejmie-rp/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/81-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Górnośląska 16</p><p><strong>Powierzchnia:</strong> 50.3m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>850 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/komfortowy-50m2-apartament-ul-gornoslaska-16-srodmiescie-przy-sejmie-rp/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-8/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 59m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>843 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-8/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-16/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 58m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>828 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-16/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.245881, 20.9899861],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-srodmiescie/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18904702-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> al. Jana Pawła II</p><p><strong>Powierzchnia:</strong> 50m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>825 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-srodmiescie/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2308245, 20.9964303],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/mieszkanie-na-sprzedaz-warszawa-srodmiescie/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/18801051.jpg" width="161" alt=""></a></figure><p><strong>Ulica:</strong> Sienna</p><p><strong>Powierzchnia:</strong> 71m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>824 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/mieszkanie-na-sprzedaz-warszawa-srodmiescie/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.191338, 21.0205633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/rezydencja-mokotow-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36731946-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Adama Naruszewicza</p><p><strong>Powierzchnia:</strong> 55m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>806 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/rezydencja-mokotow-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-14/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 58m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>793 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-14/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2298015, 21.038103],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-solec-residence/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/05/45845283-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Solec</p><p><strong>Powierzchnia:</strong> 46.3m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>787 100 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-solec-residence/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-12/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 57m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>787 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-12/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.191338, 21.0205633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-rezydencja-mokotow-warszawa-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36731949-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Adama Naruszewicza</p><p><strong>Powierzchnia:</strong> 55m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>765 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-rezydencja-mokotow-warszawa-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2251494, 21.035899],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36833092.jpg" width="131" alt=""></a></figure><p><strong>Ulica:</strong> Górnośląska</p><p><strong>Powierzchnia:</strong> 54m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>756 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2446307, 20.9637928],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-banderii/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18699915-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Banderii</p><p><strong>Powierzchnia:</strong> 101m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>750 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-banderii/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 57m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>750 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-19/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 52m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>749 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-19/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.191338, 21.0205633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/rezydencja-mokotow-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36762172-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Adama Naruszewicza</p><p><strong>Powierzchnia:</strong> 55m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>746 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/rezydencja-mokotow-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-8/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 50m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>743 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-8/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-13/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 51m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>740 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-13/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2251494, 21.035899],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-8/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/37196018.jpg" width="131" alt=""></a></figure><p><strong>Ulica:</strong> Górnośląska</p><p><strong>Powierzchnia:</strong> 54m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>734 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-8/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-w-kamienicy-ul-noakowskiego/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 56m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>729 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-w-kamienicy-ul-noakowskiego/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-9/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 49m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>725 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-9/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-27/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 55m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>721 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-27/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2269687, 20.9355746],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-jozefa-sowinskiego-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18698696-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Józefa Sowińskiego</p><p><strong>Powierzchnia:</strong> 95.3m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>714 750 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-jozefa-sowinskiego-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-15/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 49m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>711 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-15/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2329397, 21.225198],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/warszawa-57/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/09/187820191-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 160m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>710 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/warszawa-57/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 46m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>708 040 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2355285, 21.0637005],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-saska-kepa/"><img src="http://crystalhouse.pl/wp-content/uploads/2016/03/44394437-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Londyńska</p><p><strong>Powierzchnia:</strong> 40m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>699 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-saska-kepa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.206727, 20.9720929],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-grojecka-4/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18698592.jpg" width="161" alt=""></a></figure><p><strong>Ulica:</strong> Grójecka</p><p><strong>Powierzchnia:</strong> 64.5m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>696 600 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-grojecka-4/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-10/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 46m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>695 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-10/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1561298, 21.1116284],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/lot/dzialki-na-sprzedaz-warszawa-wilanow/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/10/40168670-370x215.jpg" width="370" alt=""></a></figure><p class="price"><strong>Cena:</strong> <strong><strong>690 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/lot/dzialki-na-sprzedaz-warszawa-wilanow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.206727, 20.9720929],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-grojecka/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18698610.jpg" width="161" alt=""></a></figure><p><strong>Ulica:</strong> Grójecka</p><p><strong>Powierzchnia:</strong> 63.2m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>682 560 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-grojecka/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-20/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 46m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>666 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-20/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-w-rezydencji-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/rezydencja-mokotow-naroznik-ulic-naruszewicza-i-wejnerta__1_8-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Naruszewicza</p><p><strong>Powierzchnia:</strong> 46.5m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>661 500 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-w-rezydencji-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1807444, 21.0261047],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/mieszkanie-na-sprzedaz-mokotow-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/07/26638020-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Bukowińska</p><p><strong>Powierzchnia:</strong> 53m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>649 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/mieszkanie-na-sprzedaz-mokotow-2/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-16/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 45m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>647 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-16/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2251494, 21.035899],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-10/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/37195304.jpg" width="131" alt=""></a></figure><p><strong>Ulica:</strong> Górnośląska</p><p><strong>Powierzchnia:</strong> 54m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>637 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-10/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.194157, 21.0346955],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/ekskluzywny-apartament-rezydencja-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/rezydencja-mokotow-widok-od-ulicy-naruszewicza8-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Naruszewicza</p><p><strong>Powierzchnia:</strong> 47m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>622 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/ekskluzywny-apartament-rezydencja-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-21/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 40m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>589 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-21/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-11/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 39m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>580 600 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-11/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-9/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 38m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>564 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-9/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-18/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 37m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>550 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-18/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-17/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 38m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>548 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-17/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-5/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 36m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>547 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-5/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-12/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 36m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>536 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-12/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-22/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 39m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>531 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-22/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-13/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 38m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>530 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-13/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2227501, 21.0141193],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-17/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/269-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Koszykowa</p><p><strong>Powierzchnia:</strong> 36m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>525 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-koszykowa-17/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-23/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 37m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>523 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-23/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1962166, 21.1782251],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/house/segment-na-sprzedaz-warszawa/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/10/40402292-370x215.jpg" width="370" alt=""></a></figure><p><strong>Powierzchnia:</strong> 179m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>520 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/house/segment-na-sprzedaz-warszawa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2251494, 21.035899],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-5/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/05/36981423.jpg" width="131" alt=""></a></figure><p><strong>Ulica:</strong> Górnośląska</p><p><strong>Powierzchnia:</strong> 37m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>512 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-5/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-24/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 37m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>498 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-24/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-28/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 37m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>490 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-28/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-18/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 31m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>461 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-18/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.1764506, 20.9935391],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-mokotow/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/10/40168257-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Cybernetyki</p><p><strong>Powierzchnia:</strong> 53.3m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>450 385 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/apartament-na-sprzedaz-warszawa-mokotow/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2251494, 21.035899],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-9/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/37195668.jpg" width="131" alt=""></a></figure><p><strong>Ulica:</strong> Górnośląska</p><p><strong>Powierzchnia:</strong> 37m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>436 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-9/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2251494, 21.035899],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-13/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/37107429.jpg" width="131" alt=""></a></figure><p><strong>Ulica:</strong> Górnośląska</p><p><strong>Powierzchnia:</strong> 37m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>436 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-13/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-19/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 31m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>434 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-19/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2745855, 20.9544564],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-leopolda-staffa/"><img src="http://crystalhouse.pl/wp-content/uploads/2014/01/22324993-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Leopolda Staffa</p><p><strong>Powierzchnia:</strong> 47m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>430 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-leopolda-staffa/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.221477, 21.0099915],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-21/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/tmp/thumb_370x215.jpg" alt=""></a></figure><p><strong>Ulica:</strong> Stanisława Noakowskiego</p><p><strong>Powierzchnia:</strong> 28m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>418 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-stanislawa-noakowskiego-21/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2269687, 20.9355746],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-jozefa-sowinskiego/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18698741-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Józefa Sowińskiego</p><p><strong>Powierzchnia:</strong> 46.5m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>409 200 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-jozefa-sowinskiego/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2251494, 21.035899],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-14/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/37106395.jpg" width="131" alt=""></a></figure><p><strong>Ulica:</strong> Górnośląska</p><p><strong>Powierzchnia:</strong> 34m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>400 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-14/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2269687, 20.9355746],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/business/warszawa-jozefa-sowinskiego-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18698729-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Józefa Sowińskiego</p><p><strong>Powierzchnia:</strong> 44.5m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>378 250 PLN</strong></strong></p> <a href="http://crystalhouse.pl/business/warszawa-jozefa-sowinskiego-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2251494, 21.035899],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-7/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/06/37196373.jpg" width="131" alt=""></a></figure><p><strong>Ulica:</strong> Górnośląska</p><p><strong>Powierzchnia:</strong> 27m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>368 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-gornoslaska-7/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2229739, 20.9395042],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/warszawa-jana-kazimierza-3/"><img src="http://crystalhouse.pl/wp-content/uploads/2013/10/20224218-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Jana Kazimierza</p><p><strong>Powierzchnia:</strong> 46.91m<sup>2</sup></p><p class="price"><strong>Cena:</strong> <strong><strong>361 000 PLN</strong></strong></p> <a href="http://crystalhouse.pl/apartment/warszawa-jana-kazimierza-3/" class="more">&raquo;</a></div>'
											}
										},
																				{
											latLng: [52.2348517, 21.0007633],
											data: {
												content: '<div class="offer-gmap-infobox"><a href="javascript:void(0);" class="more" style="font-size: 1.2em; top: 5px; right: 10px;" data-closeinfobox="1">x</a> <figure><a href="http://crystalhouse.pl/apartment/apartamenty-cosmopolitan-twarda-4-2/"><img src="http://crystalhouse.pl/wp-content/uploads/2015/09/14-09_JAHN-cosmo_28-min-370x215.jpg" width="370" alt=""></a></figure><p><strong>Ulica:</strong> Twarda</p> <a href="http://crystalhouse.pl/apartment/apartamenty-cosmopolitan-twarda-4-2/" class="more">&raquo;</a></div>'
											}
										},
																			]
								});
							});
						})(jQuery);
					</script>
															<section class="widget offers-list">
																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/lot/dzialka-warszawa-wilanow/">
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2014/07/26727561-370x215.jpg" width="370" alt="">
																									</a>
											</span>
																					</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/lot/dzialka-warszawa-wilanow/">Działka Warszawa Wilanów</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Typ działki:</strong> Inwestycyjna</li>
																							<li><strong>Województwo:</strong> Mazowieckie</li>
																							<li><strong>Dzielnica:</strong> Wilanów</li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>Działka Warszawa Wilanów LUKSUSOWE NIERUCHOMOŚCI WARSZAWA Działka inwestycyjna zlokalizowana na warszawskim Wilanowie. Lokalizacja ul. Wiertnicza, rejon Augustówka Powierzchnia: 1 810 m2 Wymiary:...</p>
																															</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 173/3389/OGS											 / <strong>Cena/m2:</strong> 4 254,00 <small>PLN</small> 
											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				7 700 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="173/3389/OGS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="11094" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/lot/dzialka-warszawa-wilanow/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-warszawa-bielany/">
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2013/12/21613420-370x215.jpg" width="370" alt="">
																									</a>
											</span>
																					</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-warszawa-bielany/">Działka na sprzedaż Warszawa Bielany</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Typ działki:</strong> Budowlana</li>
																							<li><strong>Województwo:</strong> Mazowieckie</li>
																							<li><strong>Dzielnica:</strong> Bielany</li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>Działka na sprzedaż Warszawa Bielany Do sprzedania działka budowlana o pow. 3458m2 na warszawskich Bielanach. Działka wg MPZP przeznaczona pod zabudowę mieszkaniowa...</p>
																															</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 207/3389/OGS											 / <strong>Cena/m2:</strong> 1 041,06 <small>PLN</small> 
											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				3 600 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="207/3389/OGS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="15904" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-warszawa-bielany/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/lot/dom-mokotow-sadyba/">
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2014/07/26726458-370x215.jpg" width="370" alt="">
																									</a>
											</span>
																					</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/lot/dom-mokotow-sadyba/">DOM MOKOTÓW SADYBA</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Typ działki:</strong> Budowlana</li>
																							<li><strong>Województwo:</strong> Mazowieckie</li>
																							<li><strong>Dzielnica:</strong> Mokotów</li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>DOM MOKOTÓW SADYBA Na sprzedaż 1/2 bliźniaka do kapitalnego remontu, lub też rozbudowy w bardzo atrakcyjnym miejscu Mokotowa &#8211; Sadyby, przy ul....</p>
																															</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 155/3389/OGS											 / <strong>Cena/m2:</strong> 5 952,00 <small>PLN</small> 
											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				2 000 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="155/3389/OGS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="11145" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/lot/dom-mokotow-sadyba/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/lot/dzialka-warszawa/">
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18906471-370x215.jpg" width="370" alt="">
																									</a>
											</span>
																					</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/lot/dzialka-warszawa/">Działka Warszawa</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Typ działki:</strong> Budowlana</li>
																							<li><strong>Województwo:</strong> Mazowieckie</li>
																							<li><strong>Dzielnica:</strong> Wawer</li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>Działka Warszawa Atrakcyjna działka o powierzchni 2103 m2 położona w Radości, zabudowana domem jednorodzinnym do wyburzenia. Bardzo dobra lokalizacja &#8211; rejon ul....</p>
																															</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 157/3389/OGS											 / <strong>Cena/m2:</strong> 832,00 <small>PLN</small> 
											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				1 750 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="157/3389/OGS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="11139" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/lot/dzialka-warszawa/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/lot/dzialka-wilanow/">
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18906556-370x215.jpg" width="370" alt="">
																									</a>
											</span>
																					</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/lot/dzialka-wilanow/">Działka Wilanów</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Typ działki:</strong> Budowlana</li>
																							<li><strong>Województwo:</strong> Mazowieckie</li>
																							<li><strong>Dzielnica:</strong> Wilanów</li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>Działka Wilanów Działka inwestycyjna zlokalizowana na warszawskim Wilanowie. Lokalizacja ul. Wiertnicza, rejon Augustówka Działka o powierzchni 1 000 m2 Wymiary: 100 m...</p>
																															</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 180/3389/OGS											 / <strong>Cena/m2:</strong> 1 400,00 <small>PLN</small> 
											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				1 400 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="180/3389/OGS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="11088" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/lot/dzialka-wilanow/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-wawer-2/">
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2013/08/18906411-370x215.jpg" width="370" alt="">
																									</a>
											</span>
																					</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-wawer-2/">Działka na sprzedaż Wawer</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Typ działki:</strong> Budowlana</li>
																							<li><strong>Województwo:</strong> Mazowieckie</li>
																							<li><strong>Dzielnica:</strong> Wawer</li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>Działka na sprzedaż Wawer Działka o powierzchni 2 804m2 o kształcie prostokąta położona w odległości ok. 400 m od Wału Miedzeszyńskiego i...</p>
																															</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 137/3389/OGS											 / <strong>Cena/m2:</strong> 450,00 <small>PLN</small> 
											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				1 261 800 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="137/3389/OGS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="11149" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-wawer-2/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-wawer/">
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2014/07/26728697-370x215.jpg" width="370" alt="">
																									</a>
											</span>
																					</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-wawer/">Działka na sprzedaż Wawer</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Typ działki:</strong> Budowlana</li>
																							<li><strong>Województwo:</strong> Mazowieckie</li>
																							<li><strong>Dzielnica:</strong> Wawer</li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>Działka na sprzedaż Wawer LUKSUSOWE NIERUCHOMOŚCI WAWER Do sprzedania działka budowlana o powierzchni 2568m2, pod zabudowę jednorodzinną. Położona w sąsiedztwie Mazowieckiego Parku...</p>
																															</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 179/3389/OGS											 / <strong>Cena/m2:</strong> 438,00 <small>PLN</small> 
											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				1 125 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="179/3389/OGS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="11091" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/lot/dzialka-na-sprzedaz-wawer/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->																			<div class="offer">	
							<div class="body">
								<div class="row">
									<div class="col-md-5 col-sm-5">
																				<figure>
											<span>
												<a href="http://crystalhouse.pl/lot/dzialki-na-sprzedaz-warszawa-wilanow/">
																	
																																								<img src="http://crystalhouse.pl/wp-content/uploads/2015/10/40168670-370x215.jpg" width="370" alt="">
																									</a>
											</span>
																					</figure>									</div>

									<article class="col-md-7 col-sm-7">
										<h4><a href="http://crystalhouse.pl/lot/dzialki-na-sprzedaz-warszawa-wilanow/">Działki na sprzedaż Warszawa Wilanów</a></h4>

										<div class="row fields">
																						<ul class="col-md-6">
																							<li><strong>Typ działki:</strong> Budowlana</li>
																							<li><strong>Województwo:</strong> Mazowieckie</li>
																							<li><strong>Dzielnica:</strong> Wilanów</li>
																						</ul>
																						<ul class="col-md-6">
																							<li><strong>Miasto:</strong> Warszawa</li>
																						</ul>
																					</div>
										<!-- .row -->

										<div class="row description">
											<p>Działki na sprzedaż Warszawa Wilanów CRYSTAL HOUSE Do sprzedania 5 atrakcyjnych działek o powierzchni 1200 m2 położonych w Wilanowie- Zawady . Bardzo...</p>
																															</div>
										<!-- .description -->
									</article>
								</div>
								<!-- .row -->

								<div class="row footer">
									<div class="col-md-6 col-sm-6">
										<p>
											<strong>Nr. oferty:</strong> 232/3389/OGS											 / <strong>Cena/m2:</strong> 575,00 <small>PLN</small> 
											</p>
									</div>
									<!-- .col-md-6 -->
									<div class="col-md-6 col-sm-6">
																				<p class="price">
																																				690 000 <small>PLN</small>																																	</p>									</div>
									<!-- .col-md-6 -->
								</div>
								<!-- .row -->
							</div>
							<!-- .body -->

														<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="Zapytaj o cenę" data-toggle="modal" data-modal-form-offer="232/3389/OGS" data-modal-form-subject="Pytanie o cenę" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<li><a href="javascript:void(0);" title="Dodaj do schowka" data-addtoclipboard="24532" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li>									<li><a href="http://crystalhouse.pl/lot/dzialki-na-sprzedaz-warszawa-wilanow/" class="block" title="Zobacz więcej" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->						</div>
						<!-- .offer -->											</section>
					<!-- .widget.offers-list -->

									
								</div>
				<!-- .col-md-9 -->
				<div class="col-md-3">
										<div class="widget offers-list-filter">
						<h2>Znajdź nieruchomość</h2>
						<form method="get" action="http://crystalhouse.pl/offer-category/kup/?" name="offers_filter" data-form-values='{"country": "Polska", "offer": "buy"}'>
														<div class="form-group">
								<input type="text" name="q"placeholder="słowo kluczowe">							</div>
							<!-- .form-group -->																												<div class="form-group" data-form-fieldset="currency">
								<div class="row">
									<div class="col-md-4"><label style="margin-top: 2px;">Waluta</label></div>
									<div class="col-md-8">
										<div class="inline-checkboxes currencies">
																					<label><input type="radio" name="currency" value="PLN" checked="checked"> PLN</label>
																					<label><input type="radio" name="currency" value="USD"> USD</label>
																					<label><input type="radio" name="currency" value="EUR"> EUR</label>
																				</div>
										<!-- .inline-checkboxes -->
									</div>
								</div>
								<!-- .row -->
							</div>
							<!-- .form-group -->
							<div data-group="offer_type_offer-category">
																<div class="form-group" data-form-fieldset="type">
									<label>Typ nieruchomości</label>
									<select name="type"><option value="">Wszystkie</option><option value="apartment">Apartamenty</option><option value="house">Domy</option><option value="lot" selected="selected">Działki</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="rent_type" style="display: none;">
									<label>Rodzaj wynajmu</label>
									<select name="rent_type" disabled="disabled"><option value="12">Wynajem długoterminowy</option><option value="10">Wynajem krótkoterminowy</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="market_type">
									<label>Rodzaj rynku</label>
									<div class="row inline-checkboxes">
																			<div class="col-xs-6" data-form-fieldset="market_type_secondary">
											<label><input type="radio" name="market_type[]" value="secondary"> wtórny</label>
										</div>
										<!-- .col-xs-6 -->
																			<div class="col-xs-6" data-form-fieldset="market_type_primary">
											<label><input type="radio" name="market_type[]" value="primary"> pierwotny</label>
										</div>
										<!-- .col-xs-6 -->
																		</div>
									<!-- .inline-radios -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="lot_type" style="display: none;">
									<label>Typ działki</label>
									<select name="lot_type" disabled="disabled"><option value="" selected="selected">Wszystkie</option><option value="construction">Budowlana</option><option value="recreational">Rekreacyjna</option><option value="agricultural">Rolna</option><option value="investment">Inwestycyjna</option><option value="industrial">Przemysłowa</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="area">
									<label><span data-type-label="default">Powierzchnia</span> <span data-type-label="house" style="display: none;">Powierzchnia domu</span></label>
									<div class="row">
										<div class="col-xs-6">
											<select name="area_from" style="text-transform: none;"><option value="">OD</option><option value="">0</option><option value="50">50m&#178;</option><option value="75">75m&#178;</option><option value="100">100m&#178;</option><option value="150">150m&#178;</option><option value="200">200m&#178;</option><option value="250">250m&#178;</option></select>										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<select name="area_to" style="text-transform: none;"><option value="">DO</option><option value="50">50m&#178;</option><option value="75">75m&#178;</option><option value="100">100m&#178;</option><option value="150">150m&#178;</option><option value="200">200m&#178;</option><option value="250">250m&#178;</option><option value="400">400m&#178;</option><option value="_">400m&#178;+</option></select>										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="area_lot" style="display: none;">
									<label>Powierzchnia działki</label>
									<div class="row">
										<div class="col-xs-6">
											<select name="area_lot_from" disabled="disabled" style="text-transform: none;"><option value="">OD</option><option value="">0</option><option value="500">500m&#178;</option><option value="1000">1.000m&#178;</option><option value="2000">2.000m&#178;</option><option value="3000">3.000m&#178;</option><option value="4000">4.000m&#178;</option><option value="5000">5.000m&#178;</option></select>										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<select name="area_lot_to" disabled="disabled" style="text-transform: none;"><option value="">DO</option><option value="500">500m&#178;</option><option value="1000">1.000m&#178;</option><option value="2000">2.000m&#178;</option><option value="3000">3.000m&#178;</option><option value="4000">4.000m&#178;</option><option value="5000">5.000m&#178;</option><option value="10000">10.000m&#178;</option><option value="20000">20.000m&#178;</option><option value="_">20.000m&#178;+</option></select>										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="price">
									<label>Cena</label>
									<div class="row">
										<div class="col-xs-6">
											<select name="price_from"><option value="">Od</option><option value="">0 PLN</option><option value="500000">500.000 PLN</option><option value="1000000">1.000.000 PLN</option><option value="2000000">2.000.000 PLN</option><option value="3000000">3.000.000 PLN</option><option value="4000000">4.000.000 PLN</option><option value="5000000">5.000.000 PLN</option></select>										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<select name="price_to"><option value="">Do</option><option value="1000000">1.000.000 PLN</option><option value="2000000">2.000.000 PLN</option><option value="3000000">3.000.000 PLN</option><option value="4000000">4.000.000 PLN</option><option value="5000000">5.000.000 PLN</option><option value="_">5.000.000+ PLN</option></select>										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="price_m2">
									<label>Cena/m2</label>
									<div class="row">
										<div class="col-xs-6">
											<select name="price_m2_from"><option value="">Od</option><option value="">0 PLN</option><option value="1000">1.000 PLN</option><option value="2000">2.000 PLN</option><option value="3000">3.000 PLN</option><option value="4000">4.000 PLN</option><option value="5000">5.000 PLN</option></select>										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<select name="price_m2_to"><option value="">Do</option><option value="1000">1.000 PLN</option><option value="2000">2.000 PLN</option><option value="3000">3.000 PLN</option><option value="4000">4.000 PLN</option><option value="5000">5.000 PLN</option><option value="_">5.000+ PLN</option></select>										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="province" style="display: none;">
									<label>Województwo</label>
									<select name="province" disabled="disabled"><option value="" selected="selected">Wszystkie</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="city">
									<label>Miasto</label>
									<select name="city"><option value="">Wszystkie</option><option value=" Le Castellet"> Le Castellet</option><option value="BENIDORM">BENIDORM</option><option value="Bielawa">Bielawa</option><option value="Cannes">Cannes</option><option value="Chojnów">Chojnów</option><option value="Chyliczki">Chyliczki</option><option value="Costa Blanca">Costa Blanca</option><option value="Costa Del Sol">Costa Del Sol</option><option value="Costa Del Sol, Marbella">Costa Del Sol, Marbella</option><option value="Cote d'Azur">Cote d'Azur</option><option value="Dołhobyczów">Dołhobyczów</option><option value="Dziekanów Polski">Dziekanów Polski</option><option value="Gdańsk">Gdańsk</option><option value="Gdynia">Gdynia</option><option value="Habdzin">Habdzin</option><option value="Hel">Hel</option><option value="Hornówek">Hornówek</option><option value="Izabelin">Izabelin</option><option value="Jabłonna">Jabłonna</option><option value="Jastarnia">Jastarnia</option><option value="Jastrzębie">Jastrzębie</option><option value="Józefosław">Józefosław</option><option value="Józefów">Józefów</option><option value="Jurata">Jurata</option><option value="Kampinos">Kampinos</option><option value="Kazimierz Dolny">Kazimierz Dolny</option><option value="Kępa Oborska">Kępa Oborska</option><option value="Kobyłka">Kobyłka</option><option value="Koczargi Stare">Koczargi Stare</option><option value="Kołobrzeg">Kołobrzeg</option><option value="Komorów">Komorów</option><option value="Konstancin-Jeziorna">Konstancin-Jeziorna</option><option value="Kościelisko">Kościelisko</option><option value="Kotorydz">Kotorydz</option><option value="Kraków">Kraków</option><option value="Kruklanki">Kruklanki</option><option value="Książnik">Książnik</option><option value="Kwitajny">Kwitajny</option><option value="Lanckorona">Lanckorona</option><option value="Lasocin">Lasocin</option><option value="Le Castellet">Le Castellet</option><option value="Lipków">Lipków</option><option value="Lisewo">Lisewo</option><option value="Lwówek Śląski">Lwówek Śląski</option><option value="Łomianki">Łomianki</option><option value="Łódź">Łódź</option><option value="Magdalenka">Magdalenka</option><option value="Marbella">Marbella</option><option value="Marki">Marki</option><option value="Michałowice">Michałowice</option><option value="Nadarzyn">Nadarzyn</option><option value="Natolin">Natolin</option><option value="Nicea">Nicea</option><option value="Obory">Obory</option><option value="Otwock">Otwock</option><option value="Pęcice Małe">Pęcice Małe</option><option value="Piaseczno">Piaseczno</option><option value="Podkowa Leśna">Podkowa Leśna</option><option value="Pruszków">Pruszków</option><option value="Raciborsko">Raciborsko</option><option value="Radość">Radość</option><option value="Radzymin">Radzymin</option><option value="Reguły">Reguły</option><option value="Rusiec">Rusiec</option><option value="Serock">Serock</option><option value="Sękocin Nowy">Sękocin Nowy</option><option value="Sękocin-Las">Sękocin-Las</option><option value="Skop">Skop</option><option value="Słoneczny Brzeg">Słoneczny Brzeg</option><option value="Słoneczny Brzeg Sunny Beach">Słoneczny Brzeg Sunny Beach</option><option value="Stara Wieś">Stara Wieś</option><option value="Stare Babice">Stare Babice</option><option value="Strzembowo">Strzembowo</option><option value="Toruń">Toruń</option><option value="Unieście">Unieście</option><option value="Walendów">Walendów</option><option value="Warszawa" selected="selected">Warszawa</option><option value="Wejherowo">Wejherowo</option><option value="Wiązowna">Wiązowna</option><option value="Władysławowo">Władysławowo</option><option value="Zakopane">Zakopane</option><option value="Zawada">Zawada</option><option value="Ząbki">Ząbki</option><option value="Zgorzelec">Zgorzelec</option><option value="Zielonka">Zielonka</option><option value="Żółwin">Żółwin</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="district">
									<label>Dzielnica</label>
									<select name="district"><option value="" selected="selected">Wszystkie</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="developer">
									<label>Developer</label>
									<select name="developer"><option value="" selected="selected">Wszystkie</option><option value="Bavaria Development">Bavaria Development</option><option value="BBI Development NFI">BBI Development NFI</option><option value="Catalina Group">Catalina Group</option><option value="Delpha Investments">Delpha Investments</option><option value="Dom Development">Dom Development</option><option value="Dom Development S.A.">Dom Development S.A.</option><option value="Dor Group">Dor Group</option><option value="Echo Investments">Echo Investments</option><option value="Fenix Group">Fenix Group</option><option value="Filadelfia Sp. z.o.o.">Filadelfia Sp. z.o.o.</option><option value="Grupa Inwestycyjna BEL">Grupa Inwestycyjna BEL</option><option value="Juvenes">Juvenes</option><option value="LD Sp. z.o.o.">LD Sp. z.o.o.</option><option value="Marvipol S.A">Marvipol S.A</option><option value="Myoni">Myoni</option><option value="Orco">Orco</option><option value="Parkowa Łazienki Sp. z.o.o">Parkowa Łazienki Sp. z.o.o</option><option value="Parkowa Łazienki Sp. z.o.o.">Parkowa Łazienki Sp. z.o.o.</option><option value="Polnord">Polnord</option><option value="Shiraz Sp. z o.o.">Shiraz Sp. z o.o.</option><option value="SM Pax">SM Pax</option><option value="SM Vistula">SM Vistula</option><option value="Tacit Investment">Tacit Investment</option><option value="Wan S.A.">Wan S.A.</option></select>								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="rooms">
									<label>Ilość pokoi</label>
									<div class="row">
										<div class="col-xs-6">
											<select name="rooms_from"><option value="">Od</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select>										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<select name="rooms_to"><option value="">Do</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="_">5+</option></select>										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="bathrooms">
									<label>Ilość łazienek</label>
									<div class="row">
										<div class="col-xs-6">
											<select name="bathrooms_from"><option value="">Od</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select>										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<select name="bathrooms_to"><option value="">Do</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="_">5+</option></select>										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="built_year">
									<label>Rok budowy</label>
									<div class="row">
										<div class="col-xs-6">
											<input type="text" name="built_year_from"placeholder="Od" maxlength="4">										</div>
										<!-- .col-xs-6 -->
										<div class="col-xs-6">
											<input type="text" name="built_year_to"placeholder="Do" maxlength="4">										</div>
										<!-- .col-xs-6 -->
									</div>
									<!-- .row -->
								</div>
								<!-- .form-group -->																<div class="form-group" data-form-fieldset="center_distance">
									<label>Odległość od centrum</label>
									<select name="center_distance"><option value="">Wszystkie</option><option value="<=1">do 1 km</option><option value="<=2" selected="selected">do 2 km</option><option value="<=5">do 5 km</option><option value="<=10">do 10 km</option><option value=">10">powyżej 10 km</option></select>								</div>
								<!-- .form-group -->																								<div class="form-group" data-form-fieldset="attributes">
									<label>Atrybuty</label>
									<div class="row attributes" data-form-fieldset="attributes_short">
																			<div class="col-xs-6">
																					<label><input type="checkbox" name="attr[]" value="parking"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/parking.png" alt="parking"></em> Parking</label>
																					<label><input type="checkbox" name="attr[]" value="reception"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/reception.png" alt="reception"></em> Portier</label>
																					<label><input type="checkbox" name="attr[]" value="balcony"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/balcony.png" alt="balcony"></em> Balkon</label>
																					<label><input type="checkbox" name="attr[]" value="aircondition"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/aircondition.png" alt="aircondition"></em> Klimatyzacja</label>
																				</div>
										<!-- .col-xs-6 -->
																			<div class="col-xs-6">
																					<label><input type="checkbox" name="attr[]" value="internet"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/internet.png" alt="internet"></em> Internet</label>
																					<label><input type="checkbox" name="attr[]" value="utility-room"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/utility-room.png" alt="utility-room"></em> Schowek</label>
																					<label><input type="checkbox" name="attr[]" value="elevator"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/elevator.png" alt="elevator"></em> Winda</label>
																					<label><input type="checkbox" name="attr[]" value="security"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/security.png" alt="security"></em> Ochrona</label>
																				</div>
										<!-- .col-xs-6 -->
																		</div>
									<!-- .row -->

																		<div class="attributes border" data-form-fieldset="attributes_long">
																				<label><input type="checkbox" name="attr[]" value="foreign-schools"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/foreign-schools.png" alt="foreign-schools"></em> Szkoły zagraniczne w pobliżu</label>
																				<label><input type="checkbox" name="attr[]" value="shopcentre"> <em><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/offer/shopcentre.png" alt="shopcentre"></em> Centra handlowe w pobliżu</label>
																			</div>
									<!-- .row -->
																	</div>
								<!-- .form-group -->
								
								<div class="form-group text-center">
																		
									<input type="hidden" name="order">
									<input type="submit" value="Szukaj" class="btn search"> 
									<a href="http://crystalhouse.pl/offer-category/kup/?" class="btn">Usuń filtry</a>								</div>
								<!-- .form-group -->
							</div>
							<!-- [data-group] -->
						</form>
					</div>
					<!-- .widget.offers-list-filter -->				</div>
				<!-- .col-md-3 -->
			</div>
			<!-- .row -->
		</div>
		<!-- .container -->
	</div>
	<!-- #content -->


		<section class="home-section popular-entries hidden-print">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6 about">
					<h2>Najczęściej wyszukiwane</h2>
					<i class="icon-search"></i>
				</div>
				<!-- .col-md-3 -->

									<div class="col-md-3 col-sm-6"><h4>Sprzedaż</h4><nav><ul><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-srodmiescie/" title="Luksusowy apartament Śródmieście">Luksusowy apartament Śródmieście</a></li><li><a href="http://crystalhouse.pl/house/dom-saska-kepa-2/" title="Ekskluzywny Dom Saska Kępa">Ekskluzywny Dom Saska Kępa</a></li><li><a href="http://crystalhouse.pl/house/dom-konstancin-jeziorna-4/" title="Dom Konstancin Jeziorna">Dom Konstancin Jeziorna</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-bielany/" title="Luksusowy apartament Warszawa Bielany">Luksusowy apartament Warszawa Bielany</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowa-nieruchomosc-warszawa-mokotow/" title="Luksusowa nieruchomość Warszawa Mokotów">Luksusowa nieruchomość Warszawa Mokotów</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-wola-okopowa/" title="Luksusowy Apartament Warszawa Wola – Okopowa">Luksusowy Apartament Warszawa Wola – Okopowa</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-mokotow/" title="Luksusowy apartament Warszawa Mokotów">Luksusowy apartament Warszawa Mokotów</a></li><li><a href="http://crystalhouse.pl/house/nowoczesny-dom-w-komorowie/" title="Nowoczesny dom w Komorowie">Nowoczesny dom w Komorowie</a></li><li><a href="http://crystalhouse.pl/apartment/apartament-warszawa-mokotow-3/" title="Luksusowy Apartament Warszawa Mokotów">Luksusowy Apartament Warszawa Mokotów</a></li><li><a href="http://crystalhouse.pl/house/luksusowy-dom-warszawa-2/" title="Luksusowy dom Warszawa Wawer">Luksusowy dom Warszawa Wawer</a></li><li><a href="http://crystalhouse.pl/apartment/ekskluzywny-apartament-mokotow/" title="Ekskluzywny apartament Mokotów">Ekskluzywny apartament Mokotów</a></li></ul></nav></div>									<div class="col-md-3 col-sm-6"><h4>Wynajem</h4><nav><ul><li><a href="http://crystalhouse.pl/apartment/ekskluzywny-apartament-warszawa-powisle/" title="Ekskluzywny apartament Warszawa Powiśle">Ekskluzywny apartament Warszawa Powiśle</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-srodmiescie-2/" title="Luksusowy apartament Warszawa Śródmieście">Luksusowy apartament Warszawa Śródmieście</a></li><li><a href="http://crystalhouse.pl/apartment/ekskluzywny-apartament-warszawa-srodmiescie/" title="Ekskluzywny apartament Warszawa Śródmieście">Ekskluzywny apartament Warszawa Śródmieście</a></li><li><a href="http://crystalhouse.pl/house/ekskluzywny-dom-konstancin-jeziorna/" title="Ekskluzywny dom Konstancin-Jeziorna">Ekskluzywny dom Konstancin-Jeziorna</a></li><li><a href="http://crystalhouse.pl/house/luksusowa-willa-warszawa/" title="Luksusowa Willa Warszawa">Luksusowa Willa Warszawa</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-saska-kepa/" title="Luksusowy apartament Saska Kępa">Luksusowy apartament Saska Kępa</a></li><li><a href="http://crystalhouse.pl/apartment/apartament-srodmiescie/" title="Apartament Śródmieście">Apartament Śródmieście</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-centrum/" title="Luksusowy apartament Warszawa centrum">Luksusowy apartament Warszawa centrum</a></li><li><a href="http://crystalhouse.pl/apartment/ekskluzywny-apartament-mokotow/" title="Ekskluzywny apartament Mokotów">Ekskluzywny apartament Mokotów</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-wilanow/" title="Luksusowy apartament Warszawa Wilanów">Luksusowy apartament Warszawa Wilanów</a></li><li><a href="http://crystalhouse.pl/apartment/penthouse-zoliborz/" title="Penthouse Żoliborz">Penthouse Żoliborz</a></li></ul></nav></div>									<div class="col-md-3 col-sm-6"><h4>Inwestycje apartamentowe</h4><nav><ul><li><a href="http://crystalhouse.pl/apartment/rezydencja-opera/" title="Rezydencja Opera">Rezydencja Opera</a></li><li><a href="http://crystalhouse.pl/apartment/morskie-oko/" title="Morskie Oko">Morskie Oko</a></li><li><a href="http://crystalhouse.pl/apartment/melody-house/" title="Melody House">Melody House</a></li><li><a href="http://crystalhouse.pl/apartment/mondrian-house/" title="Mondrian House">Mondrian House</a></li><li><a href="http://crystalhouse.pl/apartment/narbutta-residence/" title="Narbutta Residence">Narbutta Residence</a></li><li><a href="http://crystalhouse.pl/apartment/patria/" title="Patria">Patria</a></li><li><a href="http://crystalhouse.pl/apartment/madalinskiego/" title="Madalińskiego">Madalińskiego</a></li><li><a href="http://crystalhouse.pl/apartment/rezydencja-krolewska/" title="Rezydencja Królewska">Rezydencja Królewska</a></li><li><a href="http://crystalhouse.pl/apartment/wisle/" title="Przy Wiśle">Przy Wiśle</a></li><li><a href="http://crystalhouse.pl/apartment/skoczni/" title="Przy Skoczni">Przy Skoczni</a></li></ul></nav></div>							</div>
			<!-- .row -->

						<div data-expandable>
				<div class="row">
											<div class="col-md-3 col-sm-6"><h4>Wakacyjne</h4><nav><ul><li><a href="http://crystalhouse.pl/house/luksusowa-willa-z-basenem/" title="LUKSUSOWA WILLA Z BASENEM">LUKSUSOWA WILLA Z BASENEM</a></li><li><a href="http://crystalhouse.pl/house/dom-w-hiszpanii-costa-blanca/" title="Dom w Hiszpanii, Costa Blanca">Dom w Hiszpanii, Costa Blanca</a></li><li><a href="http://crystalhouse.pl/house/francjale-castellet-2/" title="Francja,Le Castellet">Francja,Le Castellet</a></li><li><a href="http://crystalhouse.pl/house/costa-del-sol-marbella-marbella-4/" title="Costa Del Sol, Marbella, Marbella">Costa Del Sol, Marbella, Marbella</a></li><li><a href="http://crystalhouse.pl/house/nowoczesna-willa-cannes/" title="Nowoczesna willa, Cannes">Nowoczesna willa, Cannes</a></li><li><a href="http://crystalhouse.pl/house/willa-z-basenem-lazurowe-wybrzeze/" title="Willa z basenem, Lazurowe Wybrzeże">Willa z basenem, Lazurowe Wybrzeże</a></li><li><a href="http://crystalhouse.pl/apartment/cannes-cannes-4/" title="Cannes, Cannes">Cannes, Cannes</a></li><li><a href="http://crystalhouse.pl/apartment/bulgaria-sloneczny-brzeg/" title="Bułgaria, Słoneczny Brzeg">Bułgaria, Słoneczny Brzeg</a></li><li><a href="http://crystalhouse.pl/house/francja-cote-dazur-saint-tropez/" title="Francja, Cote d’Azur, Saint Tropez">Francja, Cote d’Azur, Saint Tropez</a></li><li><a href="http://crystalhouse.pl/house/costa-del-sol-marbella-marbella/" title="Costa Del Sol, Marbella, Marbella">Costa Del Sol, Marbella, Marbella</a></li></ul></nav></div>											<div class="col-md-3 col-sm-6"><h4>Lokale komercyjne</h4><nav><ul><li><a href="http://crystalhouse.pl/business/lokal-z-najemca-lodz/" title="Lokal z najemcą Łódź">Lokal z najemcą Łódź</a></li><li><a href="http://crystalhouse.pl/business/lokal-z-najemca-wroclaw/" title="Lokal z najemcą Wrocław 9,5% rentowność">Lokal z najemcą Wrocław 9,5% rentowność</a></li><li><a href="http://crystalhouse.pl/business/lokal-z-najemca-sieciowym-gdansk-2/" title="Lokal z najemcą sieciowym Gdańsk – 8,6% rentowność">Lokal z najemcą sieciowym Gdańsk – 8,6% rentowność</a></li><li><a href="http://crystalhouse.pl/business/lokal-z-najemca-sieciowym-warszawa/" title="Lokal z najemcą sieciowym Warszawa Rondo Wiatraczna">Lokal z najemcą sieciowym Warszawa Rondo Wiatraczna</a></li><li><a href="http://crystalhouse.pl/business/lokal-z-najemca-warszawa/" title="Lokal z najemcą Warszawa">Lokal z najemcą Warszawa</a></li><li><a href="http://crystalhouse.pl/business/warszawa-karola-chodkiewicza-5/" title="Warszawa, Karola Chodkiewicza">Warszawa, Karola Chodkiewicza</a></li><li><a href="http://crystalhouse.pl/business/warszawa-bialy-kamien/" title="Warszawa, Biały Kamień">Warszawa, Biały Kamień</a></li><li><a href="http://crystalhouse.pl/business/warszawa-leszno/" title="Warszawa, Leszno">Warszawa, Leszno</a></li><li><a href="http://crystalhouse.pl/business/warszawa-al-wincentego-witosa-4/" title="Warszawa, al. Wincentego Witosa">Warszawa, al. Wincentego Witosa</a></li></ul></nav></div>											<div class="col-md-3 col-sm-6"><h4>Popularne lokalizacje</h4><nav><ul><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Warszawa&district=Mokot%C3%B3w" title="Warszawa-Mokotów">Warszawa-Mokotów</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Konstancin-Jeziorna" title="Konstancin-Jeziorna">Konstancin-Jeziorna</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Warszawa&district=%C5%9Ar%C3%B3dmie%C5%9Bcie" title="Warszawa-Śródmieście">Warszawa-Śródmieście</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Warszawa&district=Wilan%C3%B3w" title="Warszawa-Wilanów">Warszawa-Wilanów</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Komor%C3%B3w" title="Komorów">Komorów</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Cannes" title="Cannes">Cannes</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Costa+Del+Sol%2C+Marbella" title="COSTA DEL SOL, Marbella">COSTA DEL SOL, Marbella</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Warszawa&district=Ursyn%C3%B3w" title="Warszawa-Ursynów">Warszawa-Ursynów</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Warszawa&district=Praga-Po%C5%82udnie" title="Warszawa-Praga Południe">Warszawa-Praga Południe</a></li><li><a href="http://crystalhouse.pl/szukaj/?offer=buy&currency=PLN&city=Warszawa&district=%C5%BBoliborz" title="Warszawa- Żoliborz">Warszawa- Żoliborz</a></li></ul></nav></div>											<div class="col-md-3 col-sm-6"><h4>Popularne nieruchomości</h4><nav><ul><li><a href="http://crystalhouse.pl/house/luksusowy-dom-sprzedaz-mokotow/" title="Luksusowy dom sprzedaż Mokotów">Luksusowy dom sprzedaż Mokotów</a></li><li><a href="http://crystalhouse.pl/house/nowoczesny-dom-w-komorowie/" title="Nowoczesny dom w Komorowie">Nowoczesny dom w Komorowie</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-mokotow/" title="Luksusowy apartament Warszawa Mokotów">Luksusowy apartament Warszawa Mokotów</a></li><li><a href="http://crystalhouse.pl/apartment/apartament-warszawa-mokotow-3/" title="Luksusowy Apartament Warszawa Mokotów">Luksusowy Apartament Warszawa Mokotów</a></li><li><a href="http://crystalhouse.pl/house/luksusowy-dom-wesola/" title="Luksusowy Dom Wesoła">Luksusowy Dom Wesoła</a></li><li><a href="http://crystalhouse.pl/house/palac-konstancin-jeziorna/" title="Pałac Konstancin Jeziorna">Pałac Konstancin Jeziorna</a></li><li><a href="http://crystalhouse.pl/house/luksusowa-rezydencja/" title="Luksusowa Rezydencja">Luksusowa Rezydencja</a></li><li><a href="http://crystalhouse.pl/house/luksusowy-dom-konstancin-jeziorna/" title="Luksusowy dom Konstancin Jeziorna">Luksusowy dom Konstancin Jeziorna</a></li><li><a href="http://crystalhouse.pl/house/luksusowa-willa-stare-bielany/" title="Luksusowa willa Stare Bielany">Luksusowa willa Stare Bielany</a></li><li><a href="http://crystalhouse.pl/house/dom-konstancin-jeziorna/" title="Dom Konstancin-Jeziorna">Dom Konstancin-Jeziorna</a></li></ul></nav></div>											<div class="col-md-3 col-sm-6"><h4>Beliebte Ferienwohnungen</h4><nav><ul><li><a href="http://crystalhouse.pl/house/luksusowy-dom-sprzedaz-mokotow/" title="Luksusowy dom sprzedaż Mokotów">Luksusowy dom sprzedaż Mokotów</a></li><li><a href="http://crystalhouse.pl/house/nowoczesny-dom-w-komorowie/" title="Nowoczesny dom w Komorowie">Nowoczesny dom w Komorowie</a></li><li><a href="http://crystalhouse.pl/apartment/luksusowy-apartament-warszawa-mokotow/" title="Luksusowy apartament Warszawa Mokotów">Luksusowy apartament Warszawa Mokotów</a></li><li><a href="http://crystalhouse.pl/apartment/apartament-warszawa-mokotow-3/" title="Luksusowy Apartament Warszawa Mokotów">Luksusowy Apartament Warszawa Mokotów</a></li><li><a href="http://crystalhouse.pl/house/luksusowy-dom-wesola/" title="Luksusowy Dom Wesoła">Luksusowy Dom Wesoła</a></li><li><a href="http://crystalhouse.pl/house/palac-konstancin-jeziorna/" title="Pałac Konstancin Jeziorna">Pałac Konstancin Jeziorna</a></li><li><a href="http://crystalhouse.pl/house/luksusowa-rezydencja/" title="Luksusowa Rezydencja">Luksusowa Rezydencja</a></li><li><a href="http://crystalhouse.pl/house/luksusowy-dom-konstancin-jeziorna/" title="Luksusowy dom Konstancin Jeziorna">Luksusowy dom Konstancin Jeziorna</a></li><li><a href="http://crystalhouse.pl/house/luksusowa-willa-stare-bielany/" title="Luksusowa willa Stare Bielany">Luksusowa willa Stare Bielany</a></li><li><a href="http://crystalhouse.pl/house/dom-konstancin-jeziorna/" title="Dom Konstancin-Jeziorna">Dom Konstancin-Jeziorna</a></li></ul></nav></div>									</div>
				<!-- .row -->
			</div>
			<!-- [data-expandable] -->
			
			<div class="expander">
				<span><a href="#" data-expand data-collapse-label="Zwiń">Rozwiń</a></span>
			</div>
			<!-- .expander -->
		</div>
		<!-- .container -->
	</section>
	<!-- .popular-entries -->
	<div id="footer" class="hidden-print">
		<div class="container">
			<div class="row hidden-xs">
				<h3 class="slogan"><strong>Luxury</strong> Realty Estate Agency</h3>
			</div>
			<!-- .row -->

			<div class="row">
				<div class="col-md-3 col-sm-12 col-sm-12">
					<a href="http://crystalhouse.pl/"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/logo-dark.png" alt=""></a>
				</div>
				<!-- .col-md-3 -->

				<div class="col-md-3 col-sm-4 col-xs-6">
					<p>ul. Belwederska 36/38D/99 <br>00-594 Warszawa</p>
				</div>
				<!-- .col-md-3 -->

				<div class="col-md-3 col-sm-4 col-xs-6">
					<p class="big">Telefon<br><strong>+48 22 856 78 60</strong></p>
				</div>
				<!-- .col-md-3 -->

				<div class="col-md-3 col-sm-4 col-xs-12">
					<p class="big">Email<br><strong><a href="mailto:biuro@crystalhouse.pl">biuro@crystalhouse.pl</a></strong></p>

										<ul class="socials">
												<li><a href="https://www.facebook.com/CrystalHousepl/?ref=aymt_homepage_panel"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/socials/facebook.png" alt="Facebook"></a></li>																		<li><a href="https://www.youtube.com/channel/UCCUjgifsjADB5yLsBxlCEnQ"><img src="http://crystalhouse.pl/wp-content/themes/crystalhouse/images/icons/socials/youtube.png" alt="Youtube"></a></li>					</ul>				</div>
				<!-- .col-md-3 -->
			</div>
			<!-- .row -->
		</div>
		<!-- .container -->

		<div class="copyrights">
			<div class="container">
				<p>&copy; 2016 <a href="http://crystalhouse.pl/">CrystalHouse</a> | <a href="http://roogmedia.pl/" style="cursor: default; opacity: 1;">by Roogmedia</a></p>
			</div>
			<!-- .container -->
		</div>
		<!-- .copyrights -->
	</div>
	<!-- #footer -->

	<script type='text/javascript' src='http://crystalhouse.pl/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.50.0-2014.02.05'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"http:\/\/crystalhouse.pl\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Trwa wysy\u0142anie...","cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='http://crystalhouse.pl/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=3.8.1'></script>
<script type='text/javascript' src='http://crystalhouse.pl/wp-includes/js/wp-embed.min.js?ver=4.5.2'></script>
		<!-- Form filters values -->
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/form-filters_pl.js"></script>

	<!-- CDN libraries -->
	<script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-538a578c3e089f8a"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=false&amp;language=pl"></script>

	<!-- jQuery -->
	<!--<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery-1.11.1.min.js"></script>-->
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery.easing.1.3.min.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery.scrollTo.min.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery.sticky.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery.jcarousel.min.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery.cookie.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery.scrollintoview.min.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/jquery.resizeimagetoparent.min.js"></script>

	<!-- Bootstrap -->
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/bootstrap.min.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/bootstrap-hover-dropdown.min.js"></script>

	<!-- FancyBox -->
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/fancyBox2/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/fancyBox2/jquery.fancybox.pack.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/fancyBox2/helpers/jquery.fancybox-media.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/fancyBox2/helpers/jquery.fancybox-thumbs.js"></script>

	<!-- Gmap3 -->
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/gmap3.min.js"></script>

	<!-- Widgets -->
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/app/home-theatre.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/app/offer-gallery-carousel.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/app/offers-carousel.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/app/offers-list-map.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/app/popular-entries.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/app/wishlist.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/offers-filters.js"></script>
	<!--<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/lib.js"></script>-->

	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/app.js"></script>
	<script src="http://crystalhouse.pl/wp-content/themes/crystalhouse/js/src/vendor/smoothscroll.js"></script>
</body>
</html>
<!-- Dynamic page generated in 4.070 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2016-07-01 01:53:52 -->

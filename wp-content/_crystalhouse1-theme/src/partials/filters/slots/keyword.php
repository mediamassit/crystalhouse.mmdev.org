<?php

if($config->hasFilter('keyword')) {
	$tags_slugs = explode(',', $config->getFilter('keyword'));
	$tags = array();
	
	foreach($tags_slugs as $slug) {
		$term = get_term_by('slug', $slug, 'offer_tag');
		
		if(is_object($term)) {
			$tags[] = $term->name;
		}
	}
}

?>							<?php if(sizeof(@$tags)) : ?>
							<div class="form-group">
								<label><?php _e(get_taxonomy('offer_tag')->labels->singular_name, 'chtheme'); ?>: <span style="font-weight: 400;"><?php echo htmlspecialchars(implode(', ', $tags)); ?></span></label>
								<input type="hidden" name="keyword" value="<?php echo htmlspecialchars($config->getFilter('keyword')); ?>">
							</div>
							<!-- .form-group -->
							<?php endif; ?>
<?php

// Offer section
function offer_section_custom_taxonomy() {
	$labels = array(
		'name'                       => _x( 'Sekcje ofert', 'Taxonomy General Name', 'chtheme' ),
		'singular_name'              => _x( 'Sekcja ofert', 'Taxonomy Singular Name', 'chtheme' ),
		'menu_name'                  => __( 'Sekcje ofert', 'chtheme' ),
		'all_items'                  => __( 'Wszystkie', 'chtheme' ),
		'parent_item'                => __( 'Nadrzędna', 'chtheme' ),
		'parent_item_colon'          => __( 'Nadrzędna:', 'chtheme' ),
		'new_item_name'              => __( 'Nowa sekcja ofert', 'chtheme' ),
		'add_new_item'               => __( 'Dodaj nową sekcję ofert', 'chtheme' ),
		'edit_item'                  => __( 'Edytuj sekcję', 'chtheme' ),
		'update_item'                => __( 'Aktualizuj sekcję', 'chtheme' ),
		'separate_items_with_commas' => __( 'Oddziel sekcje przecinkami', 'chtheme' ),
		'search_items'               => __( 'Szukaj sekcji', 'chtheme' ),
		'add_or_remove_items'        => __( 'Dodaj lub usuń sekcję', 'chtheme' ),
		'choose_from_most_used'      => __( 'Najczęściej używane sekcje', 'chtheme' ),
		'not_found'                  => __( 'Nie odnaleziono', 'chtheme' ),
	);
	$rewrite = array(
		'slug'                       => 'offer-section',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'offer_section', array( 'offer_apartment', 'offer_business', 'offer_house', 'offer_lot' ), $args );

}
//add_action( 'init', 'offer_section_custom_taxonomy', 0 );

// Offer category
function offer_category_custom_taxonomy() {
	$labels = array(
		'name'                       => _x( 'Kategorie ofert', 'Taxonomy General Name', 'chtheme' ),
		'singular_name'              => _x( 'Kategoria oferty', 'Taxonomy Singular Name', 'chtheme' ),
		'menu_name'                  => __( 'Kategorie ofert', 'chtheme' ),
		'all_items'                  => __( 'Wszystkie', 'chtheme' ),
		'parent_item'                => __( 'Nadrzędna', 'chtheme' ),
		'parent_item_colon'          => __( 'Nadrzędna:', 'chtheme' ),
		'new_item_name'              => __( 'Nowa kategoria oferty', 'chtheme' ),
		'add_new_item'               => __( 'Dodaj nową kategorię oferty', 'chtheme' ),
		'edit_item'                  => __( 'Edytuj kategorię', 'chtheme' ),
		'update_item'                => __( 'Aktualizuj kategorię', 'chtheme' ),
		'separate_items_with_commas' => __( 'Oddziel kategorie przecinkami', 'chtheme' ),
		'search_items'               => __( 'Szukaj kategorii', 'chtheme' ),
		'add_or_remove_items'        => __( 'Dodaj lub usuń kategorie', 'chtheme' ),
		'choose_from_most_used'      => __( 'Najczęściej używane kategorie', 'chtheme' ),
		'not_found'                  => __( 'Nie odnaleziono', 'chtheme' ),
	);
	$rewrite = array(
		'slug'                       => 'offer-category',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'offer_category', array( 'offer_apartment', 'offer_business', 'offer_house', 'offer_lot' ), $args );

}
add_action( 'init', 'offer_category_custom_taxonomy', 0 );

// Offer tag
function offer_tag_custom_taxonomy() {
	$labels = array(
		'name'                       => _x( 'Tagi ofert', 'Taxonomy General Name', 'chtheme' ),
		'singular_name'              => _x( 'Tag oferty', 'Taxonomy Singular Name', 'chtheme' ),
		'menu_name'                  => __( 'Tagi ofert', 'chtheme' ),
		'all_items'                  => __( 'Wszystkie', 'chtheme' ),
		'parent_item'                => __( 'Nadrzędny', 'chtheme' ),
		'parent_item_colon'          => __( 'Nadrzędny:', 'chtheme' ),
		'new_item_name'              => __( 'Nowy tag oferty', 'chtheme' ),
		'add_new_item'               => __( 'Dodaj nowy tag oferty', 'chtheme' ),
		'edit_item'                  => __( 'Edytuj tag', 'chtheme' ),
		'update_item'                => __( 'Aktualizuj tag', 'chtheme' ),
		'separate_items_with_commas' => __( 'Oddziel tagi przecinkami', 'chtheme' ),
		'search_items'               => __( 'Szukaj tagów', 'chtheme' ),
		'add_or_remove_items'        => __( 'Dodaj lub usuń tagi', 'chtheme' ),
		'choose_from_most_used'      => __( 'Najczęściej używane tagi', 'chtheme' ),
		'not_found'                  => __( 'Nie odnaleziono', 'chtheme' ),
	);
	$rewrite = array(
		'slug'                       => 'offer-tag',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'offer_tag', array( 'offer_apartment', 'offer_business', 'offer_house', 'offer_lot' ), $args );

}
add_action( 'init', 'offer_tag_custom_taxonomy', 0 );
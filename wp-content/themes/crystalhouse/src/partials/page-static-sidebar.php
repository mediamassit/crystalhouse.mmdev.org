				<div class="col-md-3 sidebar hidden-print">
					<nav class="buttons">
						<ul>
							<li><a href="javascript:print();"><em class="circle-button print"></em></a></li>
							<li><a class="addthis_button_compact" href="http://www.addthis.com/bookmark.php?v=300&amp;pubid=xa-538a578c3e089f8a" title="<?php _e('Udostępnij', 'chtheme'); ?>" target="_blank"><em class="circle-button share"></em></a></li>
						</ul>
					</nav>
					<!-- .buttons -->

					<?php dynamic_sidebar('static_page_sidebar'); ?>
				</div>
				<!-- .col-md-3 -->
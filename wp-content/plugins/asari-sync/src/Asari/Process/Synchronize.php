<?php

class Asari_Process_Synchronize extends Asari_Process {
	protected $_name = 'synchronize';

	public function __construct() {
		parent::__construct();

		$this->getStatus()->setLabel('Import nowych wpisów');
	}

	public function run() {
		$this->getLog()->log('Rozpoczynanie importu');
		$added = @fopen(Asari::dir('tmp').'/added.txt', 'w');

		// Load queue and start syncing
		try {
			$options = get_option('asarisync_options');

			$api = new AsariApi($options['api_userid'], $options['api_token']);
			$queue = new Asari_Queue;
			$syncer = new Asari_Syncer($api, $this->getLog());

			$listings = $queue->get();
			$max_queue_size = @$options['sync_max_queue_size'] ? $options['sync_max_queue_size'] : 50;
			//$max_queue_size = 10;
			$chunk = array_slice($listings, 0, $max_queue_size);
			$done = 0;

			$this->getStatus()->setItemsCount(sizeof($chunk))
				->setDoneItemsCount(0)
				->save();

			foreach($chunk as $listing) {
				$add = $syncer->addListing($listing['real']);

				if($add > 0 || $add === 'remove') {
					$queue->remove($listing['real']);
					++$done;
				}

				if($add > 0) {
					@fwrite($added, $add."\r\n");
				}

				$this->getStatus()->setDoneItemsCount($done)->save();
			}
		}
		catch(AsariApiException $e) {
			return $this->error(sprintf('Import nie powiódł się, wystąpił wyjątek AsariAPI: %s', $e->getMessage()));
		}
		catch(Exception $e) {
			return $this->error(sprintf('Import nie powiódł się, wystąpił wyjątek: %s', $e->getMessage()));
		}

		// End process
		$this->getLog()->log('Poprawnie wykonano import');
		$this->getLog()->remove();
		@fclose($added);

		return $this->getStatus()->finish(array('success' => true));
	}
}
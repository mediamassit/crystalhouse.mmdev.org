var __filters = {
	"locations": {
		"country": {
			"__placeholder": "Alles",
			"Polska": "Polska",
			"Francja": "Francja",
			"Hiszpania": "Hiszpania",
			"Bu\u0142garia": "Bu\u0142garia",
			"France": "France"
		},
		"province": {
			"Polska": {
				"__placeholder": "Alles",
				"Dolno\u015bl\u0105skie": "Dolno\u015bl\u0105skie",
				"kujawsko-pomorskie": "kujawsko-pomorskie",
				"Kujawsko-Pomorskie": "Kujawsko-Pomorskie",
				"Lubelskie": "Lubelskie",
				"\u0141\u00f3dzkie": "\u0141\u00f3dzkie",
				"Ma\u0142opolskie": "Ma\u0142opolskie",
				"Mazowickie": "Mazowickie",
				"mazowieckie": "mazowieckie",
				"Mazowieckie": "Mazowieckie",
				"Pomorskie": "Pomorskie",
				"Warmi\u0144sko-Mazurskie": "Warmi\u0144sko-Mazurskie",
				"Zachodniopomorskie": "Zachodniopomorskie"
			},
			"Francja": {
				"__placeholder": "Alles",
				"Mazowieckie": "Mazowieckie"
			},
			"Hiszpania": {
				"__placeholder": "Alles",
				"Mazowieckie": "Mazowieckie"
			},
			"Bu\u0142garia": {
				"__placeholder": "Alles"
			},
			"France": {
				"__placeholder": "Alles"
			},
			"": {
				"__placeholder": "Alles",
				"Mazowieckie": "Mazowieckie"
			}
		},
		"city": {
			"Mazowieckie": {
				"__placeholder": "Alles",
				"BENIDORM": "BENIDORM",
				"Bielawa": "Bielawa",
				"B\u0142onie": "B\u0142onie",
				"Cannes": "Cannes",
				"Chyliczki": "Chyliczki",
				"Horn\u00f3wek": "Horn\u00f3wek",
				"Izabelin": "Izabelin",
				"Izabelin C": "Izabelin C",
				"Jab\u0142onna": "Jab\u0142onna",
				"Jastrz\u0119bie": "Jastrz\u0119bie",
				"J\u00f3zefos\u0142aw": "J\u00f3zefos\u0142aw",
				"J\u00f3zef\u00f3w": "J\u00f3zef\u00f3w",
				"Kampinos": "Kampinos",
				"K\u0119pa Oborska": "K\u0119pa Oborska",
				"Koby\u0142ka": "Koby\u0142ka",
				"Koczargi Stare": "Koczargi Stare",
				"Komor\u00f3w": "Komor\u00f3w",
				"Konstancin-Jeziorna": "Konstancin-Jeziorna",
				"Kotorydz": "Kotorydz",
				"Lasocin": "Lasocin",
				"Lipk\u00f3w": "Lipk\u00f3w",
				"\u0141omianki": "\u0141omianki",
				"Magdalenka": "Magdalenka",
				"Marbella": "Marbella",
				"Marki": "Marki",
				"Micha\u0142owice": "Micha\u0142owice",
				"Nadarzyn": "Nadarzyn",
				"Natolin": "Natolin",
				"Nicea": "Nicea",
				"Obory": "Obory",
				"Otwock": "Otwock",
				"O\u017car\u00f3w Mazowiecki": "O\u017car\u00f3w Mazowiecki",
				"P\u0119cice Ma\u0142e": "P\u0119cice Ma\u0142e",
				"Piaseczno": "Piaseczno",
				"Podkowa Le\u015bna": "Podkowa Le\u015bna",
				"Pruszk\u00f3w": "Pruszk\u00f3w",
				"Radzymin": "Radzymin",
				"Regu\u0142y": "Regu\u0142y",
				"Rusiec": "Rusiec",
				"Saint Tropez": "Saint Tropez",
				"Serock": "Serock",
				"S\u0119kocin Nowy": "S\u0119kocin Nowy",
				"S\u0119kocin-Las": "S\u0119kocin-Las",
				"Solec": "Solec",
				"Stara Wie\u015b": "Stara Wie\u015b",
				"Stare Babice": "Stare Babice",
				"Strach\u00f3wka": "Strach\u00f3wka",
				"Strzembowo": "Strzembowo",
				"Strzyku\u0142y": "Strzyku\u0142y",
				"Walend\u00f3w": "Walend\u00f3w",
				"Warszawa": "Warszawa",
				"Wawer": "Wawer",
				"Wi\u0105zowna": "Wi\u0105zowna",
				"Z\u0105bki": "Z\u0105bki",
				"Zielonka": "Zielonka",
				"\u017b\u00f3\u0142win": "\u017b\u00f3\u0142win"
			},
			"Warmi\u0144sko-Mazurskie": {
				"__placeholder": "Alles",
				"Kruklanki": "Kruklanki",
				"Ksi\u0105\u017cnik": "Ksi\u0105\u017cnik",
				"Lisewo": "Lisewo",
				"Olsztyn": "Olsztyn",
				"Skop": "Skop"
			},
			"": {
				"__placeholder": "Alles",
				" Le Castellet": " Le Castellet",
				"BENIDORM": "BENIDORM",
				"Cannes": "Cannes",
				"Cap D'Antibes": "Cap D'Antibes",
				"Costa Blanca": "Costa Blanca",
				"Costa Del Sol": "Costa Del Sol",
				"Costa Del Sol, Marbella": "Costa Del Sol, Marbella",
				"Cote d'Azur": "Cote d'Azur",
				"Kwitajny": "Kwitajny",
				"Le Castellet": "Le Castellet",
				"Marbella": "Marbella",
				"Nicea": "Nicea",
				"Podkowa Le\u015bna": "Podkowa Le\u015bna",
				"Saint Tropez": "Saint Tropez",
				"S\u0142oneczny Brzeg": "S\u0142oneczny Brzeg",
				"S\u0142oneczny Brzeg Sunny Beach": "S\u0142oneczny Brzeg Sunny Beach",
				"Warszawa": "Warszawa",
				"Wi\u0105zowna": "Wi\u0105zowna"
			},
			"Kujawsko-Pomorskie": {
				"__placeholder": "Alles",
				"Toru\u0144": "Toru\u0144",
				"Wierzbiczany": "Wierzbiczany"
			},
			"Pomorskie": {
				"__placeholder": "Alles",
				"Gda\u0144sk": "Gda\u0144sk",
				"Hel": "Hel",
				"Jastarnia": "Jastarnia",
				"Jurata": "Jurata",
				"Wejherowo": "Wejherowo",
				"W\u0142adys\u0142awowo": "W\u0142adys\u0142awowo"
			},
			"Mazowickie": {
				"__placeholder": "Alles",
				"Komor\u00f3w": "Komor\u00f3w"
			},
			"Zachodniopomorskie": {
				"__placeholder": "Alles",
				"Ko\u0142obrzeg": "Ko\u0142obrzeg",
				"Unie\u015bcie": "Unie\u015bcie"
			},
			"kujawsko-pomorskie": {
				"__placeholder": "Alles",
				"Wierzbiczany": "Wierzbiczany"
			},
			"Ma\u0142opolskie": {
				"__placeholder": "Alles",
				"Ko\u015bcielisko": "Ko\u015bcielisko",
				"Krak\u00f3w": "Krak\u00f3w",
				"Lanckorona": "Lanckorona",
				"Raciborsko": "Raciborsko",
				"Zakopane": "Zakopane",
				"Zawada": "Zawada"
			},
			"Lubelskie": {
				"__placeholder": "Alles",
				"Do\u0142hobycz\u00f3w": "Do\u0142hobycz\u00f3w",
				"Kazimierz Dolny": "Kazimierz Dolny",
				"Lipniak": "Lipniak"
			},
			"Dolno\u015bl\u0105skie": {
				"__placeholder": "Alles",
				"Lw\u00f3wek \u015al\u0105ski": "Lw\u00f3wek \u015al\u0105ski",
				"Zgorzelec": "Zgorzelec"
			},
			"\u0141\u00f3dzkie": {
				"__placeholder": "Alles",
				"\u0141\u00f3d\u017a": "\u0141\u00f3d\u017a"
			},
			"mazowieckie": {
				"__placeholder": "Alles",
				"Warszawa": "Warszawa"
			},
			"default": {
				"__placeholder": "Alles",
				" Le Castellet": " Le Castellet",
				"BENIDORM": "BENIDORM",
				"Bielawa": "Bielawa",
				"B\u0142onie": "B\u0142onie",
				"Cannes": "Cannes",
				"Cap D'Antibes": "Cap D'Antibes",
				"Chyliczki": "Chyliczki",
				"Costa Blanca": "Costa Blanca",
				"Costa Del Sol": "Costa Del Sol",
				"Costa Del Sol, Marbella": "Costa Del Sol, Marbella",
				"Cote d'Azur": "Cote d'Azur",
				"Do\u0142hobycz\u00f3w": "Do\u0142hobycz\u00f3w",
				"Gda\u0144sk": "Gda\u0144sk",
				"Hel": "Hel",
				"Horn\u00f3wek": "Horn\u00f3wek",
				"Izabelin": "Izabelin",
				"Izabelin C": "Izabelin C",
				"Jab\u0142onna": "Jab\u0142onna",
				"Jastarnia": "Jastarnia",
				"Jastrz\u0119bie": "Jastrz\u0119bie",
				"J\u00f3zefos\u0142aw": "J\u00f3zefos\u0142aw",
				"J\u00f3zef\u00f3w": "J\u00f3zef\u00f3w",
				"Jurata": "Jurata",
				"Kampinos": "Kampinos",
				"Kazimierz Dolny": "Kazimierz Dolny",
				"K\u0119pa Oborska": "K\u0119pa Oborska",
				"Koby\u0142ka": "Koby\u0142ka",
				"Koczargi Stare": "Koczargi Stare",
				"Ko\u0142obrzeg": "Ko\u0142obrzeg",
				"Komor\u00f3w": "Komor\u00f3w",
				"Konstancin-Jeziorna": "Konstancin-Jeziorna",
				"Ko\u015bcielisko": "Ko\u015bcielisko",
				"Kotorydz": "Kotorydz",
				"Krak\u00f3w": "Krak\u00f3w",
				"Kruklanki": "Kruklanki",
				"Ksi\u0105\u017cnik": "Ksi\u0105\u017cnik",
				"Kwitajny": "Kwitajny",
				"Lanckorona": "Lanckorona",
				"Lasocin": "Lasocin",
				"Le Castellet": "Le Castellet",
				"Lipk\u00f3w": "Lipk\u00f3w",
				"Lipniak": "Lipniak",
				"Lisewo": "Lisewo",
				"Lw\u00f3wek \u015al\u0105ski": "Lw\u00f3wek \u015al\u0105ski",
				"\u0141omianki": "\u0141omianki",
				"\u0141\u00f3d\u017a": "\u0141\u00f3d\u017a",
				"Magdalenka": "Magdalenka",
				"Marbella": "Marbella",
				"Marki": "Marki",
				"Micha\u0142owice": "Micha\u0142owice",
				"Nadarzyn": "Nadarzyn",
				"Natolin": "Natolin",
				"Nicea": "Nicea",
				"Obory": "Obory",
				"Olsztyn": "Olsztyn",
				"Otwock": "Otwock",
				"O\u017car\u00f3w Mazowiecki": "O\u017car\u00f3w Mazowiecki",
				"P\u0119cice Ma\u0142e": "P\u0119cice Ma\u0142e",
				"Piaseczno": "Piaseczno",
				"Podkowa Le\u015bna": "Podkowa Le\u015bna",
				"Pruszk\u00f3w": "Pruszk\u00f3w",
				"Raciborsko": "Raciborsko",
				"Radzymin": "Radzymin",
				"Regu\u0142y": "Regu\u0142y",
				"Rusiec": "Rusiec",
				"Saint Tropez": "Saint Tropez",
				"Serock": "Serock",
				"S\u0119kocin Nowy": "S\u0119kocin Nowy",
				"S\u0119kocin-Las": "S\u0119kocin-Las",
				"Skop": "Skop",
				"S\u0142oneczny Brzeg": "S\u0142oneczny Brzeg",
				"S\u0142oneczny Brzeg Sunny Beach": "S\u0142oneczny Brzeg Sunny Beach",
				"Solec": "Solec",
				"Stara Wie\u015b": "Stara Wie\u015b",
				"Stare Babice": "Stare Babice",
				"Strach\u00f3wka": "Strach\u00f3wka",
				"Strzembowo": "Strzembowo",
				"Strzyku\u0142y": "Strzyku\u0142y",
				"Toru\u0144": "Toru\u0144",
				"Unie\u015bcie": "Unie\u015bcie",
				"Walend\u00f3w": "Walend\u00f3w",
				"Warszawa": "Warszawa",
				"Wawer": "Wawer",
				"Wejherowo": "Wejherowo",
				"Wi\u0105zowna": "Wi\u0105zowna",
				"Wierzbiczany": "Wierzbiczany",
				"W\u0142adys\u0142awowo": "W\u0142adys\u0142awowo",
				"Zakopane": "Zakopane",
				"Zawada": "Zawada",
				"Z\u0105bki": "Z\u0105bki",
				"Zgorzelec": "Zgorzelec",
				"Zielonka": "Zielonka",
				"\u017b\u00f3\u0142win": "\u017b\u00f3\u0142win"
			}
		},
		"district": {
			"Warszawa": {
				"__placeholder": "Alles",
				"Bemowo": "Bemowo",
				"Bia\u0142o\u0142\u0119ka": "Bia\u0142o\u0142\u0119ka",
				"Bielany": "Bielany",
				"Konstancin": "Konstancin",
				"Mi\u0119dzylesie": "Mi\u0119dzylesie",
				"Mokot\u00f3w": "Mokot\u00f3w",
				"Ochota": "Ochota",
				"Powi\u015ble": "Powi\u015ble",
				"Praga-P\u00f3\u0142noc": "Praga-P\u00f3\u0142noc",
				"Praga-Po\u0142udnie": "Praga-Po\u0142udnie",
				"\u015ar\u00f3dmie\u015bcie": "\u015ar\u00f3dmie\u015bcie",
				"Targ\u00f3wek": "Targ\u00f3wek",
				"Ursus": "Ursus",
				"Ursyn\u00f3w": "Ursyn\u00f3w",
				"Wawer": "Wawer",
				"Weso\u0142a": "Weso\u0142a",
				"Wilan\u00f3w": "Wilan\u00f3w",
				"W\u0142ochy": "W\u0142ochy",
				"Wola": "Wola",
				"WOLA": "WOLA",
				"\u017boliborz": "\u017boliborz"
			},
			"Konstancin-Jeziorna": {
				"__placeholder": "Alles",
				"Chylice": "Chylice",
				"Konstancin": "Konstancin",
				"Kr\u00f3lewska G\u00f3ra": "Kr\u00f3lewska G\u00f3ra"
			},
			"Olsztyn": {
				"__placeholder": "Alles"
			},
			"Le Castellet": {
				"__placeholder": "Alles"
			},
			"Bielawa": {
				"__placeholder": "Alles"
			},
			"Costa Del Sol, Marbella": {
				"__placeholder": "Alles"
			},
			"Cannes": {
				"__placeholder": "Alles"
			},
			"Skop": {
				"__placeholder": "Alles"
			},
			"Otwock": {
				"__placeholder": "Alles"
			},
			"Wierzbiczany": {
				"__placeholder": "Alles"
			},
			"Pruszk\u00f3w": {
				"__placeholder": "Alles"
			},
			"K\u0119pa Oborska": {
				"__placeholder": "Alles"
			},
			"Zielonka": {
				"__placeholder": "Alles"
			},
			"Podkowa Le\u015bna": {
				"__placeholder": "Alles"
			},
			"Komor\u00f3w": {
				"__placeholder": "Alles"
			},
			"Lipk\u00f3w": {
				"__placeholder": "Alles"
			},
			"S\u0119kocin Nowy": {
				"__placeholder": "Alles"
			},
			"Lasocin": {
				"__placeholder": "Alles"
			},
			"Nicea": {
				"__placeholder": "Alles"
			},
			"Marki": {
				"__placeholder": "Alles"
			},
			"Jurata": {
				"__placeholder": "Alles"
			},
			"S\u0142oneczny Brzeg": {
				"__placeholder": "Alles"
			},
			"Z\u0105bki": {
				"__placeholder": "Alles"
			},
			"Wawer": {
				"__placeholder": "Alles"
			},
			"Natolin": {
				"__placeholder": "Alles"
			},
			"Unie\u015bcie": {
				"__placeholder": "Alles"
			},
			"Wi\u0105zowna": {
				"__placeholder": "Alles",
				"Ko\u015bcielna": "Ko\u015bcielna"
			},
			"Kampinos": {
				"__placeholder": "Alles"
			},
			" Le Castellet": {
				"__placeholder": "Alles"
			},
			"Saint Tropez": {
				"__placeholder": "Alles"
			},
			"Nadarzyn": {
				"__placeholder": "Alles"
			},
			"Walend\u00f3w": {
				"__placeholder": "Alles"
			},
			"Cote d'Azur": {
				"__placeholder": "Alles"
			},
			"Zakopane": {
				"__placeholder": "Alles"
			},
			"Izabelin": {
				"__placeholder": "Alles"
			},
			"Kazimierz Dolny": {
				"__placeholder": "Alles"
			},
			"Magdalenka": {
				"__placeholder": "Alles"
			},
			"J\u00f3zef\u00f3w": {
				"__placeholder": "Alles",
				"Michalin": "Michalin",
				"\u015awidry Ma\u0142e": "\u015awidry Ma\u0142e"
			},
			"Lanckorona": {
				"__placeholder": "Alles"
			},
			"Radzymin": {
				"__placeholder": "Alles"
			},
			"Stare Babice": {
				"__placeholder": "Alles"
			},
			"Raciborsko": {
				"__placeholder": "Alles"
			},
			"B\u0142onie": {
				"__placeholder": "Alles"
			},
			"Micha\u0142owice": {
				"__placeholder": "Alles"
			},
			"J\u00f3zefos\u0142aw": {
				"__placeholder": "Alles"
			},
			"Chyliczki": {
				"__placeholder": "Alles"
			},
			"Cap D'Antibes": {
				"__placeholder": "Alles"
			},
			"Marbella": {
				"__placeholder": "Alles"
			},
			"Strach\u00f3wka": {
				"__placeholder": "Alles"
			},
			"Strzyku\u0142y": {
				"__placeholder": "Alles"
			},
			"Do\u0142hobycz\u00f3w": {
				"__placeholder": "Alles"
			},
			"Ko\u015bcielisko": {
				"__placeholder": "Alles"
			},
			"S\u0119kocin-Las": {
				"__placeholder": "Alles"
			},
			"Serock": {
				"__placeholder": "Alles"
			},
			"Costa Del Sol": {
				"__placeholder": "Alles"
			},
			"Krak\u00f3w": {
				"__placeholder": "Alles",
				"Przegorza\u0142y": "Przegorza\u0142y"
			},
			"Piaseczno": {
				"__placeholder": "Alles",
				"Zalesie Dolne": "Zalesie Dolne"
			},
			"Solec": {
				"__placeholder": "Alles"
			},
			"\u0141omianki": {
				"__placeholder": "Alles"
			},
			"Hel": {
				"__placeholder": "Alles"
			},
			"Stara Wie\u015b": {
				"__placeholder": "Alles"
			},
			"Lipniak": {
				"__placeholder": "Alles"
			},
			"Lw\u00f3wek \u015al\u0105ski": {
				"__placeholder": "Alles"
			},
			"Regu\u0142y": {
				"__placeholder": "Alles"
			},
			"Horn\u00f3wek": {
				"__placeholder": "Alles"
			},
			"Kruklanki": {
				"__placeholder": "Alles"
			},
			"BENIDORM": {
				"__placeholder": "Alles"
			},
			"Zawada": {
				"__placeholder": "Alles"
			},
			"Obory": {
				"__placeholder": "Alles"
			},
			"Ksi\u0105\u017cnik": {
				"__placeholder": "Alles"
			},
			"P\u0119cice Ma\u0142e": {
				"__placeholder": "Alles"
			},
			"Jastrz\u0119bie": {
				"__placeholder": "Alles"
			},
			"Izabelin C": {
				"__placeholder": "Alles"
			},
			"\u0141\u00f3d\u017a": {
				"__placeholder": "Alles"
			},
			"Koby\u0142ka": {
				"__placeholder": "Alles"
			},
			"Jab\u0142onna": {
				"__placeholder": "Alles"
			},
			"Wejherowo": {
				"__placeholder": "Alles"
			},
			"Lisewo": {
				"__placeholder": "Alles"
			},
			"Rusiec": {
				"__placeholder": "Alles"
			},
			"Gda\u0144sk": {
				"__placeholder": "Alles"
			},
			"Ko\u0142obrzeg": {
				"__placeholder": "Alles"
			},
			"Kwitajny": {
				"__placeholder": "Alles"
			},
			"Toru\u0144": {
				"__placeholder": "Alles"
			},
			"Strzembowo": {
				"__placeholder": "Alles"
			},
			"O\u017car\u00f3w Mazowiecki": {
				"__placeholder": "Alles"
			},
			"Costa Blanca": {
				"__placeholder": "Alles"
			},
			"Kotorydz": {
				"__placeholder": "Alles"
			},
			"Koczargi Stare": {
				"__placeholder": "Alles"
			},
			"\u017b\u00f3\u0142win": {
				"__placeholder": "Alles"
			},
			"S\u0142oneczny Brzeg Sunny Beach": {
				"__placeholder": "Alles"
			},
			"W\u0142adys\u0142awowo": {
				"__placeholder": "Alles",
				"Ch\u0142apowo": "Ch\u0142apowo"
			},
			"Jastarnia": {
				"__placeholder": "Alles",
				"Jurata": "Jurata"
			},
			"Zgorzelec": {
				"__placeholder": "Alles",
				"Ujazd": "Ujazd"
			}
		}
	},
	"price": {
		"default": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Von",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN"
					},
					"to": {
						"__placeholder": "Bis",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"_": "5.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Von",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD"
					},
					"to": {
						"__placeholder": "Bis",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"_": "1.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Von",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR"
					},
					"to": {
						"__placeholder": "Bis",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"_": "1.250.000+ EUR"
					}
				}
			},
			"rent_short": {
				"pln": {
					"from": {
						"__placeholder": "Von",
						"0": "0 PLN",
						"100": "100 PLN",
						"200": "200 PLN",
						"300": "300 PLN",
						"400": "400 PLN",
						"500": "500 PLN"
					},
					"to": {
						"__placeholder": "Bis",
						"100": "100 PLN",
						"200": "200 PLN",
						"300": "300 PLN",
						"400": "400 PLN",
						"500": "500 PLN",
						"_": "500+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Von",
						"0": "0 USD",
						"20": "20 USD",
						"40": "40 USD",
						"60": "60 USD",
						"80": "80 USD",
						"100": "100 USD"
					},
					"to": {
						"__placeholder": "Bis",
						"20": "20 USD",
						"40": "40 USD",
						"60": "60 USD",
						"80": "80 USD",
						"100": "100 USD",
						"_": "100+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Von",
						"0": "0 EUR",
						"25": "25 EUR",
						"50": "50 EUR",
						"75": "75 EUR",
						"100": "100 EUR",
						"125": "125 EUR"
					},
					"to": {
						"__placeholder": "Bis",
						"25": "25 EUR",
						"50": "50 EUR",
						"75": "75 EUR",
						"100": "100 EUR",
						"125": "125 EUR",
						"_": "125+ EUR"
					}
				}
			},
			"rent_long": {
				"pln": {
					"from": {
						"__placeholder": "Von",
						"0": "0 PLN",
						"3000": "3.000 PLN",
						"4000": "4.000 PLN",
						"5000": "5.000 PLN",
						"10000": "10.000 PLN"
					},
					"to": {
						"__placeholder": "Bis",
						"3000": "3.000 PLN",
						"4000": "4.000 PLN",
						"5000": "5.000 PLN",
						"10000": "10.000 PLN",
						"_": "10.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Von",
						"0": "0 USD",
						"500": "500 USD",
						"750": "750 USD",
						"1000": "1.000 USD",
						"2000": "2.000 USD"
					},
					"to": {
						"__placeholder": "Bis",
						"500": "500 USD",
						"750": "750 USD",
						"1000": "1.000 USD",
						"2000": "2.000 USD",
						"_": "2.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Von",
						"0": "0 EUR",
						"750": "750 EUR",
						"1000": "1.000 EUR",
						"1250": "1.250 EUR",
						"2500": "2.500 EUR"
					},
					"to": {
						"__placeholder": "Bis",
						"750": "750 EUR",
						"1000": "1.000 EUR",
						"1250": "1.250 EUR",
						"2500": "2.500 EUR",
						"_": "2.500+ EUR"
					}
				}
			}
		},
		"apartment": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Von",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN"
					},
					"to": {
						"__placeholder": "Bis",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"20000000": "20.000.000 PLN",
						"_": "20.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Von",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD"
					},
					"to": {
						"__placeholder": "Bis",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"8000000": "8.000.000 USD",
						"_": "8.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Von",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR"
					},
					"to": {
						"__placeholder": "Bis",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"5000000": "5.000.000 EUR",
						"_": "5.000.000+ EUR"
					}
				}
			}
		},
		"house": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Von",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"15000000": "15.000.000 PLN"
					},
					"to": {
						"__placeholder": "Bis",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"15000000": "15.000.000 PLN",
						"20000000": "20.000.000 PLN",
						"_": "20.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Von",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"6000000": "6.000.000 USD"
					},
					"to": {
						"__placeholder": "Bis",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"6000000": "6.000.000 USD",
						"8000000": "8.000.000 USD",
						"_": "8.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Von",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"3500000": "3.500.000 EUR"
					},
					"to": {
						"__placeholder": "Bis",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"3500000": "3.500.000 EUR",
						"5000000": "5.000.000 EUR",
						"_": "5.000.000+ EUR"
					}
				}
			}
		},
		"lot": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Von",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN"
					},
					"to": {
						"__placeholder": "Bis",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"_": "10.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Von",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD"
					},
					"to": {
						"__placeholder": "Bis",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"_": "1.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Von",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR"
					},
					"to": {
						"__placeholder": "Bis",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"_": "2.500.000+ EUR"
					}
				}
			}
		},
		"business": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Von",
						"0": "0 PLN",
						"500000": "500.000 PLN",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"15000000": "15.000.000 PLN"
					},
					"to": {
						"__placeholder": "Bis",
						"1000000": "1.000.000 PLN",
						"2000000": "2.000.000 PLN",
						"3000000": "3.000.000 PLN",
						"4000000": "4.000.000 PLN",
						"5000000": "5.000.000 PLN",
						"10000000": "10.000.000 PLN",
						"15000000": "15.000.000 PLN",
						"20000000": "20.000.000 PLN",
						"_": "20.000.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Von",
						"0": "0 USD",
						"100000": "100.000 USD",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"6000000": "6.000.000 USD"
					},
					"to": {
						"__placeholder": "Bis",
						"200000": "200.000 USD",
						"300000": "300.000 USD",
						"500000": "500.000 USD",
						"750000": "750.000 USD",
						"1000000": "1.000.000 USD",
						"4000000": "4.000.000 USD",
						"6000000": "6.000.000 USD",
						"8000000": "8.000.000 USD",
						"_": "8.000.000+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Von",
						"0": "0 EUR",
						"125000": "125.000 EUR",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"3500000": "3.500.000 EUR"
					},
					"to": {
						"__placeholder": "Bis",
						"250000": "250.000 EUR",
						"500000": "500.000 EUR",
						"750000": "750.000 EUR",
						"1000000": "1.000.000 EUR",
						"1250000": "1.250.000 EUR",
						"2500000": "2.500.000 EUR",
						"3500000": "3.500.000 EUR",
						"5000000": "5.000.000 EUR",
						"_": "5.000.000+ EUR"
					}
				}
			}
		}
	},
	"price_m2": {
		"default": {
			"buy": {
				"pln": {
					"from": {
						"__placeholder": "Von",
						"0": "0 PLN",
						"1000": "1.000 PLN",
						"2000": "2.000 PLN",
						"3000": "3.000 PLN",
						"4000": "4.000 PLN",
						"5000": "5.000 PLN"
					},
					"to": {
						"__placeholder": "Bis",
						"1000": "1.000 PLN",
						"2000": "2.000 PLN",
						"3000": "3.000 PLN",
						"4000": "4.000 PLN",
						"5000": "5.000 PLN",
						"_": "5.000+ PLN"
					}
				},
				"usd": {
					"from": {
						"__placeholder": "Von",
						"0": "0 USD",
						"200": "200 USD",
						"400": "400 USD",
						"600": "600 USD",
						"800": "800 USD",
						"1000": "1.000 USD"
					},
					"to": {
						"__placeholder": "Bis",
						"200": "200 USD",
						"400": "400 USD",
						"600": "600 USD",
						"800": "800 USD",
						"_": "800+ USD"
					}
				},
				"eur": {
					"from": {
						"__placeholder": "Von",
						"0": "0 EUR",
						"250": "250 EUR",
						"500": "500 EUR",
						"750": "750 EUR",
						"1000": "1.000 EUR",
						"1250": "1.250 EUR"
					},
					"to": {
						"__placeholder": "Bis",
						"250": "250 EUR",
						"500": "500 EUR",
						"750": "750 EUR",
						"1000": "1.000 EUR",
						"1250": "1.250 EUR",
						"_": "1.250+ EUR"
					}
				}
			}
		}
	},
	"area": {
		"default": {
			"default": {
				"m2": {
					"from": {
						"__placeholder": "VON",
						"0": "0",
						"50": "50m&#178;",
						"75": "75m&#178;",
						"100": "100m&#178;",
						"150": "150m&#178;",
						"200": "200m&#178;",
						"250": "250m&#178;"
					},
					"to": {
						"__placeholder": "BIS",
						"50": "50m&#178;",
						"75": "75m&#178;",
						"100": "100m&#178;",
						"150": "150m&#178;",
						"200": "200m&#178;",
						"250": "250m&#178;",
						"400": "400m&#178;",
						"_": "400m&#178;+"
					}
				}
			}
		},
		"house": {
			"buy": {
				"m2": {
					"from": {
						"__placeholder": "VON",
						"0": "0",
						"100": "100m&#178;",
						"200": "200m&#178;",
						"300": "300m&#178;",
						"400": "400m&#178;",
						"500": "500m&#178;",
						"750": "750m&#178;",
						"1000": "1.000m&#178;"
					},
					"to": {
						"__placeholder": "BIS",
						"200": "200m&#178;",
						"300": "300m&#178;",
						"400": "400m&#178;",
						"500": "500m&#178;",
						"750": "750m&#178;",
						"1000": "1.000m&#178;",
						"2000": "2.000m&#178;",
						"_": "2.000m&#178;+"
					}
				}
			}
		},
		"lot": {
			"buy": {
				"m2": {
					"from": {
						"__placeholder": "VON",
						"0": "0",
						"500": "500m&#178;",
						"1000": "1.000m&#178;",
						"2000": "2.000m&#178;",
						"3000": "3.000m&#178;",
						"4000": "4.000m&#178;",
						"5000": "5.000m&#178;",
						"10000": "10.000m&#178;",
						"50000": "50.000m&#178;"
					},
					"to": {
						"__placeholder": "BIS",
						"500": "500m&#178;",
						"1000": "1.000m&#178;",
						"2000": "2.000m&#178;",
						"3000": "3.000m&#178;",
						"4000": "4.000m&#178;",
						"5000": "5.000m&#178;",
						"10000": "10.000m&#178;",
						"50000": "50.000m&#178;",
						"100000": "100.000m&#178;",
						"_": "100.000m&#178;+"
					}
				}
			}
		}
	},
	"rooms": {
		"default": {
			"default": {
				"from": {
					"__placeholder": "Von",
					"1": "1",
					"2": "2",
					"3": "3",
					"4": "4",
					"5": "5"
				},
				"to": {
					"__placeholder": "Bis",
					"1": "1",
					"2": "2",
					"3": "3",
					"4": "4",
					"5": "5",
					"_": "5+"
				}
			}
		}
	},
	"area_lot": {
		"m2": {
			"from": {
				"__placeholder": "VON",
				"0": "0",
				"500": "500m&#178;",
				"1000": "1.000m&#178;",
				"2000": "2.000m&#178;",
				"3000": "3.000m&#178;",
				"4000": "4.000m&#178;",
				"5000": "5.000m&#178;"
			},
			"to": {
				"__placeholder": "BIS",
				"500": "500m&#178;",
				"1000": "1.000m&#178;",
				"2000": "2.000m&#178;",
				"3000": "3.000m&#178;",
				"4000": "4.000m&#178;",
				"5000": "5.000m&#178;",
				"10000": "10.000m&#178;",
				"20000": "20.000m&#178;",
				"_": "20.000m&#178;+"
			}
		}
	}
};
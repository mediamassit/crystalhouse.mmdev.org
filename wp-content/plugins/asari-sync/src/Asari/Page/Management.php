<?php

class Asari_Page_Management {
	public function render() {
		switch(@$_GET['action']) {
			case 'hard-reset';
				@unlink(dirname(dirname(Asari::dir('templates'))).'/tmp/process_synchronize.js');
				@unlink(dirname(dirname(Asari::dir('templates'))).'/tmp/process_update-queue.js');
				wp_redirect(Asari::url(array('action' => null)));
				return;
			case 'update-queue':
				$page = new Asari_Page_ManagementUpdateQueue;
				return $page->render();
			case 'synchronize':
				$page = new Asari_Page_ManagementSynchronize;
				return $page->render();
			case 'dev-test';
				$page = new Asari_Page_DevTest;
				return $page->render();
				return;
		}

		$status = new Asari_Status;
//xdebug_var_dump($status);
		$options = get_option('asarisync_options');
		$api = new AsariApi($options['api_userid'], $options['api_token']);

		require Asari::dir('templates').'/index.php';
	}

	public function __render() {
		$options = get_option('asarisync_options');
		
		$api = new AsariApi($options['api_userid'], $options['api_token']);
		$sync = new AsariSync($api);
		$wp = new AsariWp;

		/*$ids = $api->extractListingsIds($api->getAllListings());
		$queue = array_diff($ids, $wp->getPostsAsariIDs()->listing);
		xdebug_var_dump(sizeof($queue), $queue);*/

		$queue = new Asari_Queue;
		$info = new Asari_Process_InfoFile('queue-update');
		$info->setItemsCount(10)
			->setDoneItemsCount(5)
			->setStartTime(time()-120)
			->save();
		xdebug_var_dump($info);

		require Asari::dir('templates').'/process-sync.php';

		//$queue->set(array('test', 'me', 'here'))->save();
		//var_dump($queue->getSaveTime());
		//xdebug_var_dump($api->getListingsCount());

		//$listings = $api->getAllListings();
		//xdebug_var_dump(sizeof($listings), $listings[0]);

		//xdebug_var_dump($sync->addListing(3165337));
		/*echo '<pre>';
		var_dump($api->getListing(3165337));
		echo '</pre>';*/

		//echo 'Hello, there!';

		if(ASARISYNC_DEVMODE) {
			$log = AsariSync::getLog();

			if(sizeof($log) > 0) {
				echo '<ul>';

				foreach($log as $message) {
					echo '<li><small>'.date('H:i:s', $message->time).'</small> '.$message->message.'</li>';
				}

				echo '</ul>';
			}
		}
	}
}
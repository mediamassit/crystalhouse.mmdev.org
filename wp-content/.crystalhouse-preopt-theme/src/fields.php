<?php

function chtof_all_fields($full_data = false) {
	global $wpdb;

	$fields = wp_cache_get('chtof_all_fields', 'chtheme');

	if(!$fields) {
		$fields = array(
			'lite' => array(),
			'full' => array()
		);
		$acf_groups = wp_cache_get('field_groups', 'acf');

		if(!$acf_groups) {
			$acf_groups = get_posts(array(
				'numberposts' 	=> -1,
				'post_type' 	=> 'acf',
				'orderby' 		=> 'menu_order title',
				'order' 		=> 'asc',
				'suppress_filters' => false,
			));
		}

		foreach($acf_groups as $group) {
			$rows = $wpdb->get_results( $wpdb->prepare("SELECT meta_key FROM $wpdb->postmeta WHERE post_id = %d AND meta_key LIKE %s", $group->ID, 'field_%'), ARRAY_A);

			foreach($rows as $row) {
				$field = apply_filters('acf/load_field', false, $row['meta_key'], $group->ID);
				
				if(strlen($field['name']) > 0) {
					$fields['lite'][$field['name']] = $field['key'];
					$fields['full'][$field['name']] = $field;
				}
			}
		}

		wp_cache_set('chtof_all_fields', $fields, 'chtheme', (60*60));
	}

	return $fields[$full_data ? 'full' : 'lite'];
}

function chtof_all_fields_in_order() {
	$fields = chtof_all_fields(true);
	$groups = array();
	$result = array();

	foreach($fields as $field) {
		if(!is_array($groups[$field['field_group']])) {
			$groups[$field['field_group']] = array();
		}
		
		$groups[$field['field_group']][] = $field;
	}

	foreach($groups as &$group) {
		usort($group, 'chtof_all_fields_in_order_cmp');
		$result = array_merge($result, $group); // Check it
	}

	unset($fields, $groups);
	return $result;
}

function chtof_all_fields_in_order_cmp($a, $b) {
	if($a['order_no'] == $b['order_no']) {
		return 0;
	}

	return ($a['order_no'] < $b['order_no']) ? -1 : 1;
}

function chtof_meta_fields($prefix = null, $exclude = array(), $post_id = null) {
	if(is_null($post_id)) {
		global $post;
		$post_id = $post->ID;
	}

	if(!is_array($exclude)) {
		$exclude = array($exclude);
	}

	$all_fields = chtof_all_fields_in_order();
	$fields = get_fields($post_id);
	$result = array();

	foreach($all_fields as $key => $field) {
		// Set value
		if(array_key_exists($field['name'], $fields)) {
			$all_fields[$key]['__value'] = $fields[$field['name']];

			// Check for prefix
			if(is_string($prefix)) {
				if(strpos($field['name'], $prefix) === 0) {
					$result[] = $all_fields[$key];
				}
			}
			else {
				$result[] = $all_fields[$key];
			}

			// Remove excluded
			if(in_array($field['name'], $exclude)) {
				array_pop($result);
			}
		}
	}

	unset($all_fields, $fields);

	return $result;
}

/**
 * Returns the list of all offer's ACF fields.
 * @return array
 */
function chtof_all_fields__old() {
	return array(
		'offer_broker' => 'field_5388ea3b7b007',
		'offer_price_primary' => 'field_5388ebd0b8a18',
		'offer_price_m2' => 'field_538f1948a5930',
		'offer_property_area-primary' => 'field_5388eb80b8a17',
		'offer_flag' => 'field_5388eecc36b44',
		'offer_location_street' => 'field_5388ea267b006',
		'offer_location_district' => 'field_5388ea0a7b005',
		'offer_location_city' => 'field_5388e9f37b004',
		'offer_location_province' => 'field_5388e9bb7b003',
		'offer_location_country' => 'field_538f1263f5ac4',
		'offer_location_latlng' => 'field_538f218b53068',
		'offer_location_center-distance' => 'field_538f210253067',
		'offer_asari_listing_id' => 'field_538a4d5314c87',
		'offer_asari_listing_id-real' => 'field_538f0d0c90c48',
		'offer_asari_listing_created-date' => 'field_538a4d6b14c88',
		'offer_asari_listing_updated-date' => 'field_538a4d9c14c89',
		'offer_attributes' => 'field_5388edc1294f0',
		'offer_property_built-year' => 'field_538a4c47d8d63',
		'offer_property_floors-floor' => 'field_538a4c5ad8d64',
		'offer_property_floors-all' => 'field_538a4c73d8d65',
		'offer_property_area-lot' => 'field_5388f9aab8011',
		'offer_lot-type' => 'field_5388f5a5a78d4',
		'offer_investment' => 'field_5388f727a9637',
		'offer_investment_developer' => 'field_5388f80da9639',
		'offer_investment_completion-date' => 'field_5388f824a963a',
		'offer_business_type' => 'field_5388fada52fbf',
		'offer_business_premise-type' => 'field_5388fb6452fc0',
		'offer_business_premise-has-tenant' => 'field_5388fbc152fc1',
		'offer_rooms_rooms' => 'field_5388f8b2472dd',
		'offer_rooms_bathrooms' => 'field_5388f8cc472de',
		'offer_market_type' => 'field_5388f4dade353',
		'offer_market_primary-offers' => 'field_5388fe84dc906'
	);
}

/**
 * Checks whether field was defined.
 * @param string $field The field name.
 * @param int $post_id The ID of post (optional).
 * @param string $type The type of field value (string|numeric).
 * @return bool
 */
function chtof_has($field, $post_id = null, $type = 'string') {
	if(is_null($post_id)) {
		global $post;
		$post_id = $post->ID;
	}

	// Validate based on type
	switch($type) {
		case 'string':
		default:
			return !is_null(get_field($field, $post_id)) && strlen(trim(get_field($field, $post_id))) > 0;
		case 'numeric':
			return (float) get_field($field, $post_id) > 0;
	}
}

/**
 * Checks whether any of given fields was defined.
 * @param string $fields The list of fields.
 * @param int $post_id The ID of post (optional).
 * @return bool
 */
function chtof_has_any($fields, $post_id = null) {
	if(is_null($post_id)) {
		global $post;
		$post_id = $post->ID;
	}

	foreach($fields as $field) {
		if(chtof_has($field, $post_id)) {
			return true;
		}
	}

	return false;
}

/**
 * Returns the list of offer fields.
 * @param array|null $keys The list of keys you want to get (optional).
 * @param int|null $post_id The post ID (optional).
 */
function chtof_fields($keys = array(), $post_id = null) {
	// If post ID was not provided
	if(is_null($post_id)) {
		global $post;
		$post_id = $post->ID;
	}

	// If field keys were not defined
	if(sizeof($keys) == 0) {
		$keys = array_keys(get_fields($post_id));
	}

	$fields = array();

	// Return only fields that were defined
	foreach($keys as $key => $value) {
		if(chtof_has($key, $post_id)) {
			// If callback was provided to format value
			if(is_callable($value)) {
				$fields[$key] = call_user_func($value, get_field($key, $post_id));
			}
			else {
				$fields[$key] = sprintf($value, get_field($key, $post_id));
			}
		}
	}

	return $fields;
}

/**
 * Returns the list of offer attributes.
 * @param int $post_id
 * @return array|null
 */
function chtof_attributes($post_id = null) {
	// If post ID was not provided
	if(is_null($post_id)) {
		global $post;
		$post_id = $post->ID;
	}

	// Get attributes
	$attributes = get_field('offer_attributes');

	// Return list as "label" => "value"
	if(is_array($attributes) && sizeof($attributes) > 0) {
		$field = get_field_object('offer_attributes');
		$choices = $field['choices'];

		foreach($choices as $choice => $label) {
			if(!in_array($choice, $attributes)) {
				unset($choices[$choice]);
			}
		}

		return array_flip($choices);
	}
}

/**
 * Returns label of ACF field.
 * @param string $key
 * @param bool $key_is_id
 * @return string
 */
function chtof_label($key, $key_is_id = false) {
	if(!$key_is_id) {
		$fields = chtof_all_fields();
		$key = @$fields[$key];
	}

	$field = get_field_object($key);

	if(function_exists('pll__')) {
		return pll__($field['label']);
	}

	return $field['label'];
}

/**
 * Returns the real ID of ACF field.
 * @param string $key
 * @return string
 */
function chtof_id($key) {
	$fields = chtof_all_fields();
	return @$fields[$key];
}

/**
 * Returns value of ACF field.
 * @param string $value The field value.
 * @param string $key The field key.
 * @return string
 */
function chtof_value($value, $key) {
	$field = get_field_object($key);

	// If field provides "choices" return its label instead of value
	if(is_array(@$field['choices']) && array_key_exists($value, $field['choices'])) {
		return pll__($field['choices'][$value]);
	}

	return $value;
}

function chtof_rent_types() {
	$rent_types = get_terms('offer_category', array('child_of' => RENT_TERM_ID, 'hide_empty' => false, 'lang' => 'pl'));

	foreach($rent_types as $k => $type) {
		// Not very safe - so much queries
		$term  = get_term(pll_get_term($type->term_id), 'offer_category');

		$rent_types[(string) $type->term_id] = $term ? $term->name : $type->name;
		unset($rent_types[$k]);
	}

	return $rent_types;
}

/**
 * Returns the price according to currency settings or filters definition.
 * @param float $price The price in PLN.
 * @param int $decimals The number of decimals.
 * @param bool $append_currency Whether to append currency signature.
 * @param bool $currency_signature_format String for printf of currency signature.
 * @return string
 */
function chtof_price($price, $decimals = 0, $append_currency = true, $currency_signature_format = '%s') {
	$currency = chofl_currency();

	// Get currency from GET param if available
	if(strlen(@$_GET['show_currency']) > 0) {
		$currency = $_GET['show_currency'];
	}

	// Get exchange rates
	$rates = chp_get_nbp_exchange_rates();

	// Convert PLN price to defined one
	if(isset($rates->$currency)) {
		$price = $price / $rates->$currency;
	}

	// Format price
	$price = number_format((float) $price, $decimals, ',', ' ');

	// Wrap currency in <small> tags if needed
	$currency = sprintf($currency_signature_format, $currency);

	return $append_currency ?
		$price.' '.$currency : 
		$price;
}

/**
 * Returns formatted area.
 * @param float $area
 * @return string
 */
function chtof_area_formatter($area) {
	// Convert meters to square feet
	$sqft = round((float) $area * 10.7639104);

	//return sprintf('%s m<sup>2</sup> / %s sqft', $area, $sqft);
	return sprintf('%s m<sup>2</sup>', $area);
}

/**
 * Returns formatted completion date.
 * @param float $date
 * @return string
 */
function chtof_completion_date_formatter($date) {
	if(strlen($date) !== 8) {
		return __('b/d', 'chtheme');
	}

	$year = substr($date, 0, 4);
	$month = substr($date, 4, 2);
	$day = substr($date, 6, 2);

	$time = mktime(0, 0, 0, $month, $day, $year);

	return date('d.m.Y', $time);
}

/**
 * Builds select form widget.
 * @param string $name
 * @param array $options
 * @param mixed $select_value The value to select
 * @return string
 */
function chofl_select($name, $options, $select_value = null, $atts = array()) {
	if(is_null($select_value)) {
		$select_value = @$_GET[$name];
	}

	// Format attributes
	$attributes = '';

	if(sizeof($atts) > 0) {
		foreach($atts as $attr => $value) {
			$attributes .= sprintf(' %s="%s"', $attr, $value);
		}
	}

	$html = sprintf('<select name="%s"%s>', $name, $attributes);

	foreach($options as $val => $label) {
		$label = __(pll__($label), 'chtheme');
		$html .= sprintf('<option value="%s"%s>%s</option>', $val == '__placeholder' ? '' : $val, (string) $val == (string) $select_value ? ' selected="selected"' : '', $label);
	}

	$html .= '</select>';

	return $html;
}

/**
 * Builds input text widget.
 * @param string $name
 * @param array $atts
 * @return string
 */
function chofl_input_text($name, $atts = array()) {
	if(strlen(trim(@$_GET[$name])) > 0) {
		$atts['value'] = $_GET[$name];
	}

	$attributes = '';
	unset($atts['type']);

	if(sizeof($atts) > 0) {
		foreach($atts as $attr => $value) {
			$attributes .= sprintf(' %s="%s"', $attr, $value);
		}
	}

	return sprintf('<input type="text" name="%s"%s>', $name, trim($attributes));
}

/**
 * Returns active currency.
 * @param bool $allow_external_definition Whether external definitions (GET) of currency are allowed.
 * @return string
 */
function chofl_currency($allow_external_definition = false) {
	// If you can get currency from external source, get it
	if($allow_external_definition && isset($_GET['show_currency'])) {
		$currencies = chofl_currencies(false);

		foreach($currencies as $currency) {
			if($currency === $_GET['show_currency']) {
				return $currency;
			}
		}
	}

	// Change currency for search results
	if(isset($_GET['currency'])) {
		$currencies = chofl_currencies(false);

		foreach($currencies as $currency) {
			if($currency === $_GET['currency']) {
				return $currency;
			}
		}
	}

	return isset($_SESSION['currency']) ? $_SESSION['currency'] : 'PLN';
}

/**
 * Checks whether given currency is active.
 * @param string $currency
 * @return bool
 */
function chofl_currency_active($currency, $allow_external_definition = false) {
	return chofl_currency($allow_external_definition) === $currency;
}

/**
 * Returns the list of currencies.
 * @param bool $remove_active Whether to remove active currency from the list.
 * @return array
 */
function chofl_currencies($remove_active = true) {
	// @TODO Get from Redux maybe?
	$currencies = array('PLN', 'USD', 'EUR');

	// Remove active currency if needed
	if($remove_active) {
		if(($key = array_search(chofl_currency(), $currencies)) !== false) {
			unset($currencies[$key]);
		}
	}

	return $currencies;
}

/**
 * Changes given GET parameters in the URL.
 * @param array $params
 * @param string $url
 * @return string
 */
function chtof_mod_url($params, $url = null) {
	if(is_null($url)) {
		$url = chtof_curl();
	}

	$parts = parse_url($url);
	parse_str($parts['query'], $parsed_query);
	$parts['query'] = http_build_query(array_merge($parsed_query, $params));

	return http_build_url($parts);
}

/**
 * Removes given GET parameters from the URL.
 * @param array|null $params If NULL, removes all parameters.
 * @param string $url|null If NULL, modifies current URL.
 * @return string
 */
function chtof_remf_url($params = null, $url = null) {
	if(is_null($url)) {
		$url = chtof_curl();
	}

	$parts = parse_url($url);
	parse_str($parts['query'], $parsed_query);

	if(!is_array($params)) {
		$parts['query'] = '';
	}
	else {
		foreach($parsed_query as $param => $value) {
			if(in_array($param, $params)) {
				unset($parsed_query[$param]);
			}
		}

		$parts['query'] = http_build_query($parsed_query);
	}

	return http_build_url($parts);
}

/**
 * Modifies GET parameters of given URL.
 * @param array $get The map of parameters.
 * @param string|null $url If NULL, modifies current URL.
 * @return string
 */
function chtof_murl($get, $url = null) {
	if(is_null($url)) {
		$url = chtof_curl();
	}

	$parts = parse_url($url);
	parse_str($parts['query'], $parsed_query);
	$parts['query'] = http_build_query(array_merge($parsed_query, $get));

	return http_build_url($parts);
}

/**
 * Returns current URL.
 * @return string
 */
function chtof_curl() {
	return (empty($_SERVER['HTTPS']) ? "http://" : "https://") . (empty($_SERVER['HTTP_HOST']) ? '' : $_SERVER['HTTP_HOST']) . $_SERVER['REQUEST_URI'];
}

/**
 * Returns the label of post type.
 * @param string $type
 * @return string
 */
function chtof_type_label($type) {
	$types = get_post_types();

	if(!in_array($type, $types)) {
		return;
	}

	$type = get_post_type_object($types[$type]);
	return __($type->labels->name, 'chtheme');
}

function chtof_get_slot($name, $params = array()) {
	extract($params);

	include locate_template(sprintf('%s.php', $name));
}
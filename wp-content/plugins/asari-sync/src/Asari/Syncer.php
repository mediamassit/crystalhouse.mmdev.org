<?php

class Asari_Syncer {

	/**
	 * The instance of API.
	 * @var AsariApi
	 */
	protected $_api;

	/**
	 * The instance of log.
	 * @var Asari_Log
	 */
	protected $_log;


	/**
	 * __construct
	 *
	 * @param AsariApi $api The API instance.
	 */
	public function __construct(AsariApi $api, Asari_Log $log) {
		$this->_api = $api;
		$this->_log = $log;
	}

	/**
	 * Loads listing data from AsariAPI and adds it to Wordpress database.
	 *
	 * @param id $realId The real ID of listing.
	 * @throws Asari_SyncerException
	 * @return int|false The ID of post or FALSE on failure.
	 */
	public function addListing($realId) {
		// Check if listing is unique
		$wp = new AsariWp;

		if($wp->asariPostExists($realId)) {
			return true;
		}

		// Download listing data
		$this->_log->log(sprintf('Rekord (%s): pobieranie danych z API', $realId));
		$listing = $this->_api->getListing($realId);
		//$listing = json_decode(file_get_contents(dirname(WP_CONTENT_DIR).'/api-local/data/'.$realId.'.js'))->data;

		// Insert post
		$this->_log->log(sprintf('Rekord (%s): dodawanie oferty do bazy Wordpressa', $realId));

		// Basic post data
		$title = $this->getPostTitle($listing);
		$content = @$listing->description ? @$listing->description : null;
		$date_created = @$listing->dateCreated;
		$status = 'publish';

		// Post type
		switch(@$listing->sectionName) {
			case 'ApartmentSale':
			case 'ApartmentRental':
				$type = 'offer_apartment';
				break;
			case 'CommercialSpaceSale':
			case 'CommercialSpaceRental':
			case 'CommercialObjectSale':
			case 'CommercialObjectRental':
				$type = 'offer_business';
				break;
			case 'HouseSale':
			case 'HouseRental':
				$type = 'offer_house';
				break;
			case 'LotSale':
			case 'LotRental':
				$type = 'offer_lot';
				break;
			default:
				return 'remove'; // Return some INT to remove listing from sync queue
				break;
		}

		// Post category
		$category_taxonomy = 'offer_category';

		switch(@$listing->sectionName) {
			case 'Sale':
			case 'ApartmentSale':
			case 'HouseSale':
			case 'LotSale':
			case 'CommercialSpaceSale':
			case 'CommercialObjectSale':
				$category_id = 8;
				break;
			case 'Rental':
			case 'ApartmentRental':
			case 'CommercialSpaceRental':
			case 'CommercialObjectRental':
			case 'HouseRental':
			case 'LotRental':
				$category_id = 12;
				break;
			default:
				return 'remove'; // Return some INT to remove listing from sync queue
				break;
		}

		$post_id = wp_insert_post(array(
			'post_content' => $content,
			'post_title' => $title,
			'post_status' => $status,
			'post_type' => $type,
			'post_date' => $date_created
		), true);

		if(is_object($post_id)) {
			$this->_log->log(sprintf('Rekord (%s): dodawanie oferty do bazy Wordpressa nie powiodło się, usuwanie wpisu', $realId));

			wp_delete_post($post_id, true);
			throw new Asari_SyncerException(sprintf('Nie udało się dodać oferty do bazy WP: %s', $post_id->get_error_message()));
		}

		// Add post category
		wp_set_object_terms($post_id, $category_id, $category_taxonomy);

		// Add post meta
		$this->_log->log(sprintf('Rekord (%s): dodawanie meta danych', $realId));

		$custom_fields = $this->getPostCustomFields($listing);

		/*if(sizeof($custom_fields['standard'])) {
			foreach($custom_fields['standard'] as $field => $value) {
				add_post_meta($post_id, $field, $value);
			}
		}*/

		add_post_meta($post_id, 'asari_id', $listing->id);
		add_post_meta($post_id, 'asari_listingId', $listing->listingId);

		if(sizeof($custom_fields['acf'])) {
			foreach($custom_fields['acf'] as $field => $value) {
				update_field($field, $value, $post_id);
			}
		}

		// Set post language
		pll_set_post_language($post_id, 'pl');

		// Assign broker
		if(is_object(@$listing->user)) {
			$this->_log->log(sprintf('Rekord (%s): dodawanie agenta', $realId));
			$this->assignBroker(@$listing->user, $post_id);
		}
		
		// Upload post images and attach them
		try {
			$attachment_ids = array();
			$thumbnail_id = null;
			$images = $listing->images;

			if(sizeof($images) > 0) {
				$this->_log->log(sprintf('Rekord (%s), obrazy: upload %s obrazów rozpoczęty', $realId, sizeof($images)));

				foreach($images as $image) {
					$img = $this->uploadImage($post_id, 'http://asariweb.pl/image/show/'.$image->id.'.jpg', $realId);

					if(!$img) {
						$this->_log->log(sprintf('Rekord (%s), obrazy: upload pliku (%s) nie powiódł się, usuwanie wpisu i załączników', $realId, $image->id));

						wp_delete_post($post_id, true);
						$this->deleteAttachments($attachment_ids);
						throw new Asari_SyncerException('Upload obrazu nie powiódł się');
						break;
					}

					// Set first image by default
					if(is_null($thumbnail_id)) {
						$thumbnail_id = $img;
					}

					// If API returns first image ID, set it instead
					if($image->id === @$listing->firstImageId) {
						$thumbnail_id = $img;
					}

					$attachment_ids[] = $img;
				}
			}
		}
		catch(Exception $e) {
			$this->_log->log(sprintf('Rekord (%s), obrazy: upload pliku (%s) nie powiódł się, usuwanie wpisu i załączników', $realId, $image->id));

			wp_delete_post($post_id, true);
			$this->deleteAttachments($attachment_ids);
			throw new Asari_SyncerException($e->getMessage());
		}

		// Add images to gallery
		foreach($attachment_ids as $attch) {
			add_post_meta($post_id, 'offer_gallery', $attch);
		}

		// Set featured image
		if(is_int(@$thumbnail_id)) {
			add_post_meta($post_id, '_thumbnail_id', $thumbnail_id);
		}

		$this->_log->log(sprintf('Rekord (%s), obrazy: poprawnie dodane', $realId));

		// Add post translation
		$this->addPostTranslation($post_id, 'en');

		return $post_id;
	}

	/**
	 * Adds translation to the post.
	 * @param int $post_id
	 * @param string $lang
	 * @return int|bool
	 */
	public function addPostTranslation($post_id, $lang = 'en') {
		global $wpdb;

		$post = get_post($post_id);

		if(isset($post) && $post != null) {
			$args = array(
				'comment_status' => $post->comment_status,
				'ping_status'    => $post->ping_status,
				'post_author'    => $post->post_author,
				'post_content'   => '', //$post->post_content,
				'post_excerpt'   => '', //$post->post_excerpt,
				'post_name'      => $post->post_name,
				'post_parent'    => $post->post_parent,
				'post_password'  => $post->post_password,
				'post_status'    => $post->post_status,
				'post_title'     => $post->post_title,
				'post_type'      => $post->post_type,
				'to_ping'        => $post->to_ping,
				'menu_order'     => $post->menu_order
			);

			$new_post_id = wp_insert_post($args);

			// Duplicate post taxonomies
			$taxonomies = get_object_taxonomies($post->post_type);
			foreach($taxonomies as $taxonomy) {
				$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'ids'));

				foreach($post_terms as $k => $term) {
					if(pll_get_term($term, $lang)) {
						$post_terms[$k] = pll_get_term($term, $lang);
					}
				}

				wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
			}
	 
			// Duplicate post meta
			$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
			if(count($post_meta_infos) != 0) {
				$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
				
				foreach ($post_meta_infos as $meta_info) {
					$meta_key = $meta_info->meta_key;
					$meta_value = addslashes($meta_info->meta_value);
					$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
				}

				$sql_query.= implode(" UNION ALL ", $sql_query_sel);
				$wpdb->query($sql_query);
			}

			// Set post language
			pll_set_post_language($new_post_id, $lang);
			pll_save_post_translations(array(
				'pl' => $post_id,
				$lang => $new_post_id
			));

			return $new_post_id;
		}

		return false;
	}

	/**
	 * Uploads listing image
	 *
	 * @param int $post_id The listing post ID.
	 * @param string $url The image URL.
	 * @throws Asari_SyncerException
	 * @return int|false The ID of attachment or FALSE on failure.
	 */
	public function uploadImage($post_id, $url, $realId) {
		$post = get_post($post_id);

		// If post was not found
		if(!isset($post)) {
			throw new Asari_SyncerException(sprintf('Rekord (%s), obrazy: nie można dodać obrazu, wpis (%s) nie istnieje', $realId, $post_id));
			return false;
		}

		// Request image
		if(!class_exists('WP_Http')) {
			include_once(ABSPATH.WPINC.'/class-http.php');
		}

		$this->_log->log(sprintf('Rekord (%s), obrazy: pobieranie obrazu dla wpisu (%s)', $realId, $post_id));

		$photo = new WP_Http;
		$photo = $photo->request($url);

		if($photo['response']['code'] != 200 && $photo['response']['code'] != 404) {
			throw new Asari_SyncerException(sprintf('Rekord (%s), obrazy: nie można dodać obrazu, niepoprawna odpowiedź (%s)', $realId, $photo['response']['code']));
			return false;
		}

		// Upload image
		$this->_log->log(sprintf('Rekord (%s), obrazy: upload obrazu dla wpisu %s', $realId, $post_id));

		$photo['filename'] = basename($url);
		$attachment = wp_upload_bits($photo['filename'], null, $photo['body'], date("Y-m", strtotime($photo['headers']['last-modified'])));

		if($attachment['error']) {
			throw new Asari_SyncerException(sprintf('Nie udało się dodać obrazu: %s', $attachment['error']));
			return false;
		}

		$filetype = wp_check_filetype(basename($attachment['file']), null);

		$postinfo = array(
			'post_mime_type' => $filetype['type'],
			'post_title' => $photo['filename'],
			'post_content' => '',
			'post_status' => 'inherit',
		);

		// Insert attachment
		$this->_log->log(sprintf('Rekord (%s), obrazy: załączanie obrazu do wpisu %s', $realId, $post_id));

		$filename = $attachment['file'];
		$attach_id = wp_insert_attachment($postinfo, $filename, $post_id);

		if(!function_exists('wp_generate_attachment_data')) {
			require_once(ABSPATH.'wp-admin/includes/image.php');
		}

		$attach_data = wp_generate_attachment_metadata($attach_id, $filename);
		wp_update_attachment_metadata($attach_id, $attach_data);

		$this->_log->log(sprintf('Rekord (%s), obrazy: poprawnie dodano załącznik %s do wpisu %s', $realId, $post_id, $attach_id));

		return $attach_id;
	}

	protected function slugify($input) {
		if(!function_exists('iconv')) {
			return $input;
		}

		$string = html_entity_decode($input, ENT_COMPAT, "UTF-8");
		$oldLocale = setlocale(LC_CTYPE, 0);

		setlocale(LC_CTYPE, 'en_US.UTF-8');
		$string = iconv("UTF-8", "ASCII//TRANSLIT", $string);
		setlocale(LC_CTYPE, $oldLocale);

		return strtolower(preg_replace('/[^a-zA-Z0-9]+/', '', $string));
	}

	/**
	 * Assigns broker to post.
	 * @param object $broker
	 * @param int @post_id
	 * @return void
	 */
	public function assignBroker($broker, $post_id) {
		$first_name = $broker->firstName;
		$last_name = $broker->lastName;
		$login = strtolower($first_name{0} .'.'. $this->slugify($last_name));

		$user = get_user_by('email', $broker->email);

		// Look by login
		if(!$user) {
			$user = get_user_by('login', $login);
		}

		// If broker does not exists
		if(!$user) {
			// Create broker
			$user_id = wp_create_user($login, $login.'123', $broker->email);
			wp_update_user(array(
				'ID' => $user_id,
				'first_name' => $first_name,
				'last_name' => $last_name,
				'nickname' => $first_name.' '.$last_name,
				'display_name' => $first_name.' '.$last_name
			));
			
			// Assign role
			$user = new WP_User($user_id);
			$user->remove_role('subscriber');
			$user->add_role('broker');
		}
		else {
			$user_id = $user->ID;
		}

		update_field('field_5388ea3b7b007', $user_id, $post_id); // Offer broker
	}

	/**
	 * Removes attachments permanently
	 * @param array $ids The list of attachment IDs.
	 * @return void
	 */
	public function deleteAttachments($ids) {
		foreach($ids as $id) {
			wp_delete_attachment($id, true);
		}
	}

	public function getCity($listing) {
		if(is_object($listing->location)) {
			$city = strlen(@$listing->location->locality) > 1 ? @$listing->location->locality : null;
		}
		// Foreign city
		else {
			$city = strlen(@$listing->foreignLocation) > 1 ? @$listing->foreignLocation : null;
		}

		return $city;
	}

	public function getStreet($listing) {
		if(is_object($listing->street)) {
			$street = strlen(@$listing->street->name) > 1 ? @$listing->street->name : null;
		}
		// Foreign street
		else {
			$street = strlen(@$listing->foreignStreet) > 1 ? @$listing->foreignStreet : null;
		}

		return $street;
	}

	/**
	 * Returns post title based on listing.
	 * @param object $listing
	 * @return string
	 */
	public function getPostTitle($listing) {
		$city = $this->getCity($listing);
		$street = $this->getStreet($listing);

		// Format title
		if(!is_null($city)) {
			return $city . ($street ? ', '.$street : '');
		}

		return $listing->listingId;
	}

	/**
	 * Converts API date to timestamp.
	 * @param string $date
	 * @return int
	 */
	protected function apiDateToTimestamp($date) {
		return strtotime($date);
	}

	/**
	 * Returns price converted to PLN.
	 * @param float $price
	 * @param string $currency
	 * @return float
	 */
	public function getPriceInPLN($price, $currency = 'PLN') {
		$rates = chp_get_nbp_exchange_rates();

		switch($currency) {
			case 'EUR':
				return floor(($price * $rates->EUR)*100)/100;
			case 'USD':
				return floor(($price * $rates->USD)*100)/100;
			case 'PLN':
			default: 
				return $price;
		}

		return $price;
	}

	/**
	 * Returns custom fields definitions for listing.
	 * @param object $listing
	 * @return array
	 */
	public function getPostCustomFields($listing) {
		$fields = array(
			'standard' => array(),
			'acf' => array()
		);

		$acf_map = array(
			/*'offer_broker' => 'field_5388ea3b7b007',
			'offer_price_primary' => 'field_5388ebd0b8a18',
			'offer_price_m2' => 'field_538f1948a5930',
			'offer_property_area-primary' => 'field_5388eb80b8a17',
			'offer_flag' => 'field_5388eecc36b44',
			'offer_location_street' => 'field_5388ea267b006',
			'offer_location_district' => 'field_5388ea0a7b005',
			'offer_location_city' => 'field_5388e9f37b004',
			'offer_location_province' => 'field_5388e9bb7b003',
			'offer_location_country' => 'field_538f1263f5ac4',
			'offer_location_latlng' => 'field_538f218b53068',
			'offer_location_center-distance' => 'field_538f210253067',
			'offer_asari_listing_id' => 'field_538a4d5314c87',
			'offer_asari_listing_id-real' => 'field_538f0d0c90c48',
			'offer_asari_listing_created-date' => 'field_538a4d6b14c88',
			'offer_asari_listing_updated-date' => 'field_538a4d9c14c89',
			'offer_attributes' => 'field_5388edc1294f0',
			'offer_property_built-year' => 'field_538a4c47d8d63',
			'offer_property_floors-floor' => 'field_538a4c5ad8d64',
			'offer_property_floors-all' => 'field_538a4c73d8d65',
			'offer_property_area-lot' => 'field_5388f9aab8011',
			'offer_lot-type' => 'field_5388f5a5a78d4',
			'offer_investment' => 'field_5388f727a9637',
			'offer_investment_developer' => 'field_5388f80da9639',
			'offer_investment_completion-date' => 'field_5388f824a963a',
			'offer_business_type' => 'field_5388fada52fbf',
			'offer_business_premise-type' => 'field_5388fb6452fc0',
			'offer_business_premise-has-tenant' => 'field_5388fbc152fc1',
			'offer_rooms_rooms' => 'field_5388f8b2472dd',
			'offer_rooms_bathrooms' => 'field_5388f8cc472de',
			'offer_market_type' => 'field_5388f4dade353',
			'offer_market_primary-offers' => 'field_5388fe84dc906'*/
			'offer_attributes' 					=> 'field_5388edc1294f0',
			'offer_property_built-year' 		=> 'field_538a4c47d8d63',
			'offer_property_floors-floor' 		=> 'field_538a4c5ad8d64',
			'offer_property_floors-all' 		=> 'field_538a4c73d8d65',
			'offer_property_area-lot' 			=> 'field_5388f9aab8011',
			'offer_lot-type' 					=> 'field_5388f5a5a78d4',
			'offer_investment' 					=> 'field_5388f727a9637',
			'offer_investment_developer' 		=> 'field_5388f80da9639',
			'offer_investment_completion-date' 	=> 'field_5388f824a963a',
			'offer_investment_area-from' 		=> 'field_5392db9d229e8',
			'offer_investment_area-to' 			=> 'field_5392dc3c229e9',
			'offer_broker' 						=> 'field_5388ea3b7b007',
			'offer_price_primary' 				=> 'field_5388ebd0b8a18',
			'offer_price_m2' 					=> 'field_538f1948a5930',
			'offer_property_area-primary' 		=> 'field_5388eb80b8a17',
			'offer_flag' 						=> 'field_5388eecc36b44',
			'offer_location_street' 			=> 'field_5388ea267b006',
			'offer_location_district' 			=> 'field_5388ea0a7b005',
			'offer_location_city' 				=> 'field_5388e9f37b004',
			'offer_location_province' 			=> 'field_5388e9bb7b003',
			'offer_location_country' 			=> 'field_538f1263f5ac4',
			'offer_location_latlng' 			=> 'field_538f218b53068',
			'offer_location_center-distance' 	=> 'field_538f210253067',
			'offer_asari_listing_id' 			=> 'field_538a4d5314c87',
			'offer_asari_listing_id-real' 		=> 'field_538f0d0c90c48',
			'offer_asari_listing_created-date' 	=> 'field_538a4d6b14c88',
			'offer_asari_listing_updated-date' 	=> 'field_538a4d9c14c89',
			'offer_business_type' 				=> 'field_5388fada52fbf',
			'offer_business_premise-type' 		=> 'field_5388fb6452fc0',
			'offer_business_premise-has-tenant' => 'field_5388fbc152fc1',
			'offer_rooms_rooms' 				=> 'field_5388f8b2472dd',
			'offer_rooms_bedrooms' 				=> 'field_5398e0d536255',
			'offer_rooms_bathrooms' 			=> 'field_5388f8cc472de',
			'offer_market_type' 				=> 'field_5388f4dade353',
			'offer_market_primary-offers' 		=> 'field_5388fe84dc906'
		);

		// Asari
		$fields['acf']['offer_asari_listing_id'] = @$listing->listingId;
		$fields['acf']['offer_asari_listing_id-real'] = @$listing->id;
		
		if(@$listing->dateCreated) {
			$fields['acf']['offer_asari_listing_created-date'] = $this->apiDateToTimestamp($listing->dateCreated);
		}
		
		if(@$listing->lastUpdated) {
			$fields['acf']['offer_asari_listing_updated-date'] = $this->apiDateToTimestamp($listing->lastUpdated);
		}

		// Area and price
		if(is_object(@$listing->price) && $listing->price->amount > 0) {
			$fields['acf']['offer_price_primary'] = $this->getPriceInPLN($listing->price->amount, $listing->price->currency);
		}

		if(is_object(@$listing->priceM2) && $listing->priceM2->amount > 0) {
			$fields['acf']['offer_price_m2'] = $this->getPriceInPLN($listing->priceM2->amount, $listing->priceM2->currency);
		}

		if($listing->totalArea > 0) {
			$fields['acf']['offer_property_area-primary'] = $listing->totalArea;
		}

		// Location
		if(is_object(@$listing->country)) {
			$fields['acf']['offer_location_country'] = trim($listing->country->name);
		}

		if(is_object(@$listing->location)) {
			$fields['acf']['offer_location_district'] = @$listing->location->quarter;
			$fields['acf']['offer_location_city'] = @$listing->location->locality;
			$fields['acf']['offer_location_province'] = @$listing->location->province;
		}

			if(strlen(trim(@$listing->foreignLocation)) > 1) {
				$fields['acf']['offer_location_city'] = trim(@$listing->foreignLocation);
			}

		if(is_object(@$listing->street)) {
			$fields['acf']['offer_location_street'] = trim($listing->street->name);
		}

			if(strlen(trim(@$listing->foreignStreet)) > 1) {
				$fields['acf']['offer_location_street'] = trim($listing->foreignStreet);
			}

		if(is_int(@$listing->centerDistance)) {
			$fields['acf']['offer_location_center-distance'] = $listing->centerDistance;
		}

		/*if(!is_null(@$listing->geoLat) && !is_null(@$listing->geoLng)) {
			$fields['acf']['offer_location_latlng'] = $listing->geoLat.','.$listing->geoLng;
		}*/

		// Property
		if(@$listing->yearBuilt > 0) {
			$fields['acf']['offer_property_built-year'] = $listing->yearBuilt;
		}

		if(!is_null(@$listing->floorNo)) {
			$fields['acf']['offer_property_floors-floor'] = $listing->floorNo;
		}

		if(!is_null(@$listing->noOfFloors)) {
			$fields['acf']['offer_property_floors-all'] = $listing->noOfFloors;
		}

		// Rooms
		if(!is_null(@$listing->noOfRooms)) {
			$fields['acf']['offer_rooms_rooms'] = $listing->noOfRooms;
		}

		if(!is_null(@$listing->noOfBathrooms)) {
			$fields['acf']['offer_rooms_bathrooms'] = $listing->noOfBathrooms;
		}

		// Type: House
		if(in_array(@$listing->sectionName, array('HouseSale', 'HouseRental'))) {
			if(@$listing->lotArea > 0 && $listing->lotAreaUnit == 'Sqm') {
				$fields['acf']['offer_property_area-lot'] = $listing->lotArea;
			}
		}

		// Type: Lot
		if(in_array(@$listing->sectionName, array('LotSale', 'LotRental'))) {
			switch(@$listing->lotType) {
				case 'ResidentalBuilding':
					$fields['acf']['offer_lot-type'] = 'construction';
					break;
				case 'Investment':
					$fields['acf']['offer_lot-type'] = 'investment';
					break;
				case 'Agricultural':
					$fields['acf']['offer_lot-type'] = 'agricultural';
					break;
				case 'Commercial':
					$fields['acf']['offer_lot-type'] = 'industrial';
					break;
			}
		}

		// Type: Business
		if(in_array(trim(@$listing->sectionName), array('CommercialSpaceSale', 'CommercialSpaceRental'))) {
			switch(trim(@$listing->commercialSpaceType)) {
				case 'Office':
					$fields['acf']['offer_business_type'] = 'office';
					break;
				case 'Recreation':
				case 'Services':
					$fields['acf']['offer_business_type'] = 'premise';
					$fields['acf']['offer_business_premise-type'] = 'commerce';
					break;
				case 'Catering':
					$fields['acf']['offer_business_type'] = 'premise';
					$fields['acf']['offer_business_premise-type'] = 'catering';
					break;
				case 'WareHouse':
					$fields['acf']['offer_business_type'] = 'premise';
					$fields['acf']['offer_business_premise-type'] = 'industrial';
					break;
				case 'Other':
				default:
					$fields['acf']['offer_business_type'] = 'premise';
					$fields['acf']['offer_business_premise-type'] = 'other';
					break;
			}

			if(in_array(trim(@$listing->sectionName), array('CommercialObjectSale', 'CommercialObjectRental'))) {
				$fields['acf']['offer_business_type'] = 'commercial_object';
			}

			/*if(@$listing->sublease == true) {
				$fields['acf']['offer_business_premise-has-tenant'] = 1;
			}*/
		}

		// Market type
		if(strlen(trim(@$listing->mortgageMarket)) > 0) {
			switch(trim(@$listing->mortgageMarket)) {
				case 'Secondary':
					$fields['acf']['offer_market_type'] = 'secondary';
					break;
				case 'Primary':
					$fields['acf']['offer_market_type'] = 'primary';
					break;
			}
		}

		// Attributes
		$attributes = array();

		if(@$listing->parking == true) {
			$attributes[] = 'parking';
		}

		if(@$listing->reception == true) {
			$attributes[] = 'reception';
		}

		if(@$listing->noOfBalconies > 0) {
			$attributes[] = 'balcony';
		}

		if(@$listing->airConditioning == true) {
			$attributes[] = 'aircondition';
		}

		if(@$listing->internet == true) {
			$attributes[] = 'internet';
		}

		if(@$listing->utilityRoom == true) {
			$attributes[] = 'utility-room';
		}

		if(@$listing->elevator == true) {
			$attributes[] = 'elevator';
		}

		if(@$listing->security == true) {
			$attributes[] = 'security';
		}

		if(sizeof($attributes) > 0) {
			$fields['acf']['offer_attributes'] = $attributes;
		}

		foreach($fields['acf'] as $key => $value) {
			if(array_key_exists($key, $acf_map)) {
				unset($fields['acf'][$key]);
				$fields['acf'][$acf_map[$key]] = $value;
			}
		}

		return $fields;
	}
}

class Asari_SyncerException extends Exception {
}
			<div class="widget navigation-tabs">
				<ul>
					<?php if(!$no_buy) : ?>
						<li><a href="<?php echo chtof_murl(array('offer' => 'buy')); ?>"<?php if($config->isCategory('buy')) : ?> class="active"<?php endif; ?>><?php _e('Sprzedaż', 'chtheme'); ?></a></li>
					<?php endif; ?>

					<?php if(!$no_rent) : ?>
						<li><a href="<?php echo chtof_murl(array('offer' => 'rent')); ?>"<?php if($config->isCategory('rent')) : ?> class="active"<?php endif; ?>><?php _e('Wynajem', 'chtheme'); ?></a></li>
					<?php endif; ?>

					<li><a href="#" data-toggle="modal" data-target="#modal-contact-subject" data-form-subject="<?php _e('Zgłaszam', 'chtheme'); ?>: "><?php _e('Zgłaszam', 'chtheme'); ?></a></li>
					<li><a href="#" data-toggle="modal" data-target="#modal-contact-subject" data-form-subject="<?php _e('Poszukuję', 'chtheme'); ?>: "><?php _e('Poszukuję', 'chtheme'); ?></a></li>
				</ul>
			</div>
			<!-- .widget.tab-buttons -->
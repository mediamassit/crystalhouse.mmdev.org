<?php
/**
 * Template Name: Page - Contact
 */
	get_header();

	$google_map = (object) array(
		'address' => trim((string) rwmb_meta('gmap_address')),
		'latlng' => explode(',', trim((string) rwmb_meta('gmap_latlng'))),
		'zoom' => (int) rwmb_meta('gmap_zoom'),
		'marker_title' => trim((string) rwmb_meta('gmap_marker_title'))
	);

	if(strlen($google_map->address) <= 0) $google_map->address = null;
	if(sizeof($google_map->latlng) !== 2) $google_map->latlng = null;
	if($google_map->zoom <= 0) $google_map->zoom = null;
	if(strlen($google_map->marker_title) <= 0) $google_map->marker_title = null;
?>

	<div id="content">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="container">
			<?php get_template_part('src/partials/breadcrumbs'); ?>
		</div>
		<!-- .container -->

		<section class="page contact">
			<div class="google-map"></div>

			<div class="container">
				<div class="row">
					<div class="col-md-6 left">
						<article>
							<?php echo apply_filters('the_content', rwmb_meta('contact_form')); ?>
						</article>
					</div>
					<!-- .col-md-6 -->

					<div class="col-md-6 right">
						<article>
							<?php the_content(); ?>
						</article>

						<div class="quick-contact">
							<p class="big"><?php _e('Telefon', 'chtheme'); ?><br><strong><?php echo get_opt('opt-contact-phone'); ?></strong></p>
							<p class="big"><?php _e('Email', 'chtheme'); ?><br><strong><a href="mailto:<?php echo get_opt('opt-contact-email'); ?>"><?php echo get_opt('opt-contact-email'); ?></a></strong></p>

							<?php get_template_part('src/partials/module-socials'); ?>
						</div>
						<!-- .quick-contact -->
					</div>
					<!-- .col-md-6 -->
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</section>
		<!-- .page.contact -->
		<?php endwhile; endif; ?>
	</div>
	<!-- #content -->

	<script type="text/javascript">
	(function($) {
		$(window).load(function() {
			$('.google-map').gmap3({
				map: {
					<?php if(!$google_map->latlng) : ?>address: "<?php echo $google_map->address; ?>",<?php endif; ?>
					options: {
						zoom: <?php echo $google_map->zoom ? $google_map->zoom : 17; ?>,
						<?php if($google_map->latlng) : ?>center: new google.maps.LatLng(<?php echo implode(',', $google_map->latlng); ?>),<?php endif; ?>
						styles: [{"stylers": [{ "hue": "#005eff" }, { "lightness": 13 }]}],
						scrollwheel: false
					}
				},
				marker: {
					<?php if($google_map->latlng) : ?>latLng: [<?php echo implode(',', $google_map->latlng); ?>],<?php endif; ?>
					<?php if(!$google_map->latlng) : ?>address: "<?php echo $google_map->address; ?>",<?php endif; ?>
					options: {
						icon: THEME_URL+"/images/spotlight-poi-blue.png",
						title: "<?php echo $google_map->marker_title; ?>"
					}
				}
			});
		});
	})(jQuery);
	</script>

<?php get_footer('slim'); ?>
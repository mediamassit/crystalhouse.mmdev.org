<?php
/**
 * Template Name: Page - Team
 */
	get_header();
?>

	<div id="content">
		<div class="container">
			<div class="page-title">
				<h1><?php _e('Nie odnaleziono', 'chtheme'); ?></h1>
			</div>
			<!-- .page-title -->

			<div class="row">
				<div class="col-md-9 main">
					<section class="page" style="min-height: 600px;">
						<p><?php _e('Wskazana podstrona nie istnieje.', 'chtheme'); ?></p>
					</section>
					<!-- .page -->
				</div>
				<!-- .col-md-12 -->
			</div>
			<!-- .row -->
		</div>
		<!-- .container -->
	</div>
	<!-- #content -->

	<?php get_template_part('src/partials/page-contact-form'); ?>

<?php get_footer(); ?>
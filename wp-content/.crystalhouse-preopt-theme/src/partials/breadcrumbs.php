<?php
	$categories = get_the_terms($post->ID, array('category', 'offer_category'));
	$category = @$categories[0];
?>
			<div class="breadcrumbs hidden-print">
				<ul>
					<li><?php _e('jesteś tutaj:', 'chtheme'); ?></li>
					<li><a href="<?php echo pll_home_url(); ?>"><?php bloginfo('title'); ?></a></li>
					<?php if(is_post_type_archive()) : ?>
					<li class="active"><a href="<?php echo get_post_type_archive_link($post->post_type); ?>"><?php echo __(post_type_archive_title('', false), 'chtheme'); ?></a></li>
					<?php
						elseif(is_tax()) :
							$term = $wp_query->queried_object;
					?>
					<li class="active"><a href="<?php echo get_term_link($term->term_id, $term->taxonomy); ?>"><?php _e($term->name, 'chtheme'); ?></a></li>
					<?php else : ?>
						<?php if($category) : // Offers category ?>
						<li><a href="<?php echo get_term_link($category->term_id, $category->taxonomy); ?>"><?php echo $category->name; ?></a></li>
						<?php endif; ?>

						<?php if(preg_match('#^offer_#', $post->post_type) > 0) : ?><li><a href="<?php echo get_post_type_archive_link($post->post_type); ?>"><?php echo chtof_type_label($post->post_type); ?></a></li><?php endif; ?>
						<li class="active"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php endif; ?>
				</ul>
			</div>
			<!-- .breadcrumbs -->
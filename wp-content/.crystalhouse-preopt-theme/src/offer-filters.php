<?php

/**
 * Configures the list of returned offers.
 */
class Cht_Offers_List_Config {
	/**
	 * Offers list limit (per page).
	 * @param int
	 */
	protected $_limit = 10;

	/**
	 * Offers list post type.
	 * @param array
	 */
	protected $_postType = array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business');

	/**
	 * Offers list category.
	 * @param array
	 */
	protected $_category = array();
	
	/**
	 * Offers list section.
	 * @param array
	 */
	protected $_section = array();

	/**
	 * Offers list filters.
	 * @param array
	 */
	protected $_filters = array();

	/**
	 * Builds object from given request array.
	 * @param array $request
	 * @return self
	 */
	static public function init($request, $config = array('default_offer' => 'buy')) {
		$self = new self;

		// Setup filters
		$self->setFilters($request);

		// Setup post type
		switch(@$request['type']) {
			default:
				$self->setPostType(array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business'));
				break;
			case 'apartment':
				$self->setPostType('offer_apartment');
				break;
			case 'house':
				$self->setPostType('offer_house');
				break;
			case 'lot':
				$self->setPostType('offer_lot');
				break;
		}

		// Setup offer type
		$categories = cht_get_offer_categories();

		if(!isset($request['offer'])) {
			$request['offer'] = $config['default_offer'];
		}

		switch(@$request['offer']) {
			default:
				$self->setCategory(array());
				break;
			case 'buy':
				$self->setCategory($categories->buy);
				break;
			case 'rent':
				switch((int) @$_GET['rent_type']) {
					default:
						$self->setCategory($categories->long_rent);
						break;
					case $categories->long_rent:
						$self->setCategory((int) @$_GET['rent_type']);
						break;
					case $categories->short_rent:
						$self->setCategory((int) @$_GET['rent_type']);
						break;
				}
				break;
		}

		// Setup pagination
		if(get_query_var('page') > 0) {
			$self->setFilter('page', get_query_var('page'));
		}
		else {
			$self->setFilter('page', ((int) $_GET['page'] > 0) ? (int) $_GET['page'] : 1);
		}

		// Setup order
		$order = @explode('_', @$request['order'], 2);

		if(in_array($order[0], array('date', 'price'))) {
			$self->setOrder($order[0], $order[1]);
		}
		else {
			$self->setOrder('date', 'DESC');
		}

		return $self;
	}

	/**
	 * __construct
	 */
	public function __construct() {
	}

	/**
	 * Sets given filter.
	 * @param string $name
	 * @param string $value
	 * @return self
	 */
	public function setFilter($name, $value) {
		$this->_filters[$name] = $value;

		return $this;
	}

	/**
	 * Sets filters.
	 * @param array $filters
	 * @return self
	 */
	public function setFilters($filters) {
		$this->_filters = (array) $filters;

		return $this;
	}

	/**
	 * Returns given filter value.
	 * @param string $name
	 * @return string
	 */
	public function getFilter($name) {
		return @$this->_filters[$name];
	}

	/**
	 * Checks whether given filter was defined.
	 * @param string $name
	 * @return string
	 */
	public function hasFilter($name) {
		return strlen(trim($this->getFilter($name))) > 0;
	}

	/**
	 * Returns filters array.
	 * @return array
	 */
	public function getFilters() {
		return $this->_filters;
	}

	/**
	 * Sets order filter.
	 * @param string $column
	 * @param string $order
	 * @return self
	 */
	public function setOrder($column, $order = 'DESC') {
		$this->setFilter('orderby', (string) $column);
		$this->setFilter('order', strtoupper($order) === 'DESC' ? 'DESC' : 'ASC');

		return $this;
	}

	/**
	 * Sets post type.
	 * @param string $post_type
	 * @return self
	 */
	public function setPostType($post_type) {
		$this->_postType = is_array($post_type) ? $post_type : array($post_type);

		if(is_null($post_type)) {
			$this->_postType = null;
		}

		return $this;
	}

	/**
	 * Returns post type.
	 * @return string
	 */
	public function getPostType() {
		return $this->_postType;
	}

	/**
	 * Sets category.
	 * @param string $category
	 * @return self
	 */
	public function setCategory($category) {
		$this->_category = is_array($category) ? $category : array($category);

		if(is_null($category)) {
			$this->_category = null;
		}

		return $this;
	}

	/**
	 * Returns category.
	 * @return array
	 */
	public function getCategory() {
		return $this->_category;
	}

	public function getCategorySlug() {
		$categories = cht_get_offer_categories();

		if(sizeof($this->getCategory()) == 1 && reset($this->getCategory()) === $categories->buy) {
			return 'buy';
		}

		if(in_array($categories->long_rent, $this->getCategory()) || in_array($categories->short_rent, $this->getCategory()) || in_array($categories->rent, $this->getCategory())) {
			return 'rent';
		}

		if(sizeof($this->getCategory()) == 1 && reset($this->getCategory()) === $categories->investments) {
			return 'investments';
		}
	}

	public function isCategory($slug) {
		return $this->getCategorySlug() == $slug;
	}

	/**
	 * Sets section.
	 * @param string $section
	 * @return self
	 */
	public function setSection($section) {
		$this->_section = is_array($section) ? $section : array($section);

		if(is_null($section)) {
			$this->_section = null;
		}

		return $this;
	}

	/**
	 * Returns section.
	 * @return string
	 */
	public function getSection() {
		return $this->_section;
	}

	/**
	 * Sets posts limit.
	 * @param int $limit
	 * @return self
	 */
	public function setLimit($limit) {
		$this->_limit = $limit;

		return $this;
	}

	/**
	 * Returns posts limit.
	 * @return int
	 */
	public function getLimit() {
		return $this->_limit;
	}

}

/**
 * Returns the list of offers.
 */
class Cht_Offers_List {
	/**
	 * Offers list config object.
	 * object
	 */
	protected $_config;

	/**
	 * __construct
	 * @param object $config
	 */
	public function __construct(Cht_Offers_List_Config $config) {
		$this->setConfig($config);
	}

	/**
	 * Sets offers list config.
	 * @param object $config
	 * @param self
	 */
	public function setConfig(Cht_Offers_List_Config $config) {
		$this->_config = $config;

		return $this;
	}

	/**
	 * Returns offers list config.
	 * @return object
	 */
	public function getConfig() {
		return $this->_config;
	}

	/**
	 * Returns the list of offers.
	 * @return WP_Query
	 */
	public function fetchAll() {
		$config = $this->getConfig();
		$filters = $config->getFilters();
		$category = $config->getCategory();
		$section = $config->getSection();

		$args = array(
			'posts_per_page' => $config->getLimit(),
			'post_type' => $config->getPostType(),
			'meta_query' => array(
				'relation' => 'AND'
			),
			'tax_query' => array()
		);

		// Paging
		if((int) $filters['page'] > 0) {
			$args['paged'] = (int) $filters['page'];
		}

		// @TODO Query
		if(strlen(trim($filters['q'])) > 0) {
			$args['s'] = $filters['q'];
		}

		// Section
		/*if(is_array($section) && sizeof($section) > 0) {
			//$args['cat'] = (int) $filters['cat'];
			$args['tax_query'][] = array('taxonomy' => 'offer_section', 'field' => 'id', 'terms' => $section);
		}*/
		/*else {
			$args['tax_query'][] = array('taxonomy' => 'offer_section', 'field' => 'id', 'terms' => array(84, 85), 'operator' => 'NOT IN'); // Exclude investments by default
		}*/
		
		// Categories
		if(!is_null($category) && sizeof($category) > 0) {
			//$args['cat'] = (int) $filters['cat'];
			$args['tax_query'][] = array('taxonomy' => 'offer_category', 'field' => 'id', 'terms' => $category);
		}

		// Tag
		if(strlen(trim($filters['keyword'])) > 0) {
			$keywords = array_map('trim', explode(',', $filters['keyword']));

			$args['tax_query'][] = array('taxonomy' => 'offer_tag', 'field' => 'slug', 'terms' => $keywords);
		}

		// Order
		if(strlen(trim($filters['order'])) > 0) {
			if($filters['order'] == 'ASC') $args['order'] = 'ASC';
			else $args['order'] = 'DESC';
		}

		if(strlen(trim($filters['orderby'])) > 0) {
			if($filters['orderby'] == 'price') {
				$args['orderby'] = 'meta_value_num';
				$args['meta_key'] = 'offer_price_primary';
			}
			else $args['orderby'] = 'date';
		}

		// Price
		if($this->filter_has_value($filters['price_from'])) 	$args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_price_primary', '>='.$this->get_price_in_pln($filters['price_from'], $filters));
		if($this->filter_has_value($filters['price_to'])) 		$args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_price_primary', '<='.$this->get_price_in_pln($filters['price_to'], $filters));

		// Price (m2)
		if($this->filter_has_value($filters['price_m2_from']))  $args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_price_m2', '>='.$filters['price_m2_from']);
		if($this->filter_has_value($filters['price_m2_to'])) 	$args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_price_m2', '<='.$filters['price_m2_to']);

		// Flags
		if((int) $filters['is_exclusive'] > 0) {
			$args['meta_query'][] = array('key' => 'offer_flag', 'value' => 'exclusive', 'compare' => 'LIKE');
		}

		if((int) $filters['is_new'] > 0) {
			$args['meta_query'][] = array('key' => 'offer_flag', 'value' => 'new', 'compare' => 'LIKE');
		}

		if((int) $filters['is_recommended'] > 0) {
			$args['meta_query'][] = array('key' => 'offer_flag', 'value' => 'recommended', 'compare' => 'LIKE');
		}

		if((int) $filters['is_no_fee'] > 0) {
			$args['meta_query'][] = array('key' => 'offer_flag', 'value' => 'no-fee', 'compare' => 'LIKE');
		}

		// Location
		if(strlen(trim($filters['city'])) > 0) {
			$args['meta_query'][] = array('key' => 'offer_location_city', 'value' => $filters['city']);
		}

		if(strlen(trim($filters['district'])) > 0) {
			$args['meta_query'][] = array('key' => 'offer_location_district', 'value' => $filters['district']);
		}

		if(strlen(trim($filters['street'])) > 0) {
			$args['meta_query'][] = array('key' => 'offer_location_street', 'value' => $filters['street']);
		}

		if(strlen(trim($filters['province'])) > 0) {
			$args['meta_query'][] = array('key' => 'offer_location_province', 'value' => $filters['province']);
		}

		if(filter_has_value($filters['center_distance'])) {
			$args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_location_center-distance', $filters['center_distance']);
		}

		// Attributes
		if(is_array($filters['attr']) && sizeof($filters['attr']) > 0) {
			$attrs = $filters['attr'];

			if(in_array('has-tenant', $filters['attr'])) {
				$args['meta_query'][] = array('key' => 'offer_business_premise-has-tenant', 'value' => 1);
				unset($attrs[array_search('has-tenant', $attrs)]);
			}

			foreach($attrs as $attr) {
				$args['meta_query'][] = array('key' => 'offer_attributes', 'value' => $attr, 'compare' => 'LIKE');
			}
		}

		// Built year
		if($this->filter_has_value($filters['built_year_from'])) $args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_property_built-year', '>='.$filters['built_year_from']);
		if($this->filter_has_value($filters['built_year_to'])) 	 $args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_property_built-year', '<='.$filters['built_year_to']);

		// Area lot
		if($this->filter_has_value($filters['area_lot_from'])) 	$args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_property_area-lot', '>='.$filters['area_lot_from']);
		if($this->filter_has_value($filters['area_lot_to'])) 	$args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_property_area-lot', '<='.$filters['area_lot_to']);

		// Area
		if($this->filter_has_value($filters['area_from'])) {
			//$args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_property_area-primary', '>='.$filters['area_from']);
			$args['search_cht_area_from'] = $filters['area_from'];
			$args['search_cht_area_to'] = 9999999;
		}
		if($this->filter_has_value($filters['area_to'])){
			//$args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_property_area-primary', '<='.$filters['area_to']);
			$args['search_cht_area_to'] = $filters['area_to'];
		}

		// Lot type
		if(strlen(trim($filters['lot_type'])) > 0) {
			$args['meta_query'][] = array('key' => 'offer_lot-type', 'value' => $filters['lot_type']);
		}

		// Investment
		if(strlen(trim($filters['investment'])) > 0) {
			$args['meta_query'][] = array('key' => 'offer_investment_group', 'value' => $filters['investment'], 'compare' => 'LIKE');
		}

		// Developer
		if(strlen(trim($filters['developer'])) > 0) {
			$args['meta_query'][] = array('key' => 'offer_investment_developer', 'value' => $filters['developer'], 'compare' => 'LIKE');
		}

		// Business type
		if(strlen(trim($filters['business_type'])) > 0) {
			$args['meta_query'][] = array('key' => 'offer_business_type', 'value' => $filters['business_type']);
		}

		// Business premise type
		if(strlen(trim($filters['business_premise_type'])) > 0) {
			$args['meta_query'][] = array('key' => 'offer_business_premise-type', 'value' => $filters['business_premise_type']);
		}

		// Business premise has tenant
		if((int) $filters['business_has_tenant'] > 0) {
			$args['meta_query'][] = array('key' => 'offer_business_premise-has-tenant', 'value' => 1);
		}

		// Rooms
		if($this->filter_has_value($filters['rooms_from'])) 	$args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_rooms_rooms', '>='.$filters['rooms_from']);
		if($this->filter_has_value($filters['rooms_to'])) 		$args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_rooms_rooms', '<='.$filters['rooms_to']);
		if(isset($filters['rooms'])) 							$args['meta_query'][] = array('key' => 'offer_rooms_rooms', 'value' => (int) $filters['rooms']);

		if($this->filter_has_value($filters['bathrooms_from'])) $args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_rooms_bathrooms', '>='.$filters['bathrooms_from']);
		if($this->filter_has_value($filters['bathrooms_to'])) 	$args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_rooms_bathrooms', '<='.$filters['bathrooms_to']);
		if(isset($filters['bathrooms'])) 						$args['meta_query'][] = array('key' => 'offer_rooms_bathrooms', 'value' => (int) $filters['bathrooms']);

		if($this->filter_has_value($filters['bedrooms_from']))  $args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_rooms_bedrooms', '>='.$filters['bedrooms_from']);
		if($this->filter_has_value($filters['bedrooms_to'])) 	$args['meta_query'][] = $this->get_filter_numeric_meta_query('offer_rooms_bedrooms', '<='.$filters['bedrooms_to']);
		if(isset($filters['bedrooms'])) 						$args['meta_query'][] = array('key' => 'offer_rooms_bedrooms', 'value' => (int) $filters['bedrooms']);

		// Market type
		if(is_array($filters['market_type'])) {
			if(sizeof($filters['market_type']) == 1) $args['meta_query'][] = array('key' => 'offer_market_type', 'value' => $filters['market_type'][0]);
			//else $args['meta_query'][] = array('key' => 'offer_market_type', 'value' => $filters['market_type']);
		}

		// Developer
		if(strlen(trim($filters['developer'])) > 0) {
			$args['meta_query'][] = array('key' => 'offer_investment_developer', 'value' => $filters['developer']);
		}

		// Has Google Map coordinates
		if(strlen(trim($filters['has_google_map'])) > 0) {
			$args['meta_query'][] = array('key' => 'offer_location_latlng', 'value' => ',', 'compare' => 'LIKE');
		}

		$args['lang'] = pll_current_language();


		/*echo '<div style="padding: 10px 100px;"><p><button href="javascript:void(0);" onclick="jQuery(this).closest(\'div\').find(\'.dev-data\').toggle(); return false;">DEV+</button></p><div class="dev-data" style="display: none;">';
		xdebug_var_dump($args);
		echo '</div></div>';*/

		$results = new WP_Query($args);
		return $results;
	}

	/**
	 * Returns the list of offers for Google map.
	 * @return WP_Query
	 */
	public function fetchAllForMap() {
		$config = clone $this->_config;
		$config->setFilter('page', 1);
		$config->setLimit(-1);

		$post_type = array_values(array_intersect(array_keys(get_opt('opt-gmap-archive')), $config->getPostType()));
		$config->setPostType($post_type);
		$config->setFilter('has_google_map', '1');

		$offers = new self($config);
		return $offers->fetchAll();
	}

	/**
	 * Returns meta query for numeric range filter.
	 * @param string $key
	 * @param string $value
	 * @return array
	 */
	protected function get_filter_numeric_meta_query($key, $value) {
		if(substr($value, 0, 2) === '>=' || substr($value, 0, 2) === '<=') {
			$compare = substr($value, 0, 2);
			$value = substr($value, 2);
		}
		elseif($value{0} === '>' || $value{0} === '<') {
			$compare = $value{0};
			$value = substr($value, 1);
		}

		return array(
			'key' => $key,
			'value' => $value,
			'compare' => $compare,
			'type' => 'NUMERIC'
		);
	}

	/**
	 * Converts given filter value to float.
	 * @param string $value
	 * @return float
	 */
	protected function get_filter_numeric_value($value) {
		return $this->filter_has_value($value) ? (float) $value : null;
	}

	/**
	 * Checks whether filter value was defined.
	 * @param mixed $value
	 * @return bool
	 */
	protected function filter_has_value($value) {
		return $value && strlen(trim($value)) > 0 && $value !== '_';
	}

	/**
	 * Converts given price to PLN.
	 * @param float $price
	 * @param array $filters
	 * @param float
	 */
	protected function get_price_in_pln($price, $filters) {
		if(!isset($filters['currency']) || $filters['currency'] == 'PLN') {
			return $price;
		}

		$rates = chp_get_nbp_exchange_rates();

		if(isset($rates->$filters['currency'])) {
			return floor(($price * $rates->$filters['currency'])*100)/100;
		}

		return $price;
	}
}





function get_filter_numeric_meta_query($key, $value) {
	if(substr($value, 0, 2) === '>=' || substr($value, 0, 2) === '<=') {
		$compare = substr($value, 0, 2);
		$value = substr($value, 2);
	}
	elseif($value{0} === '>' || $value{0} === '<') {
		$compare = $value{0};
		$value = substr($value, 1);
	}

	return array(
		'key' => $key,
		'value' => $value,
		'compare' => $compare,
		'type' => 'NUMERIC'
	);
}

/**
 * Converts given filter value to float.
 * @param string $value
 * @return float
 */
function get_filter_numeric_value($value) {
	if(!$value || strlen(trim($value)) <= 0) {
		return null;
	}

	return (float) $value;
}

/**
 * Checks whether filter value was defined.
 * @param mixed $value
 * @return bool
 */
function filter_has_value($value) {
	return $value && strlen(trim($value)) > 0;
}

/**
 * Converts given price to PLN.
 * @param float $price
 * @param array $filters
 * @param float
 */
function get_price_in_pln($price, $filters) {
	if(!isset($filters['currency']) || $filters['currency'] == 'PLN') {
		return $price;
	}

	$rates = chp_get_nbp_exchange_rates();

	if(isset($rates->$filters['currency'])) {
		return floor(($price * $rates->$filters['currency'])*100)/100;
	}

	return $price;
}

function cht_area_posts_where($where, &$wp_query) {
	global $wpdb;

	$area_from = $wp_query->get('search_cht_area_from');
	$area_to = $wp_query->get('search_cht_area_to');
	
	if($area_to || $area_from) {
		if(!$area_from) $area_from = 0;
		if(!$area_to) $area_to = 9999999;

		$where .= " AND ((CASE WHEN mt_cht_area.meta_value > 0 THEN mt_cht_area.meta_value ELSE mt_cht_area_from.meta_value END) <= ".intval($area_to)." AND (CASE WHEN mt_cht_area_to.meta_value > 0 THEN mt_cht_area_to.meta_value ELSE mt_cht_area.meta_value END) >= ".intval($area_from).")";
	}
	
	return $where;
}
add_filter('posts_where', 'cht_area_posts_where', 10, 2);

function cht_area_posts_join($join, &$wp_query) {
	global $wpdb;
	
	$area_from = $wp_query->get('search_cht_area_from');
	$area_to = $wp_query->get('search_cht_area_to');

	if($area_to || $area_from) {
		$join .= " LEFT JOIN wp_postmeta AS mt_cht_area ON (wp_posts.ID = mt_cht_area.post_id AND mt_cht_area.meta_key = 'offer_property_area-primary') 
  LEFT JOIN wp_postmeta AS mt_cht_area_from ON (wp_posts.ID = mt_cht_area_from.post_id AND mt_cht_area_from.meta_key = 'offer_investment_area-from') 
  LEFT JOIN wp_postmeta AS mt_cht_area_to ON (wp_posts.ID = mt_cht_area_to.post_id AND mt_cht_area_to.meta_key = 'offer_investment_area-to') ";
	}
	
	return $join;
}
add_filter('posts_join', 'cht_area_posts_join', 10, 2);

function cht_get_offers($filters, $post_type = null, $per_page = 4, $category = null, $section = null) {
	if(!is_array($post_type)) {
		$post_type = array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business');
	}

	$args = array(
		'posts_per_page' => $per_page,
		'post_type' => $post_type,
		'meta_query' => array(
			'relation' => 'AND'
		),
		'tax_query' => array()
	);

	// Paging
	if((int) $filters['page'] > 0) {
		$args['paged'] = (int) $filters['page'];
	}

	// @TODO Query
	if(strlen(trim($filters['q'])) > 0) {
		$args['s'] = $filters['q'];
	}

	// Section
	if(!is_null($section)) {
		//$args['cat'] = (int) $filters['cat'];
		$args['tax_query'][] = array('taxonomy' => 'offer_section', 'field' => 'id', 'terms' => $section);
	}
	else {
		$args['tax_query'][] = array('taxonomy' => 'offer_section', 'field' => 'id', 'terms' => array(84, 85), 'operator' => 'NOT IN'); // Exclude investments by default
	}
	
	// Categories
	if(!is_null($category) && sizeof($category) > 0) {
		//$args['cat'] = (int) $filters['cat'];
		$args['tax_query'][] = array('taxonomy' => 'offer_category', 'field' => 'id', 'terms' => $category);
	}

	// Tag
	if(strlen(trim($filters['keyword'])) > 0) {
		$keywords = array_map('trim', explode(',', $filters['keyword']));

		$args['tax_query'][] = array('taxonomy' => 'offer_tag', 'field' => 'slug', 'terms' => $keywords);
	}

	// Order
	if(strlen(trim($filters['order'])) > 0) {
		if($filters['order'] == 'ASC') $args['order'] = 'ASC';
		else $args['order'] = 'DESC';
	}

	if(strlen(trim($filters['orderby'])) > 0) {
		if($filters['orderby'] == 'price') {
			$args['orderby'] = 'meta_value_num';
			$args['meta_key'] = 'offer_price_primary';
		}
		else $args['orderby'] = 'date';
	}

	// Price
	if(filter_has_value($filters['price_from'])) 	$args['meta_query'][] = get_filter_numeric_meta_query('offer_price_primary', '>='.get_price_in_pln($filters['price_from'], $filters));
	if(filter_has_value($filters['price_to'])) 		$args['meta_query'][] = get_filter_numeric_meta_query('offer_price_primary', '<='.get_price_in_pln($filters['price_to'], $filters));

	// Price (m2)
	if(filter_has_value($filters['price_m2_from'])) $args['meta_query'][] = get_filter_numeric_meta_query('offer_price_m2', '>='.$filters['price_m2_from']);
	if(filter_has_value($filters['price_m2_to'])) 	$args['meta_query'][] = get_filter_numeric_meta_query('offer_price_m2', '<='.$filters['price_m2_to']);

	// Flags
	if((int) $filters['is_exclusive'] > 0) {
		$args['meta_query'][] = array('key' => 'offer_flag', 'value' => 'exclusive', 'compare' => 'LIKE');
	}

	if((int) $filters['is_new'] > 0) {
		$args['meta_query'][] = array('key' => 'offer_flag', 'value' => 'new', 'compare' => 'LIKE');
	}

	if((int) $filters['is_recommended'] > 0) {
		$args['meta_query'][] = array('key' => 'offer_flag', 'value' => 'recommended', 'compare' => 'LIKE');
	}

	if((int) $filters['is_no_fee'] > 0) {
		$args['meta_query'][] = array('key' => 'offer_flag', 'value' => 'no-fee', 'compare' => 'LIKE');
	}

	// Location
	if(strlen(trim($filters['city'])) > 0) {
		$args['meta_query'][] = array('key' => 'offer_location_city', 'value' => $filters['city']);
	}

	if(strlen(trim($filters['district'])) > 0) {
		$args['meta_query'][] = array('key' => 'offer_location_district', 'value' => $filters['district']);
	}

	if(strlen(trim($filters['street'])) > 0) {
		$args['meta_query'][] = array('key' => 'offer_location_street', 'value' => $filters['street']);
	}

	if(strlen(trim($filters['province'])) > 0) {
		$args['meta_query'][] = array('key' => 'offer_location_province', 'value' => $filters['province']);
	}

	if(filter_has_value($filters['center_distance'])) {
		$args['meta_query'][] = get_filter_numeric_meta_query('offer_location_center-distance', $filters['center_distance']);
	}

	// Attributes
	if(is_array($filters['attr']) && sizeof($filters['attr']) > 0) {
		$attrs = $filters['attr'];

		if(in_array('has-tenant', $filters['attr'])) {
			$args['meta_query'][] = array('key' => 'offer_business_premise-has-tenant', 'value' => 1);
			unset($attrs[array_search('has-tenant', $attrs)]);
		}

		foreach($attrs as $attr) {
			$args['meta_query'][] = array('key' => 'offer_attributes', 'value' => $attr, 'compare' => 'LIKE');
		}
	}

	// Built year
	if(filter_has_value($filters['built_year_from'])) 	$args['meta_query'][] = get_filter_numeric_meta_query('offer_property_built-year', '>='.$filters['built_year_from']);
	if(filter_has_value($filters['built_year_to'])) 	$args['meta_query'][] = get_filter_numeric_meta_query('offer_property_built-year', '<='.$filters['built_year_to']);

	// Area lot
	if(filter_has_value($filters['area_lot_from'])) 	$args['meta_query'][] = get_filter_numeric_meta_query('offer_property_area-lot', '>='.$filters['area_lot_from']);
	if(filter_has_value($filters['area_lot_to'])) 		$args['meta_query'][] = get_filter_numeric_meta_query('offer_property_area-lot', '<='.$filters['area_lot_to']);

	// Area
	if(filter_has_value($filters['area_from'])) { 	$args['meta_query'][] = get_filter_numeric_meta_query('offer_property_area-primary', '>='.$filters['area_from']); $args['search_cht_area'] = 100; }
	if(filter_has_value($filters['area_to'])) 		$args['meta_query'][] = get_filter_numeric_meta_query('offer_property_area-primary', '<='.$filters['area_to']);

	// Lot type
	if(strlen(trim($filters['lot_type'])) > 0) {
		$args['meta_query'][] = array('key' => 'offer_lot-type', 'value' => $filters['lot_type']);
	}

	// Investment
	/*if(strlen(trim($filters['is_investment'])) > 0) {
		$args['meta_query'][] = array('key' => 'offer_investment', 'value' => 1);
	}*/

	if(strlen(trim($filters['developer'])) > 0) {
		$args['meta_query'][] = array('key' => 'offer_investment_developer', 'value' => $filters['developer'], 'compare' => 'LIKE');
	}

	// Business type
	if(strlen(trim($filters['business_type'])) > 0) {
		$args['meta_query'][] = array('key' => 'offer_business_type', 'value' => $filters['business_type']);
	}

	// Business premise type
	if(strlen(trim($filters['business_premise_type'])) > 0) {
		$args['meta_query'][] = array('key' => 'offer_business_premise-type', 'value' => $filters['business_premise_type']);
	}

	// Business premise has tenant
	if((int) $filters['business_has_tenant'] > 0) {
		$args['meta_query'][] = array('key' => 'offer_business_premise-has-tenant', 'value' => 1);
	}

	// Rooms
	if(filter_has_value($filters['rooms_from'])) 	$args['meta_query'][] = get_filter_numeric_meta_query('offer_rooms_rooms', '>='.$filters['rooms_from']);
	if(filter_has_value($filters['rooms_to'])) 		$args['meta_query'][] = get_filter_numeric_meta_query('offer_rooms_rooms', '<='.$filters['rooms_to']);
	if(isset($filters['rooms'])) 					$args['meta_query'][] = array('key' => 'offer_rooms_rooms', 'value' => (int) $filters['rooms']);

	if(filter_has_value($filters['bathrooms_from'])) $args['meta_query'][] = get_filter_numeric_meta_query('offer_rooms_bathrooms', '>='.$filters['bathrooms_from']);
	if(filter_has_value($filters['bathrooms_to'])) 	$args['meta_query'][] = get_filter_numeric_meta_query('offer_rooms_bathrooms', '<='.$filters['bathrooms_to']);
	if(isset($filters['bathrooms'])) 				$args['meta_query'][] = array('key' => 'offer_rooms_bathrooms', 'value' => (int) $filters['bathrooms']);

	if(filter_has_value($filters['bedrooms_from'])) $args['meta_query'][] = get_filter_numeric_meta_query('offer_rooms_bedrooms', '>='.$filters['bedrooms_from']);
	if(filter_has_value($filters['bedrooms_to'])) 	$args['meta_query'][] = get_filter_numeric_meta_query('offer_rooms_bedrooms', '<='.$filters['bedrooms_to']);
	if(isset($filters['bedrooms'])) 				$args['meta_query'][] = array('key' => 'offer_rooms_bedrooms', 'value' => (int) $filters['bedrooms']);

	// Market type
	if(is_array($filters['market_type'])) {
		if(sizeof($filters['market_type']) == 1) $args['meta_query'][] = array('key' => 'offer_market_type', 'value' => $filters['market_type'][0]);
		//else $args['meta_query'][] = array('key' => 'offer_market_type', 'value' => $filters['market_type']);
	}

	// Developer
	if(strlen(trim($filters['developer'])) > 0) {
		$args['meta_query'][] = array('key' => 'offer_investment_developer', 'value' => $filters['developer']);
	}

	$args['lang'] = pll_current_language();

	//xdebug_var_dump($args);
 	$results = new WP_Query($args);
	return $results;
}

function cht_get_offers_for_map($filters, $post_type = null, $per_page = 4, $category = null, $section = null) {
	$filters = array_merge($filters, array('page' => 1));
	$offers = cht_get_offers($filters, $post_type, -1, $category, $section);

	return $offers;
}
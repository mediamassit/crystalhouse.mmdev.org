<?php

set_time_limit(0);

class Asari_Page_DevTest {

	public function render() {
		try {
			$options = get_option('asarisync_options');

			$log = new Asari_Log('devtest');
			$api = new AsariApi($options['api_userid'], $options['api_token']);
			$queue = new Asari_Queue;
			$syncer = new Asari_Syncer($api, $log);

			$queue = new Asari_QueueDev;

			//xdebug_var_dump($syncer->getPostTitle($listing));
			//xdebug_var_dump($syncer->assignBroker(array('name' => 'Dariusz  Przewocki', 'license_number' => 1237812), $listing));
			//xdebug_var_dump($syncer->addListing(2731674));

			$files = array_slice(glob(dirname(WP_CONTENT_DIR).'/api-local/data/*.js'), 0, 50);
			$listings = array();

			//xdebug_var_dump($syncer->addPostTranslation(174, 'en'));

			foreach($files as $file) {
				$data = json_decode(file_get_contents($file))->data;
				$listing = $data;

				//xdebug_var_dump($listing);
				//xdebug_var_dump($syncer->getPostCustomFields($listing));
				$add = $syncer->addListing($listing->id);
				xdebug_var_dump($add);
				if($add > 1) break;
			}

			/*foreach($files as $file) {
				$data = json_decode(file_get_contents($file))->data;
				$listings[] = array('real' => $data->id, 'listing' => $data->listingId);
			}

			var_dump($queue->put(array_slice($listings, 0, 10))->save());*/
		}
		catch(Exception $e) {
			echo sprintf('Failed to synchronize, exception occured: %s', $e->getMessage());
			return;
		}
	}

}
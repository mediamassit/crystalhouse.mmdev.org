								<div class="form-group" data-form-fieldset="center_distance"<?php if(@$disabled) : ?> style="display: none;"<?php endif; ?>>
									<label><?php echo chtof_label('offer_location_center-distance'); ?></label>
									<?php
										echo chofl_select('center_distance', array(
											'' => __('Wszystkie', 'chtheme'),
											'<=1' => __('do', 'chtheme').' 1 km',
											'<=2' => __('do', 'chtheme').' 2 km',
											'<=5' => __('do', 'chtheme').' 5 km',
											'<=10' => __('do', 'chtheme').' 10 km',
											'>10' => __('powyżej', 'chtheme').' 10 km'
										), null, @$disabled ? array('disabled' => 'disabled') : array());
									?>
								</div>
								<!-- .form-group -->
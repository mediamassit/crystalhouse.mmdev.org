	<div class="asc-params asc-errors">
		<!--<div id="message" class="updated fade" style=""><p><strong>Queue processing successfully finished.</strong></p></div>-->

		<?php if(sizeof($processes)) : ?>
			<?php foreach($processes as $process) : ?>
				<?php if($process['success'] === false) : ?>
					<div class="error fade">
						<p>Ostatnie wywołanie procesu "<?php echo $process['label']; ?>" <?php echo date('Y/m/d H:i:s', $process['started_at']); ?> nie powiodło się: <strong><?php if($process['error'] == null) : ?>Unknown error<?php else : ?><?php echo $process['error']['message']; ?><?php endif; ?></strong></p>
						<?php if($process['name'] === 'update-queue') : ?><p><small>Spróbuj ponownie zaktualizować kolejkę.</small></p><?php endif; ?>
					</div>
				<?php elseif($process['success'] === true) : ?>
					<div class="updated fade">
						<p>Ostatnie wywołanie procesu "<?php echo $process['label']; ?>" przebiegło pomyślnie <?php echo date('Y/m/d H:i:s', $process['started_at']+$process['processing_time']); ?> <small>(przetworzono <?php echo $process['items_done_count']; ?> rekordów w ~<?php echo ceil($process['processing_time']/60); ?> min.)</small></p>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
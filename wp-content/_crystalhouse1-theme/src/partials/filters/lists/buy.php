					<div class="widget offers-list-filter">
						<h2><?php _e('Znajdź nieruchomość', 'chtheme'); ?></h2>
						<form method="get" action="<?php echo chtof_remf_url(); ?>" name="offers_filter" data-form-values='{"country": "Polska", "offer": "buy"}'>
							<?php include locate_template('src/partials/filters/slots/q.php'); ?>
							<?php include locate_template('src/partials/filters/slots/keyword.php'); ?>
							<?php include locate_template('src/partials/filters/slots/currency.php'); ?>

							<div data-group="offer_type_offer-category">
								<?php chtof_get_slot('src/partials/filters/slots/type', array('types' => array('offer_apartment', 'offer_house', 'offer_lot'))); ?>
								<?php chtof_get_slot('src/partials/filters/slots/rent_type', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/market_type'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/lot_type', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/area'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/area_lot', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/price'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/price_m2'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/province', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/city'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/district'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/developer', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/rooms'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/bathrooms'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/built_year'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/center_distance'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/attributes'); ?>

								<div class="form-group text-center">
									<?php chtof_get_slot('src/partials/filters/slots/submit'); ?>
								</div>
								<!-- .form-group -->
							</div>
							<!-- [data-group] -->
						</form>
					</div>
					<!-- .widget.offers-list-filter -->
<?php
/**
 * Template Name: Search
 */
get_header();

$config = Cht_Offers_List_Config::init($_GET, array(
	'default_offer' => null
));
//$config->setPostType(array('offer_apartment', 'offer_house', 'offer_lot'));
$categories = cht_get_offer_categories();

$list = new Cht_Offers_List($config);
$offers = $list->fetchAll();
$offers_map = $list->fetchAllForMap();

?>

	<div id="content">
		<div class="container">
			<?php get_template_part('src/partials/breadcrumbs'); ?>
			<?php include locate_template('src/partials/filters/navigation_tabs.php'); ?>
			<?php include locate_template('src/partials/filters/page_title.php'); ?>

			<div class="row">
				<?php include locate_template('src/partials/filters/list.php'); ?>

				<div class="col-md-3">
					<div class="widget offers-list-filter">
						<h2><?php _e('Znajdź nieruchomość', 'chtheme'); ?></h2>
						<form method="get" action="<?php echo chtof_remf_url(); ?>" name="offers_filter" data-form-values='{"country": "Polska"<?php if(sizeof($config->getCategory()) === 0) : ?>, "offer": "buy"<?php endif; ?>}'>
							<?php include locate_template('src/partials/filters/slots/q.php'); ?>
							<?php include locate_template('src/partials/filters/slots/keyword.php'); ?>
							<?php include locate_template('src/partials/filters/slots/offer.php'); ?>
							<?php include locate_template('src/partials/filters/slots/currency.php'); ?>
							<?php //get_template_part('src/partials/filters/slots/measure'); ?>

							<div data-group="offer_type_all">
								<?php chtof_get_slot('src/partials/filters/slots/rent_type', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/type', array('types' => array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business'))); ?>
								<?php chtof_get_slot('src/partials/filters/slots/business_type', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/premise_type', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/market_type'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/lot_type', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/area'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/area_lot', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/price'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/price_m2'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/province', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/city'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/district'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/developer', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/investment', array('disabled' => true)); ?>
								<?php chtof_get_slot('src/partials/filters/slots/rooms'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/bathrooms'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/built_year'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/center_distance'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/attributes'); ?>
								<?php chtof_get_slot('src/partials/filters/slots/attributes_business', array('disabled' => true)); ?>

								<div class="form-group text-center">
									<?php chtof_get_slot('src/partials/filters/slots/submit'); ?>
								</div>
								<!-- .form-group -->
							</div>
							<!-- [data-group] -->
						</form>
					</div>
					<!-- .widget.offers-list-filter -->
				</div>
				<!-- .col-md-3 -->
			</div>
			<!-- .row -->
		</div>
		<!-- .container -->
	</div>
	<!-- #content -->

<?php get_footer(); ?>
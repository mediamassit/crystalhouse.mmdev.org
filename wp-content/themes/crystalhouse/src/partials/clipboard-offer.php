<?php

// Setup thumbnail
$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(370, 215));
$thumb = is_array($thumb) ? '<img src="'.$thumb[0].'" width="'.$thumb[1].'" alt="">' : '<img src="'.get_bloginfo('template_url').'/tmp/thumb_370x215.jpg" alt="">';

// Setup price
if(cht_offer_is_for_long_rent()) {
	$price = '<strong>'.chtof_price(get_field('offer_price_primary')).' <small>'.__('/ msc', 'chtheme').'</small></strong>';
}
elseif(cht_offer_is_for_short_rent()) {
	$price = '<strong>'.chtof_price(get_field('offer_price_primary')).' <small>'.__('/ dzień', 'chtheme').'</small></strong>';
}
else {
	$price = '<strong>'.chtof_price(get_field('offer_price_primary')).'</strong>';
}

?>										<article class="offer">
											<a href="javascript:void(0);" data-removefromclipboard="<?php echo $post->ID; ?>" class="remove">x</a>
											<figure><a href="<?php echo get_permalink($post->ID); ?>"><?php echo $thumb; ?></a></figure>
											<ul>
												<?php if(chtof_has('offer_asari_listing_id')) : ?><li><strong><?php echo chtof_label('offer_asari_listing_id'); ?>:</strong> <?php echo get_field('offer_asari_listing_id'); ?></li><?php endif; ?>
												<?php if(chtof_has('offer_location_street')) : ?><li><strong><?php echo chtof_label('offer_location_street'); ?>:</strong> <?php echo get_field('offer_location_street'); ?></li><?php endif; ?>
												<?php if(chtof_has('offer_property_area-primary')) : ?><li><strong><?php echo chtof_label('offer_property_area-primary'); ?>:</strong> <?php echo get_field('offer_property_area-primary'); ?>m<sup>2</sup></li><?php endif; ?>
												<li><strong><?php echo chtof_label('offer_price_primary'); ?>:</strong> <strong><?php echo $price; ?></strong></li>
											</ul>
										</article>
<?php

class Asari_Process_Status extends Asari_JsonFile {
	protected $_filenamePrefix = 'process';

	/**
	 * __construct
	 * @param $name string The name of process.
	 */
	public function __construct($name) {
		$this->_name = (string) $name;
		$this->_data = array(
			'started_at' => time(),
			'started_at_string' => date_i18n('Y/m/d H:i:s', time()),
			'running' => false,
			'name' => $this->_name,
			'label' => 'Process label',
			'items_count' => 0,
			'items_done_count' => 0,
			'progress' => 0,
			'processing_time' => 0,
			'estimated_time' => null,
			'log_filename' => null,
			'success' => null,
			'error' => null
		);

		parent::__construct();
	}

	public function start() {
		$this->_data['started_at'] = time();
		$this->_data['running'] = true;
		$this->_data['items_count'] = 0;
		$this->_data['items_done_count'] = 0;
		$this->_data['progress'] = 0;
		$this->_data['processing_time'] = 0;
		$this->_data['estimated_time'] = null;
		$this->_data['success'] = null;
		$this->_data['error'] = null;

		return $this->save();
	}

	public function finish($params = array()) {
		$this->_data = array_merge($this->_data, $params);

		$this->_data['progress'] = 100;
		$this->_data['running'] = false;

		return $this->save();
	}

	public function getName() {
		return $this->_name;
	}

	public function setLabel($label) {
		$this->_data['label'] = (string) $label;

		return $this;
	}

	public function getLabel() {
		return $this->_data['label'];
	}

	public function setStartTime($timestamp) {
		$this->_data['started_at'] = (float) $timestamp;

		return $this;
	}

	public function getStartTime() {
		return $this->_data['started_at'];
	}

	public function setItemsCount($count) {
		$this->_data['items_count'] = (float) $count;

		return $this;
	}

	public function getItemsCount() {
		return $this->_data['items_count'];
	}

	public function setDoneItemsCount($count) {
		$this->_data['items_done_count'] = (float) $count;

		return $this;
	}

	public function getDoneItemsCount() {
		return $this->_data['items_done_count'];
	}

	public function setOnRun($bool) {
		$this->_data['running'] = (bool) $bool;

		return $this;
	}

	public function isRunning() {
		if(sizeof($this->_data) <= 0) {
			return false;
		}

		return $this->_data['running'];
	}

	public function getProgress() {
		return @floor($this->getDoneItemsCount()/$this->getItemsCount()*100);
	}

	public function getProcessingTime() {
		return time()-$this->getStartTime();
	}

	public function getEstimatedTime() {
		if($this->getProcessingTime() <= 0 || $this->getDoneItemsCount() <= 0) {
			return null;
		}

		$avg_time = $this->getProcessingTime()/$this->getDoneItemsCount();

		return ceil($avg_time*($this->getItemsCount()-$this->getDoneItemsCount()));
	}

	public function setLogFilename($filename) {
		$this->_data['log_filename'] = (string) $filename;

		return $this;
	}

	public function getLogFilename() {
		return $this->_data['log_filename'];
	}

	public function calculateDynamics() {
		$this->_data['progress'] = $this->getProgress();
		$this->_data['processing_time'] = $this->getProcessingTime();
		$this->_data['estimated_time'] = $this->getEstimatedTime();
		$this->_data['started_at_string'] = date_i18n('Y/m/d H:i:s', $this->getStartTime());

		return $this;
	}

	public function save() {
		$this->calculateDynamics();

		return parent::save();
	}

	protected function load() {
		return parent::load();
	}
}
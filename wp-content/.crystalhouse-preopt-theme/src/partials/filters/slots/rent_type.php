<?php 

	if(!isset($rent_types)) {
		$rent_types = chtof_rent_types();
	}

?>								<div class="form-group" data-form-fieldset="rent_type"<?php if($disabled) : ?> style="display: none;"<?php endif; ?>>
									<label><?php _e('Rodzaj wynajmu', 'chtheme'); ?></label>
									<?php
										echo chofl_select('rent_type', $rent_types, null, $disabled ? array('disabled' => 'disabled') : array());
									?>
								</div>
								<!-- .form-group -->
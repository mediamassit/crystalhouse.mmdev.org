<?php

global $chtheme_options;
require __DIR__.'/src/http_build_url.php';
require __DIR__.'/src/options.php';
require __DIR__.'/src/shortcodes.php';
require __DIR__.'/src/fields.php';
require __DIR__.'/src/meta-boxes/page-contact.php';
require __DIR__.'/src/meta-boxes/page-homepage.php';
require __DIR__.'/src/meta-boxes/page-clipboard.php';
require __DIR__.'/src/meta-boxes/page-landing.php';
require __DIR__.'/src/widgets/navigation.php';
require __DIR__.'/src/main-navigation-walker.php';
require __DIR__.'/src/main-navigation-select-walker.php';
require __DIR__.'/src/offer-filters.php';
require __DIR__.'/src/clipboard.php';
require __DIR__.'/src/polylang.php';
require __DIR__.'/src/contact-form-7.php';
require __DIR__.'/src/form_filters.php';
require __DIR__.'/src/watermark.php';




define('BUY_TERM_ID', 8);
define('RENT_TERM_ID', 20);
define('LONG_RENT_TERM_ID', 12);
define('SHORT_RENT_TERM_ID', 10);
define('INVESTMENTS_TERM_ID', 84);
define('CLIPBOARD_PAGE_ID', 608);

/**
 * Setup theme
 * @return void
 */
function chtheme_setup() {
	add_editor_style();

	add_theme_support('menus');

	// Image sizes
	add_theme_support('post-thumbnails');

	//add_image_size('thumbnail', 370, 215, true);
	//add_image_size('medium', 870, 440, true);
	add_image_size('gallery-thumb', 150, 115, true);

	//set_post_thumbnail_size(150, 75, true);

	// Remove meta tags
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links');
	remove_action('wp_head', 'feed_links_extra');
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'wp_shortlink_wp_head');

	// Enable translations
	load_theme_textdomain('chtheme', get_template_directory() . '/languages');
}
add_action('after_setup_theme', 'chtheme_setup');

/**
 * Cache theme form filters every 30 minutes.
 * @return void
 */
function chtheme_cache_filters() {
	// Cache form filters
	$content = cht_get_form_filters();
	$path = sprintf(get_template_directory() . '/js/form-filters_%s.js', pll_current_language());

	if(!file_exists($path) || time()-filemtime($path) >= (60*30)) { // 30 minutes
		file_put_contents($path, $content);
	}
}
add_action('get_header', 'chtheme_cache_filters');

/**
 * Init theme widgets
 * @return void
 */
function chtheme_widgets_init() {
	register_sidebar( array(
		'name' => __('Najczęściej wyszukiwane', 'chtheme'),
		'id' => 'footer_top_searches',
		'before_widget' => '<div class="col-md-3 col-sm-6">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
		'before_list' => '<nav>',
		'after_list' => '</nav>'
	) );

	register_sidebar( array(
		'name' => __('Panel boczny strony', 'chtheme'),
		'id' => 'static_page_sidebar',
		'before_widget' => '<nav class="navigation">',
		'after_widget' => '</nav>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
		'before_list' => '',
		'after_list' => ''
	) );

	register_nav_menus(
		array(
			'main-menu' => __('Menu główne', 'chtheme')
		)
	);
}
add_action('widgets_init', 'chtheme_widgets_init');

/**
 * Register labels for theme translation.
 * @return void
 */
function chtheme_register_labels() {
	return array(
		__('Apartamenty', 'chtheme'),
		__('Apartament', 'chtheme'),
		__('Oferty komercyjne', 'chtheme'),
		__('Komercyjna', 'chtheme'),
		__('Domy', 'chtheme'),
		__('Dom', 'chtheme'),
		__('Działki', 'chtheme'),
		__('Działka', 'chtheme'),
		__('Kategorie ofert', 'chtheme'),
		__('Kategoria oferty', 'chtheme'),
		__('Tagi ofert', 'chtheme'),
		__('Tag oferty', 'chtheme')
	);
}

/**
 * Remove Redux demos and notifications
 */
function chtheme_remove_redux_filters() {
	if(class_exists('ReduxFrameworkPlugin')) {
		remove_filter('plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
	}
	if(class_exists('ReduxFrameworkPlugin')) {
		remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );    
	}
}
add_action('init', 'chtheme_remove_redux_filters');

/**
 * Remove plugins update notifications.
 */
function chtheme_remove_update_notifications($value) {
	$value->response = array();
	return $value;
}
add_filter('site_transient_update_plugins', 'chtheme_remove_update_notifications');

/**
 * Rewrite subdomains.
 * @return bool|void
 */
function chtheme_rewrite_subdomains() {
	$redirect = true;
	$domain = cht_get_domain();
	$subdomain = null;

	// Look for subdomain in request URL
	preg_match('#^(.*?)\.'.preg_quote($domain).'#', $_SERVER['HTTP_HOST'], $matches);

	if(isset($matches[1]) && strlen($matches[1]) > 0) {
		$subdomain = $matches[1];
	}

	// If subdomain was requested
	if(!is_null($subdomain) && $subdomain !== 'www' && has_opt('opt-subdomains')) {
		$option = explode("\r\n", trim(get_opt('opt-subdomains')));
		$map = array();

		// Get subdomains map
		foreach($option as $line) {
			if(sizeof($split = explode(':', $line, 2)) == 2) {
				$map[trim($split[0])] = trim($split[1]);
			}
		}
//var_dump($map, $subdomain, get_bloginfo('home').'/'.$map[$subdomain]); die;
		// If subdomain was mapped
		if(array_key_exists($subdomain, $map)) {
			if($redirect) {
				wp_redirect(get_bloginfo('home').'/'.$map[$subdomain]);
				die;
			}
			else {
				$_SERVER['PATH_INFO'] = $map[$subdomain];
			}

			return true;
		}
		// Redirect to homepage if subdomains wasn't mapped
		else {
			wp_redirect(get_bloginfo('home'));
			die;
		}
	}

	return true;
}
add_filter('do_parse_request', 'chtheme_rewrite_subdomains');

/**
 * Returns website domain.
 * @return string
 */
function cht_get_domain() {
	return preg_replace('#^https?:\/\/(www.)?#', '', get_bloginfo('wpurl'));
}

/**
 * Checks whether currently edited post has given template.
 * @param string|array $template_name
 * @return bool
 */
function post_has_template($template_name) {
	if(!is_array($template_name)) {
		$template_name = array((string) $template_name);
	}

	// Include in back-end only
	if ( ! defined( 'WP_ADMIN' ) || ! WP_ADMIN )
		return false;

	// Always include for ajax
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
		return true;

	if(isset($_GET['post'])) $post_id = (int) $_GET['post'];
	elseif(isset($_POST['post_ID'])) $post_id = (int) $_POST['post_ID'];
	else $post_id = false;

	// Check for page template
	$template = get_post_meta($post_id, '_wp_page_template', true);

	if(!in_array($template, $template_name)) {
		return false;
	}

	return true;
}

/**
 * Set custom excerpt length.
 * @return int
 */
function custom_excerpt_length($length) {
	return 22;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);

/**
 * Sets excerpt suffix.
 * @return string
 */
function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

/**
 * Returns the post excerpt.
 * @param int $limit
 * @return string
 */
function cht_excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);

	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	}
	else {
		$excerpt = implode(" ",$excerpt);
	} 

	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}

/**
 * Returns given theme option.
 * @param string $opt
 * @return mixed|null
 */
function get_opt($opt) {
	global $chtheme_options;

	return $chtheme_options[$opt];
}

/**
 * Checks whether given theme option was declared.
 * @param string $opt
 * @return bool
 */
function has_opt($opt) {
	global $chtheme_options;

	return isset($chtheme_options[$opt]) && strlen(trim($chtheme_options[$opt])) > 0;
}

/**
 * Returns trimmed string without new lines.
 * @param string $str
 * @return str
 */
function strip_newlines($str) {
	return preg_replace("#\r\n#", '', trim((string) $str));
}

/**
 * Checks whether the offer is added to given category (takes children into account).
 * @param int|array $category
 * @param int|null $post_id
 * @return bool
 */
function cht_offer_is_in_category($category, $post_id = null) {
	if(is_null($post_id)) {
		global $post;
		$post_id = $post->ID;
	}

	// Convert category to array
	if(!is_array($category)) {
		$category = array($category);
	}

	// Check category
	if(is_object_in_term($post_id, 'offer_category', $category)) {
		return true;
	}

	// Check children of category
	foreach((array) $category as $cat) {
		$descendants = get_term_children((int) $cat, 'offer_category');

		if($descendants && is_object_in_term($post_id, 'offer_category', $descendants)) {
			return true;
		}
	}

	return false;
}

/**
 * Returns the object with offer categories.
 * @return object
 */
function cht_get_offer_categories() {
	return (object) array(
		'buy' => pll_get_term(BUY_TERM_ID),
		'rent' => pll_get_term(RENT_TERM_ID),
		'short_rent' => pll_get_term(SHORT_RENT_TERM_ID),
		'long_rent' => pll_get_term(LONG_RENT_TERM_ID),
		'investments' => pll_get_term(INVESTMENTS_TERM_ID)
	);
}

/**
 * Returns buy category ID.
 * @return int
 */
function cht_get_buy_category() {
	return cht_get_offer_categories()->buy;
}

/**
 * Returns rent category ID.
 * @return int
 */
function cht_get_rent_category() {
	return cht_get_offer_categories()->rent;
}

/**
 * Returns short rent category ID.
 * @return int
 */
function cht_get_short_rent_category() {
	return cht_get_offer_categories()->short_rent;
}

/**
 * Returns long rent category ID.
 * @return int
 */
function cht_get_long_rent_category() {
	return cht_get_offer_categories()->long_rent;
}

/**
 * Checks whether the offer is for sale.
 * @param int|null $post_id
 * @return bool
 */
function cht_offer_is_for_sale($post_id = null) {
	return cht_offer_is_in_category(array(cht_get_buy_category()), $post_id);
}

/**
 * Checks whether the offer is for rent.
 * @param int|null $post_id
 * @return bool
 */
function cht_offer_is_for_rent($post_id = null) {
	return cht_offer_is_in_category(array(cht_get_rent_category()), $post_id);
}

/**
 * Checks whether the offer is for short-term rent.
 * @param int|null $post_id
 * @return bool
 */
function cht_offer_is_for_short_rent($post_id = null) {
	return cht_offer_is_in_category(array(cht_get_short_rent_category()), $post_id);
}

/**
 * Checks whether the offer is for long-term rent.
 * @param int|null $post_id
 * @return bool
 */
function cht_offer_is_for_long_rent($post_id = null) {
	return cht_offer_is_in_category(array(cht_get_long_rent_category()), $post_id);
}

/**
 * Returns the map of offer flags.
 * @return array|null
 */
function cht_get_offer_flags() {
	$flag = get_field('offer_flag');

	if($flag && sizeof($flag) > 0) {
		$field = get_field_object('offer_flag');
		$choices = $field['choices'];

		foreach($choices as $choice => $label) {
			if(!in_array($choice, $flag)) {
				unset($choices[$choice]);
			}
		}

		return array_flip($choices);
	}
}

/**
 * Checks whether given ACF field is defined.
 * @param string $field
 * @param int|null $post_id
 * @param string $type
 */
function has_acf_field($field, $post_id = null, $type = 'string') {
	if(is_null($post_id)) {
		global $post;
		$post_id = $post->ID;
	}

	// Validate based on type
	switch($type) {
		case 'string':
		default:
			return !is_null(get_field($field, $post_id)) && strlen(trim(get_field($field, $post_id))) > 0;
		case 'numeric':
			return (float) get_field($field, $post_id) > 0;
	}
}

/**
 * Converts square meters to square feet.
 * @param float $sqm
 * @return float
 */
function cht_sqm2feet($sqm) {
	return round((float) $sqm * 10.7639104);
}

/**
 * Returns the list of offers locations.
 * @return array
 */
function get_offers_locations() {
	global $wpdb;

	$locations = array();
	$rows = $wpdb->get_results("SELECT meta_value, meta_key 
	FROM $wpdb->postmeta 
	WHERE meta_key = 'offer_location_district' OR meta_key = 'offer_location_province' OR meta_key = 'offer_location_city'");

	foreach($rows as $row) {
		if(!@in_array($row->meta_value, $locations[$row->meta_key])) {
			$locations[$row->meta_key][] = $row->meta_value;
		}
	}

	foreach($locations as $key => $value) {
		sort($locations[$key]);
	}

	return $locations;
}

function get_offers_districts() {
	$posts = get_posts(array(
		'posts_per_page' => -1,
		'post_type' => array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business'),
		'meta_key' => 'offer_location_district',
		'order' => 'ASC',
		'orderby' => 'meta_value'
	));

	$districts = array();

	if(sizeof($posts) > 0) {
		foreach($posts as $post) {
			$city = get_field('offer_location_city', $post->ID);
			$district = get_field('offer_location_district', $post->ID);

			// Prevent pushing blank values
			if(strlen($city) <= 0) continue;

			if(!is_array(@$districts[$city])) {
				$districts[$city] = array();
			}

			if(!in_array($district, $districts[$city], true)) {
				$districts[$city][] = $district;
			}
		}
	}

	return $districts;
}

/**
 * Returns the list of offers developers.
 * @return array
 */
function get_offers_developers() {
	global $wpdb;

	$data = cht_get_transient('get_offers_developers');

	if($data === false) {
		$data = array();
		$rows = $wpdb->get_results("SELECT meta_value, meta_key 
		FROM $wpdb->postmeta 
		WHERE meta_key = 'offer_investment_developer' ORDER BY meta_value");

		foreach($rows as $row) {
			$data[$row->meta_value] = $row->meta_value;
		}

		cht_set_transient('get_offers_developers', $data, 60*60*12); // 12 hours
	}

	return $data;
}

/**
 * Returns the list of offers investment groups.
 * @return array
 */
function get_offers_investment_groups() {
	global $wpdb;

	$data = cht_get_transient('get_offers_investment_groups');

	if($data === false) {
		$data = array();
		$rows = $wpdb->get_results("SELECT meta_value, meta_key 
		FROM $wpdb->postmeta 
		WHERE meta_key = 'offer_investment_group' ORDER BY meta_value");

		foreach($rows as $row) {
			$data[$row->meta_value] = $row->meta_value;
		}

		cht_set_transient('get_offers_investment_groups', $data, 60*60*12); // 12 hours
	}

	return $data;
}

/**
 * Displays database queries counter in footer if user is an admin.
 * @return void
 */
function chtheme_footer_db_queries(){
	if(current_user_can('manage_options')) {
		echo '<center class="hidden-print" style="background: #333;"><p style="margin: 0; padding: 10px; color: #fff;">'.get_num_queries().' queries in '.timer_stop(0).' seconds.</p></center>'.PHP_EOL;
	}
}
add_action('wp_footer', 'chtheme_footer_db_queries');

/**
 * Returns cleared results of paginate_links() function.
 * @param array $args List of arguments for paginate_links().
 * @return array
 */
function cht_get_pagination($args) {
	$links = paginate_links(array_merge($args, array('type' => 'array')));
	$data = array();

	foreach($links as $link) {
		preg_match_all('#<([A-Za-z]+)\s?(.*?)>(.*?)<\/[A-Za-z]+>#', $link, $matches);
		
		$tag = @$matches[1][0];
		$atts = @$matches[2][0];
		$text = @$matches[3][0];

		if($tag == 'a' && !strpos($atts, 'next') && !strpos($atts, 'prev')) {
			preg_match('#href=\'(.*?)\'#', $atts, $link);
			$data[] = array(
				'text' => $text,
				'link' => chtof_mod_url(array('page' => (int) $text)),//$text == '1' ? '?page=1' : @$link[1],
				'current' => false
			);
		}

		if($tag == 'span' && (strpos($atts, 'dots') || strpos($atts, 'current'))) {
			$data[] = array(
				'text' => $text,
				'link' => null,
				'current' => strpos($atts, 'current') !== false
			);
		}
	}

	return $data;
}

/**
 * Redirects search query if its matches offer ID.
 * @return void
 */
function redirect_q_to_offer() {
	$filters = $_GET;

	// Redirect to offer if ID matches
	if(isset($filters['q']) && strlen(trim($filters['q'])) > 0) {
		$args = array(
			'post_type' => array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business'),
			'meta_query' => array(
				array(
					'key' => 'offer_asari_listing_id',
					'value' => $filters['q'],
					'compare' => 'LIKE'
				)
			)
		);

		$results = new WP_Query($args);

		if(sizeof($results->posts) > 0) {
			wp_redirect(get_permalink($results->post->ID));
			die;
		}
	}

	// Redirect to proper search page
	/*if(sizeof($filters) == 1 && isset($filters['q']) && strlen(trim($filters['q'])) > 0) {
		$categories = cht_get_offer_categories();

		// Look in BUY category
		$args = array(
			'post_type' => array('offer_apartment', 'offer_house', 'offer_lot'),
			's' => $filters['q'],
			'tax_query' => array(
				array('taxonomy' => 'offer_category', 'field' => 'id', 'terms' => $categories->buy)
			)
		);

		$results = new WP_Query($args);

		if(sizeof($results->posts) > 0) {
			//wp_redirect( chtof_murl(get_permalink(pll_get_post(642)), array('q' => $filters['q'])) );
			//die('buy');
			return;
		}

		// Look in RENT category
		$args = array(
			'post_type' => array('offer_apartment', 'offer_house', 'offer_lot'),
			's' => $filters['q'],
			'tax_query' => array(
				array('taxonomy' => 'offer_category', 'field' => 'id', 'terms' => array($categories->short_rent, $categories->long_rent))
			)
		);

		$results = new WP_Query($args);

		if(sizeof($results->posts) > 0) {
			wp_redirect( chtof_murl(array('q' => $filters['q'], 'offer' => 'rent'), get_permalink(pll_get_post(642))) );
			die;
		}
	}*/
}
add_action('template_redirect', 'redirect_q_to_offer');

/**
 * Clears clipboard.
 * @return void
 */
function clear_clipboard_redirect() {
	if(isset($_GET['clear-clipboard'])) {
		chp_clipboard_clear();
		wp_redirect(get_permalink(pll_get_post(CLIPBOARD_PAGE_ID)));
		die;
	}
}
add_action('template_redirect', 'clear_clipboard_redirect');

/**
 * Prettifies JSON string.
 * @param string $json
 *
 * @author Kendall Hopkins
 * @license http://stackoverflow.com/questions/6054033/pretty-printing-json-with-php
 */
function cht_json_prettify($json) {
	$result = '';
	$level = 0;
	$in_quotes = false;
	$in_escape = false;
	$ends_line_level = NULL;
	$json_length = strlen( $json );

	for($i = 0; $i < $json_length; ++$i) {
		$char = $json[$i];
		$new_line_level = NULL;
		$post = "";

		if($ends_line_level !== NULL) {
			$new_line_level = $ends_line_level;
			$ends_line_level = NULL;
		}

		if($in_escape) {
			$in_escape = false;
		}
		else if($char === '"') {
			$in_quotes = !$in_quotes;
		}
		else if(!$in_quotes ) {
			switch($char) {
				case '}': case ']':
					$level--;
					$ends_line_level = NULL;
					$new_line_level = $level;
					break;
				case '{': case '[':
					$level++;
				case ',':
					$ends_line_level = $level;
					break;
				case ':':
					$post = " ";
					break;
				case " ": case "\t": case "\n": case "\r":
					$char = "";
					$ends_line_level = $new_line_level;
					$new_line_level = NULL;
					break;
			}
		} 
		else if( $char === '\\' ) {
			$in_escape = true;
		}
		
		if($new_line_level !== NULL) {
			$result .= "\n".str_repeat("\t", $new_line_level);
		}

		$result .= $char.$post;
	}

	return $result;
}

/**
 * Add offer_category filter to post types in admin panel.
 */
function chtheme_restrict_offers_by_offer_category() {
	if(!is_admin()) {
		return;
	}

    global $typenow;
    global $wp_query;

	if(in_array($typenow, array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business'))) {
        wp_dropdown_categories(array(
			'show_option_all' => __('Wszystkie kategorie', 'chtheme'),
			'taxonomy' =>  'offer_category',
			'name' =>  'ocat',
			'orderby' =>  'name',
			'selected' =>  $_GET['ocat'],
			'hierarchical' =>  true,
			'depth' =>  3,
			'show_count' =>  false,
			'hide_empty' =>  true
		));
    }
}
add_action('restrict_manage_posts', 'chtheme_restrict_offers_by_offer_category');

function chtheme_append_offer_category_to_admin_query($query) {
	if(!is_admin()) {
		return;
	}

	global $pagenow;
	$qv = &$query->query_vars;

    if($pagenow == 'edit.php' && isset($_GET['ocat']) && in_array($qv['post_type'], array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business'))) {
		$term = get_term_by('id', $_GET['ocat'], 'offer_category');
		$qv['offer_category'] = $term->slug;
	}
}
add_filter('parse_query', 'chtheme_append_offer_category_to_admin_query');

/**
 * Add offer_section filter to post types in admin panel.
 */
function chtheme_restrict_offers_by_offer_section() {
	if(!is_admin()) {
		return;
	}

    global $typenow;
    global $wp_query;

	if(in_array($typenow, array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business'))) {
        wp_dropdown_categories(array(
			'show_option_all' => __('Wszystkie sekcje', 'chtheme'),
			'taxonomy' =>  'offer_section',
			'name' =>  'osec',
			'orderby' =>  'name',
			'selected' =>  $_GET['osec'],
			'hierarchical' =>  true,
			'depth' =>  3,
			'show_count' =>  false,
			'hide_empty' =>  true
		));
    }
}
//add_action('restrict_manage_posts', 'chtheme_restrict_offers_by_offer_section');

function chtheme_append_offer_section_to_admin_query($query) {
	if(!is_admin()) {
		return;
	}

	global $pagenow;
	$qv = &$query->query_vars;

    if($pagenow == 'edit.php' && isset($_GET['osec']) && in_array($qv['post_type'], array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business'))) {
		$term = get_term_by('id', $_GET['osec'], 'offer_section');
		$qv['offer_section'] = $term->slug;
	}
}
add_filter('parse_query', 'chtheme_append_offer_section_to_admin_query');

/**
 * Add IDs column to offer posts in admin panel.
 */
function add_offer_columns($columns) {
    return array_merge(array_slice($columns, 0, -1), array('offer_ids' => __('ID oferty')), array_slice($columns, -1));
}
add_filter('manage_offer_apartment_posts_columns', 'add_offer_columns');
add_filter('manage_offer_house_posts_columns', 'add_offer_columns');
add_filter('manage_offer_lot_posts_columns', 'add_offer_columns');
add_filter('manage_offer_business_posts_columns', 'add_offer_columns');

function chtheme_admin_columns_content($column_name, $post_ID) {
    if($column_name == 'offer_ids') {
		echo sprintf('%s<br><strong>%s: <span style="color: red;">%s</span></strong><br><strong>%s:</strong> %s', get_field('offer_asari_listing_id'), __('Wordpress ID', 'chtheme'), get_the_ID(), __('Asari ID', 'chtheme'), get_field('offer_asari_listing_id-real'));
    }
}
add_action('manage_posts_custom_column', 'chtheme_admin_columns_content', 10, 4);

function my_column_width() {
    echo '<style type="text/css">.column-offer_ids { width: 130px !important; }</style>';
}
add_action('admin_head', 'my_column_width');

/**
 * Add metabox with offer IDS.
 */
function chtheme_offer_ids_metabox_display() {
?>
	<?php if(chtof_has('offer_asari_listing_id')) : ?>
	<p style="overflow: hidden;"><label style="float: left; display: block; width: 45%;"><strong><?php _e('Numer oferty', 'chtheme'); ?>:</strong></label> <span><?php the_field('offer_asari_listing_id'); ?></span></p>
	<?php endif; ?>

	<p style="overflow: hidden;"><label style="float: left; display: block; width: 45%;"><strong><?php _e('Wordpress ID', 'chtheme'); ?>:</strong></label> <span style="color: red;"><? echo get_the_ID(); ?></span></p>

	<?php if(chtof_has('offer_asari_listing_id-real')) : ?>
	<p style="overflow: hidden;"><label style="float: left; display: block; width: 45%;"><strong><?php _e('Asari ID', 'chtheme'); ?>:</strong></label> <span><?php the_field('offer_asari_listing_id-real'); ?></span></p>
	<?php endif; ?>
<?php
}

function chtheme_offer_ids_metabox() {
	foreach(array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business') as $post_type) {
		add_meta_box('rwmb_offer_ids', __('Identyfikatory oferty', 'chtheme'), 'chtheme_offer_ids_metabox_display', $post_type, 'side', 'high');
	}
}
add_action('admin_init', 'chtheme_offer_ids_metabox');

/**
 * Bind variables to Google map address pattern.
 * @param string $pattern
 * @param array $bind
 * @return string
 */
function cht_get_gmap_address($pattern, $bind = array()) {
	if(sizeof($bind) > 0) {
		foreach($bind as $key => $value) {
			$pattern = str_replace('%'.$key, $value, $pattern);
		}
	}

	return $pattern;
}

function cht_get_locations_tree($return_json = false) {
	$tree = cht_get_transient('get_locations_tree:tree');
	$json = cht_get_transient('get_locations_tree:json');

	if($tree === false || $json === false) {
		global $wpdb;

		$rows = $wpdb->get_results("SELECT
			mt1.meta_value AS country, 
			mt2.meta_value AS province, 
			mt3.meta_value AS city, 
			mt4.meta_value AS district
		FROM 
			`wp_posts`
		INNER JOIN wp_postmeta AS mt1 ON (wp_posts.ID = mt1.post_id AND mt1.meta_key = 'offer_location_country') 
		INNER JOIN wp_postmeta AS mt2 ON (wp_posts.ID = mt2.post_id AND mt2.meta_key = 'offer_location_province')
		INNER JOIN wp_postmeta AS mt3 ON (wp_posts.ID = mt3.post_id AND mt3.meta_key = 'offer_location_city')
		INNER JOIN wp_postmeta AS mt4 ON (wp_posts.ID = mt4.post_id AND mt4.meta_key = 'offer_location_district')
		ORDER BY district ASC");

		$tree = array();
		$json = array(
			'country' => array(),
			'province' => array(),
			'city' => array(),
			'district' => array()
		);

		setlocale(LC_COLLATE, 'pl_PL.UTF-8');

		foreach($rows as $row) {
			if(!@is_array($tree[$row->country])) {
				$tree[$row->country] = array();
			}

			if(!@is_array($tree[$row->country][$row->province])) {
				$tree[$row->country][$row->province] = array();
			}

			if(!@is_array($tree[$row->country][$row->province][$row->city])) {
				$tree[$row->country][$row->province][$row->city] = array();
			}

			if(!in_array($row->district, $tree[$row->country][$row->province][$row->city], true)) {
				$tree[$row->country][$row->province][$row->city][] = $row->district;
			}

			/**
			 * JSON
			 */
			// Countries
			if(!@in_array($row->country, $json['country']) && strlen(trim($row->country)) > 0) {
				$json['country'][$row->country] = $row->country;
			}

			// Provinces
			if(!@is_array($json['province'][$row->country]) && strlen(trim($row->country)) > 0) {
				$json['province'][$row->country] = array();
			}

				if(!@in_array($row->province, $json['province'][$row->country]) && strlen(trim($row->province)) > 0) {
					$json['province'][$row->country][$row->province] = $row->province;
					asort($json['province'][$row->country], SORT_LOCALE_STRING);
				}
			
			// Cities
			if(!@is_array($json['city'][$row->province]) && strlen(trim($row->province)) > 0) {
				$json['city'][$row->province] = array();
			}

				if(!@in_array($row->city, $json['city'][$row->province]) && strlen(trim($row->city)) > 0) {
					$json['city'][$row->province][$row->city] = $row->city;
					asort($json['city'][$row->province], SORT_LOCALE_STRING);
				}
			
			// Districts
			if(!@is_array($json['district'][$row->city]) && strlen(trim($row->city)) > 0) {
				$json['district'][$row->city] = array();
			}

				if(!@in_array($row->district, $json['district'][$row->city]) && strlen(trim($row->district)) > 0) {
					$json['district'][$row->city][$row->district] = $row->district;
				}
		}

		cht_set_transient('get_locations_tree:tree', $tree, 60*60*12); // 12 hours
		cht_set_transient('get_locations_tree:json', $json, 60*60*12); // 12 hours
	}

	return $return_json ? $json : $tree;
}

/**
 * Returns offer's Google Map address.
 * @param object $post
 * @return string|null
 */
function cht_get_offer_gmap_address($post) {
	$address = '';
	$country = get_field('offer_location_country', $post);
	$province = get_field('offer_location_province', $post);
	$city = get_field('offer_location_city', $post);
	$district = get_field('offer_location_district', $post);
	$street = get_field('offer_location_street', $post);

	if(strlen(trim($country)) <= 0 || strlen(trim($city)) <= 0) {
		return null;
	}

	$address .= $country;

	if(strlen(trim($province)) > 0) {
		$address .= ', '.$province;
	}

	if(strlen(trim($city)) > 0) {
		$address .= ', '.$city;
	}

	if(strlen(trim($district)) > 0) {
		$address .= ', '.$district;
	}

	if(strlen(trim($street)) > 0) {
		$address .= ', '.$street;
	}

	return $address;
}

/**
 * Returns Google Map coordinates for given address.
 * @param string $address
 * @param array|null
 */
function cht_get_gmap_location($address) {
	// Get geocode data for given address
	$url = 'http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false';

	$ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 2);
    $response = curl_exec($ch);
    curl_close($ch);

	if(!$response || strlen($response) <= 0) {
		return false;
	}

	// Decode returned JSON
	$json = json_decode($response, true);

	if(!is_array($json)) {
		return false;
	}

	if(!@is_array(@$json['results'][0]['geometry']['location'])) {
		return false;
	}

	return @$json['results'][0]['geometry']['location'];
}

/**
 * Generate and save Google Map coordinates on post save.
 * @param int $post_id
 * @return void
 */
function cht_save_post($post_id) {
    if(!in_array($_POST['post_type'], array('offer_apartment', 'offer_house', 'offer_lot', 'offer_business'))) {
        return;
    }

	$post = get_post($post_id);
	$lat_lng = get_field('offer_location_latlng', $post->ID);
	$address = cht_get_offer_gmap_address($post);

    if(strlen(trim($lat_lng)) <= 0 && strlen($address) > 0) {
		$location = cht_get_gmap_location($address);

		if(@is_array(@$location)) {
			$new_value = @$location['lat'].', '.@$location['lng'];
			update_field('field_538f218b53068', $new_value, $post->ID);
			update_field('field_538f218b53068', $new_value, pll_get_post($post->ID, 'en'));
		}
	}
}
add_action('save_post', 'cht_save_post');
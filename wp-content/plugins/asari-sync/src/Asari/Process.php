<?php

// @TODO "Deactivate" flag, which stops all plugin future actions (for example if there are no exchange rates provided by "crystalhouse" plugin etc.)
class Asari_Process {
	protected $_name = 'process_name';
	protected $_status;
	protected $_log;

	public function __construct() {
		$this->_status = new Asari_Process_Status($this->_name);
		$this->_log = new Asari_Log($this->_name);
	}

	public function init() {
		$status = new Asari_Status;

		if($status->isProcessing()) {
			throw new RuntimeException('Inny proces jest aktualnie przetwarzany, spróbuj ponownie później');
		}

		$this->getStatus()->setLogFilename($this->getLog()->getFilename());
		return $this->getStatus()->start();
	}

	public function getName() {
		return $this->_name;
	}

	public function getStatus() {
		return $this->_status;
	}

	public function getLog() {
		return $this->_log;
	}

	public function setupErrorHandler() {
		return set_error_handler(array(&$this, 'errorHandler'), E_ALL);
	}

	public function unsetErrorHandler() {
		return restore_error_handler();
	}

	public function errorHandler($err_severity, $err_msg, $err_file, $err_line, array $err_context) {
		if(0 === error_reporting()) { return false;}

		switch($err_severity) {
			case E_ERROR:               throw new Asari_Process_ErrorException($err_msg);
			case E_WARNING:             throw new Asari_Process_ErrorException($err_msg);
			case E_PARSE:               throw new Asari_Process_ErrorException($err_msg);
			case E_NOTICE:              throw new Asari_Process_ErrorException($err_msg);
			case E_CORE_ERROR:          throw new Asari_Process_ErrorException($err_msg);
			case E_CORE_WARNING:        throw new Asari_Process_ErrorException($err_msg);
			case E_COMPILE_ERROR:       throw new Asari_Process_ErrorException($err_msg);
			case E_COMPILE_WARNING:     throw new Asari_Process_ErrorException($err_msg);
			case E_USER_ERROR:          throw new Asari_Process_ErrorException($err_msg);
			case E_USER_WARNING:        throw new Asari_Process_ErrorException($err_msg);
			case E_USER_NOTICE:         throw new Asari_Process_ErrorException($err_msg);
			case E_STRICT:              throw new Asari_Process_ErrorException($err_msg);
			case E_RECOVERABLE_ERROR:   throw new Asari_Process_ErrorException($err_msg);
			case E_DEPRECATED:          throw new Asari_Process_ErrorException($err_msg);
			case E_USER_DEPRECATED:     throw new Asari_Process_ErrorException($err_msg);
		}
	}

	protected function error($message) {
		$this->getLog()->log($message);
		
		return $this->getStatus()->finish(array(
			'success' => false, 
			'error' => array(
				'time' => time(),
				'message' => $message
			)
		));
	}
}

class Asari_Process_ErrorException extends RuntimeException {
}
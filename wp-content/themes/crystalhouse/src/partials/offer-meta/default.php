						<section class="widget offer-meta">
							<?php if(chtof_has_any(array('offer_location_city', 'offer_location_district', 'offer_location_street'))) : ?>
							<ul>
								<?php if(chtof_has('offer_location_city')) : ?>
								<li><strong><?php echo chtof_label('offer_location_city'); ?></strong>
									<span><?php the_field('offer_location_city'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_location_district')) : ?>
								<li><strong><?php echo chtof_label('offer_location_district'); ?></strong>
									<span><?php the_field('offer_location_district'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_location_street')) : ?>
								<li><strong><?php echo chtof_label('offer_location_street'); ?></strong>
									<span><?php the_field('offer_location_street'); ?></span></li>
								<?php endif; ?>
							</ul>
							<?php endif; ?>

							<?php if(chtof_has_any(array('offer_investment_developer', 'offer_investment_completion-date', 'offer_investment_group', 'offer_investment_area-from'))) : ?>
							<ul>
								<?php if(chtof_has('offer_investment_group')) : ?>
								<li><strong><?php echo chtof_label('offer_investment_group'); ?></strong>
									<span><?php echo the_field('offer_investment_group'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_investment_developer')) : ?>
								<li><strong><?php echo chtof_label('offer_investment_developer'); ?></strong>
									<span><?php the_field('offer_investment_developer'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_investment_completion-date')) : ?>
								<li><strong><?php echo chtof_label('offer_investment_completion-date'); ?></strong>
									<span><?php echo chtof_completion_date_formatter(get_field('offer_investment_completion-date')); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_investment_area-from')) : ?>
								<li><strong><?php echo chtof_label('offer_investment_area-from'); ?></strong>
									<span><?php echo the_field('offer_investment_area-from'); ?> m<sup>2</sup><?php if(chtof_has('offer_investment_area-to')) : ?> - <?php echo the_field('offer_investment_area-to'); ?> m<sup>2</sup><?php endif; ?></span></li>
								<?php endif; ?>
							</ul>
							<?php endif; ?>

							<?php if(chtof_has_any(array('offer_market_type', 'offer_property_floors-floor', 'offer_property_floors-all', 'offer_property_built-year', 'offer_property_area-primary', 'offer_property_area-lot'))) : ?>
							<ul>
								<?php if(chtof_has('offer_market_type')) : ?>
								<li><strong><?php echo chtof_label('offer_market_type'); ?></strong>
									<span><?php echo chtof_value(get_field('offer_market_type'), 'offer_market_type'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_property_floors-floor')) : ?>
								<li><strong><?php echo chtof_label('offer_property_floors-floor'); ?></strong>
									<span><?php the_field('offer_property_floors-floor'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_property_floors-all')) : ?>
								<li><strong><?php echo chtof_label('offer_property_floors-all'); ?></strong>
									<span><?php the_field('offer_property_floors-all'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_property_built-year')) : ?>
								<li><strong><?php echo chtof_label('offer_property_built-year'); ?></strong>
									<span><?php the_field('offer_property_built-year'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_property_area-primary')) : ?>
								<li><strong><?php echo chtof_label('offer_property_area-primary'); ?></strong>
									<span><?php the_field('offer_property_area-primary'); ?> m<sup>2</sup> / <?php echo cht_sqm2feet(get_field('offer_property_area-primary')); ?> sqft</span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_property_area-lot')) : ?>
								<li><strong><?php echo chtof_label('offer_property_area-lot'); ?></strong>
									<span><?php the_field('offer_property_area-lot'); ?> m<sup>2</sup> / <?php echo cht_sqm2feet(get_field('offer_property_area-lot')); ?> sqft</span></li>
								<?php endif; ?>
							</ul>
							<?php endif; ?>

							<?php if(chtof_has_any(array('offer_rooms_rooms', 'offer_rooms_bedrooms', 'offer_rooms_bathrooms'))) : ?>
							<ul>
								<?php if(chtof_has('offer_rooms_rooms')) : ?>
								<li><strong><?php echo chtof_label('offer_rooms_rooms'); ?></strong>
									<span><?php the_field('offer_rooms_rooms'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_rooms_bedrooms')) : ?>
								<li><strong><?php echo chtof_label('offer_rooms_bedrooms'); ?></strong>
									<span><?php the_field('offer_rooms_bedrooms'); ?></span></li>
								<?php endif; ?>

								<?php if(chtof_has('offer_rooms_bathrooms')) : ?>
								<li><strong><?php echo chtof_label('offer_rooms_bathrooms'); ?></strong>
									<span><?php the_field('offer_rooms_bathrooms'); ?></span></li>
								<?php endif; ?>
							</ul>
							<?php endif; ?>

							<?php
								// Additional fields
								if(chtof_has('offer_additional-fields')) :
									$additional = explode("\r\n", trim(get_field('offer_additional-fields')));
							?>
							<ul>
								<?php
									foreach($additional as $line) :
										$field = explode(':', $line);

										if(sizeof($field) === 2) :
								?>
								<li><strong><?php echo trim(@$field[0]); ?></strong>
									<span><?php echo trim(@$field[1]); ?></span></li>
								<?php
										endif;
									endforeach;
								?>
							</ul>
							<?php endif; ?>

							<?php if(chtof_has('offer_location_center-distance')) : ?>
							<ul>
								<li><strong><?php echo chtof_label('offer_location_center-distance'); ?></strong>
									<span><?php the_field('offer_location_center-distance'); ?> km</span></li>
							</ul>
							<?php endif; ?>

							<?php get_template_part('src/partials/offers-list/slots/attributes'); ?>
						</section>
						<!-- .offer-meta -->
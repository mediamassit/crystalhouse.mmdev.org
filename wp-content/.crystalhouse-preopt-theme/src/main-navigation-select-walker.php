<?php


class cht_Main_Navigation_Select_Walker extends Walker_Nav_Menu {
	function start_lvl(&$output, $depth){
		$indent = str_repeat("\t", $depth); // don't output children opening tag (`<ul>`)
	}
 
	function end_lvl(&$output, $depth){
		$indent = str_repeat("\t", $depth); // don't output children closing tag
	}

	function start_el(&$output, $item, $depth, $args) {
 		$url = '#' !== $item->url ? $item->url : '';
 		$output .= '<option value="' . $url . '"'.(in_array("current_page_item",$item->classes) ? ' selected="selected"' : '').'>' . str_repeat("&nbsp;&nbsp;&nbsp;", $depth) . $item->title;
	}	
 
	function end_el(&$output, $item, $depth){
		$output .= "</option>\n"; // replace closing </li> with the option tag
	}
}
<?php
/**
 * Plugin Name: CrystalHouse
 * Description: Provides structure for CrystalHouse website.
 * Version: 1.0
 * Author: 365grupa
 */

require __DIR__.'/src/taxonomies.php';
require __DIR__.'/src/roles.php';
require __DIR__.'/src/post-type/offer_apartment.php';
require __DIR__.'/src/post-type/offer_business.php';
require __DIR__.'/src/post-type/offer_house.php';
require __DIR__.'/src/post-type/offer_lot.php';
require __DIR__.'/src/meta-boxes.php';
require __DIR__.'/src/nbp-currencies.php';

function op_fix_timezone() {
	date_default_timezone_set(get_option('timezone_string'));
}
add_action('init', 'op_fix_timezone', 1);

function chplugin_start_session() {
	if(!session_id()){
		session_start();
	}

	// Set currency
	// @TODO Move it to cookies!
	if(isset($_GET['_set_session_currency']) && in_array($_GET['_set_session_currency'], chofl_currencies(false))) {
		$_SESSION['currency'] = $_GET['_set_session_currency'];
		wp_redirect(chtof_remf_url(array('_set_session_currency')));
		die;
	}
}
add_action('init', 'chplugin_start_session');
add_action('admin_init', 'chplugin_start_session');

/**
 * Load plugin textdomain.
 */
function chplugin_load_textdomain() {
	load_plugin_textdomain('chtheme', false, get_template_directory() . '/languages');
}
add_action('plugins_loaded', 'chplugin_load_textdomain');

/*
 * Get current language code
 */
function cht_get_current_language() {
	global $polylang;

	return is_object(@$polylang->curlang) ? $polylang->curlang->slug : @$polylang->pll_get_current_language(false)->slug;
}

// Transient API
function cht_set_transient($transient, $value, $expiration = 60, $append_language_code = true) {
	if($append_language_code) {
		$transient .= '_'.cht_get_current_language();
	}

	return set_transient($transient, $value, $expiration);
}

function cht_get_transient($transient, $append_language_code = true) {
	if($append_language_code) {
		$transient .= '_'.cht_get_current_language();
	}

	return get_transient($transient);
}

function cht_delete_transient($transient, $append_language_code = true) {
	if($append_language_code) {
		$transient .= '_'.cht_get_current_language();
	}

	return delete_transient($transient);
}
// endof: Transient API

function cht_precache_slider_offers($slider) {
	preg_match_all('#\[ch_home_slide.*?offer_id="([0-9]+)".*?\]#', $slider, $matches);
	$post_ids = @$matches[1];

	if(is_array($post_ids) && sizeof($post_ids) > 0) {
		foreach($post_ids as $key => $value) {
			$post_ids[$key] = pll_get_post($value);
		}

		$query = new WP_Query(array(
			'post_type' => 'any',
			'post__in' => $post_ids,
			'nopaging' => true
		));
	}
}



function cht_preload_posts_thumbnail_posts(WP_Query $query = null) {
	if(sizeof($query->posts) > 0) {
		global $wpdb;

		$post_ids = wp_list_pluck($query->posts, 'ID');
		$thumbnails = $wpdb->get_col("SELECT meta_value FROM $wpdb->postmeta WHERE `meta_key` = '_thumbnail_id' AND `post_id` IN(".implode(',', $post_ids).")");

		$posts = new WP_Query(array(
			'post_type' => 'attachment',
			'post_status' => 'any',
			'post__in' => $thumbnails,
			'nopaging' => true,
			'update_post_meta_cache' => true
		));
	}
}
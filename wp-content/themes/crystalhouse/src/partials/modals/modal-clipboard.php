	<div class="modal clipboard vertical-middle fade" id="modal-clipboard" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="javascript:void(0);" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<div class="nav">
						<p><a href="<?php echo get_permalink(pll_get_post(CLIPBOARD_PAGE_ID)); ?>" class="btn"><?php _e('Obejrzyj', 'chtheme'); ?></a></p>
						<p>lub <strong><a href="javascript:void(0);" data-dismiss="modal" aria-hidden="true"><?php _e('Dodaj kolejne', 'chtheme'); ?></a></strong></p>
					</div>
					<!-- .nav -->

					<div class="message">
						<p><?php _e('Dodałeś nieruchomość do', 'chtheme'); ?> <a href="<?php echo get_permalink(pll_get_post(CLIPBOARD_PAGE_ID)); ?>"><?php _e('schowka', 'chtheme'); ?></a></p>
					</div>
					<!-- .message -->
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->
<?php

get_header();

//include locate_template('src/filters/temp_head.php');

$config = Cht_Offers_List_Config::init($_GET);
$config->setPostType('offer_apartment');
$list = new Cht_Offers_List($config);
$categories = cht_get_offer_categories();

$offers = $list->fetchAll();
$offers_map = $list->fetchAllForMap();

?>

	<div id="content">
		<div class="container">
			<?php get_template_part('src/partials/breadcrumbs'); ?>
			<?php chtof_get_slot('src/partials/filters/navigation_tabs', array('categories' => $categories, 'config' => $config)); ?>
			<?php include locate_template('src/partials/filters/page_title.php'); ?>

			<div class="row">
				<?php include locate_template('src/partials/filters/list.php'); ?>

				<div class="col-md-3">
					<?php include locate_template('src/partials/filters/lists/apartment.php'); ?>
				</div>
				<!-- .col-md-3 -->
			</div>
			<!-- .row -->
		</div>
		<!-- .container -->
	</div>
	<!-- #content -->

<?php get_footer(); ?>
										<?php
											$offer_attributes = chtof_attributes();

											if(chtof_has('offer_business_premise-has-tenant')) {
												$offer_attributes[chtof_label('offer_business_premise-has-tenant')] = 'has-tenant';
											}

											if(sizeof($offer_attributes) > 0) : 
										?>
										<p class="flags">
											<?php foreach($offer_attributes as $label => $attr) : ?>
											<img src="<?php bloginfo('template_url'); ?>/images/icons/offer/<?php echo $attr; ?>.png" alt="<?php _e($label, 'chtheme'); ?>" title="<?php pll_e($label, 'chtheme'); ?>" data-tooltip="1" data-placement="bottom">
											<?php endforeach; ?>
										</p>
										<?php endif; ?>
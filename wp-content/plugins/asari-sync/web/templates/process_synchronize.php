	<div class="asc-params">
		<h3>Status procesu</h3>
		<ul>
			<li><strong>Rozpoczęto:</strong> <span data-syncprocess-started><?php echo date_i18n('Y/m/d H:i:s', $running_process['started_at']); ?></span></li>
			<li><strong>Czas przetwarzania:</strong> <span data-syncprocess-runtime data-time="<?php echo $running_process['processing_time']; ?>">b/d</span></li>
			<li><strong>Pozostało:</strong> ~<span data-syncprocess-estimatedtime><?php if($running_process['estimated_time'] > 0) : ?><?php echo $running_process['estimated_time']; ?>sek.<?php else : ?>b/d<?php endif; ?></span></li>
			<li><strong>Postęp:</strong> <span data-syncprocess-done><?php echo $running_process['items_done_count']; ?></span> z <span data-syncprocess-scheduled><?php echo $running_process['items_count']; ?></span> wpisów zostało dodanych</li>
		</ul>
	</div>
	<!-- .asc-params -->
	<?php if(sizeof($running_process['messages']) > 0) : ?>
	<div class="asc-log">
		<h3>Log zdarzeń</h3>
		<ul>
		<?php foreach($running_process['messages'] as $message) : ?>
			<li><time><?php echo date('Y/m/d H:i:s', $message['time']); ?></time> <?php echo $message['message']; ?></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<!-- .asc-log -->
	<?php endif; ?>

	<?php
		if($is_processing) :
	?>
	<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			$('.asc-wrap').ascProcessing({
				process_name: 'synchronize',
				status_url: "<?php echo $status->getJsonFileUrl(); ?>",
				redirect_url: "<?php echo admin_url('tools.php?page=asarisync'); ?>"
			});
		});
	})(jQuery);
	</script>
	<?php endif; ?>
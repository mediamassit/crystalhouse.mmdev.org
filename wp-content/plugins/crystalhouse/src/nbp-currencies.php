<?php

function chp_catch_nbp_parse_rate($rate) {
	return (float) str_replace(',', '.', $rate);
}

function chp_catch_nbp_exchange_rates() {
	$url = 'http://www.nbp.pl/Kursy/xml/LastA.xml';
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	//$response = curl_exec($ch);
	curl_close($ch);
	$response = null;
	if(!$response || strlen($response) <= 0) {
		return false;
	}

	preg_match_all('#<kod_waluty>([a-zA-Z0-9]+)<\/kod_waluty>\s*<kurs_sredni>([0-9,]+)<\/kurs_sredni>#i', $response, $matches);

	if(sizeof(@$matches[0]) <= 0 || sizeof($matches[1]) !== sizeof($matches[2])) {
		return false;
	}

	return array_combine($matches[1], array_map('chp_catch_nbp_parse_rate', $matches[2]));
}

function chp_load_nbp_exchange_rates() {
	$cache = dirname(__DIR__).'/tmp/nbp-currencies.js';

	if(!file_exists($cache) || (time() - (int) @filemtime($cache)) >= 60*60) {
		$rates = chp_catch_nbp_exchange_rates();

		if(is_array($rates) && array_key_exists('USD', $rates) && array_key_exists('EUR', $rates)) {
			@file_put_contents($cache, json_encode($rates));
		}
	}

	// @TODO If cache is not stored or if is expired - show warning in admin panel
}

add_action('init', 'chp_load_nbp_exchange_rates');

function chp_get_nbp_exchange_rates() {
	chp_load_nbp_exchange_rates();

	$cache = dirname(__DIR__).'/tmp/nbp-currencies.js';

	if(file_exists($cache)) {
		$rates = @json_decode(@file_get_contents($cache));

		if(!is_object($rates)) {
			return null;
		}

		return $rates;
	}
}

function chp_get_nbp_exchange_rates_update_time() {
	return filemtime(dirname(__DIR__).'/tmp/nbp-currencies.js');
}

<?php
	get_header();
?>

	<div id="content">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="container">
			<?php get_template_part('src/partials/breadcrumbs'); ?>

			<?php //get_template_part('src/partials/page-meta'); ?>

			<div class="page-title">
				<h1><?php the_title(); ?></h1>
			</div>
			<!-- .page-title -->

			<div class="row divider">
				<div class="col-md-9 main">
					<section class="page">
						<?php the_content(); ?>
					</section>
					<!-- .page -->
				</div>
				<!-- .col-md-9 -->

				<?php get_template_part('src/partials/page-static-sidebar'); ?>
			</div>
			<!-- .row -->
		</div>
		<!-- .container -->
		<?php endwhile; endif; ?>
	</div>
	<!-- #content -->

	<?php get_template_part('src/partials/page-contact-form'); ?>

<?php get_footer(); ?>
<?php

function cth_get_top_searches_widgets() {
	ob_start();

	$widgets = dynamic_sidebar('footer_top_searches');
	
	if($widgets) {
		$html = ob_get_contents();
	}

	ob_end_clean();

	$widgets = explode('<div class="col-md-3 col-sm-6">', $html);
	array_shift($widgets);

	foreach($widgets as &$widget) {
		$widget = '<div class="col-md-3 col-sm-6">'.$widget;
	}

	return $widgets;
}

$top_searches_widgets = cth_get_top_searches_widgets();
$top_searches_widgets_1 = array_slice($top_searches_widgets, 0, 3);
$top_searches_widgets_2 = array_slice($top_searches_widgets, 3);

?>
	<section class="home-section popular-entries hidden-print">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6 about">
					<h2><?php _e('Najczęściej wyszukiwane', 'chtheme'); ?></h2>
					<i class="icon-search"></i>
				</div>
				<!-- .col-md-3 -->

				<?php if(sizeof($top_searches_widgets_1) > 0) : foreach($top_searches_widgets_1 as $widget) : ?>
					<?php echo $widget; ?>
				<?php endforeach; endif; ?>
			</div>
			<!-- .row -->

			<?php if(sizeof($top_searches_widgets_2) > 0) : ?>
			<div data-expandable>
				<div class="row">
					<?php foreach($top_searches_widgets_2 as $widget) : ?>
						<?php echo $widget; ?>
					<?php endforeach; ?>
				</div>
				<!-- .row -->
			</div>
			<!-- [data-expandable] -->
			<?php endif; ?>

			<div class="expander">
				<span><a href="#" data-expand data-collapse-label="<?php _e('Zwiń', 'chtheme'); ?>"><?php _e('Rozwiń', 'chtheme'); ?></a></span>
			</div>
			<!-- .expander -->
		</div>
		<!-- .container -->
	</section>
	<!-- .popular-entries -->
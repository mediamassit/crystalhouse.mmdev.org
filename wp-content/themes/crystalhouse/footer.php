
	<?php get_template_part('src/partials/popular-entries'); ?>

	<div id="footer" class="hidden-print">
		<div class="container">
			<div class="row hidden-xs">
				<h3 class="slogan"><?php echo get_opt('opt-theme-headline'); ?></h3>
			</div>
			<!-- .row -->

			<div class="row">
				<div class="col-md-3 col-sm-12 col-sm-12">
					<a href="<?php echo pll_home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo-dark.png" alt=""></a>
				</div>
				<!-- .col-md-3 -->

				<div class="col-md-3 col-sm-4 col-xs-6">
					<p><?php echo preg_replace("#\r\n#", '<br>', get_opt('opt-contact-address')); ?></p>
				</div>
				<!-- .col-md-3 -->

				<div class="col-md-3 col-sm-4 col-xs-6">
					<p class="big"><?php _e('Telefon', 'chtheme'); ?><br><strong><?php echo reset(explode("\r\n", get_opt('opt-contact-phone'))); ?></strong></p>
				</div>
				<!-- .col-md-3 -->

				<div class="col-md-3 col-sm-4 col-xs-12">
					<p class="big"><?php _e('Email', 'chtheme'); ?><br><strong><a href="mailto:<?php echo get_opt('opt-contact-email'); ?>"><?php echo get_opt('opt-contact-email'); ?></a></strong></p>

					<?php get_template_part('src/partials/module-socials'); ?>
				</div>
				<!-- .col-md-3 -->
			</div>
			<!-- .row -->
		</div>
		<!-- .container -->

		<div class="copyrights">
			<div class="container">
				<p>&copy; <?php echo date('Y'); ?> <a href="<?php echo pll_home_url(); ?>"><?php bloginfo('title'); ?></a> | <a href="http://roogmedia.pl/" style="cursor: default; opacity: 1;"><?php _e('by Roogmedia', 'chtheme'); ?></a></p>
			</div>
			<!-- .container -->
		</div>
		<!-- .copyrights -->
	</div>
	<!-- #footer -->

	<?php wp_footer(); ?>
	<?php get_template_part('src/footer-scripts'); ?>

</body>
</html>
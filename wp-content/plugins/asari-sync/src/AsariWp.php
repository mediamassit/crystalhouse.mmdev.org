<?php

class AsariWp {

	public function __construct() {
	}

	public function getPostsAsariIDs() {
		// @TODO Check if meta isn't assigned to REVISION
		global $wpdb;

		$realIds = array();
		$listingIds = array();
		$query = $wpdb->query("SELECT `meta_key`, `meta_value` FROM $wpdb->postmeta WHERE `meta_key` IN('asari_id', 'asari_listingId')");

		if($query === false) {
			throw new RuntimeException('Cannot get posts Asari IDs: query failed');
		}

		foreach($wpdb->last_result as $k => $v) {
			switch($v->meta_key) {
				case 'asari_id':
					$realIds[] = $v->meta_value;
					break;
				case 'asari_listingId':
					$listingIds[] = $v->meta_value;
					break;
			}
		}

		return (object) array('real' => array_unique($realIds), 'listing' => array_unique($listingIds));
	}

	public function asariPostExists($realId) {
		global $wpdb;

		// TODO: Does it count Trash folder or not?
		$query = $wpdb->query($wpdb->prepare("SELECT `meta_key`, `meta_value` FROM $wpdb->postmeta WHERE `meta_key` = 'asari_id' AND `meta_value` = %s", (float) $realId));

		if($query === false) {
			throw new RuntimeException('Cannot check if Asari post exists: query failed');
		}

		return sizeof($wpdb->last_result) > 0;
	}

}
								<div class="form-group" data-form-fieldset="type">
									<label><?php _e('Typ nieruchomości', 'chtheme'); ?></label>
									<?php
										$post_types_select = array();

										foreach($types as $type) {
											$post_types_select[str_replace('offer_', '', $type)] = chtof_type_label($type);
										}

										echo chofl_select('type', array_merge(!@$all_disabled ? array(
											'' => __('Wszystkie', 'chtheme')
										) : array(), $post_types_select));
									?>
								</div>
								<!-- .form-group -->
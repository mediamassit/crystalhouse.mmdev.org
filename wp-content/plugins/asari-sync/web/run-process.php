<?php

set_time_limit(0);
ignore_user_abort(1);

require_once __DIR__.'../../../../../wp-load.php';

try {
	// Check process name
	if(!isset($_GET['name'])) {
		throw new RuntimeException('Niezdefiniowano procesu');
	}

	if(!class_exists(@$_GET['name'], true)) {
		throw new RuntimeException('Nie można odnaleźć procesu');
	}

	// Setup process variables
	$error = null;
	$process = new $_GET['name'];

	// Validate process object
	if(!($process instanceof Asari_Process)) {
		throw new RuntimeException('Dane proces nie jest odpowiednim obiektem');
	}

	//$process->setupErrorHandler();
	$init = $process->init();

	if($init !== true) {
		$error = 'Wystąpił błąd w trakcie inicjalizaji procesu';
	}
}
catch(Exception $e) {
	$error = sprintf('Wyjątek: %s', $e->getMessage());
}

if(!isset($_GET['dev'])) {
	ob_start();
}

?><html>
<head>
	<meta charset="utf-8">
	<title>AsariSync - Manager procesów</title>
	<style type="text/css">
		body {
			color: #444;
			font-size: 14px;
			font-family: Arial, Tahoma, sans-serif;
			background: #EAEAEA;
		}

		center {
			margin: 60px 20px 20px 20px;
			padding: 10px;
			background: #FFF;
		}

		p {
			margin: 0;
			padding: 2px;
		}
	</style>
</head>
<body>
	<center>

	<?php if(!is_null($error)) : ?>
		<p style="color: red;"><strong><?php echo $error; ?></strong></p>
		<script>setTimeout(function() {
			window.opener.location.reload(); window.close();
		}, 2000);</script>
	<?php else : ?>
		<p><strong>Przetwarzanie procesu zostało rozpoczęte!</strong></p>
		<script>window.opener.location.reload(); window.close();</script>
	<?php endif; ?>

	<p><small>Okienko zostanie zamknięte za kilka sekund.<small></p>

	</center>
</body>
</html>
<?php
if(!isset($_GET['dev'])) {
	header('Content-Length: '.ob_get_length());
	header('Content-Encoding: none');
	header('Connection: close');
	ob_end_flush();
	ob_flush();
	flush();
	if(session_id()) session_write_close();
}

if(is_null($error)) {
	$process->run();
	//$process->unsetErrorHandler();
}
	<div class="modal classic vertical-middle fade" id="modal-contact-offer" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="#" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<?php $popup = get_post(pll_get_post(733)); echo apply_filters('the_content', $popup->post_content); unset($popup); ?>
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->
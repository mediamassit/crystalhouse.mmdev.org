								<div class="form-group" data-form-fieldset="business_type"<?php if($disabled) : ?> style="display: none;"<?php endif; ?>>
									<label><?php echo chtof_label('offer_business_type'); ?></label>
									<?php
										$business_types = get_field_object(chtof_id('offer_business_type'));
										$business_types = $business_types['choices'];

										echo chofl_select('business_type', array_merge(array(
											'' => __('Wszystkie', 'chtheme')
										), $business_types));
									?>
								</div>
								<!-- .form-group -->
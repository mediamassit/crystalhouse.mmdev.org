							<div class="form-group" data-form-fieldset="currency">
								<div class="row">
									<div class="col-md-4"><label style="margin-top: 2px;"><?php _e('Waluta', 'chtheme'); ?></label></div>
									<div class="col-md-8">
										<div class="inline-checkboxes currencies">
										<?php
											$currencies = chofl_currencies(false);
											$active_currency = in_array($config->getFilter('currency'), $currencies) ? $config->getFilter('currency') : 'PLN';

											foreach($currencies as $currency) :
										?>
											<label><input type="radio" name="currency" value="<?php echo $currency; ?>"<?php if($currency == $active_currency) : ?> checked="checked"<?php endif; ?>> <?php echo $currency; ?></label>
										<?php endforeach; ?>
										</div>
										<!-- .inline-checkboxes -->
									</div>
								</div>
								<!-- .row -->
							</div>
							<!-- .form-group --><?php unset($currencies, $active_currency, $currency);  ?>
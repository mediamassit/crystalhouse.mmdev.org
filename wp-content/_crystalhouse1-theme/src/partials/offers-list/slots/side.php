							<nav class="side">
								<ul>
									<li><a href="#modal-contact-offer" role="button" rel="tooltip" title="<?php _e('Zapytaj o cenę', 'chtheme'); ?>" data-toggle="modal" data-modal-form-offer="<?php echo get_field('offer_asari_listing_id'); ?>" data-modal-form-subject="<?php _e('Pytanie o cenę', 'chtheme'); ?>" data-tooltip="1"><i class="icon-offer-question"></i></a></li>
									<?php if(!chp_offer_in_clipboard($post->ID)) : ?><li><a href="javascript:void(0);" title="<?php _e('Dodaj do schowka', 'chtheme'); ?>" data-addtoclipboard="<?php echo $post->ID; ?>" data-tooltip="1"><i class="icon-offer-clipboard"></i></a></li><?php endif; ?>
									<li><a href="<?php echo get_permalink($post->ID); ?>" class="block" title="<?php _e('Zobacz więcej', 'chtheme'); ?>" data-tooltip="1"><i class="icon-offer-more"></i></a></li>
								</ul>
							</nav>
							<!-- .side -->
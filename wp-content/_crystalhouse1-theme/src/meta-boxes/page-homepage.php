<?php

function register_homepage_meta_boxes($meta_boxes) {
    if(!class_exists('RW_Meta_Box') ) return;

	// Gallery
    $meta_boxes = array(
		array(
			'title' => __('Slider', 'chtheme'),
			'pages' => array('page'),
			'context' => 'normal',
			'priority' => 'high',
			'fields' => array(
				array(
					'name'  => '',
					'id'    => "home_image_slider",
					'type'  => 'textarea',
					'rows'  => 10
				)
			)
		)
	);

	if(post_has_template(array('page-templates/homepage.php', 'page-templates/homepage-business.php', 'page-templates/homepage-rent-long.php', 'page-templates/homepage-rent-short.php'))) {
		foreach($meta_boxes as $meta_box) {
			new RW_Meta_Box($meta_box);
		}
	}
}
add_action('admin_init', 'register_homepage_meta_boxes');

// Add help tabs
function register_homepage_editor_help() {
	if(!post_has_template(array('page-templates/homepage.php', 'page-templates/homepage-business.php', 'page-templates/homepage-rent-long.php', 'page-templates/homepage-rent-short.php'))) {
		return;
	}

	// Home slide shortcode
	ob_start();
?><div style="display: block; width: 100%; overflow: hidden;">
	<div style="float: left; width: 50%;">
		<p><strong>Shortcode:</strong> <span style="font-family: monospace;">[ch_home_slide]</span></p>
		<p><strong>Parametry</strong></p>
			<ul>
				<li><i>offer_id</i> - identyfikator oferty w bazie Wordpressa</li>
				<li><i>img</i> - URL obrazka</li>
				<li><i>width</i> - szerokość obrazka</li>
				<li><i>height</i> - wysokość obrazka</li>
			</ul>
		<p>Możliwe jest stworzenie slajdu z dowolnym tytułem, ceną i odnośnikiem (należy pominąć parametr <i>offer_id</i>):</p>
			<ul>
				<li><i>url</i> - URL do którego prowadzi slajd</li>
				<li><i>title</i> - tytuł slajdu</li>
				<li><i>price</i> - tekst wyświetlany w miejscu ceny (opcjonalny)</li>
			</ul>
	</div>

	<div style="float: left; width: 50%;">
		<p><strong>Przykłady</strong></p>
		<p style="font-family: monospace;">[ch_home_slide img="<?php bloginfo('home'); ?>/wp-content/themes/crystalhouse/tmp/slide-1.jpg" offer_id="1"]</p>
		<p style="font-family: monospace;">[ch_home_slide img="<?php bloginfo('home'); ?>/wp-content/themes/crystalhouse/tmp/slide-2.jpg" title="Slajd kontaktowy" url="<?php bloginfo('home'); ?>/kontakt"]</p>
	</div>
</div><?php
	$content = ob_get_clean();

	$screen = get_current_screen();
	$screen->add_help_tab(array( 
		'id' => 'cht-shortcode-home_slide-help',
		'title' => 'Slider',
		'content' => $content
	));

	// Offers carousel shortcode
	ob_start();
?><div style="display: block; width: 100%; overflow: hidden;">
	<div style="float: left; width: 50%;">
		<p><strong>Shortcode:</strong> <span style="font-family: monospace;">[ch_offers_carousel]</span></p>
		<p><strong>Parametry</strong></p>
			<ul>
				<li><i>title</i> - nagłówek karuzeli</li>
				<li><i>url</i> - URL do którego prowadzi nagłówek karuzeli</li>
				<li><i>ids</i> - identyfikatory ofert, po przecinku, np.: 1,2,3,4</li>
				<li><i>type</i> - typ oferty (<i>offer_apartment</i>, <i>offer_house</i>, <i>offer_lot</i>, <i>offer_business</i>), po przecinku</li>
				<li><i>category</i> - identyfikator kategorii oferty, po przecinku np.: 1,2,3,4</li>
				<li><i>section</i> - identyfikator sekcji oferty, po przecinku np.: 1,2,3,4</li>
				<li><i>limit</i> - maksymalna ilość ofert do wyświetlenia (opcjonalne, domyślnie: 10)</li>
				<li><i>class</i> - dodatkowe klasy CSS (opcjonalne)</li>
				<li><i>filters</i> - filtry dla listy ofert, w formacie parametrów URL (<i>"param1=val1&amp;param2=val2"</i>)</li>
			</ul>
		<p><strong>Przykłady</strong></p>
		<p style="font-family: monospace;">[ch_offers_carousel title="Wybrane oferty" ids="1,2,3,4,5"]</p>
		<p style="font-family: monospace;">[ch_offers_carousel title="Nowe i polecane oferty" filters="is_new=1&is_recommended=1"]</p>
		<p style="font-family: monospace;">[ch_offers_carousel title="Wynajem długoterminowy" category="12"]</p>
		<p style="font-family: monospace;">[ch_offers_carousel title="Wynajem krótkoterminowy" category="10"]</p>
	</div>

	<div style="float: left; width: 50%;">
		<p><strong>Dostępne filtry dla parametru <i>filters</i>:</strong></p>
			<ul>
				<li><i>keyword={tag1},{tag2}</i> - tag oferty, po przecinku</li>
				<li><i>price_from={numeric}</i>, <i>price_to={numeric}</i> - określa zakres cen (waluta: PLN)</li>
				<li><i>orderby={date|price}</i> - pole wg którego zostanie posortowana lista: <i>date</i> lub <i>price</i></li>
				<li><i>order={ASC|DESC}</i> - tryb sortowania: <i>ASC</i> (rosnąco) lub <i>DESC</i> (malejąco)</li>
				<li><i>is_exclusive=1</i> - zwraca oferty "na wyłączność"</li>
				<li><i>is_new=1</i> - zwraca nowe oferty</li>
				<li><i>is_recommended=1</i> - zwraca polecane oferty</li>
				<li><i>is_no_fee=1</i> - zwraca oferty z prowizją 0%</li>
				<li><i>attr={slug1},{slug2}</i> - zwraca oferty oznaczone danymi atrybutami</li>
				<li><i>rooms_from={numeric}</i>, <i>rooms_to={numeric}</i> - określa zakres ilości pokoi</li>
				<li><i>rooms={numeric}</i> - zwraca oferty z podaną ilością pokoi</li>
				<li><i>bedrooms_from={numeric}</i>, <i>bedrooms_to={numeric}</i> - określa zakres ilości sypialni</li>
				<li><i>bedrooms={numeric}</i> - zwraca oferty z podaną ilością sypialni</li>
				<li><i>bathrooms_from={numeric}</i>, <i>bathrooms_to={numeric}</i> - określa zakres ilości łazienek</li>
				<li><i>bathrooms={numeric}</i> - zwraca oferty z podaną ilością łazienek</li>
			</ul>
	</div>
</div><?php
	$content = ob_get_clean();

	$screen = get_current_screen();
	$screen->add_help_tab(array( 
		'id' => 'cht-shortcode-offers_carousel-help',
		'title' => 'Karuzela ofert',
		'content' => $content
	));

	// Additional shortcodes
	ob_start();
?><div style="display: block; width: 100%; overflow: hidden;">
	<div style="float: left; width: 50%;">
		<p><strong>Shortcode:</strong> <span style="font-family: monospace;">[ch_home_section]</span></p>
		<p>Otacza poszczególne sekcje na stronie głównej.</p>
		<p><strong>Parametry</strong></p>
			<ul>
				<li><i>class</i> - dodatkowe klasy CSS (opcjonalne)</li>
			</ul>
	</div>
	<div style="float: left; width: 50%;">
		<p><strong>Przykłady</strong></p>
		<p style="font-family: monospace;">[ch_home_section]</p>
		<p style="font-family: monospace;">[ch_home_section class="well-gray"]</p>
	</div>
</div>
<p style="width: 100%; height: 1px; background: #ccc;"></p>
<div style="display: block; width: 100%; overflow: hidden;">
	<div style="float: left; width: 50%;">
		<p><strong>Shortcode:</strong> <span style="font-family: monospace;">[ch_home_featured_offer]</span></p>
		<p>Reklamy.</p>
		<p><strong>Parametry</strong></p>
			<ul>
				<li><i>offer_id</i> - identyfikator oferty w bazie Wordpressa</li>
				<li><i>img</i> - URL obrazka<br><small>dla kolumny szerokiej "big=1" powinien mieć on wymiary: 590px x 390px, dla wąskiej: 280px x 180px</small></li>
			</ul>
		<p>Możliwe jest stworzenie widoku z dowolnym tytułem i odnośnikiem (należy pominąć parametr <i>offer_id</i>):</p>
			<ul>
				<li><i>url</i> - URL do którego prowadzi slajd</li>
				<li><i>title</i> - podpis slajdu</li>
			</ul>
	</div>
	<div style="float: left; width: 50%;">
		<p><strong>Przykłady</strong></p>
		<p style="font-family: monospace;">[ch_home_featured_offer img="<?php bloginfo('home'); ?>/wp-content/themes/crystalhouse/tmp/estate-featured-1.jpg" offer_id="1"]</p>
		<p style="font-family: monospace;">[ch_home_featured_offer img="<?php bloginfo('home'); ?>/wp-content/themes/crystalhouse/tmp/estate-featured-2.jpg" title="Przykładowy tytuł" url="<?php bloginfo('home'); ?>"]</p>
	</div>
</div>
<p style="width: 100%; height: 1px; background: #ccc;"></p>
<div style="display: block; width: 100%; overflow: hidden;">
	<div>
		<p><strong>Shortcode:</strong> <span style="font-family: monospace;">[ch_home_featured]</span></p>
		<p>Otacza sekcję "ofert promowanych".</p>
	</div>
</div>
<p style="width: 100%; height: 1px; background: #ccc;"></p>
<div style="display: block; width: 100%; overflow: hidden;">
	<div>
		<p><strong>Shortcode:</strong> <span style="font-family: monospace;">[ch_home_featured_col]</span></p>
		<p>Otacza zawartość kolumny sekcji "ofert promowanych".</p>
		<p><strong>Parametry</strong></p>
			<ul>
				<li><i>big=1</i> - określa, czy kolumna ma być szeroka (opcjonalny)</li>
			</ul>
	</div>
</div><?php
	$content = ob_get_clean();

	$screen = get_current_screen();
	$screen->add_help_tab(array( 
		'id' => 'cht-shortcode-extras-help',
		'title' => 'Shortcode\'y',
		'content' => $content
	));
}
add_action('load-post.php', 'register_homepage_editor_help');
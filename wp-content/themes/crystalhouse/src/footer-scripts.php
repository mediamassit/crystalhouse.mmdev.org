	<!-- Form filters values -->
	<script src="<?php bloginfo('template_url'); ?>/js/form-filters_<?php echo pll_current_language(); ?>.js"></script>

	<!-- CDN libraries -->
	<script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-538a578c3e089f8a"></script>
	<script src="http://maps.google.com/maps/api/js?key=AIzaSyAwvZqy6XB8Pd4qxHtzeoxQC57i3-UiHjE&amp;language=<?php echo pll_current_language(); ?>"></script>

	<!-- jQuery -->
	<!--<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/jquery-1.11.1.min.js"></script>-->
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/jquery.easing.1.3.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/jquery.scrollTo.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/jquery.sticky.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/jquery.jcarousel.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/jquery.cookie.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/jquery.scrollintoview.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/jquery.resizeimagetoparent.min.js"></script>

	<!-- Bootstrap -->
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/bootstrap.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/bootstrap-hover-dropdown.min.js"></script>

	<!-- FancyBox -->
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/fancyBox2/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/fancyBox2/jquery.fancybox.pack.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/fancyBox2/helpers/jquery.fancybox-media.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/fancyBox2/helpers/jquery.fancybox-thumbs.js"></script>

	<!-- Gmap3 -->
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/gmap3.min.js"></script>

	<!-- Widgets -->
	<script src="<?php bloginfo('template_url'); ?>/js/src/app/home-theatre.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/app/offer-gallery-carousel.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/app/offers-carousel.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/app/offers-list-map.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/app/popular-entries.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/app/wishlist.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/offers-filters.js"></script>
	<!--<script src="<?php bloginfo('template_url'); ?>/js/lib.js"></script>-->

	<script src="<?php bloginfo('template_url'); ?>/js/app.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/src/vendor/smoothscroll.js"></script>
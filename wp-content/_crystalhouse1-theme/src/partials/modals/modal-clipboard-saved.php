	<div class="modal clipboard vertical-middle fade" id="modal-clipboard-saved" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<a href="javascript:void(0);" class="close-button" data-dismiss="modal" aria-hidden="true"></a>
				<div class="modal-body">
					<div class="nav">
						<p style="margin-top: 25px;"><a href="<?php echo get_permalink(pll_get_post(CLIPBOARD_PAGE_ID)); ?>" class="btn"><?php _e('Obejrzyj', 'chtheme'); ?></a></p>
					</div>
					<!-- .nav -->

					<div class="message">
						<p><?php _e('Zapisano schowek', 'chtheme'); ?></p>
					</div>
					<!-- .message -->
				</div>
				<!-- .modal-body -->
			</div>
			<!-- .modal-content -->
		</div>
		<!-- .modal-dialog -->
	</div>
	<!-- .modal.classic -->
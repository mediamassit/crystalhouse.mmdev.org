					<?php if($offers_map->have_posts()) : ?>
					<section class="widget google-map offers-list-map closed" style="display: none;">
						<div class="map"></div>
					</section>
					<!-- .widget.google-map -->

					<script>
						(function($) {
							$(window).load(function() {
								$('.widget.offers-list-map').offersListMap({
									map: {
										options: {
											zoom: 10,
											center: new google.maps.LatLng(52.2329379,21.0011941), // Warszawa
											disableDefaultUI: false,
											scrollwheel: false
										}
									},
									markers: [
										<?php
											while($offers_map->have_posts()) : 
												$offers_map->the_post();
										?>
										{
											latLng: [<?php echo get_field('offer_location_latlng'); ?>],
											data: {
												content: '<?php get_template_part('src/partials/offers-list/slots/map-infobox'); ?>'
											}
										},
										<?php
											endwhile;
											wp_reset_postdata();
										?>
									]
								});
							});
						})(jQuery);
					</script>
					<?php endif; ?>
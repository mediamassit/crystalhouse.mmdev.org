<?php

function add_crystalhouse_roles_function() {
    if(is_null(get_role('broker'))) {
        $editor = get_role('editor');
		add_role('broker', __('Agent prowadzący'), $editor->capabilities);
    }
}
add_action( 'init', 'add_crystalhouse_roles_function' );
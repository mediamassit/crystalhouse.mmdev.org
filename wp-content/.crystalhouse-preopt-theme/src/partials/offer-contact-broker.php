<?php

	// @TODO Get broker avatar
	$broker = get_field('offer_broker');

	$avatar = types_render_usermeta_field("avatar", array('output' => 'raw', 'user_id' => $broker['ID']));
	$phone = types_render_usermeta_field("phone-number", array('output' => 'raw', 'user_id' => $broker['ID']));
	$mobile = types_render_usermeta_field("mobile-number", array('output' => 'raw', 'user_id' => $broker['ID']));

?>						<?php if($broker) : ?>
						<section class="widget contact-broker hidden-print">
							<?php
								// For apartment investments or primary market
								if(has_term(pll_get_term(84), 'offer_category', $post->ID) || get_field('offer_market_type') == 'primary') :
							?>
							<h3><strong><?php _e('Jesteś zainteresowany wynajmem lub kupnem nieruchomości w tej lokalizacji?', 'chtheme'); ?></strong></h3>
							<?php endif; ?>

							<h3><?php _e('Skontaktuj się z agentem', 'chtheme'); ?></h3>
							<article>
								<?php if(strlen(trim((string) $avatar)) > 0) : ?><figure><img src="<?php echo $avatar; ?>" alt=""></figure><?php endif; ?>
								<h5><?php echo $broker['user_firstname']; ?> <?php echo $broker['user_lastname']; ?></h5>
								<p class="role"><?php if(strlen(trim($broker['user_description'])) > 0) : ?><?php echo $broker['user_description']; ?><?php else : ?><?php _e('agent nieruchomości', 'chtheme'); ?><?php endif; ?></p>
								<p>
									<?php if(strlen(trim((string) $phone)) > 0) : ?>
										<strong><?php _e('T:', 'chtheme'); ?></strong> <?php echo $phone; ?><br>
									<?php else : ?>
										<strong><?php _e('T:', 'chtheme'); ?></strong> <?php echo reset(explode("\r\n", get_opt('opt-contact-phone'))); ?><br>
									<?php endif; ?>

									<?php if(strlen(trim((string) $mobile)) > 0) : ?>
									<strong><?php _e('M:', 'chtheme'); ?></strong> <?php echo $mobile; ?><br>
									<?php endif; ?>

									<?php if($broker['user_email']) : ?><strong><?php _e('E:', 'chtheme'); ?></strong> <a href="mailto:<?php echo $broker['user_email']; ?>"><?php echo $broker['user_email']; ?></a><?php endif; ?>
								</p>
							</article>

							<?php if(strlen(trim($broker['user_email'])) > 0) : ?>
							<?php echo apply_filters('the_content', pll_current_language() === 'pl' ? '[contact-form-7 id="736"]' : '[contact-form-7 id="737"]'); ?>
							<?php else : ?>
							<form></form>
							<?php endif; ?>
						</section>
						<!-- .contact-broker -->
						<?php endif; ?>
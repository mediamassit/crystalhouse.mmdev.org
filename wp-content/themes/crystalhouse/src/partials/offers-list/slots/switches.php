					<div class="row widget offers-list-switches">
						<div class="col-md-5 left">
							<?php if($offers_map->have_posts()) : ?><a href="#" class="button-googlemap" data-close-label="<?php _e('Zamknij mapę nieruchomości', 'chtheme'); ?>" data-open-gmap="#content .google-map"><?php _e('Pokaż mapę nieruchomości', 'chtheme'); ?></a><?php endif; ?>
						</div>
						<!-- .col-md-5 -->

						<div class="col-md-7 right">
							<ul class="inline-switch">
							<?php foreach(chofl_currencies(false) as $currency) : ?>
								<li><a href="<?php echo chtof_mod_url(array('show_currency' => $currency)); ?>"<?php if(chofl_currency_active($currency, true)) : ?> class="active"<?php endif; ?>><?php echo $currency; ?></a></li>
							<?php endforeach; ?>
							</ul>

							<?php
								echo chofl_select('order', array(
									'date_desc' => __('Najnowsze', 'chtheme'),
									'date_asc' => __('Najstarsze', 'chtheme'),
									'price_desc' => __('Cena najwyższa', 'chtheme'),
									'price_asc' => __('Cena najniższa', 'chtheme')
								));
							?>
						</div>
						<!-- .col-md-7 -->
					</div>
					<!-- .widget.offers-list-switches -->
<?php

function offer_house_custom_post_type() {

	$labels = array(
		'name'                => _x( 'Domy', 'Post Type General Name', 'chtheme' ),
		'singular_name'       => _x( 'Dom', 'Post Type Singular Name', 'chtheme' ),
		'menu_name'           => __( 'Domy', 'chtheme' ),
		'parent_item_colon'   => __( 'Nadrzędny:', 'chtheme' ),
		'all_items'           => __( 'Wszystkie', 'chtheme' ),
		'view_item'           => __( 'Zobacz', 'chtheme' ),
		'add_new_item'        => __( 'Dodaj nowy dom', 'chtheme' ),
		'add_new'             => __( 'Dodaj nowy', 'chtheme' ),
		'edit_item'           => __( 'Edytuj dom', 'chtheme' ),
		'update_item'         => __( 'Aktualizuj dom', 'chtheme' ),
		'search_items'        => __( 'Szukaj', 'chtheme' ),
		'not_found'           => __( 'Nie odnaleziono', 'chtheme' ),
		'not_found_in_trash'  => __( 'Nie odnaleziono w koszu', 'chtheme' ),
	);
	$rewrite = array(
		'slug'                => 'house',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'dom', 'chtheme' ),
		'description'         => '',
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'          => array( 'offer_category', 'offer_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-admin-home',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);
	register_post_type( 'offer_house', $args );
	add_post_type_support( 'offer_house', 'excerpt' );

}
add_action( 'init', 'offer_house_custom_post_type', 0 );
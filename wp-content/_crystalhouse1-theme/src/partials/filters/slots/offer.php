							<div class="form-group" data-form-fieldset="offer">
								<label><?php _e('Chcę', 'chtheme'); ?></label>
								<div class="row inline-radios">
									<div class="col-xs-6" data-form-fieldset="offer_buy">
										<label><input type="radio" name="offer" value="buy"<?php if($config->isCategory('buy')) : ?> checked="checked"<?php endif; ?>> <?php _e('kupić', 'chtheme'); ?></label>
									</div>
									<!-- .col-xs-6 -->

									<div class="col-xs-6" data-form-fieldset="offer_rent">
										<label><input type="radio" name="offer" value="rent"<?php if($config->isCategory('rent')) : ?> checked="checked"<?php endif; ?>> <?php _e('wynająć', 'chtheme'); ?></label>
									</div>
									<!-- .col-xs-6 -->
								</div>
								<!-- .inline-checkboxes -->
							</div>
							<!-- .form-group -->
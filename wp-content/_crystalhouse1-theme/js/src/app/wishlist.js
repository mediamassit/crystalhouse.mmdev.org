(function($) {

	$.fn.offersWishlist = function() {
		var $clipboard = $('#top .side .clipboard');

		function disable_wishlist_dropdown() {
			$('[data-toggle=wishlist]').addClass('stop-dropdown');
		}

		function enable_wishlist_dropdown() {
			$('[data-toggle=wishlist]').removeClass('stop-dropdown');
		}

		function set_counter_value(val) {
			if(val > 0) {
				$clipboard.removeClass('empty').find('[data-toggle=wishlist] em').html(val);
				enable_wishlist_dropdown();
			}
			else {
				$clipboard.addClass('empty').find('[data-toggle=wishlist] em').html('');
				enable_wishlist_dropdown();
			}
		}

		function get_counter_value() {
			return parseInt($clipboard.find('[data-toggle=wishlist] em').text());
		}

		function bind_remove_event($elm) {
			$elm.find('[data-removefromclipboard]').click(function() {
				var id = $(this).attr('data-removefromclipboard');

				$.ajax({
					url: WISHLIST_API_REMOVE,
					dataType: 'json',
					data: {'id': id},
					context: $(this)
				})
				.done(function(data) {
					if(data.success) {
						$(this).closest('.offer').fadeOut(400, function() {
							$(this).remove();
							set_counter_value($('.wishlist-dropdown').find('.offers .offer').length);
						});
					}
				})
				.fail(function(jqXHR, textStatus, errorThrown) {
					console.log('Clipboard error:'+errorThrown);
				});

				return false;
			})
		}

		$('[data-toggle=wishlist]').click(function() {
			var $w = $(this).parent().find('.wishlist-dropdown');

			if(!$(this).closest('.clipboard').is('.empty')) {
				$w.toggleClass('open');
				return false;
			}
			else {
				return true;
			}
		});
		$(document).click(function(e) {
			var $t = $(e.target);

			if(!$t.is('.wishlist-dropdown') && $t.closest('.wishlist-dropdown').length <= 0) {
				$('.wishlist-dropdown').removeClass('open');
			}
		});

		$('.wishlist-dropdown').bind('wishlist-refresh', function() {
			$.ajax({
				url: WISHLIST_API_GET,
				dataType: 'json',
				context: $(this)
			})
			.done(function(data) {
				if(!data.success) {
					return;
				}

				var $offers = $(this).find('.offers');
					$offers.html('');

				if(data.data && data.data.length > 0) {
					$offers.html(data.data);
				}

				bind_remove_event($offers);
				set_counter_value($offers.find('.offer').length);
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				console.log('Clipboard error:'+errorThrown);
			});
		});

		$('[data-saveclipboard]').click(function() {
			var $self = $(this);

			$(this).addClass('disabled').css('opacity', 0.5);

			$.ajax({
				url: WISHLIST_API_SAVE,
				dataType: 'json',
				context: $(this)
			})
			.done(function(data) {
				$self.removeClass('disabled').css('opacity', 1);
				$('.wishlist-dropdown').removeClass('open');
				$('#modal-clipboard-saved').modal('show');
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				$self.removeClass('disabled').css('opacity', 1);
				console.log('Clipboard error:'+errorThrown);
			});

			return false;
		});

		$('[data-clearclipboard]').click(function() {
			var $self = $(this);

			$.ajax({
				url: WISHLIST_API_CLEAR,
				dataType: 'json',
				context: $(this)
			})
			.done(function(data) {
				$self.removeClass('disabled').css('opacity', 1);
				$('.wishlist-dropdown').trigger('wishlist-refresh');
				$('.wishlist-dropdown').removeClass('open');
				$('#modal-clipboard-clean').modal('show');
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				$self.removeClass('disabled').css('opacity', 1);
				console.log('Clipboard error:'+errorThrown);
			});

			return false;
		});

		$('[data-addtoclipboard]').click(function() {
			var id = $(this).attr('data-addtoclipboard');

			$.ajax({
				url: WISHLIST_API_PUT,
				dataType: 'json',
				data: {'id': id},
				context: $(this)
			})
			.done(function(data) {
				$(this).fadeOut(200);
				$('.wishlist-dropdown').trigger('wishlist-refresh');
				$('#modal-clipboard').modal('show');
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				console.log('Clipboard error:'+errorThrown);
			});

			return false;
		});

		bind_remove_event($('body'));
	}

})(jQuery);
						<section class="widget price">
							<p>
								<?php if(chtof_has('offer_price_primary')) : ?>
									<?php if(cht_offer_is_for_long_rent()) : ?>
									<strong><?php echo chtof_price(get_field('offer_price_primary')); ?> <small><?php _e('/ msc', 'chtheme'); ?></small></strong>
									<?php elseif(cht_offer_is_for_short_rent()) : ?>
									<strong><?php echo chtof_price(get_field('offer_price_primary')); ?> <small><?php _e('/ dzień', 'chtheme'); ?></small></strong>
									<?php else : ?>
									<strong><?php echo chtof_price(get_field('offer_price_primary')); ?></strong>
									<?php endif; ?>
								<?php else : ?>
									<?php if(chtof_has('offer_price_m2')) : ?>
									<strong><?php echo chtof_price(get_field('offer_price_m2'), 0); ?> / m<sup>2</sup></strong>
									<?php else : ?>
									<!--<strong><?php _e('b/d', 'chtheme'); ?></strong>-->
									<strong><a href="#" title="<?php _e('Zapytaj o cenę', 'chtheme'); ?>" data-toggle="modal" data-target="#modal-contact-offer" data-modal-form-offer="<?php echo get_field('offer_asari_listing_id'); ?>" data-modal-form-subject="<?php _e('Pytanie o cenę', 'chtheme'); ?>" style="color: #fff;"><span style="font-size: 0.7em; text-transform: uppercase;"><?php _e('Zapytaj o cenę', 'chtheme'); ?></span></a></strong>
									<?php endif; ?>
								<?php endif; ?>
								
								<?php if(chtof_has('offer_price_primary') && cht_offer_is_for_sale() && chtof_has('offer_price_m2')) : ?>
								<br><?php echo chtof_price(get_field('offer_price_m2'), 0); ?> / m<sup>2</sup>
								<?php endif; ?>
							</p>
						</section>
						<!-- .price -->